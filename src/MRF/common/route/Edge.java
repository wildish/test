package MRF.common.route;

public class Edge {
	public int id;
    public int target;
    public int source;
    public double length_cost;
    public double time_cost;  
}
