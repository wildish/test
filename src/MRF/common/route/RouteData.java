package MRF.common.route;

import java.util.ArrayList;
import java.util.List;

import MRF.common.geometry.Line;

public class RouteData {
	// private static double maxDistance = 50;
	private static List<String> roadNames = new ArrayList<String>();
	private static List<Line> lines = new ArrayList<Line>();
	// private static boolean isValid = false;
	// private static int minLine = 0;
	// private static int minLinePart = 0;
	// private static boolean isOnRoad = false;
	private static RouteData route = new RouteData();

	public static RouteData instance() {
		return route;
	}

	public void addRoad(String roadName, Line line) {
		roadNames.add(roadName);
		lines.add(line);
	}

	public void clear() {
		// isValid = false;
		roadNames.clear();
		lines.clear();
		// isOnRoad = false;
	}

	// / <summary>
	// / get Postion In Route
	// / </summary>
	// / <param name="sourcePoint">GPS Point</param>
	// / <returns>false invalid, recompute route,true valid</returns>
	// @SuppressLint("DefaultLocale")
	// public boolean getGPSPostionInRoute(Point sourcePoint, String message,
	// String currentRoadName, int angle) {
	// List<Point> pts = null;
	// for (int i = minLine; i < lines.size(); i++) {
	// pts = lines.get(i).getListPoints();
	// int j = i == minLine ? minLinePart : 0;
	// for (; j < pts.size() - 1; j++) {
	// seg.setP0(pts.get(j));
	// seg.setP1(pts.get(j + 1));
	// if (seg.distance(sourcePoint) < len) {
	// len = seg.distance(sourcePoint);
	// minLine = i;
	// minLinePart = j;
	// }
	// }
	// }
	// if (len > maxDistance) {
	// if (isOnRoad) {
	// return false;
	// } else
	// return true;
	// } else {
	// isOnRoad = true;
	// }
	//
	// pts = lines.get(minLine).getListPoints();
	// seg.setP0(pts.get(minLinePart));
	// seg.setP1(pts.get(minLinePart + 1));
	// sourcePoint = seg.project(sourcePoint);
	// currentRoadName = "";
	// angle = 0;
	// if (isOnRoad) {
	// currentRoadName = roadNames.get(minLine);
	// angle = (int) (seg.getAngle() * 180 / Math.PI);
	// }
	// double fac = seg.segmentFraction(sourcePoint);
	//
	// if (fac < 0)
	// sourcePoint = seg.getP0();
	// else if (fac > 1)
	// sourcePoint = seg.getP1();
	// seg.setP0(sourcePoint);
	// len = seg.getLength();
	// for (int i = minLinePart + 1; i < pts.size() - 1; i++) {
	// seg.setP0(pts.get(i));
	// seg.setP1(pts.get(i + 1));
	// len += seg.getLength();
	// }
	// if (minLine == lines.size() - 1) {
	// message = String.format("%f to target ", len);
	// } else {
	// String direction = "";
	// seg.setP0(pts.get(pts.size() - 2));
	// seg.setP1(pts.get(pts.size() - 1));
	// LineSegment seg2 = new LineSegment();
	// pts = lines.get(minLine + 1).getListPoints();
	// seg2.setP0(pts.get(0));
	// seg2.setP1(pts.get(1));
	// int dir = seg.orientationIndex(seg2);
	// if (dir > 0) {
	// direction = "left";
	// } else if (dir < 0)
	// direction = "right";
	// message = String.format("%f turn %s to %s ", len, direction,
	// roadNames.get(minLine + 1));
	// }
	// return true;
	// }
}
