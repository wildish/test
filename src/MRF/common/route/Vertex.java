package MRF.common.route;

import java.util.List;

import MRF.common.geometry.Point;

public class Vertex {
	public int id;
    public List<Edge> edges;
    public Point pt;
}
