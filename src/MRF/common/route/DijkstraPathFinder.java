package MRF.common.route;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import DataBaseLib.DBFactory;
import DataBaseLib.DBProvider;
import MRF.common.Data.GeometryWithData;
import MRF.common.Data.SpatialResult;
import MRF.common.Data.sqlite.SpatialDataAccess;
import MRF.common.MapTemplate.Global;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import MRF.common.Util.Algorithm.CGAlgorithms;
import MRF.common.geometry.GeometryFromWKB;
import MRF.common.geometry.GeometryToWKT;
import MRF.common.geometry.GeometryTypes;
import MRF.common.geometry.Line;
import MRF.common.geometry.LineSegment;
import MRF.common.geometry.Point;
import WebRequestHandle.Business.DataModel.KeyValuePair;
import android.annotation.SuppressLint;

@SuppressLint("UseSparseArrays")
public class DijkstraPathFinder {
	private List<Vertex> vertices = new ArrayList<Vertex>();
	private List<Edge> edges = new ArrayList<Edge>();
	private HashMap<Integer, LinePart> edgeGeometrys = new HashMap<Integer, LinePart>();
	private MRF.common.index.GridIndex nodesIndex = new MRF.common.index.GridIndex(
			100, GeometryTypes.WKBPoint);
	private static DijkstraPathFinder finder = new DijkstraPathFinder();

	private DijkstraPathFinder() {
	}

	public static DijkstraPathFinder getInstance() {
		return finder;
	}

	private void readData(String connectionString) throws Exception {
		String sql = "select gid,length,time_cost as tim_cost,source,target,st_asbinary(geom) as st_asbinary,name from road_network where status=1 order by gid asc";
		DBProvider database = DBFactory.getDBProvider();
		JSONArray queryResult = database.getTable(sql);
		HashSet<Integer> bag = new HashSet<Integer>();
		String name = "";
		long verCount = 0;
		for (int i = 0; i < queryResult.length(); i++) {
			JSONObject reader = queryResult.getJSONObject(i);
			Edge e = new Edge();
			e.id = reader.getInt("gid");
			e.length_cost = reader.getDouble("length");
			e.time_cost = reader.getDouble("time_cost");
			e.source = reader.getInt("source");
			e.target = reader.getInt("target");
			if (!bag.contains(e.source))
				bag.add(e.source);
			if (!bag.contains(e.target))
				bag.add(e.target);
			verCount = Math.max(Math.max(verCount, e.source),
					Math.max(e.source, e.target));
			edges.add(e);
			if (reader.get("name") == null) {
				name = "";
			} else
				name = reader.getString("name");
			LinePart part = new LinePart(name,
					(Line) GeometryFromWKB.parse((byte[]) reader
							.get("st_asbinary")));
			edgeGeometrys.put(e.id, part);
		}
		for (int i = 0; i < verCount + 1; i++) {
			Vertex v = new Vertex();
			vertices.add(v);
		}
		String sql2 = "select id,st_asbinary(the_geom) as st_asbinary from road_network_vertices_pgr";
		JSONArray queryResult2 = database.getTable(sql2);
		int id = -1;
		Point pt = null;
		for (int i = 0; i < queryResult2.length(); i++) {
			JSONObject reader1 = queryResult2.getJSONObject(i);
			id = (int) reader1.getInt("id");
			pt = (Point) GeometryFromWKB.parse((byte[]) reader1
					.get("st_asbinary"));
			nodesIndex.addElement(id, pt);
			Vertex temp = new Vertex();
			temp.pt = pt;
			vertices.set(id, temp);
		}

		for (Edge e : edges) {
			Vertex temp = vertices.get(e.source);
			if (temp.edges == null) {
				temp.edges = new ArrayList<Edge>();
			}
			if (!temp.edges.contains(e)) {
				temp.edges.add(e);
			}
			temp.id = e.source;
			vertices.set(e.source, temp);
			temp = vertices.get(e.target);
			if (temp.edges == null) {
				temp.edges = new ArrayList<Edge>();
			}
			if (!temp.edges.contains(e)) {
				temp.edges.add(e);
			}
			temp.id = e.target;
			vertices.set(e.target, temp);
		}
		// SingleSourceShortestPathForNonNegativeEdgeLength(vertices[30],
		// vertices[60], bag.size() + 1, routeType.lengthShortest);
	}

	private void readDataFromLocal() {
		SpatialResult result = SpatialDataAccess.getInstance().query(
				Global.DefaultDataSourceName, "road_network",
				"length,time_cost as timcost,source,target,name", "gid",
				"geom", "status=1 order by gid", null);
		// "select gid,length,time_cost,source,target,st_asbinary(geom),name from road_network where status=1 order by gid asc";

		HashSet<Integer> bag = new HashSet<Integer>();
		String name = "";
		long verCount = 0;
		List<GeometryWithData> dataList = result.getDatas();
		HashMap<String, Object> data = null;
		for (int i = 0; i < dataList.size(); i++) {
			data = (HashMap<String, Object>) dataList.get(i).getData();
			Edge e = new Edge();
			e.id = Integer.parseInt(data.get("gid").toString());
			e.length_cost = Double.parseDouble(data.get("length").toString());
			e.time_cost = Double.parseDouble(data.get("timcost").toString());
			e.source = Integer.parseInt(data.get("source").toString());
			e.target = Integer.parseInt(data.get("target").toString());
			if (bag.contains(e.source) == false)
				bag.add(e.source);
			if (bag.contains(e.target) == false)
				bag.add(e.target);
			verCount = Math.max(Math.max(verCount, e.source),
					Math.max(e.source, e.target));
			edges.add(e);
			if (data.containsKey("name") == false) {
				name = "";
			} else
				name = data.get("name").toString();
			LinePart part = new LinePart(name, (Line) dataList.get(i)
					.getWKBGeometry());
			edgeGeometrys.put(e.id, part);
		}
		for (int i = 0; i < verCount + 1; i++) {
			Vertex v = new Vertex();
			vertices.add(v);
		}
		result = SpatialDataAccess.getInstance().query(
				Global.DefaultDataSourceName, "road_network_vertices_pgr", "",
				"id", "the_geom", "", null);
		// "select id,st_asbinary(the_geom) from road_network_vertices_pgr";

		int id = -1;
		Point pt = null;
		dataList = result.getDatas();
		for (int i = 0; i < dataList.size(); i++) {
			data = (HashMap<String, Object>) dataList.get(i).getData();
			id = Integer.parseInt(data.get("id").toString());
			pt = (Point) dataList.get(i).getWKBGeometry();
			nodesIndex.addElement(id, pt);
			Vertex temp = new Vertex();
			temp.pt = pt;
			vertices.set(id, temp);
		}

		for (Edge e : edges) {
			Vertex temp = vertices.get(e.source);
			if (temp.edges == null) {
				temp.edges = new ArrayList<Edge>();
			}
			if (!temp.edges.contains(e)) {
				temp.edges.add(e);
			}
			temp.id = e.source;
			vertices.set(e.source, temp);
			temp = vertices.get(e.target);
			if (temp.edges == null) {
				temp.edges = new ArrayList<Edge>();
			}
			if (!temp.edges.contains(e)) {
				temp.edges.add(e);
			}
			temp.id = e.target;
			vertices.set(e.target, temp);
		}
		// SingleSourceShortestPathForNonNegativeEdgeLength(vertices[30],
		// vertices[60], bag.size() + 1, routeType.lengthShortest);
	}

	public String computePath(Point sourcePt, Point targetPt, RouteType type,
			String connectionString) throws Exception {
		synchronized (finder) {
			List<Line> lines = new ArrayList<Line>();
			List<String> roadNames = new ArrayList<String>();
			if (edges.size() < 1) {
				if (StringUtil.isEmpty(connectionString)) {
					readDataFromLocal();
				} else
					readData(connectionString);
			}
			int source = nodesIndex.getNearstElementId(sourcePt);
			int target = nodesIndex.getNearstElementId(targetPt);
			if (source == target) {
				getPathBySameNode(vertices.get(source), sourcePt, targetPt,
						lines, roadNames);
			} else {
				List<Integer> path = singleSourceShortestPathForNonNegativeEdgeLength(
						vertices.get(source), vertices.get(target),
						vertices.size() + 1, type);
				int nodeIndex1 = path.get(0);
				int nodeIndex2 = path.get(1);

				getLineSegment(vertices.get(nodeIndex1), sourcePt, targetPt,
						path.size(), nodeIndex1, nodeIndex2, lines, roadNames,
						true);
				if (path.size() > 2) {
					for (int i = 1; i < path.size() - 2; i++) {
						nodeIndex1 = path.get(i);
						nodeIndex2 = path.get(i + 1);
						outPutPath(vertices.get(nodeIndex1), nodeIndex1,
								nodeIndex2, lines, roadNames);
					}
					nodeIndex1 = path.get(path.size() - 2);
					nodeIndex2 = path.get(path.size() - 1);
					getLineSegment(vertices.get(nodeIndex2), targetPt,
							targetPt, path.size(), nodeIndex1, nodeIndex2,
							lines, roadNames, false);
				}
			}
			StringBuilder result = new StringBuilder();
			result.append("{\"d\":[{");
			for (int i = 0; i < roadNames.size(); i++) {
				if (i > 0)
					result.append("},{");
				result.append("\"name\":\"");
				result.append(roadNames.get(i).replace("\"", " "));
				result.append("\",\"geometry\":\"");
				result.append(GeometryToWKT.write(lines.get(i)));
				result.append("\",\"length\":");
				result.append(lines.get(i).getLength());
			}
			result.append("}]}");
			return result.toString();
		}

	}

	public String computePathCached(Point sourcePt, Point targetPt,
			RouteType type, String connectionString) throws Exception {
		synchronized (finder) {
			List<Line> lines = new ArrayList<Line>();
			List<String> roadNames = new ArrayList<String>();
			if (edges.size() < 1) {
				if (StringUtil.isBlank(connectionString)) {
					readDataFromLocal();
				} else
					readData(connectionString);
			}
			int source = nodesIndex.getNearstElementId(sourcePt);
			int target = nodesIndex.getNearstElementId(targetPt);
			if (source == target) {
				getPathBySameNode(vertices.get(source), sourcePt, targetPt,
						lines, roadNames);
			} else {
				List<Integer> path = singleSourceShortestPathForNonNegativeEdgeLength(
						vertices.get(source), vertices.get(target),
						vertices.size() + 1, type);
				int nodeIndex1 = path.get(0);
				int nodeIndex2 = path.get(1);

				getLineSegment(vertices.get(nodeIndex1), sourcePt, targetPt,
						path.size(), nodeIndex1, nodeIndex2, lines, roadNames,
						true);
				if (path.size() > 2) {
					for (int i = 1; i < path.size() - 2; i++) {
						nodeIndex1 = path.get(i);
						nodeIndex2 = path.get(i + 1);
						outPutPath(vertices.get(nodeIndex1), nodeIndex1,
								nodeIndex2, lines, roadNames);
					}
					nodeIndex1 = path.get(path.size() - 2);
					nodeIndex2 = path.get(path.size() - 1);
					getLineSegment(vertices.get(nodeIndex2), targetPt,
							targetPt, path.size(), nodeIndex1, nodeIndex2,
							lines, roadNames, false);
				}
			}
			RouteData.instance().clear();
			StringBuilder result = new StringBuilder();
			result.append("{\"d\":[{");
			for (int i = 0; i < roadNames.size(); i++) {
				if (i > 0)
					result.append("},{");
				result.append("\"name\":\"");
				result.append(roadNames.get(i).replace("\"", " "));
				result.append("\",\"geometry\":\"");
				result.append(GeometryToWKT.write(lines.get(i)));
				result.append("\",\"length\":");
				result.append(lines.get(i).getLength());
				RouteData.instance().addRoad(
						MRFUtil.replaceCharsForQuery(roadNames.get(i)),
						lines.get(i));
			}
			result.append("}]}");
			return result.toString();
		}

	}

	private void getPathBySameNode(Vertex vertex, Point sourcePt,
			Point targetPt, List<Line> lines, List<String> roadNames) {
		Edge sourceEdge = new Edge(), targetEdge = new Edge();
		double minDistanceToSource = Double.MAX_VALUE;
		double minDistanceTotarget = Double.MAX_VALUE;
		int sourceIndex = 0;
		int targetIndex = 0;
		double tempLen = 0;
		for (Edge way : vertex.edges) {
			// List<Point> pts = ((Line) edgeGeometrys.get(way.id).geometry)
			// .getListPoints();
			List<double[]> pts = ((Line) edgeGeometrys.get(way.id).geometry)
					.getListPoints();
			for (int i = 0; i < pts.size() - 1; i++) {
				// tempLen = MRF.common.Util.Algorithm.CGAlgorithms
				// .distancePointLinePerpendicular(sourcePt, pts.get(i),
				// pts.get(i + 1));
				tempLen = CGAlgorithms.distancePointLinePerpendicular(sourcePt,
						pts.get(i), pts.get(i + 1));
				if (tempLen < minDistanceToSource) {
					sourceEdge = way;
					sourceIndex = i;
					minDistanceToSource = tempLen;
				}
				tempLen = CGAlgorithms.distancePointLinePerpendicular(targetPt,
						pts.get(i), pts.get(i + 1));
				if (tempLen < minDistanceTotarget) {
					targetEdge = way;
					targetIndex = i;
					minDistanceTotarget = tempLen;
				}
			}
		}
		if (sourceEdge.id == targetEdge.id) {
			// List<Point> pts = ((Line)
			// edgeGeometrys.get(sourceEdge.id).geometry)
			// .getListPoints();
			List<double[]> pts = ((Line) edgeGeometrys.get(sourceEdge.id).geometry)
					.getListPoints();
			Line lineGeo = new Line();
			if (sourceIndex == targetIndex) {
				if (sourceIndex == pts.size() - 1) {
					sourceIndex--;
				}
				LineSegment seg = new LineSegment(pts.get(sourceIndex),
						pts.get(sourceIndex + 1));
				lineGeo.addPoint(seg.project(sourcePt));
				lineGeo.addPoint(seg.project(targetPt));
				lines.add(lineGeo);
				roadNames.add(edgeGeometrys.get(sourceEdge.id).name);
			} else {
				if (sourceIndex < targetIndex) {
					if (sourceIndex == pts.size() - 1) {
						sourceIndex--;
					}
					LineSegment seg = new LineSegment(pts.get(sourceIndex),
							pts.get(sourceIndex + 1));
					lineGeo.addPoint(seg.project(sourcePt));
					for (int i = sourceIndex + 1; i <= targetIndex; i++) {
						lineGeo.addPoint(pts.get(i));
					}
					if (targetIndex > pts.size() - 2) {
						targetIndex = pts.size() - 2;
					}
					seg = new LineSegment(pts.get(targetIndex),
							pts.get(targetIndex + 1));
					lineGeo.addPoint(seg.project(targetPt));
				} else {
					if (sourceIndex > pts.size() - 2) {
						sourceIndex = pts.size() - 2;
					}
					LineSegment seg = new LineSegment(pts.get(sourceIndex),
							pts.get(sourceIndex + 1));
					lineGeo.addPoint(seg.project(sourcePt));
					for (int i = sourceIndex; i >= targetIndex; i--) {
						lineGeo.addPoint(pts.get(i));
					}
					if (targetIndex < 1) {
						targetIndex = 1;
					}
					seg = new LineSegment(pts.get(targetIndex - 1),
							pts.get(targetIndex));
					lineGeo.addPoint(seg.project(targetPt));
				}
				roadNames.add(edgeGeometrys.get(sourceEdge.id).name);
				lines.add(lineGeo);
			}
		} else {
			// List<Point> pts = ((Line)
			// edgeGeometrys.get(sourceEdge.id).geometry)
			// .getListPoints();
			List<double[]> pts = ((Line) edgeGeometrys.get(sourceEdge.id).geometry)
					.getListPoints();
			if (Math.abs(pts.get(pts.size() - 1)[0] - vertex.pt.X)
					+ Math.abs(pts.get(pts.size() - 1)[1] - vertex.pt.Y) > 1) {
				Collections.reverse(pts);
				sourceIndex = pts.size() - 1 - sourceIndex;
			}
			Line lineGeo = new Line();
			if (sourceIndex == pts.size() - 1) {
				sourceIndex--;
			}
			LineSegment seg = new LineSegment(pts.get(sourceIndex),
					pts.get(sourceIndex + 1));
			lineGeo.addPoint(seg.project(sourcePt));
			for (int i = sourceIndex + 1; i <= pts.size() - 1; i++) {
				lineGeo.addPoint(pts.get(i));
			}
			pts = ((Line) edgeGeometrys.get(targetEdge.id).geometry)
					.getListPoints();
			if (Math.abs(pts.get(0)[0] - vertex.pt.X)
					+ Math.abs(pts.get(0)[1] - vertex.pt.Y) > 1) {
				Collections.reverse(pts);
				targetIndex = pts.size() - 1 - targetIndex;
			}
			if (edgeGeometrys.get(sourceEdge.id).name.compareTo(edgeGeometrys
					.get(targetEdge.id).name) != 0) {
				roadNames.add(edgeGeometrys.get(sourceEdge.id).name);
				roadNames.add(edgeGeometrys.get(targetEdge.id).name);
				lines.add(lineGeo);
				lineGeo = new Line();
			} else {
				roadNames.add(edgeGeometrys.get(sourceEdge.id).name);
			}
			for (int i = 0; i <= targetIndex; i++) {
				lineGeo.addPoint(pts.get(i));
			}
			if (targetIndex == pts.size() - 1) {
				targetIndex--;
			}
			seg = new LineSegment(pts.get(targetIndex),
					pts.get(targetIndex + 1));
			lineGeo.addPoint(seg.project(targetPt));
			lines.add(lineGeo);
		}
	}

	private void outPutPath(Vertex node, int nodeIndex1, int nodeIndex2,
			List<Line> lines, List<String> roadNames) {
		Edge way = new Edge();
		for (Edge e : node.edges) {
			if ((e.source == nodeIndex1 && e.target == nodeIndex2)
					|| (e.target == nodeIndex1 && e.source == nodeIndex2)) {
				way = e;
				break;
			}
		}
		Point pt = vertices.get(nodeIndex1).pt;
		// List<Point> wayPoints = ((Line) edgeGeometrys.get(way.id).geometry)
		// .getListPoints();
		// if (Math.abs(pt.X - wayPoints.get(0).X)
		// + Math.abs(pt.Y - wayPoints.get(0).Y) > 1) {
		// Collections.reverse(wayPoints);
		// }
		List<double[]> wayPoints = ((Line) edgeGeometrys.get(way.id).geometry)
				.getListPoints();
		if (Math.abs(pt.X - wayPoints.get(0)[0])
				+ Math.abs(pt.Y - wayPoints.get(0)[1]) > 1) {
			Collections.reverse(wayPoints);
		}
		Line lineGeo = new Line();
		for (int i = 0; i < wayPoints.size(); i++)
			lineGeo.addPoint(wayPoints.get(i));
		String tempName = edgeGeometrys.get(way.id).name;
		if (tempName.compareTo(roadNames.get(roadNames.size() - 1)) == 0) {
			Line temp = lines.get(lines.size() - 1);
			temp.addPoints(lineGeo.getListPoints());
			lines.set(lines.size() - 1, temp);
		} else {
			roadNames.add(tempName);
			lines.add(lineGeo);
		}
	}

	private void getLineSegment(Vertex node, Point pt, Point targetPt,
			int pathCount, int nodeIndex1, int nodeIndex2, List<Line> lines,
			List<String> roadNames, boolean isFirstSeg) {
		int type = 0;
		double minlen = Double.MAX_VALUE;
		double partMinLen = Double.MAX_VALUE;
		List<Integer> edgeNodeId = new ArrayList<Integer>();
		double pointToSegLen = 0;
		int minDistanceEdgeIndex = -1;
		int nodeLineIndex = -1;
		double nodeLineLen = Double.MAX_VALUE;
		LineSegment seg = null;
		Point nodePt1 = vertices.get(nodeIndex1).pt;
		Point nodePt2 = vertices.get(nodeIndex2).pt;
		Line geo = null;
		// List<Point> pts = null;
		// for (int i = 0; i < node.edges.size(); i++) {
		// Edge e = node.edges.get(i);
		// edgeNodeId.add(0);
		// geo = (Line) edgeGeometrys.get(e.id).geometry;
		// pts = geo.getListPoints();
		// partMinLen = Double.MAX_VALUE;
		// for (int j = 0; j < pts.size() - 1; j++) {
		// seg = new LineSegment(pts.get(j), pts.get(j + 1));
		// pointToSegLen = seg.distance(pt);
		// if (pointToSegLen < partMinLen) {
		// edgeNodeId.set(i, j);
		// partMinLen = pointToSegLen;
		// }
		// }
		// if (partMinLen < minlen) {
		// minDistanceEdgeIndex = i;
		// minlen = partMinLen;
		// }
		// double len = Math.min(
		// pts.get(0).distance(nodePt1)
		// + pts.get(pts.size() - 1).distance(nodePt2), pts
		// .get(0).distance(nodePt2)
		// + pts.get(pts.size() - 1).distance(nodePt1));
		// if (len < nodeLineLen) {
		// nodeLineIndex = i;
		// nodeLineLen = len;
		// }
		// }
		List<double[]> pts = null;
		for (int i = 0; i < node.edges.size(); i++) {
			Edge e = node.edges.get(i);
			edgeNodeId.add(0);
			geo = (Line) edgeGeometrys.get(e.id).geometry;
			pts = geo.getListPoints();
			partMinLen = Double.MAX_VALUE;
			for (int j = 0; j < pts.size() - 1; j++) {
				seg = new LineSegment(pts.get(j), pts.get(j + 1));
				pointToSegLen = seg.distance(pt);
				if (pointToSegLen < partMinLen) {
					edgeNodeId.set(i, j);
					partMinLen = pointToSegLen;
				}
			}
			if (partMinLen < minlen) {
				minDistanceEdgeIndex = i;
				minlen = partMinLen;
			}
			// double len = Math.min(
			// pts.get(0).distance(nodePt1)
			// + pts.get(pts.size() - 1).distance(nodePt2), pts
			// .get(0).distance(nodePt2)
			// + pts.get(pts.size() - 1).distance(nodePt1));
			double len = Math
					.min(MRFUtil.distance(pts.get(0), nodePt1)
							+ MRFUtil
									.distance(pts.get(pts.size() - 1), nodePt2),
							MRFUtil.distance(pts.get(0), nodePt2)
									+ MRFUtil.distance(pts.get(pts.size() - 1),
											nodePt1));
			if (len < nodeLineLen) {
				nodeLineIndex = i;
				nodeLineLen = len;
			}
		}
		String tempName = "";
		Line lineGeo = new Line();
		if (isFirstSeg) {
			if (minDistanceEdgeIndex == nodeLineIndex) {
				type = 0;
				tempName = edgeGeometrys.get(node.edges.get(nodeLineIndex).id).name;
				geo = (Line) edgeGeometrys
						.get(node.edges.get(nodeLineIndex).id).geometry;
				pts = geo.getListPoints();
				if (Math.abs(nodePt1.X - pts.get(0)[0])
						+ Math.abs(nodePt1.Y - pts.get(0)[1]) > 1) {
					Collections.reverse(pts);
					edgeNodeId.set(nodeLineIndex,
							pts.size() - 1 - edgeNodeId.get(nodeLineIndex));
				}
				if (edgeNodeId.get(nodeLineIndex) > pts.size() - 2) {
					edgeNodeId.set(nodeLineIndex, pts.size() - 2);
				}
				seg = new LineSegment(pts.get(edgeNodeId.get(nodeLineIndex)),
						pts.get(edgeNodeId.get(nodeLineIndex) + 1));
				lineGeo.addPoint(seg.project(pt));
				if (pathCount == 2) {
					minlen = Double.MAX_VALUE;
					int tempIndex = 0;
					for (int i = edgeNodeId.get(nodeLineIndex) + 1; i < pts
							.size() - 1; i++) {
						partMinLen = MRFUtil.distance(pts.get(i), targetPt);
						if (partMinLen < minlen) {
							minlen = partMinLen;
							tempIndex = i;
						}
					}
					for (int i = edgeNodeId.get(nodeLineIndex) + 1; i <= tempIndex; i++) {
						lineGeo.addPoint(pts.get(i));
					}
					if (tempIndex > pts.size() - 2)
						tempIndex = pts.size() - 2;
					seg = new LineSegment(pts.get(tempIndex),
							pts.get(tempIndex + 1));
					lineGeo.addPoint(seg.project(targetPt));
				} else {
					for (int i = edgeNodeId.get(nodeLineIndex) + 1; i < pts
							.size(); i++) {
						lineGeo.addPoint(pts.get(i));
					}
				}
			} else {
				type = 1;
				tempName = edgeGeometrys.get(node.edges
						.get(minDistanceEdgeIndex).id).name;
				geo = (Line) edgeGeometrys.get(node.edges
						.get(minDistanceEdgeIndex).id).geometry;
				pts = geo.getListPoints();
				if (Math.abs(nodePt1.X - pts.get(pts.size() - 1)[0])
						+ Math.abs(nodePt1.Y - pts.get(pts.size() - 1)[1]) > 1) {
					Collections.reverse(pts);
					edgeNodeId.set(minDistanceEdgeIndex, pts.size() - 1
							- edgeNodeId.get(minDistanceEdgeIndex));
				}
				if (edgeNodeId.get(minDistanceEdgeIndex) > pts.size() - 2) {
					edgeNodeId.set(minDistanceEdgeIndex, pts.size() - 2);
				}
				seg = new LineSegment(pts.get(edgeNodeId
						.get(minDistanceEdgeIndex)), pts.get(edgeNodeId
						.get(minDistanceEdgeIndex) + 1));
				lineGeo.addPoint(seg.project(pt));
				for (int i = edgeNodeId.get(minDistanceEdgeIndex) + 1; i < pts
						.size(); i++) {
					lineGeo.addPoint(pts.get(i));
				}
			}
			lines.add(lineGeo);
			roadNames.add(tempName);
			if (type == 1) {
				outPutPath(node, nodeIndex1, nodeIndex2, lines, roadNames);
			}
		} else {
			if (minDistanceEdgeIndex == nodeLineIndex) {
				type = 0;
				tempName = edgeGeometrys.get(node.edges.get(nodeLineIndex).id).name;
				geo = (Line) edgeGeometrys
						.get(node.edges.get(nodeLineIndex).id).geometry;
				pts = geo.getListPoints();
				if (Math.abs(nodePt1.X - pts.get(0)[0])
						+ Math.abs(nodePt1.Y - pts.get(0)[1]) > 1) {
					Collections.reverse(pts);
					edgeNodeId.set(nodeLineIndex,
							pts.size() - 1 - edgeNodeId.get(nodeLineIndex));
				}
				for (int i = 0; i < edgeNodeId.get(nodeLineIndex) + 1; i++) {
					lineGeo.addPoint(pts.get(i));
				}
				if (edgeNodeId.get(nodeLineIndex) > pts.size() - 2) {
					edgeNodeId.set(nodeLineIndex, pts.size() - 2);
				}
				seg = new LineSegment(pts.get(edgeNodeId.get(nodeLineIndex)),
						pts.get(edgeNodeId.get(nodeLineIndex) + 1));
				lineGeo.addPoint(seg.project(pt));
			} else {
				outPutPath(node, nodeIndex1, nodeIndex2, lines, roadNames);
				tempName = edgeGeometrys.get(node.edges
						.get(minDistanceEdgeIndex).id).name;
				geo = (Line) edgeGeometrys.get(node.edges
						.get(minDistanceEdgeIndex).id).geometry;
				pts = geo.getListPoints();
				if (Math.abs(nodePt2.X - pts.get(0)[0])
						+ Math.abs(nodePt2.Y - pts.get(0)[1]) > 1) {
					Collections.reverse(pts);
					edgeNodeId.set(minDistanceEdgeIndex, pts.size() - 1
							- edgeNodeId.get(minDistanceEdgeIndex));
				}
				for (int i = 0; i < edgeNodeId.get(minDistanceEdgeIndex) + 1; i++) {
					lineGeo.addPoint(pts.get(i));
				}
				if (edgeNodeId.get(minDistanceEdgeIndex) > pts.size() - 2) {
					edgeNodeId.set(minDistanceEdgeIndex, pts.size() - 2);
				}
				seg = new LineSegment(pts.get(edgeNodeId
						.get(minDistanceEdgeIndex)), pts.get(edgeNodeId
						.get(minDistanceEdgeIndex) + 1));
				lineGeo.addPoint(seg.project(pt));
			}
			if (tempName.equals(roadNames.get(roadNames.size() - 1))) {
				Line temp = lines.get(lines.size() - 1);
				temp.addPoints(lineGeo.getListPoints());
				lines.set(lines.size() - 1, temp);
			} else {
				roadNames.add(tempName);
				lines.add(lineGeo);
			}
		}

	}

	private double getCost(Edge e, RouteType type, double totalCost) {
		double cost = Double.MAX_VALUE;
		if (type == RouteType.lengthShortest)
			cost = e.length_cost;
		else if (type == RouteType.timeShortest)
			cost = e.time_cost;
		return cost + totalCost;
	}

	private List<Integer> singleSourceShortestPathForNonNegativeEdgeLength(
			Vertex source, Vertex target, int total, RouteType type) {
		List<Double> totalCosts = new ArrayList<Double>();
		List<Integer> prev = new ArrayList<Integer>();
		// double min = double.MaxValue;
		MinHeap<Integer, Double> crossingEdges = new MinHeap<Integer, Double>(
				total);
		for (Vertex v : vertices) // Initialization
		{
			if (v.id != source.id) // Where v has not yet been removed from Q
									// (unvisited nodes)
			{
				totalCosts.add(Double.MAX_VALUE); // Unknown distance function
													// from source to v
				prev.add(-1);
			} else {
				totalCosts.add(0D);
				prev.add(-1); // Previous node in optimal path from source
			}
			crossingEdges.insert(v.id, totalCosts.get(v.id)); // All nodes
																// initially in
																// Q (unvisited
																// nodes)
		}
		int targetIdx = -1;
		while (crossingEdges.count > 0) {
			KeyValuePair<Integer, Double> minEdge = crossingEdges.extractMin();
			int sourceInx = minEdge.getKey();
			double edgeLen = 0;
			if (vertices.get(sourceInx).edges != null
					&& vertices.get(sourceInx).edges.size() > 0) {
				for (Edge newEdge : vertices.get(sourceInx).edges) {
					if (newEdge.source == sourceInx)
						targetIdx = newEdge.target;
					else
						targetIdx = newEdge.source;
					edgeLen = getCost(newEdge, type, totalCosts.get(sourceInx));
					if (edgeLen < totalCosts.get(targetIdx)) {
						totalCosts.set(targetIdx, edgeLen);
						prev.set(targetIdx, sourceInx);
						crossingEdges.update(targetIdx, edgeLen);
					}
				}
			} else {
				// Console.WriteLine();
			}
		}

		List<Integer> path = new ArrayList<Integer>();
		int u = target.id;
		// path.AddFirst(u);
		while (prev.get(u) > -1) {
			path.add(0, u);
			u = prev.get(u);
		}
		path.add(0, u);

		return path;
	}
}
