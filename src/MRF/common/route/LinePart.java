package MRF.common.route;

import MRF.common.geometry.Line;

public class LinePart {
	public String name;
	public Line geometry;

	public LinePart(String name, Line geo) {
		this.name = name;
		this.geometry = geo;
	}
}
