package MRF.common.route;

import java.util.HashMap;

import WebRequestHandle.Business.DataModel.KeyValuePair;

public class MinHeap<T1, T2 extends Comparable<T2>> {
	private KeyValuePair<T1, T2>[] heap;
	public int count = 0;
	private HashMap<T1, Integer> heapIndex;
	private static final int BufferSize = 100;

	@SuppressWarnings("unchecked")
	public MinHeap() {
		heap = new KeyValuePair[BufferSize];
		heapIndex = new HashMap<T1, Integer>(BufferSize);
	}

	@SuppressWarnings("unchecked")
	public MinHeap(int capacity) {
		heap = new KeyValuePair[capacity];
		heapIndex = new HashMap<T1, Integer>(capacity);
	}

	public void insert(T1 key, T2 value) {
		KeyValuePair<T1, T2> kvp = new KeyValuePair<T1, T2>(key, value);
		heapIndex.put(key, count);
		heap[count++] = kvp;
		bubbleUp(count - 1);
	}

	public void delete(T1 key) {
		int nodeIndex = heapIndex.get(key);
		KeyValuePair<T1, T2> kvp = heap[--count];
		heap[nodeIndex] = kvp;
		heapIndex.put(kvp.getKey(), nodeIndex);
		heapIndex.remove(key);
		int parentIndex = getParentIndex(nodeIndex);
		boolean bubbleUp = false;
		if (parentIndex > 0) {
			KeyValuePair<T1, T2> parentNode = heap[parentIndex];
			if (kvp.getValue().compareTo(parentNode.getValue()) < 0) {
				swap(nodeIndex, parentIndex);
				heapIndex.put(key, parentIndex);
				heapIndex.put(parentNode.getKey(), nodeIndex);
				bubbleUp(parentIndex);
				bubbleUp = true;
			}
		}
		if (!bubbleUp) {
			if (count > (nodeIndex + 1)) {
				bubbleDown(nodeIndex);
			}
		}
	}

	public KeyValuePair<T1, T2> find(T1 key) {
		if (heapIndex.containsKey(key))
			return heap[heapIndex.get(key)];
		return null;
	}

	public void update(T1 key, T2 value) {
		int nodeIndex = heapIndex.get(key);
		heap[nodeIndex].setValue(value);

		int parentIndex = getParentIndex(nodeIndex);
		boolean bubbleUp = false;
		if (parentIndex >= 0) {
			KeyValuePair<T1, T2> parentNode = heap[parentIndex];
			if (parentNode.getValue().compareTo(value) > 0) {
				swap(nodeIndex, parentIndex);
				heapIndex.put(key, parentIndex);
				heapIndex.put(parentNode.getKey(), nodeIndex);
				bubbleUp(parentIndex);
				bubbleUp = true;
			}
		}
		if (!bubbleUp) {
			int minIndex = getMinChildIndex(nodeIndex);
			if (minIndex > 0) {
				KeyValuePair<T1, T2> minNode = heap[minIndex];
				if (value.compareTo(minNode.getValue()) > 0) {
					swap(nodeIndex, minIndex);
					heapIndex.put(key, minIndex);
					heapIndex.put(minNode.getKey(), nodeIndex);
					bubbleDown(minIndex);
				}
			}
		}
	}

	public KeyValuePair<T1, T2> extractMin() {
		KeyValuePair<T1, T2> min = heap[0];
		count--;
		if (count > 0) {
			KeyValuePair<T1, T2> kvp = heap[count];
			heap[0] = kvp;
			T1 key = heap[0].getKey();
			heapIndex.put(key, 0);
			heapIndex.remove(min.getKey());
			if (count > 1) {
				bubbleDown(0);
			}
		}
		return min;
	}

	private void bubbleUp(int nodeIndex) {
		if (nodeIndex != 0) {
			int parentIndex = getParentIndex(nodeIndex);
			KeyValuePair<T1, T2> node = heap[nodeIndex];
			KeyValuePair<T1, T2> parentNode = heap[parentIndex];
			if (parentNode.getValue().compareTo(node.getValue()) > 0) {
				swap(nodeIndex, parentIndex);
				heapIndex.put(node.getKey(), parentIndex);
				heapIndex.put(parentNode.getKey(), nodeIndex);
				bubbleUp(parentIndex);
			}
		}
	}

	private void bubbleDown(int nodeIndex) {
		KeyValuePair<T1, T2> node = heap[nodeIndex];
		int minIndex = getMinChildIndex(nodeIndex);
		if (minIndex > 0) {
			KeyValuePair<T1, T2> minNode = heap[minIndex];
			if (node.getValue().compareTo(minNode.getValue()) > 0) {
				swap(minIndex, nodeIndex);
				heapIndex.put(node.getKey(), minIndex);
				heapIndex.put(minNode.getKey(), nodeIndex);
				bubbleDown(minIndex);
			}
		}
	}

	private void swap(int node1, int node2) {
		KeyValuePair<T1, T2> tmpNode = heap[node1];
		heap[node1] = heap[node2];
		heap[node2] = tmpNode;
	}

	private int getParentIndex(int index) {
		return (index + 1) / 2 - 1;
	}

	private int getChildStartIndex(int index) {
		return index * 2 + 1;
	}

	private int getChildEndIndex(int index) {
		return index * 2 + 2;
	}

	private int getMinChildIndex(int index) {
		int startIndex = getChildStartIndex(index);
		if (startIndex >= count)
			return -1;
		int endIndex = getChildEndIndex(index);
		if (endIndex >= count)
			return startIndex;
		return (heap[startIndex].getValue()
				.compareTo(heap[endIndex].getValue()) < 0) ? startIndex
				: endIndex;
	}
}
