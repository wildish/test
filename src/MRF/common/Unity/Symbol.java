package MRF.common.Unity;

public class Symbol {
	public String Name;
	public String GroupName;
	public String Body;
	public int Width;
	public int Height;
	public boolean IsEditable;

	public static class Pattern {
		public String Name;
		public String Body;
		public int X;
		public int Y;
		public int Width;
		public int Height;
		public String ViewBox;
		public String Units;
		public Pattern() {
			super();
		}
	}

	public static class LineStyle {
		public String Name;
		public String Type;
		public String Body;
	}
}
