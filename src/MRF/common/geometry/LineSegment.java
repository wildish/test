package MRF.common.geometry;

import MRF.common.Util.MRFUtil;
import MRF.common.Util.Algorithm.CGAlgorithms;
import MRF.common.Util.Algorithm.HCoordinate;

@SuppressWarnings("rawtypes")
public class LineSegment implements Comparable {

	private double[] p0, p1;

	public double[] getP0() {
		return p0;
	}

	public void setP0(double[] p0) {
		this.p0 = p0;
	}

	public double[] getP1() {
		return p1;
	}

	public void setP1(double[] p1) {
		this.p1 = p1;
	}

	/**
	 * Creates an instance of this class using two coordinates
	 * 
	 * @param p0
	 *            The start-double[]
	 * @param p1
	 *            The end-double[]
	 */
	public LineSegment(double[] p0, double[] p1) {
		this.p0 = p0;
		this.p1 = p1;
	}

	public LineSegment(LineSegment ls) {
		this(ls.getP0(), ls.getP1());
	}

	public LineSegment() {
		this(new double[2], new double[2]);
	}

	public LineSegment(double x0, double y0, double x1, double y1) {
		this(new double[] { x0, y0 }, new double[] { x1, y1 });
	}

	public double[] GetCoordinate(int i) {
		return i == 0 ? p0 : p1;
	}

	public void setCoordinates(LineSegment ls) {
		setCoordinates(ls.p0, ls.p1);
	}

	public void setCoordinates(double[] p0, double[] p1) {
		this.p0[0] = p0[0];
		this.p0[1] = p0[1];
		this.p1[0] = p1[0];
		this.p1[1] = p1[1];
	}

	public double getLength() {
		return Math.sqrt(Math.pow(p0[0] - p1[0], 2)
				+ Math.pow(p0[1] - p1[1], 2));
	}

	public boolean getIsHorizontal() {
		return Math.abs(p0[1] - p1[1]) < Double.MIN_VALUE;
	}

	public boolean getIsVertical() {
		return Math.abs(p0[0] - p1[0]) < Double.MIN_VALUE;
	}

	public int orientationIndex(LineSegment seg) {
		int orient0 = CGAlgorithms.orientationIndex(p0, p1, seg.p0);
		int orient1 = CGAlgorithms.orientationIndex(p0, p1, seg.p1);
		// this handles the case where the points are Curve or collinear
		if (orient0 >= 0 && orient1 >= 0)
			return Math.max(orient0, orient1);
		// this handles the case where the points are R or collinear
		if (orient0 <= 0 && orient1 <= 0)
			return Math.max(orient0, orient1);
		// points lie on opposite sides ==> indeterminate orientation
		return 0;
	}

	public int orientationIndex(double[] p) {
		return CGAlgorithms.orientationIndex(p0, p1, p);
	}

	public void reverse() {
		double[] temp = p0;
		p0 = p1;
		p1 = temp;
	}

	public void normalize() {
		if (MRFUtil.compareTo(p1, p0) < 0)
			reverse();
	}

	/**
	 * @return The angle this segment makes with the x-axis (in radians).
	 */
	public double getAngle() {
		return Math.atan2(p1[1] - p0[1], p1[0] - p0[0]);
	}

	public double[] getMidPoint() {
		return new double[] { (p0[0] + p1[0]) / 2, (p0[1] + p1[1]) / 2 };
	}

	public double distance(LineSegment ls) {
		return CGAlgorithms.distanceLineLine(p0, p1, ls.p0, ls.p1);
	}

	/**
	 * Computes the distance between this line segment and a double[].
	 * 
	 * @param p
	 * @return
	 */
	public double distance(double[] p) {
		return CGAlgorithms.distancePointLine(p, p0, p1);
	}

	public double distance(Point p) {
		return distance(new double[] { p.X, p.Y });
	}

	public double distancePerpendicular(double[] p) {
		return CGAlgorithms.distancePointLinePerpendicular(p, p0, p1);
	}

	public double[] pointAlong(double segmentLengthFraction) {
		double[] coord = new double[2];
		coord[0] = p0[0] + segmentLengthFraction * (p1[0] - p0[0]);
		coord[1] = p0[1] + segmentLengthFraction * (p1[1] - p0[1]);
		return coord;
	}

	public double[] pointAlongOffset(double segmentLengthFraction,
			double offsetDistance) {
		// the double[] on the segment line
		double segx = p0[0] + segmentLengthFraction * (p1[0] - p0[0]);
		double segy = p0[1] + segmentLengthFraction * (p1[1] - p0[1]);

		double dx = p1[0] - p0[0];
		double dy = p1[1] - p0[1];
		double len = Math.sqrt(dx * dx + dy * dy);
		double ux = 0.0;
		double uy = 0.0;
		if (offsetDistance != 0.0) {
			if (len <= 0.0)
				try {
					throw new Exception(
							"Cannot compute offset from zero-length line segment");
				} catch (Exception e) {
					e.printStackTrace();
				}

			// u is the vector that is the length of the offset, in the
			// direction of the segment
			ux = offsetDistance * dx / len;
			uy = offsetDistance * dy / len;
		}

		// the offset double[] is the seg double[] plus the offset vector
		// rotated 90
		// degrees CCW
		double offsetx = segx - uy;
		double offsety = segy + ux;

		double[] coord = new double[] { offsetx, offsety };
		return coord;
	}

	public double projectionFactor(double[] p) {
		if (p.equals(p0))
			return 0.0;
		if (p.equals(p1))
			return 1.0;

		// Otherwise, use comp.graphics.algorithms Frequently Asked Questions
		// method
		/*
		 * AC dot AB r = ------------ ||AB||^2 r has the following meaning: r=0
		 * double[] = A r=1 double[] = B r<0 double[] is on the backward
		 * extension of AB r>1 double[] is on the forward extension of AB 0<r<1
		 * double[] is interior to AB
		 */
		double dx = p1[0] - p0[0];
		double dy = p1[1] - p0[1];
		double len = dx * dx + dy * dy;

		// handle zero-length segments
		if (len <= 0.0)
			return Double.NaN;

		double r = ((p[0] - p0[0]) * dx + (p[1] - p0[1]) * dy) / len;
		return r;
	}

	public double segmentFraction(double[] inputPt) {
		double segFrac = projectionFactor(inputPt);
		if (segFrac < 0.0)
			segFrac = 0.0;
		else if (segFrac > 1.0)
			segFrac = 1.0;
		return segFrac;
	}

	public double[] project(Point p) {
		return project(new double[] { p.X, p.Y });
	}

	public double[] project(double[] p) {
		if (MRFUtil.isEqual(p, p0) || MRFUtil.isEqual(p, p1))
			return p;

		double r = projectionFactor(p);
		double[] coord = new double[] { p0[0] + r * (p1[0] - p0[0]),
				p0[1] + r * (p1[1] - p0[1]) };
		return coord;
	}

	public LineSegment project(LineSegment seg) {
		double pf0 = projectionFactor(seg.p0);
		double pf1 = projectionFactor(seg.p1);
		// check if segment projects at all
		if (pf0 >= 1.0 && pf1 >= 1.0)
			return null;
		if (pf0 <= 0.0 && pf1 <= 0.0)
			return null;

		double[] newp0 = project(seg.p0);
		if (pf0 < 0.0)
			newp0 = p0;
		if (pf0 > 1.0)
			newp0 = p1;

		double[] newp1 = project(seg.p1);
		if (pf1 < 0.0)
			newp1 = p0;
		if (pf1 > 1.0)
			newp1 = p1;

		return new LineSegment(newp0, newp1);
	}

	public double[] closestPoint(double[] p) {
		double factor = projectionFactor(p);
		if (factor > 0 && factor < 1)
			return project(p);
		double dist0 = MRFUtil.distance(p0, p);
		double dist1 = MRFUtil.distance(p1, p);
		return dist0 < dist1 ? p0 : p1;
	}

	/**
	 * Computes the closest points on a line segment.
	 * 
	 * @param line
	 * @return A pair of Coordinates which are the closest points on the line
	 *         segments.
	 */
	public double[][] closestPoints(LineSegment line) {
		// test for intersection
		double[] intPt = intersection(line);
		if (intPt != null)
			return new double[][] { intPt, intPt };

		/*
		 * if no intersection closest pair contains at least one endpoint. Test
		 * each endpoint in turn.
		 */
		double[][] closestPt = new double[2][3];

		double[] close00 = closestPoint(line.p0);
		double minDistance = MRFUtil.distance(close00, line.p0);
		closestPt[0] = close00;
		closestPt[1] = line.p0;

		double[] close01 = closestPoint(line.p1);
		double dist = MRFUtil.distance(close01, line.p1);
		if (dist < minDistance) {
			minDistance = dist;
			closestPt[0] = close01;
			closestPt[1] = line.p1;
		}

		double[] close10 = line.closestPoint(p0);
		dist = MRFUtil.distance(close10, p0);
		if (dist < minDistance) {
			minDistance = dist;
			closestPt[0] = p0;
			closestPt[1] = close10;
		}

		double[] close11 = line.closestPoint(p1);
		dist = MRFUtil.distance(close11, p1);
		if (dist < minDistance) {
			minDistance = dist;
			closestPt[0] = p1;
			closestPt[1] = close11;
		}

		return closestPt;
	}

	public double[] intersection(LineSegment line) {
		double[] intPt = HCoordinate.intersection(p0, p1, line.p0, line.p1);
		return intPt;
	}

	public double[] lineIntersection(LineSegment line) {
		double[] intPt = HCoordinate.intersection(p0, p1, line.p0, line.p1);
		return intPt;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof LineSegment))
			return false;
		LineSegment other = (LineSegment) o;
		return p0.equals(other.p0) && p1.equals(other.p1);
	}

	public static boolean operator(LineSegment obj1, LineSegment obj2) {
		return obj1.equals(obj2);
	}

	public static boolean unOperator(LineSegment obj1, LineSegment obj2) {
		return !(obj1.equals(obj2));
	}

	public int compareTo(Object o) {
		LineSegment other = (LineSegment) o;
		int comp0 = MRFUtil.compareTo(p0, other.p0);
		return comp0 != 0 ? comp0 : MRFUtil.compareTo(p1, other.p1);
	}

	/**
	 * Returns <c>true</c> if <c>other</c> is topologically equal to this
	 * LineSegment (e.g. irrespective of orientation).
	 * 
	 * @param other
	 *            A <c>LineSegment</c> with which to do the comparison.
	 * @return <c>true</c> if <c>other</c> is a <c>LineSegment</c> with the same
	 *         values for the x and y ordinates.
	 */
	public boolean EqualsTopologically(LineSegment other) {
		return p0.equals(other.p0) && p1.equals(other.p1)
				|| p0.equals(other.p1) && p1.equals(other.p0);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("LINESTRING( ");
		sb.append("{");
		sb.append(p0[0]);
		sb.append("} ");
		sb.append("{");
		sb.append(p0[1]);
		sb.append("}, ");
		sb.append("{");
		sb.append(p1[0]);
		sb.append("} ");
		sb.append("{");
		sb.append(p1[1]);
		sb.append("})");
		return sb.toString();
	}

	/**
	 * @return HashCode.
	 */
	@Override
	public int hashCode() {
		long bits0 = (long) p0[0];
		// BitConverter.DoubleToInt64Bits(_p0[1]) * 31;
		bits0 ^= (long) p0[1] * 31;
		int hash0 = (((int) bits0) ^ ((int) (bits0 >> 32)));

		long bits1 = (long) p1[0];
		bits1 ^= (long) p1[1] * 31;
		int hash1 = (((int) bits1) ^ ((int) (bits1 >> 32)));

		// XOR is supposed to be a good way to combine hashcodes
		return hash0 ^ hash1;

		// return base.GetHashCode();
	}

	// private double[] p0, p1;
	//
	// public double[] getP0() {
	// return p0;
	// }
	//
	// public void setP0(double[] p0) {
	// this.p0 = p0;
	// }
	//
	// public double[] getP1() {
	// return p1;
	// }
	//
	// public void setP1(double[] p1) {
	// this.p1 = p1;
	// }
	//
	// /**
	// * Creates an instance of this class using two coordinates
	// *
	// * @param p0
	// * The start-double[]
	// * @param p1
	// * The end-double[]
	// */
	// public LineSegment(double[] p0, double[] p1) {
	// this.p0 = p0;
	// this.p1 = p1;
	// }
	//
	// public LineSegment(LineSegment ls) {
	// this(ls.getP0(), ls.getP1());
	// }
	//
	// public LineSegment() {
	// this(new double[](), new double[]());
	// }
	//
	// public LineSegment(double x0, double y0, double x1, double y1) {
	// this(new double[](x0, y0), new double[](x1, y1));
	// }
	//
	// public double[] GetCoordinate(int i) {
	// return i == 0 ? p0 : p1;
	// }
	//
	// public void setCoordinates(LineSegment ls) {
	// setCoordinates(ls.p0, ls.p1);
	// }
	//
	// public void setCoordinates(double[] p0, double[] p1) {
	// p0[0] = p0[0];
	// p0[1] = p0[1];
	// p1[0] = p1[0];
	// p1[1] = p1[1];
	// }
	//
	// public double getLength() {
	// return Math.sqrt(Math.pow(p0[0] - p1[0], 2) + Math.pow(p0[1] - p1[1],
	// 2));
	// }
	//
	// public boolean getIsHorizontal() {
	// return Math.abs(p0[1] - p1[1]) < Double.MIN_VALUE;
	// }
	//
	// public boolean getIsVertical() {
	// return Math.abs(p0[0] - p1[0]) < Double.MIN_VALUE;
	// }
	//
	// public int orientationIndex(LineSegment seg) {
	// int orient0 = CGAlgorithms.orientationIndex(p0, p1, seg.p0);
	// int orient1 = CGAlgorithms.orientationIndex(p0, p1, seg.p1);
	// // this handles the case where the points are Curve or collinear
	// if (orient0 >= 0 && orient1 >= 0)
	// return Math.max(orient0, orient1);
	// // this handles the case where the points are R or collinear
	// if (orient0 <= 0 && orient1 <= 0)
	// return Math.max(orient0, orient1);
	// // points lie on opposite sides ==> indeterminate orientation
	// return 0;
	// }
	//
	// public int orientationIndex(double[] p) {
	// return CGAlgorithms.orientationIndex(p0, p1, p);
	// }
	//
	// public void reverse() {
	// double[] temp = p0;
	// p0 = p1;
	// p1 = temp;
	// }
	//
	// public void normalize() {
	// if (p1.compareTo(p0) < 0)
	// reverse();
	// }
	//
	// /**
	// * @return The angle this segment makes with the x-axis (in radians).
	// */
	// public double getAngle() {
	// return Math.atan2(p1[1] - p0[1], p1[0] - p0[0]);
	// }
	//
	// public double[] getMidPoint() {
	// return new double[]((p0[0] + p1[0]) / 2, (p0[1] + p1[1]) / 2);
	// }
	//
	// public double distance(LineSegment ls) {
	// return CGAlgorithms.distanceLineLine(p0, p1, ls.p0, ls.p1);
	// }
	//
	// /**
	// * Computes the distance between this line segment and a double[].
	// *
	// * @param p
	// * @return
	// */
	// public double distance(double[] p) {
	// return CGAlgorithms.distancePointLine(p, p0, p1);
	// }
	//
	// public double distancePerpendicular(double[] p) {
	// return CGAlgorithms.distancePointLinePerpendicular(p, p0, p1);
	// }
	//
	// public double[] pointAlong(double segmentLengthFraction) {
	// double[] coord = new double[]();
	// coord[0] = p0[0] + segmentLengthFraction * (p1[0] - p0[0]);
	// coord[1] = p0[1] + segmentLengthFraction * (p1[1] - p0[1]);
	// return coord;
	// }
	//
	// public double[] pointAlongOffset(double segmentLengthFraction,
	// double offsetDistance) {
	// // the double[] on the segment line
	// double segx = p0[0] + segmentLengthFraction * (p1[0] - p0[0]);
	// double segy = p0[1] + segmentLengthFraction * (p1[1] - p0[1]);
	//
	// double dx = p1[0] - p0[0];
	// double dy = p1[1] - p0[1];
	// double len = Math.sqrt(dx * dx + dy * dy);
	// double ux = 0.0;
	// double uy = 0.0;
	// if (offsetDistance != 0.0) {
	// if (len <= 0.0)
	// try {
	// throw new Exception(
	// "Cannot compute offset from zero-length line segment");
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// // u is the vector that is the length of the offset, in the
	// // direction of the segment
	// ux = offsetDistance * dx / len;
	// uy = offsetDistance * dy / len;
	// }
	//
	// // the offset double[] is the seg double[] plus the offset vector rotated
	// 90
	// // degrees CCW
	// double offsetx = segx - uy;
	// double offsety = segy + ux;
	//
	// double[] coord = new double[](offsetx, offsety);
	// return coord;
	// }
	//
	// public double projectionFactor(double[] p) {
	// if (p.equals(p0))
	// return 0.0;
	// if (p.equals(p1))
	// return 1.0;
	//
	// // Otherwise, use comp.graphics.algorithms Frequently Asked Questions
	// // method
	// /*
	// * AC dot AB r = ------------ ||AB||^2 r has the following meaning: r=0
	// * double[] = A r=1 double[] = B r<0 double[] is on the backward extension
	// of AB
	// * r>1 double[] is on the forward extension of AB 0<r<1 double[] is
	// interior
	// * to AB
	// */
	// double dx = p1[0] - p0[0];
	// double dy = p1[1] - p0[1];
	// double len = dx * dx + dy * dy;
	//
	// // handle zero-length segments
	// if (len <= 0.0)
	// return Double.NaN;
	//
	// double r = ((p[0] - p0[0]) * dx + (p[1] - p0[1]) * dy) / len;
	// return r;
	// }
	//
	// public double segmentFraction(double[] inputPt) {
	// double segFrac = projectionFactor(inputPt);
	// if (segFrac < 0.0)
	// segFrac = 0.0;
	// else if (segFrac > 1.0)
	// segFrac = 1.0;
	// return segFrac;
	// }
	//
	// public double[] project(double[] p) {
	// if (p.equals(p0) || p.equals(p1))
	// return new double[](p);
	//
	// double r = projectionFactor(p);
	// double[] coord = new double[](p0[0] + r * (p1[0] - p0[0]), p0[1] + r
	// * (p1[1] - p0[1]));
	// return coord;
	// }
	//
	// public LineSegment project(LineSegment seg) {
	// double pf0 = projectionFactor(seg.p0);
	// double pf1 = projectionFactor(seg.p1);
	// // check if segment projects at all
	// if (pf0 >= 1.0 && pf1 >= 1.0)
	// return null;
	// if (pf0 <= 0.0 && pf1 <= 0.0)
	// return null;
	//
	// double[] newp0 = project(seg.p0);
	// if (pf0 < 0.0)
	// newp0 = p0;
	// if (pf0 > 1.0)
	// newp0 = p1;
	//
	// double[] newp1 = project(seg.p1);
	// if (pf1 < 0.0)
	// newp1 = p0;
	// if (pf1 > 1.0)
	// newp1 = p1;
	//
	// return new LineSegment(newp0, newp1);
	// }
	//
	// public double[] closestPoint(double[] p) {
	// double factor = projectionFactor(p);
	// if (factor > 0 && factor < 1)
	// return project(p);
	// double dist0 = p0.distance(p);
	// double dist1 = p1.distance(p);
	// return dist0 < dist1 ? p0 : p1;
	// }
	//
	// /**
	// * Computes the closest points on a line segment.
	// *
	// * @param line
	// * @return A pair of Coordinates which are the closest points on the line
	// * segments.
	// */
	// public double[][] closestPoints(LineSegment line) {
	// // test for intersection
	// double[] intPt = intersection(line);
	// if (intPt != null)
	// return new double[][] { intPt, intPt };
	//
	// /*
	// * if no intersection closest pair contains at least one endpoint. Test
	// * each endpoint in turn.
	// */
	// double[][] closestPt = new double[][2];
	//
	// double[] close00 = closestPoint(line.p0);
	// double minDistance = close00.distance(line.p0);
	// closestPt[0] = close00;
	// closestPt[1] = line.p0;
	//
	// double[] close01 = closestPoint(line.p1);
	// double dist = close01.distance(line.p1);
	// if (dist < minDistance) {
	// minDistance = dist;
	// closestPt[0] = close01;
	// closestPt[1] = line.p1;
	// }
	//
	// double[] close10 = line.closestPoint(p0);
	// dist = close10.distance(p0);
	// if (dist < minDistance) {
	// minDistance = dist;
	// closestPt[0] = p0;
	// closestPt[1] = close10;
	// }
	//
	// double[] close11 = line.closestPoint(p1);
	// dist = close11.distance(p1);
	// if (dist < minDistance) {
	// minDistance = dist;
	// closestPt[0] = p1;
	// closestPt[1] = close11;
	// }
	//
	// return closestPt;
	// }
	//
	// public double[] intersection(LineSegment line) {
	// double[] intPt = HCoordinate.intersection(p0, p1, line.p0, line.p1);
	// return intPt;
	// }
	//
	// public double[] lineIntersection(LineSegment line) {
	// double[] intPt = HCoordinate.intersection(p0, p1, line.p0, line.p1);
	// return intPt;
	// }
	//
	// @Override
	// public boolean equals(Object o) {
	// if (o == null)
	// return false;
	// if (!(o instanceof LineSegment))
	// return false;
	// LineSegment other = (LineSegment) o;
	// return p0.equals(other.p0) && p1.equals(other.p1);
	// }
	//
	// public static boolean operator(LineSegment obj1, LineSegment obj2) {
	// return obj1.equals(obj2);
	// }
	//
	// public static boolean unOperator(LineSegment obj1, LineSegment obj2) {
	// return !(obj1.equals(obj2));
	// }
	//
	// public int compareTo(Object o) {
	// LineSegment other = (LineSegment) o;
	// int comp0 = p0.compareTo(other.p0);
	// return comp0 != 0 ? comp0 : p1.compareTo(other.p1);
	// }
	//
	// /**
	// * Returns <c>true</c> if <c>other</c> is topologically equal to this
	// * LineSegment (e.g. irrespective of orientation).
	// *
	// * @param other
	// * A <c>LineSegment</c> with which to do the comparison.
	// * @return <c>true</c> if <c>other</c> is a <c>LineSegment</c> with the
	// same
	// * values for the x and y ordinates.
	// */
	// public boolean EqualsTopologically(LineSegment other) {
	// return p0.equals(other.p0) && p1.equals(other.p1)
	// || p0.equals(other.p1) && p1.equals(other.p0);
	// }
	//
	// @Override
	// public String toString() {
	// StringBuilder sb = new StringBuilder("LINESTRING( ");
	// sb.append(String.format("{%f} ", p0[0]));
	// sb.append(String.format("{%f}, ", p0[1]));
	// sb.append(String.format("{%f} ", p1[0]));
	// sb.append(String.format("{%f})", p1[1]));
	// return sb.toString();
	// }
	//
	// /**
	// * @return HashCode.
	// */
	// @Override
	// public int hashCode() {
	// long bits0 = (long) p0[0];
	// // BitConverter.DoubleToInt64Bits(_p0[1]) * 31;
	// bits0 ^= (long) p0[1] * 31;
	// int hash0 = (((int) bits0) ^ ((int) (bits0 >> 32)));
	//
	// long bits1 = (long) p1[0];
	// bits1 ^= (long) p1[1] * 31;
	// int hash1 = (((int) bits1) ^ ((int) (bits1 >> 32)));
	//
	// // XOR is supposed to be a good way to combine hashcodes
	// return hash0 ^ hash1;
	//
	// // return base.GetHashCode();
	// }
}
