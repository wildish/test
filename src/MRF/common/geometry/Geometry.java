package MRF.common.geometry;

public class Geometry {
	private int dimensions;
	private WKBGeometryType geometryType;
	private Envelope env;

    public int getDimensions() {
		return dimensions;
	}

	public void setDimensions(int dimensions) {
		this.dimensions = dimensions;
	}

	public WKBGeometryType getGeometryType() {
		return geometryType;
	}

	public void setGeometryType(WKBGeometryType geometryType) {
		this.geometryType = geometryType;
	}

	public Envelope getEnv() {
		return env;
	}

	public void setEnv(Envelope env) {
		this.env = env;
	}
    
    public Geometry() {
        geometryType = WKBGeometryType.WKBGEOMETRYNULL;
        dimensions = 0;
        env = new Envelope();
    }

    public boolean isEqual(Geometry otherGeometry) {
        return false;
    }

    public byte[] toWKB() {
        return GeometryToWKB.write(this);
    }

    public String toWKT() {
        return GeometryToWKT.write(this);
    }

    public Geometry clone() {
        return new Geometry();
    }
}
