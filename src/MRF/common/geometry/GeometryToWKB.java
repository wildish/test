package MRF.common.geometry;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import MRF.common.Util.DataHelper;

enum WkbByteOrder {
	XDR, NDR
}

public class GeometryToWKB {
	public static byte[] write(Geometry g) {
		return write(g, WkbByteOrder.NDR);
	}

	/**
	 * Writes a geometry to a byte array using the specified encoding.
	 * 
	 * @param g
	 *            The geometry to write
	 * @param wkbByteOrder
	 *            Byte order
	 * @return WKB representation of the geometry
	 */
	public static byte[] write(Geometry g, WkbByteOrder wkbByteOrder) {
		ByteArrayOutputStream bw = new ByteArrayOutputStream();
		// DataOutputStream dos = new DataOutputStream(bw);

		// Write the byteorder format.
		try {
			bw.write(new byte[] { (byte) wkbByteOrder.ordinal() });
		} catch (IOException e) {
		}

		// Write the type of this geometry
		writeType(g, bw, wkbByteOrder);

		// Write the geometry
		writeGeometry(g, bw, wkbByteOrder);

		return bw.toByteArray();
	}

	/**
	 * Writes the type number for this geometry.
	 * 
	 * @param geometry
	 *            The geometry to determine the type of.
	 * @param bWriter
	 *            Binary Writer
	 * @param byteorder
	 *            Byte order
	 */
	private static void writeType(Geometry geometry,
			ByteArrayOutputStream bWriter, WkbByteOrder byteorder) {
		// Determine the type of the geometry.
		switch (geometry.getGeometryType()) {
		// Points are type 1.
		case WKBPOINT:
			writeUInt32(WKBGeometryType.WKBPOINT.ordinal(), bWriter, byteorder);
			break;
		// Linestrings are type 2.
		case WKBLINESTRING:
			writeUInt32(WKBGeometryType.WKBLINESTRING.ordinal(), bWriter,
					byteorder);
			break;
		// Polygons are type 3.
		case WKBPOLYGON:
			writeUInt32(WKBGeometryType.WKBPOLYGON.ordinal(), bWriter,
					byteorder);
			break;
		// Mulitpoints are type 4.
		case WKBMULTIPOINT:
			writeUInt32(WKBGeometryType.WKBMULTIPOINT.ordinal(), bWriter,
					byteorder);
			break;
		// Multilinestrings are type 5.
		case WKBMULTILINESTRING:
			writeUInt32(WKBGeometryType.WKBMULTILINESTRING.ordinal(), bWriter,
					byteorder);
			break;
		// Multipolygons are type 6.
		case WKBMULTIPOLYGON:
			writeUInt32(WKBGeometryType.WKBMULTIPOLYGON.ordinal(), bWriter,
					byteorder);
			break;
		// Geometrycollections are type 7.
		case WKBGEOMETRYCOLLECTION:
			writeUInt32(WKBGeometryType.WKBGEOMETRYCOLLECTION.ordinal(),
					bWriter, byteorder);
			break;
		// If the type is not of the above 7 throw an exception.
		default:
			try {
				throw new RuntimeException("Invalid Geometry Type");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Writes the geometry to the binary writer.
	 * 
	 * @param geometry
	 *            The geometry to be written.
	 * @param bWriter
	 * @param byteorder
	 *            Byte order
	 */
	private static void writeGeometry(Geometry geometry,
			ByteArrayOutputStream bWriter, WkbByteOrder byteorder) {
		switch (geometry.getGeometryType()) {
		// Write the point.
		case WKBPOINT:
			writePoint((Point) geometry, bWriter, byteorder);
			break;
		case WKBLINESTRING:
			Line ls = (Line) geometry;
			writeLineString(ls, bWriter, byteorder);
			break;
		case WKBPOLYGON:
			writePolygon((Polygon) geometry, bWriter, byteorder);
			break;
		// Write the Multipoint.
		case WKBMULTIPOINT:
			writeMultiPoint((MultiPoint) geometry, bWriter, byteorder);
			break;
		// Write the Multilinestring.
		case WKBMULTILINESTRING:
			writeMultiLineString((MultiLine) geometry, bWriter, byteorder);
			break;
		// Write the Multipolygon.
		case WKBMULTIPOLYGON:
			writeMultiPolygon((MultiPolygon) geometry, bWriter, byteorder);
			break;
		// Write the Geometrycollection.
		case WKBGEOMETRYCOLLECTION:
			writeGeometryCollection((GeometryCollection) geometry, bWriter,
					byteorder);
			break;
		// If the type is not of the above 7 throw an exception.
		default:
			try {
				throw new RuntimeException("Invalid Geometry Type");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Writes a point.
	 * 
	 * @param point
	 *            The point to be written.
	 * @param bWriter
	 *            Stream to write to.
	 * @param byteorder
	 *            Byte order
	 */
	private static void writePoint(Point point, ByteArrayOutputStream bWriter,
			WkbByteOrder byteorder) {
		// Write the x coordinate.
		writeDouble(point.X, bWriter, byteorder);
		// Write the y coordinate.
		writeDouble(point.Y, bWriter, byteorder);
	}

	private static void writePoint(double[] point,
			ByteArrayOutputStream bWriter, WkbByteOrder byteorder) {
		// Write the x coordinate.
		writeDouble(point[0], bWriter, byteorder);
		// Write the y coordinate.
		writeDouble(point[1], bWriter, byteorder);
	}

	/**
	 * Writes a linestring.
	 * 
	 * @param ls
	 *            The linestring to be written.
	 * @param bWriter
	 *            Stream to write to.
	 * @param byteorder
	 *            Byte order
	 */
	private static void writeLineString(Line ls, ByteArrayOutputStream bWriter,
			WkbByteOrder byteorder) {
		int count = ls.getPartCount();
		// Write the number of points in this linestring.
		writeUInt32(count, bWriter, byteorder);

		// Loop on each vertices.
		// for (Point p : ls.getListPoints()) {
		// writePoint(p, bWriter, byteorder);
		// }
		for (double[] p : ls.getListPoints()) {
			writePoint(p, bWriter, byteorder);
		}
	}

	/**
	 * Writes a polygon.
	 * 
	 * @param poly
	 *            The polygon to be written.
	 * @param bWriter
	 *            Stream to write to.
	 * @param byteorder
	 *            Byte order
	 */
	private static void writePolygon(Polygon poly,
			ByteArrayOutputStream bWriter, WkbByteOrder byteorder) {
		// Get the number of rings in this polygon.
		int numRings = poly.getPartCount();

		// Write the number of rings to the stream (add one for the shell)
		writeUInt32(numRings, bWriter, byteorder);

		// Write the exterior of this polygon.
		writeLineString(poly.getExteriorRing(), bWriter, byteorder);

		// Loop on the number of rings - 1 because we already wrote the shell.
		for (Line lr : poly.getInteriorRings())
			// Write the (lineString)LinearRing.
			writeLineString(lr, bWriter, byteorder);
	}

	/**
	 * Writes a multipoint.
	 * 
	 * @param mp
	 *            The multipoint to be written.
	 * @param bWriter
	 *            Stream to write to.
	 * @param byteorder
	 *            Byte order
	 */
	private static void writeMultiPoint(MultiPoint mp,
			ByteArrayOutputStream bWriter, WkbByteOrder byteorder) {
		// Write the number of points.
		writeUInt32(mp.getPartCount(), bWriter, byteorder);

		// Loop on the number of points.
		for (Point p : mp.getListPoints()) {
			// Write Points Header
			try {
				bWriter.write(new byte[] { (byte) byteorder.ordinal() });
			} catch (IOException e) {
				// e.printStackTrace();
			}
			writeUInt32(WKBGeometryType.WKBPOINT.ordinal(), bWriter, byteorder);
			// Write each point.
			writePoint(p, bWriter, byteorder);
		}
	}

	/**
	 * Writes a multilinestring.
	 * 
	 * @param mls
	 *            The multilinestring to be written.
	 * @param bWriter
	 *            Stream to write to.
	 * @param byteorder
	 *            Byte order
	 */
	private static void writeMultiLineString(MultiLine mls,
			ByteArrayOutputStream bWriter, WkbByteOrder byteorder) {
		// Write the number of linestrings.
		writeUInt32(mls.getPartCount(), bWriter, byteorder);

		// Loop on the number of linestrings.
		for (Line ls : mls.GetListLines()) {
			// Write LineString Header
			try {
				bWriter.write(new byte[] { (byte) byteorder.ordinal() });
			} catch (IOException e) {
				// e.printStackTrace();
			}
			writeUInt32(WKBGeometryType.WKBLINESTRING.ordinal(), bWriter,
					byteorder);
			// Write each linestring.
			writeLineString(ls, bWriter, byteorder);
		}
	}

	/**
	 * Writes a multipolygon.
	 * 
	 * @param mp
	 *            The mulitpolygon to be written.
	 * @param bWriter
	 *            Stream to write to.
	 * @param byteorder
	 *            Byte order
	 */
	private static void writeMultiPolygon(MultiPolygon mp,
			ByteArrayOutputStream bWriter, WkbByteOrder byteorder) {
		// Write the number of polygons.
		writeUInt32(mp.GetPartCount(), bWriter, byteorder);

		// Loop on the number of polygons.
		for (Polygon poly : mp.GetPolygons()) {
			// Write polygon header
			try {
				bWriter.write(new byte[] { (byte) byteorder.ordinal() });
			} catch (IOException e) {
				// e.printStackTrace();
			}
			writeUInt32(WKBGeometryType.WKBPOLYGON.ordinal(), bWriter,
					byteorder);
			// Write each polygon.
			writePolygon(poly, bWriter, byteorder);
		}
	}

	/**
	 * Writes a geometrycollection.
	 * 
	 * @param gc
	 *            The geometrycollection to be written.
	 * @param bWriter
	 *            Stream to write to.
	 * @param byteorder
	 *            Byte order
	 */
	private static void writeGeometryCollection(GeometryCollection gc,
			ByteArrayOutputStream bWriter, WkbByteOrder byteorder) {
		// Get the number of geometries in this geometrycollection.
		int numGeometries = gc.getPartCount();

		// Write the number of geometries.
		writeUInt32(numGeometries, bWriter, byteorder);

		// Loop on the number of geometries.
		for (int i = 0; i < numGeometries; i++) {
			// Write the byte-order format of the following geometry.
			try {
				bWriter.write(DataHelper.intToBytes(byteorder.ordinal()));
			} catch (IOException e) {
				// e.printStackTrace();
			}
			// Write the type of each geometry.
			writeType(gc.getGeometries().get(i), bWriter, byteorder);
			// Write each geometry.
			writeGeometry(gc.getGeometries().get(i), bWriter, byteorder);
		}
	}

	/**
	 * Writes an unsigned integer to the binarywriter using the specified
	 * encoding
	 * 
	 * @param value
	 *            Value to write
	 * @param writer
	 *            Binary Writer
	 * @param byteOrder
	 *            byteorder
	 */
	private static void writeUInt32(int value, ByteArrayOutputStream writer,
			WkbByteOrder byteOrder) {
		// if (byteOrder == WkbByteOrder.XDR) {
		// byte[] bytes = DataHelper.intToBytes(value);
		// bytes = DataHelper.reverse(bytes);
		// value = DataHelper.bytesToInt(bytes, 0);
		// }
		try {
			writer.write(DataHelper.intToBytes(value));
		} catch (IOException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * Writes a double to the binarywriter using the specified encoding
	 * 
	 * @param value
	 *            Value to write
	 * @param writer
	 *            Binary Writer
	 * @param byteOrder
	 *            byteorder
	 */
	private static void writeDouble(double value, ByteArrayOutputStream writer,
			WkbByteOrder byteOrder) {
		// if (byteOrder == WkbByteOrder.XDR) {
		// byte[] bytes = DataHelper.doubleToBytes(value);
		// bytes = DataHelper.reverse(bytes);
		// value = DataHelper.bytesToDouble(bytes, 0);
		// }

		try {
			writer.write(DataHelper.doubleToBytes(value));
		} catch (IOException e) {

		}
	}

}
