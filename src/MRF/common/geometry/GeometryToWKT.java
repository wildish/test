package MRF.common.geometry;

import java.io.StringWriter;
import java.util.List;

public class GeometryToWKT {
	public static String write(Geometry geometry) {
		StringWriter sw = new StringWriter();
		write(geometry, sw);
		return sw.toString();
	}

	/**
	 * Converts a Geometry to its Well-known Text representation. Geometry is
	 * written to the output stream as &lt;Gemoetry Tagged Text&gt; string (see
	 * the OpenGIS Simple Features Specification).
	 * 
	 * @param geometry
	 *            A geometry to process.
	 * @param writer
	 *            Stream to write out the geometry's text representation.
	 */
	public static void write(Geometry geometry, StringWriter writer) {
		appendGeometryTaggedText(geometry, writer);
	}

	/**
	 * Converts a Geometry to &lt;Geometry Tagged Text &gt; format, then Appends
	 * it to the writer.
	 * 
	 * @param geometry
	 *            The Geometry to process.
	 * @param writer
	 *            The output stream to Append to.
	 */
	private static void appendGeometryTaggedText(Geometry geometry,
			StringWriter writer) {
		if (geometry == null)
			throw new NullPointerException(
					"Cannot write Well-Known Text: geometry was null");
		;
		if (geometry instanceof Point) {
			Point point = (Point) geometry;
			appendPointTaggedText(point, writer);
		} else if (geometry instanceof Line)
			appendLineStringTaggedText((Line) geometry, writer);
		else if (geometry instanceof Polygon)
			appendPolygonTaggedText((Polygon) geometry, writer);
		else if (geometry instanceof MultiPoint)
			appendMultiPointTaggedText((MultiPoint) geometry, writer);
		else if (geometry instanceof MultiLine)
			appendMultiLineStringTaggedText((MultiLine) geometry, writer);
		else if (geometry instanceof MultiPolygon)
			appendMultiPolygonTaggedText((MultiPolygon) geometry, writer);
		else if (geometry instanceof GeometryCollection)
			appendGeometryCollectionTaggedText((GeometryCollection) geometry,
					writer);
		else
			throw new RuntimeException("Unsupported Geometry implementation:"
					+ geometry.getGeometryType().name());
	}

	/**
	 * Converts a Point to &lt;Point Tagged Text&gt; format, then Appends it to
	 * the writer.
	 * 
	 * @param coordinate
	 *            the <code>Point</code> to process
	 * @param writer
	 *            the output writer to Append to
	 */
	private static void appendPointTaggedText(Point coordinate,
			StringWriter writer) {
		writer.write("POINT");
		appendPointText(coordinate, writer);
	}

	/**
	 * Converts a LineString to LineString tagged text format,
	 * 
	 * @param lineString
	 *            The LineString to process.
	 * @param writer
	 *            The output stream writer to Append to.
	 */
	private static void appendLineStringTaggedText(Line lineString,
			StringWriter writer) {
		writer.write("LINESTRING");
		appendLineStringText(lineString, writer);
	}

	/**
	 * Converts a Polygon to &lt;Polygon Tagged Text&gt; format, then Appends it
	 * to the writer.
	 * 
	 * @param polygon
	 *            The Polygon to process.
	 * @param writer
	 *            The stream writer to Append to.
	 */
	private static void appendPolygonTaggedText(Polygon polygon,
			StringWriter writer) {
		writer.write("POLYGON");
		appendPolygonText(polygon, writer);
	}

	/**
	 * Converts a MultiPoint to &lt;MultiPoint Tagged Text&gt; format, then
	 * Appends it to the writer.
	 * 
	 * @param multipoint
	 *            The MultiPoint to process.
	 * @param writer
	 *            The output writer to Append to.
	 */
	private static void appendMultiPointTaggedText(MultiPoint multipoint,
			StringWriter writer) {
		writer.write("MULTIPOINT");
		appendMultiPointText(multipoint, writer);
	}

	/**
	 * Converts a MultiLineString to &lt;MultiLineString Tagged Text&gt; format,
	 * then Appends it to the writer.
	 * 
	 * @param multiLineString
	 *            The MultiLineString to process
	 * @param writer
	 *            The output stream writer to Append to.
	 */
	private static void appendMultiLineStringTaggedText(
			MultiLine multiLineString, StringWriter writer) {
		writer.write("MULTILINESTRING");
		appendMultiLineStringText(multiLineString, writer);
	}

	/**
	 * Converts a MultiPolygon to &lt;MultiPolygon Tagged Text&gt; format, then
	 * Appends it to the writer.
	 * 
	 * @param multiPolygon
	 *            The MultiPolygon to process
	 * @param writer
	 *            The output stream writer to Append to.
	 */
	private static void appendMultiPolygonTaggedText(MultiPolygon multiPolygon,
			StringWriter writer) {

		writer.write("MULTIPOLYGON");
		appendMultiPolygonText(multiPolygon, writer);

	}

	/**
	 * Converts a GeometryCollection to &lt;GeometryCollection Tagged Text&gt;
	 * format, then Appends it to the writer.
	 * 
	 * @param geometryCollection
	 *            The GeometryCollection to process
	 * @param writer
	 *            The output stream writer to Append to.
	 */
	private static void appendGeometryCollectionTaggedText(
			GeometryCollection geometryCollection, StringWriter writer) {
		writer.write("GEOMETRYCOLLECTION");
		appendGeometryCollectionText(geometryCollection, writer);
	}

	/**
	 * Converts a Point to Point Text format then Appends it to the writer.
	 * 
	 * @param coordinate
	 *            The Point to process.
	 * @param writer
	 *            The output stream writer to Append to.
	 */
	private static void appendPointText(Point coordinate, StringWriter writer) {
		if (coordinate == null)
			writer.write("EMPTY");
		else {
			writer.write("(");
			appendCoordinate(coordinate, writer);
			writer.write(")");
		}
	}

	/**
	 * Converts a Point to &lt;Point&gt; format, then Appends it to the writer.
	 * 
	 * @param coordinate
	 *            The Point to process.
	 * @param writer
	 *            The output writer to Append to.
	 */
	private static void appendCoordinate(Point coordinate, StringWriter writer) {
		if (coordinate.getDimensions() == 2) {
			// writer.write(String.format("%s %s", WriteNumber(coordinate.X),
			// WriteNumber(coordinate.Y)));
			writer.write(WriteNumber(coordinate.X));
			writer.write(" ");
			writer.write(WriteNumber(coordinate.Y));
		} else if (coordinate.getDimensions() == 3) {
			// writer.write(String.format("%s %s %s", WriteNumber(coordinate.X),
			// WriteNumber(coordinate.Y), WriteNumber(coordinate.Z)));
			writer.write(WriteNumber(coordinate.X));
			writer.write(" ");
			writer.write(WriteNumber(coordinate.Y));
			writer.write(" ");
			writer.write(WriteNumber(coordinate.Z));
		}
	}

	private static void appendCoordinate(double[] coordinate,
			StringWriter writer) {
		if (coordinate.length == 2) {
			writer.write(WriteNumber(coordinate[0]));
			writer.write(" ");
			writer.write(WriteNumber(coordinate[1]));
		} else if (coordinate.length == 3) {
			writer.write(WriteNumber(coordinate[0]));
			writer.write(" ");
			writer.write(WriteNumber(coordinate[1]));
			writer.write(" ");
			writer.write(WriteNumber(coordinate[2]));
		}
	}

	/**
	 * Converts a double to a string, not in scientific notation.
	 * 
	 * @param d
	 *            The double to convert.
	 * @return The double as a string, not in scientific notation.
	 */
	private static String WriteNumber(double d) {
		return d + "";
	}

	/**
	 * Converts a LineString to &lt;LineString Text&gt; format, then Appends it
	 * to the writer.
	 * 
	 * @param lineString
	 *            The LineString to process.
	 * @param writer
	 *            The output stream to Append to.
	 */
	private static void appendLineStringText(Line lineString,
			StringWriter writer) {

		if (lineString == null || lineString.getPartCount() == 0)
			writer.write("EMPTY");
		else {
			writer.write("(");
			// List<Point> pts = lineString.getListPoints();
			// for (int i = 0; i < pts.size(); i++) {
			// if (i > 0)
			// writer.write(",");
			// appendCoordinate(pts.get(i), writer);
			// }
			List<double[]> pts = lineString.getListPoints();
			for (int i = 0; i < pts.size(); i++) {
				if (i > 0)
					writer.write(",");
				appendCoordinate(pts.get(i), writer);
			}
			writer.write(")");
		}
	}

	/**
	 * Converts a Polygon to &lt;Polygon Text&gt; format, then Appends it to the
	 * writer.
	 * 
	 * @param polygon
	 *            The Polygon to process.
	 * @param writer
	 */
	private static void appendPolygonText(Polygon polygon, StringWriter writer) {
		if (polygon == null || polygon.getPartCount() == 0)
			writer.write("EMPTY");
		else {
			writer.write("(");
			appendLineStringText(polygon.getExteriorRing(), writer);
			for (int i = 0; i < polygon.getInteriorRings().size(); i++) {
				writer.write(", ");
				appendLineStringText(polygon.getInteriorRings().get(i), writer);
			}
			writer.write(")");
		}
	}

	/**
	 * Converts a MultiPoint to &lt;MultiPoint Text&gt; format, then Appends it
	 * to the writer.
	 * 
	 * @param multiPoint
	 *            The MultiPoint to process.
	 * @param writer
	 *            The output stream writer to Append to.
	 */
	private static void appendMultiPointText(MultiPoint multiPoint,
			StringWriter writer) {

		if (multiPoint == null || multiPoint.getPartCount() == 0)
			writer.write("EMPTY");
		else {
			writer.write("(");
			for (int i = 0; i < multiPoint.getListPoints().size(); i++) {
				if (i > 0)
					writer.write(", ");
				appendCoordinate(multiPoint.getListPoints().get(i), writer);
			}
			writer.write(")");
		}
	}

	/**
	 * Converts a MultiLineString to &lt;MultiLineString Text&gt; format, then
	 * Appends it to the writer.
	 * 
	 * @param multiLineString
	 *            The MultiLineString to process.
	 * @param writer
	 *            The output stream writer to Append to.
	 */
	private static void appendMultiLineStringText(MultiLine multiLineString,
			StringWriter writer) {

		if (multiLineString == null || multiLineString.getPartCount() == 0)
			writer.write("EMPTY");
		else {
			writer.write("(");
			for (int i = 0; i < multiLineString.GetListLines().size(); i++) {
				if (i > 0)
					writer.write(", ");
				appendLineStringText(multiLineString.GetListLines().get(i),
						writer);
			}
			writer.write(")");
		}
	}

	/**
	 * Converts a MultiPolygon to &lt;MultiPolygon Text&gt; format, then Appends
	 * to it to the writer.
	 * 
	 * @param multiPolygon
	 *            The MultiPolygon to process.
	 * @param writer
	 *            The output stream to Append to.
	 */
	private static void appendMultiPolygonText(MultiPolygon multiPolygon,
			StringWriter writer) {

		if (multiPolygon == null || multiPolygon.GetPartCount() == 0)
			writer.write("EMPTY");
		else {
			writer.write("(");
			for (int i = 0; i < multiPolygon.GetPartCount(); i++) {
				if (i > 0)
					writer.write(", ");
				appendPolygonText(multiPolygon.GetPolygons().get(i), writer);
			}
			writer.write(")");
		}
	}

	/**
	 * Converts a GeometryCollection to &lt;GeometryCollection Text &gt; format,
	 * then Appends it to the writer.
	 * 
	 * @param geometryCollection
	 *            The GeometryCollection to process.
	 * @param writer
	 *            The output stream writer to Append to.
	 */
	private static void appendGeometryCollectionText(
			GeometryCollection geometryCollection, StringWriter writer) {
		if (geometryCollection == null
				|| geometryCollection.getPartCount() == 0)
			writer.write("EMPTY");
		else {
			writer.write("(");
			for (int i = 0; i < geometryCollection.getGeometries().size(); i++) {
				if (i > 0)
					writer.write(", ");
				appendGeometryTaggedText(geometryCollection.getGeometries()
						.get(i), writer);
			}
			writer.write(")");
		}
	}
}
