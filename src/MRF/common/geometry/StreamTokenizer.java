package MRF.common.geometry;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

enum TokenType {
	Word, Number, Eol, Eof, Whitespace, Symbol
}

public class StreamTokenizer {
	TokenType _currentTokenType;
	StringReader _reader;
	String _currentToken;
	boolean _ignoreWhitespace = false;
	int _lineNumber = 1;
	int _colNumber = 1;
	int b = -1;
	boolean isReader = false;

	public StreamTokenizer(StringReader reader, boolean ignoreWhitespace) {
		if (reader == null) {
			throw new NullPointerException("reader");
		}
		_reader = reader;
		_ignoreWhitespace = ignoreWhitespace;
	}

	public int get_lineNumber() {
		return _lineNumber;
	}

	public void set_lineNumber(int _lineNumber) {
		this._lineNumber = _lineNumber;
	}

	public int get_colNumber() {
		return _colNumber;
	}

	public void set_colNumber(int _colNumber) {
		this._colNumber = _colNumber;
	}

	public double getNumericValue() throws Exception {
		String number = this.getStringValue();
		if (this.getTokenType() == TokenType.Number) {
			return Double.parseDouble(number);
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append("The token '");
		buffer.append(number);
		buffer.append("' is not a number at line ");
		buffer.append(this.get_lineNumber());
		buffer.append(" column ");
		buffer.append(this.get_colNumber());
		buffer.append(".");
		throw new Exception(buffer.toString());
	}

	public String getStringValue() {
		return _currentToken;
	}

	public TokenType getTokenType() {
		return _currentTokenType;
	}

	public TokenType nextToken(boolean ignoreWhitespace) {
		TokenType nextTokenType;
		if (ignoreWhitespace) {
			nextTokenType = nextNonWhitespaceToken();
		} else {
			nextTokenType = nextTokenAny();
		}
		return nextTokenType;
	}

	public TokenType nextToken() {
		return nextToken(_ignoreWhitespace);
	}

	private TokenType nextTokenAny() {
		TokenType nextTokenType = TokenType.Eof;
		char[] chars = new char[1];
		_currentToken = "";
		_currentTokenType = TokenType.Eof;
		int finished = 0;
		try {
			// TODO
			if (!isReader) {
				finished = _reader.read(chars, 0, 1);
				isReader = false;
			} else {
				finished = 1;
				chars[0] = (char) b;
				isReader = true;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean isException = false;
		boolean isNumber = false;
		boolean isWord = false;
		byte[] ba = null;
		char[] ascii = null;
		char currentCharacter;
		char nextCharacter;
		while (finished != 0) {
			// convert int to char
			try {
				// TODO ba = new byte[] { (byte) _reader.read() };
				b = _reader.read();
				ba = new byte[] { (byte) b };
			} catch (IOException e1) {
				ba = new byte[] {};
				isException = true;
			}

			try {
				ascii = new String(ba, "ASCII").toCharArray();
			} catch (UnsupportedEncodingException e1) {
				ascii = new char[] {};
			}

			currentCharacter = chars[0];
			nextCharacter = ascii[0];
			_currentTokenType = getType(currentCharacter);
			nextTokenType = getType(nextCharacter);

			// handling of words with _
			if (isWord && currentCharacter == '_') {
				_currentTokenType = TokenType.Word;
			}
			// handing of words ending in numbers
			if (isWord && _currentTokenType.equals(TokenType.Number)) {
				_currentTokenType = TokenType.Word;
			}

			if (_currentTokenType.equals(TokenType.Word)
					&& nextCharacter == '_') {
				// enable words with _ inbetween
				nextTokenType = TokenType.Word;
				isWord = true;
			}
			if (_currentTokenType.equals(TokenType.Word)
					&& nextTokenType.equals(TokenType.Number)) {
				// enable words ending with numbers
				nextTokenType = TokenType.Word;
				isWord = true;
			}

			// handle negative numbers
			if (currentCharacter == '-'
					&& nextTokenType.equals(TokenType.Number)
					&& isNumber == false) {
				_currentTokenType = TokenType.Number;
				nextTokenType = TokenType.Number;
				// isNumber = true;
			}

			// this handles numbers with a decimal point
			if (isNumber && nextTokenType.equals(TokenType.Number)
					&& currentCharacter == '.') {
				_currentTokenType = TokenType.Number;
			}
			if (_currentTokenType.equals(TokenType.Number)
					&& nextCharacter == '.' && isNumber == false) {
				nextTokenType = TokenType.Number;
				isNumber = true;
			}
			_colNumber++;
			if (_currentTokenType.equals(TokenType.Eol)) {
				_lineNumber++;
				_colNumber = 1;
			}

			_currentToken = _currentToken + currentCharacter;
			// if (_currentTokenType==TokenType.Word && nextCharacter=='_')
			// {
			// enable words with _ inbetween
			// finished = _reader.Read(chars,0,1);
			// }
			if (!_currentTokenType.equals(nextTokenType)) {
				finished = 0;
			} else if (_currentTokenType.equals(TokenType.Symbol)
					&& currentCharacter != '-') {
				finished = 0;
			} else {
				// TODO finished = _reader.read(chars, 0, 1);
				if (!isException) {
					finished = 1;
					chars[0] = (char) b;
					isReader = true;
				}
			}
		}
		return _currentTokenType;
	}

	// / <summary>
	// / Determines a characters type (e.g. number, symbols, character).
	// / </summary>
	// / <param name="character">The character to determine.</param>
	// / <returns>The TokenType the character is.</returns>
	private TokenType getType(char character) {
		if (Character.isDigit(character)) {
			return TokenType.Number;
		} else if (Character.isLetter(character)) {
			return TokenType.Word;
		} else if (character == '\n') {
			return TokenType.Eol;
		} else if (Character.isWhitespace(character)
				|| Character.isISOControl(character)) {
			return TokenType.Whitespace;
		} else // (Char.IsSymbol(character))
		{
			return TokenType.Symbol;
		}

	}

	// / <summary>
	// / Returns next token that is not whitespace.
	// / </summary>
	// / <returns></returns>
	private TokenType nextNonWhitespaceToken() {

		TokenType tokentype = this.nextTokenAny();
		while (tokentype.equals(TokenType.Whitespace)
				|| tokentype.equals(TokenType.Eol)) {
			tokentype = this.nextTokenAny();
		}

		return tokentype;
	}

}
