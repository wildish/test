package MRF.common.geometry;

public enum Location {
	Interior,
    Boundary,
    Exterior,
    Null
}
