package MRF.common.geometry;

public class Point extends Geometry implements Comparable<Point> {

	public double X;
	public double Y;
	public double Z;

	public Envelope getEnv() {
		Envelope env = super.getEnv();
		env.minX = X;
		env.maxX = X + 0.0000001;
		env.minY = Y;
		env.maxY = Y + 0.0000001;
		return env;
	}

	public Point() {
		this.setGeometryType(WKBGeometryType.WKBPOINT);
		this.setDimensions(2);
	}

	public Point(Point p) {
		this.setGeometryType(WKBGeometryType.WKBPOINT);
		this.setDimensions(p.getDimensions());
		X = p.X;
		Y = p.Y;
		if (this.getDimensions() == 3) {
			this.Z = p.Z;
		}
	}

	public Point(double[] p) {
		if (p.length == 2) {
			this.setGeometryType(WKBGeometryType.WKBPOINT);
			this.setDimensions(2);
			this.X = p[0];
			this.Y = p[1];
		} else {
			this.setGeometryType(WKBGeometryType.WKBPOINT);
			this.setDimensions(3);
			this.X = p[0];
			this.Y = p[1];
			this.Z = p[2];
		}
	}

	public Point(double dimX, double dimY) {
		this.setGeometryType(WKBGeometryType.WKBPOINT);
		this.setDimensions(2);
		this.X = dimX;
		this.Y = dimY;
	}

	public Point(double dimX, double dimY, double dimZ) {
		this.setGeometryType(WKBGeometryType.WKBPOINT);
		this.setDimensions(3);
		this.X = dimX;
		this.Y = dimY;
		this.Z = dimZ;
	}

	public boolean isEqual(Point otherPoint) {
		if (otherPoint == null || !(otherPoint instanceof Point)) {
			return false;
		} else if (this.getDimensions() != otherPoint.getDimensions()) {
			return false;
		} else {
			if (this.getDimensions() == 2) {
				return this.X == otherPoint.X && this.Y == otherPoint.Y;
			} else {
				return this.X == otherPoint.X && this.Y == otherPoint.Y
						&& this.Z == otherPoint.Z;
			}
		}
	}

	// public void copyTo(Point otherPoint) {
	// try {
	// if (otherPoint == null) {
	// throw new Exception(
	// "WKBParser Point [copyTo] Parameter Point is null");
	// } else {
	// otherPoint.setGeometryType(this.getGeometryType());
	// otherPoint.setDimensions(this.getDimensions());
	// otherPoint.X = this.X;
	// otherPoint.Y = this.Y;
	// if (this.getDimensions() == 3) {
	// otherPoint.Z = this.Z;
	// }
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	@Override
	public Geometry clone() {
		Point pt = new Point();
		pt.setGeometryType(this.getGeometryType());
		pt.setDimensions(this.getDimensions());
		pt.X = this.X;
		pt.Y = this.Y;
		if (this.getDimensions() == 3) {
			pt.Z = this.Z;
		}
		return pt;
	}

	@Override
	public String toWKT() {
		return GeometryToWKT.write(this);
	}

	@Override
	public byte[] toWKB() {
		return GeometryToWKB.write(this);
	}

	public double distance(Point point) {
		return Math.sqrt(Math.pow(X - point.X, 2) + Math.pow(Y - point.Y, 2));
	}
	
	public double distance(double[] point) {
		return Math.sqrt(Math.pow(X - point[0], 2) + Math.pow(Y - point[1], 2));
	}

	@Override
	public int compareTo(Point other) {
		if (X < other.X)
			return -1;
		if (X > other.X)
			return 1;
		if (Y < other.Y)
			return -1;
		return Y > other.Y ? 1 : 0;
	}
}
