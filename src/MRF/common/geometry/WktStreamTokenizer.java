package MRF.common.geometry;

import java.io.StringReader;

public class WktStreamTokenizer extends StreamTokenizer {
	public WktStreamTokenizer(StringReader reader) {
		super(reader, true);
		if (reader == null) {
			throw new NullPointerException("reader");
		}
	}

	private void readToken(String expectedToken) throws Exception {
		this.nextToken();
		if (!expectedToken.equals(this.getStringValue())) {
			StringBuffer buffer = new StringBuffer();
			buffer.append("Expecting ('");
			buffer.append(expectedToken);
			buffer.append("') but got a '");
			buffer.append(this.getStringValue());
			buffer.append("' at line ");
			buffer.append(this.get_lineNumber());
			buffer.append(" column ");
			buffer.append(this.get_colNumber());
			buffer.append(".");
			throw new Exception(buffer.toString());
		}
	}

	public String readDoubleQuotedWord() {
		String word = "";
		try {
			readToken("\"");
		} catch (Exception e) {
			e.printStackTrace();
		}
		nextToken(false);
		while (!"\"".equals(getStringValue())) {
			word = word + this.getStringValue();
			nextToken(false);
		}
		return word;
	}

	public void readAuthority(String authority, long authorityCode) {
		// AUTHORITY["EPGS","9102"]]

		try {
			if (!"AUTHORITY".equals(getStringValue())) {
				readToken("AUTHORITY");
			}
			readToken("[");
			authority = this.readDoubleQuotedWord();
			readToken(",");
			authorityCode = Long.parseLong(readDoubleQuotedWord());
			readToken("]");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
