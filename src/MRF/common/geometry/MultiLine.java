package MRF.common.geometry;

import java.util.ArrayList;
import java.util.List;

public class MultiLine extends Geometry{
	private List<Line> listLines;

    public MultiLine()
    {
        this.setGeometryType(WKBGeometryType.WKBMULTILINESTRING);
        this.setDimensions(0);
        listLines = new ArrayList<Line>();
    }

    public MultiLine(List<Line> lines)
    {
        this.setGeometryType(WKBGeometryType.WKBMULTILINESTRING);
        listLines = new ArrayList<Line>();
        setListLines(lines);
    }

    public void setListLines(List<Line> lines)
    {
        try {
			if (lines == null || lines.size() == 0)
			{
			    throw new Exception("WKBParser Multi Line Parameter points is null");
			}
			else
			{
			    int dimBackup = 0;
			    for (int i = 0; i < lines.size(); i++)
			    {
			        if (!(lines.get(i) instanceof Line))
			        {
			            throw new Exception(
			                    "WKBParser Multi Line Parameter lines have a class that is falset WKBLine");
			        }
			        else
			        {
			            if (i == 0)
			            {
			                dimBackup = lines.get(0).getDimensions();
			                this.getEnv().reset();
			                listLines.add(lines.get(0));
			                this.getEnv().expandEnv(lines.get(0).getEnv());
			            }
			            else if (dimBackup != lines.get(i).getDimensions())
			            {
			                throw new Exception(
			                        "WKBParser Multi Line Parameter lines have WKBLine with different dimensions");
			            }
			            else
			            {
			                listLines.add(lines.get(i));
			                this.getEnv().expandEnv(lines.get(i).getEnv());
			                // [listLines addObject:lines[i]];
			                // [this.env expandEnv:[(WKBLine *) lines[i] env]];
			            }
			        }
			    }
			    this.setDimensions(dimBackup);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    public void addLine(Line line)
    {
        listLines.add(line);
        this.getEnv().expandEnv(line.getEnv());
    }

    public List<Line> GetListLines()
    {
        return listLines;
    }

    public void removeListLines()
    {
        listLines.clear();
        this.setDimensions(0);
        this.getEnv().reset();
    }

    public boolean IsEqual(MultiLine otherLineM)
    {
        if (otherLineM == null || !(otherLineM instanceof MultiLine))
        {
            return false;
        }
        else if (this.getDimensions() != otherLineM.getDimensions())
        {
            return false;
        }
        else if (listLines.size() != otherLineM.GetListLines().size())
        {
            return false;
        }
        else
        {
            List<Line> listOtherLines = otherLineM.GetListLines();
            for (int i = 0; i < listLines.size(); i++)
            {
                if (!(listLines.get(i).isEqual(listOtherLines.get(i))))
                {
                    return false;
                }
            }
            return true;
        }
    }

    public void CopyTo(MultiLine otherLineM)
    {
        try {
			if (otherLineM == null)
			{
			    throw new Exception(
			            "WKBParser Multi Line [copyTo] Parameter Multi Line is null");
			}
			else
			{
			    otherLineM.setGeometryType(this.getGeometryType());
			    otherLineM.setDimensions(this.getDimensions());
			    otherLineM.removeListLines();
			    otherLineM.setListLines(listLines);
			    otherLineM.getEnv().reset();
			    otherLineM.getEnv().expandEnv(this.getEnv());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    @Override
    public  Geometry clone()
    {
        MultiLine line = new MultiLine();
        line.setGeometryType(this.getGeometryType());
        line.setDimensions(this.getDimensions());
        line.removeListLines();
        line.setListLines(listLines);
        line.getEnv().reset();
        line.getEnv().expandEnv(this.getEnv());
        return line;
    }
    @Override
    public  String toWKT()
    {
        return GeometryToWKT.write(this);
    }
    @Override
    public  byte[] toWKB()
    {
        return GeometryToWKB.write(this);
    }

    public int getPartCount()
    {
        return listLines.size();
    }

    public Line getPartAtIndex(int index)
    {
        if (index > listLines.size())
        {
            return null;
        }
        else
        {
            return listLines.get(index);
        }
    }
}
