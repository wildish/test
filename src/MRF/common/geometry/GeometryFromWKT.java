package MRF.common.geometry;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;

public class GeometryFromWKT {

	public static Geometry Parse(String wellKnownText) {
		StringReader reader = new StringReader(wellKnownText);
		return parse(reader);
	}

	/**
	 * Converts a Well-known Text representation to a <see
	 * cref="SharpMap.Geometries.Geometry"/>.
	 * 
	 * @param reader
	 *            A Reader which will return a Geometry Tagged Text string (see
	 *            the OpenGIS Simple Features Specification)
	 * @return Returns a <see cref="SharpMap.Geometries.Geometry"/> read from
	 *         StreamReader. An exception will be thrown if there is a parsing
	 *         problem.
	 */
	public static Geometry parse(StringReader reader) {
		WktStreamTokenizer tokenizer;
		try {
			tokenizer = new WktStreamTokenizer(reader);
			return readGeometryTaggedText(tokenizer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Returns the next array of Coordinates in the stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next element returned by the stream should be "(" (the
	 *            beginning of "(x1 y1, x2 y2, ..., xn yn)" or "EMPTY".
	 * @return The next array of Coordinates in the stream, or an empty array of
	 *         "EMPTY" is the next element returned by the stream.
	 */
	// private static List<Point> getCoordinates(WktStreamTokenizer tokenizer)
	// throws Exception {
	// List<Point> coordinates = null;
	// coordinates = new ArrayList<Point>();
	// String nextToken = getNextEmptyOrOpener(tokenizer);
	// if (nextToken == "EMPTY")
	// return coordinates;
	//
	// coordinates.add(new Point(getNextNumber(tokenizer),
	// getNextNumber(tokenizer)));
	// nextToken = getNextCloserOrComma(tokenizer);
	// while (nextToken == ",") {
	// coordinates.add(new Point(getNextNumber(tokenizer),
	// getNextNumber(tokenizer)));
	// nextToken = getNextCloserOrComma(tokenizer);
	// }
	// return coordinates;
	// }
	private static List<double[]> getCoordinates(WktStreamTokenizer tokenizer)
			throws Exception {
		List<double[]> coordinates = null;
		coordinates = new ArrayList<double[]>();
		String nextToken = getNextEmptyOrOpener(tokenizer);
		if ("EMPTY".equals(nextToken)) {
			return coordinates;
		}
		coordinates.add(new double[] { getNextNumber(tokenizer),
				getNextNumber(tokenizer) });
		nextToken = getNextCloserOrComma(tokenizer);
		while (",".equals(nextToken)) {
			coordinates.add(new double[] { getNextNumber(tokenizer),
					getNextNumber(tokenizer) });
			nextToken = getNextCloserOrComma(tokenizer);
		}
		return coordinates;
	}

	/**
	 * Returns the next number in the stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known text format. The
	 *            next token must be a number.
	 * @return Returns the next number in the stream.
	 * @throws Exception
	 *             Exception is thrown if the next token is not a number.
	 */
	private static double getNextNumber(WktStreamTokenizer tokenizer)
			throws Exception {
		tokenizer.nextToken();
		return tokenizer.getNumericValue();
	}

	/**
	 * Returns the next "EMPTY" or "(" in the stream as uppercase text.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next token must be "EMPTY" or "(".
	 * @return the next "EMPTY" or "(" in the stream as uppercase text.
	 * @throws Exception
	 *             Exception is thrown if the next token is not "EMPTY" or "(".
	 */
	private static String getNextEmptyOrOpener(WktStreamTokenizer tokenizer)
			throws Exception {
		tokenizer.nextToken();
		String nextWord = tokenizer.getStringValue();
		if (nextWord.equals("EMPTY") || nextWord.equals("("))
			return nextWord;

		throw new Exception("Expected 'EMPTY' or '(' but encountered '"
				+ nextWord + "'");

	}

	/**
	 * Returns the next ")" or "," in the stream.
	 * 
	 * @param tokenizer
	 *            tokenizer over a stream of text in Well-known Text format. The
	 *            next token must be ")" or ",".
	 * @return Returns the next ")" or "," in the stream.
	 * @throws Exception
	 *             Exception is thrown if the next token is not ")" or ",".
	 */
	private static String getNextCloserOrComma(WktStreamTokenizer tokenizer)
			throws Exception {
		tokenizer.nextToken();
		String nextWord = tokenizer.getStringValue();
		if (",".equals(nextWord) || ")".equals(nextWord)) {
			return nextWord;
		}
		throw new Exception("Expected ')' or ',' but encountered '" + nextWord
				+ "'");
	}

	/**
	 * Returns the next ")" in the stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next token must be ")".
	 * @return Returns the next ")" in the stream.
	 * @throws Exception
	 *             Exception is thrown if the next token is not ")".
	 */
	@SuppressWarnings("unused")
	private static String getNextCloser(WktStreamTokenizer tokenizer)
			throws Exception {

		String nextWord = getNextWord(tokenizer);
		if (")".equals(nextWord)) {
			return nextWord;
		}
		throw new Exception("Expected ')' but encountered '" + nextWord + "'");
	}

	/**
	 * Returns the next word in the stream as uppercase text.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next token must be a word.
	 * @return Returns the next word in the stream as uppercase text.
	 * @throws Exception
	 *             Exception is thrown if the next token is not a word.
	 */
	@SuppressLint("DefaultLocale")
	private static String getNextWord(WktStreamTokenizer tokenizer)
			throws Exception {
		TokenType type = tokenizer.nextToken();
		String token = tokenizer.getStringValue();
		if (type == TokenType.Number) {
			throw new Exception("Expected a number but got " + token);
		} else if (type == TokenType.Word) {
			return token.toUpperCase();
		} else if ("(".equals(token)) {
			return "(";
		} else if (")".equals(token)) {
			return ")";
		} else if (",".equals(token)) {
			return ",";
		}
		throw new Exception("Not a valid symbol in WKT format.");
	}

	/**
	 * Creates a Geometry using the next token in the stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next tokens must form a &lt;Geometry Tagged Text&gt;.
	 * @return Returns a Geometry specified by the next token in the stream.
	 * @throws Exception
	 *             Exception is thrown if the coordinates used to create a
	 *             Polygon shell and holes do not form closed linestrings, or if
	 *             an unexpected token is encountered.
	 */
	@SuppressLint("DefaultLocale")
	private static Geometry readGeometryTaggedText(WktStreamTokenizer tokenizer)
			throws Exception {
		tokenizer.nextToken();
		String type = tokenizer.getStringValue().toUpperCase();
		Geometry geometry = null;
		if ("POINT".equalsIgnoreCase(type)) {
			geometry = readPointText(tokenizer);
		} else if ("LINESTRING".equalsIgnoreCase(type)) {
			geometry = readLineStringText(tokenizer);
		} else if ("MULTIPOINT".equalsIgnoreCase(type)) {
			geometry = readMultiPointText(tokenizer);
		} else if ("MULTILINESTRING".equalsIgnoreCase(type)) {
			geometry = readMultiLineStringText(tokenizer);
		} else if ("POLYGON".equalsIgnoreCase(type)) {
			geometry = readPolygonText(tokenizer);
		} else if ("MULTIPOLYGON".equalsIgnoreCase(type)) {
			geometry = readMultiPolygonText(tokenizer);
		} else if ("GEOMETRYCOLLECTION".equalsIgnoreCase(type)) {
			geometry = readMultiPolygonText(tokenizer);
		} else {
			StringBuffer buffer = new StringBuffer();
			buffer.append("Geometrytype '");
			buffer.append(type);
			buffer.append("' is not supported.");
			throw new Exception(buffer.toString());
		}
		/*
		 * switch (type) { case "POINT": geometry = readPointText(tokenizer);
		 * break; case "LINESTRING": geometry = readLineStringText(tokenizer);
		 * break; case "MULTIPOINT": geometry = readMultiPointText(tokenizer);
		 * break; case "MULTILINESTRING": geometry =
		 * readMultiLineStringText(tokenizer); break; case "POLYGON": geometry =
		 * readPolygonText(tokenizer); break; case "MULTIPOLYGON": geometry =
		 * readMultiPolygonText(tokenizer); break; case "GEOMETRYCOLLECTION":
		 * geometry = readGeometryCollectionText(tokenizer); break; default:
		 * throw new Exception(String.format(
		 * "Geometrytype '%s' is not supported.", type)); }
		 */
		return geometry;
	}

	/**
	 * Creates a <see cref="MultiPolygon"/> using the next token in the stream.
	 * 
	 * @param tokenizer
	 *            tokenizer over a stream of text in Well-known Text format. The
	 *            next tokens must form a MultiPolygon.
	 * @return a <code>MultiPolygon</code> specified by the next token in the
	 *         stream, or if if the coordinates used to create the <see
	 *         cref="Polygon"/> shells and holes do not form closed linestrings.
	 * @throws Exception
	 */
	private static MultiPolygon readMultiPolygonText(
			WktStreamTokenizer tokenizer) throws Exception {
		MultiPolygon polygons = new MultiPolygon();
		String nextToken = getNextEmptyOrOpener(tokenizer);
		if ("EMPTY".equals(nextToken)) {
			return polygons;
		}
		List<Polygon> listPolygon = new ArrayList<Polygon>();
		Polygon polygon = readPolygonText(tokenizer);
		listPolygon.add(polygon);
		nextToken = getNextCloserOrComma(tokenizer);
		while (",".equals(nextToken)) {
			polygon = readPolygonText(tokenizer);
			listPolygon.add(polygon);
			nextToken = getNextCloserOrComma(tokenizer);
		}
		polygons.SetPolygons(listPolygon);

		return polygons;
	}

	/**
	 * Creates a Polygon using the next token in the stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next tokens must form a &lt;Polygon Text&gt;.
	 * @return Returns a Polygon specified by the next token in the stream
	 * @throws Exception
	 *             Exception is thown if the coordinates used to create the
	 *             Polygon shell and holes do not form closed linestrings, or if
	 *             an unexpected token is encountered.
	 */
	private static Polygon readPolygonText(WktStreamTokenizer tokenizer)
			throws Exception {
		Polygon pol = new Polygon();
		String nextToken = getNextEmptyOrOpener(tokenizer);
		if ("EMPTY".equals(nextToken))
			return pol;

		pol.addRing(new Line(getCoordinates(tokenizer)));
		nextToken = getNextCloserOrComma(tokenizer);
		while (",".equals(nextToken)) {
			// Add holes
			pol.addRing(new Line(getCoordinates(tokenizer)));
			nextToken = getNextCloserOrComma(tokenizer);
		}
		return pol;

	}

	/**
	 * Creates a Point using the next token in the stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next tokens must form a &lt;Point Text&gt;.
	 * @return Returns a Point specified by the next token in the stream.
	 * @throws Exception
	 *             Exception is thrown if an unexpected token is encountered.
	 */
	private static Point readPointText(WktStreamTokenizer tokenizer)
			throws Exception {
		String nextToken = getNextEmptyOrOpener(tokenizer);
		if (nextToken.equals("EMPTY"))
			return new Point();
		return new Point(getNextNumber(tokenizer), getNextNumber(tokenizer));
	}

	/**
	 * Creates a Point using the next token in the stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next tokens must form a &lt;Point Text&gt;.
	 * @return Returns a Point specified by the next token in the stream.
	 * @throws Exception
	 *             Exception is thrown if an unexpected token is encountered.
	 */
	private static MultiPoint readMultiPointText(WktStreamTokenizer tokenizer)
			throws Exception {
		MultiPoint mp = new MultiPoint();
		String nextToken = getNextEmptyOrOpener(tokenizer);
		if ("EMPTY".equals(nextToken))
			return mp;

		List<Point> listPoints = new ArrayList<Point>();
		listPoints.add(new Point(getNextNumber(tokenizer),
				getNextNumber(tokenizer)));
		nextToken = getNextCloserOrComma(tokenizer);
		while (",".equals(nextToken)) {
			listPoints.add(new Point(getNextNumber(tokenizer),
					getNextNumber(tokenizer)));
			nextToken = getNextCloserOrComma(tokenizer);
		}
		mp.setListPoints(listPoints);
		return mp;
	}

	/**
	 * Creates a <see cref="MultiLineString"/> using the next token in the
	 * stream.
	 * 
	 * @param tokenizer
	 *            tokenizer over a stream of text in Well-known Text format. The
	 *            next tokens must form a MultiLineString Text
	 * @return a <see cref="MultiLineString"/> specified by the next token in
	 *         the stream
	 * @throws Exception
	 */
	private static MultiLine readMultiLineStringText(
			WktStreamTokenizer tokenizer) throws Exception {
		MultiLine lines = new MultiLine();
		String nextToken = getNextEmptyOrOpener(tokenizer);
		if ("EMPTY".equals(nextToken))
			return lines;

		List<Line> listlines = new ArrayList<Line>();
		listlines.add(readLineStringText(tokenizer));
		nextToken = getNextCloserOrComma(tokenizer);
		while (",".equals(nextToken)) {
			listlines.add(readLineStringText(tokenizer));
			nextToken = getNextCloserOrComma(tokenizer);
		}
		lines.setListLines(listlines);
		return lines;
	}

	/**
	 * Creates a LineString using the next token in the stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next tokens must form a LineString Text.
	 * @return Returns a LineString specified by the next token in the stream.
	 * @throws Exception
	 *             Exception is thrown if an unexpected token is encountered.
	 */
	private static Line readLineStringText(WktStreamTokenizer tokenizer)
			throws Exception {
		return new Line(getCoordinates(tokenizer));
	}

	/**
	 * Creates a <see cref="GeometryCollection"/> using the next token in the
	 * stream.
	 * 
	 * @param tokenizer
	 *            Tokenizer over a stream of text in Well-known Text format. The
	 *            next tokens must form a GeometryCollection Text.
	 * @return A <see cref="GeometryCollection"/> specified by the next token in
	 *         the stream.
	 * @throws Exception
	 */
	// private static GeometryCollection readGeometryCollectionText(
	// WktStreamTokenizer tokenizer) throws Exception {
	// GeometryCollection geometries = null;
	// geometries = new GeometryCollection();
	// String nextToken = getNextEmptyOrOpener(tokenizer);
	// if (nextToken.equals("EMPTY"))
	// return geometries;
	// geometries.addGeometry(readGeometryTaggedText(tokenizer));
	// nextToken = getNextCloserOrComma(tokenizer);
	// while (nextToken.equals(",")) {
	// geometries.addGeometry(readGeometryTaggedText(tokenizer));
	// nextToken = getNextCloserOrComma(tokenizer);
	// }
	// return geometries;
	// }
}
