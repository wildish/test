package MRF.common.geometry;

import java.util.ArrayList;
import java.util.List;

public class MultiPoint extends Geometry {
	private List<Point> listPoints;

	public MultiPoint() {
		this.setGeometryType(WKBGeometryType.WKBMULTIPOINT);
		this.setDimensions(0);
		listPoints = new ArrayList<Point>();
	}

	public MultiPoint(List<Point> points) {
		this.setGeometryType(WKBGeometryType.WKBMULTIPOINT);
		listPoints = new ArrayList<Point>();
		setListPoints(points);
	}

	public void setListPoints(List<Point> points) {
		try {
			if (points == null || points.size() == 0) {
				throw new Exception("WKBParser Line Parameter points is null");
			} else {
				int dimBackup = 0;
				for (int i = 0; i < points.size(); i++) {
					if (!(points.get(i) instanceof Point)) {
						throw new Exception(
								"WKBParser  Multi Point Parameter points have a class that is not WKBPoint");
					} else {
						if (i == 0) {
							dimBackup = points.get(0).getDimensions();
							listPoints.add(points.get(0));
							this.getEnv().expand(points.get(0).X,
									points.get(0).Y);
						} else if (dimBackup != points.get(i).getDimensions()) {
							throw new Exception(
									"WKBParser  Multi Point Parameter points have WKBPoint with different dimensions");
						} else {
							listPoints.add(points.get(i));
							// this.env expand:[(WKBPoint *) points[i]
							// dimensionX]
							// y:[(WKBPoint *) points[i] dimensionY]];
							this.getEnv().expand(points.get(i).X,
									points.get(i).Y);
						}

					}
				}
				this.setDimensions(dimBackup);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Point> getListPoints() {
		return listPoints;
	}

	public void removeListPoints() {
		listPoints.clear();
		this.getEnv().reset();
		this.setDimensions(0);
	}

	public boolean isEqual(MultiPoint otherPointM) {
		if (otherPointM == null || !(otherPointM instanceof MultiPoint)) {
			return false;
		} else if (this.getDimensions() != otherPointM.getDimensions()) {
			return false;
		} else if (listPoints.size() != otherPointM.getListPoints().size()) {
			return false;
		} else {
			List<Point> listOtherPoints = otherPointM.getListPoints();
			for (int i = 0; i < listPoints.size(); i++) {
				if (!(listPoints.get(i).isEqual(listOtherPoints.get(i)))) {
					return false;
				}
			}
			return true;
		}
	}

	public void copyTo(MultiPoint otherPointM) {
		try {
			if (otherPointM == null) {
				throw new Exception(
						"WKBParser Multi Point [copyTo] Parameter Line is nil");
			} else {
				otherPointM.setGeometryType(this.getGeometryType());
				otherPointM.setDimensions(this.getDimensions());
				otherPointM.removeListPoints();
				otherPointM.setListPoints(listPoints);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Geometry clone() {
		MultiPoint points = new MultiPoint();
		points.setGeometryType(this.getGeometryType());
		points.setDimensions(this.getDimensions());
		points.removeListPoints();
		points.setListPoints(listPoints);
		return points;
	}

	@Override
	public String toWKT() {
		return GeometryToWKT.write(this);
	}

	@Override
	public byte[] toWKB() {
		return GeometryToWKB.write(this);
	}

	public int getPartCount() {
		return listPoints.size();
	}

	public Point getPartAtIndex(int index) {
		if (index >= listPoints.size()) {
			return null;
		} else {
			return listPoints.get(index);
		}
	}
}
