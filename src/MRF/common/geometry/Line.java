package MRF.common.geometry;

import java.util.ArrayList;
import java.util.List;

import MRF.common.Util.MRFUtil;

public class Line extends Geometry {

	private List<double[]> listPoints;
	private double length = 0;
	private boolean isNeedComputeLength = true;

	public Line() {
		this.setGeometryType(WKBGeometryType.WKBLINESTRING);
		this.setDimensions(0);
		listPoints = new ArrayList<double[]>();
		isNeedComputeLength = true;
	}

	public Line(List<double[]> points) {
		this.setGeometryType(WKBGeometryType.WKBLINESTRING);
		listPoints = new ArrayList<double[]>();
		setListPoints(points);
		isNeedComputeLength = true;
	}

	public void setListPoints(List<double[]> points) {
		try {
			if (points == null || points.size() == 0) {
				throw new Exception("Parser Line Parameter points is null");
			} else {
				int dimBackup = 0;
				Envelope env = this.getEnv();
				int size = points.size();
				for (int i = 0; i < size; i++) {
					double[] p = points.get(i);
					// if (!(p instanceof Point)) {
					// throw new Exception(
					// "WKBParser Line Parameter points have a class that is not Point");
					// } else {
					if (i == 0) {
						dimBackup = p.length;
						env.reset();
						listPoints.add(p);
						env.expand(p[0], p[1]);
					} else if (dimBackup != p.length) {
						throw new Exception(
								"Parser Line Parameter points have Point with different dimensions");
					} else {
						listPoints.add(p);
						env.expand(p[0], p[1]);
					}

					// }
				}
				this.setDimensions(dimBackup);
			}
			isNeedComputeLength = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addPoint(double[] pt) {
		if (this.getDimensions() == 0)
			this.setDimensions(pt.length);
		if (listPoints.size() < 1) {
			listPoints.add(pt);
		} else if (listPoints.size() > 0
				&& MRFUtil.distance(pt, listPoints.get(listPoints.size() - 1)) > 1e-7) {
			listPoints.add(pt);
		}
		isNeedComputeLength = true;
	}

	public List<double[]> getListPoints() {
		return listPoints;
	}

	public void removeListPoints() {
		listPoints.clear();
		this.getEnv().reset();
		this.setDimensions(0);
		isNeedComputeLength = true;
	}

	public boolean isEqual(Line otherLine) {
		if (otherLine == null || !(otherLine instanceof Line)) {
			return false;
		} else if (this.getDimensions() != otherLine.getDimensions()) {
			return false;
		} else if (listPoints.size() != otherLine.getListPoints().size()) {
			return false;
		} else {
			List<double[]> listOtherPoints = otherLine.getListPoints();
			for (int i = 0; i < listPoints.size(); i++) {
				if (!MRFUtil.isEqual(listPoints.get(i), listOtherPoints.get(i))) {
					return false;
				}
			}
			return true;
		}
	}

	public void copyTo(Line otherLine) {
		try {
			if (otherLine == null) {
				throw new Exception(
						"WKBParser Line [copyTo] Parameter Line is nil");
			} else {
				otherLine.setGeometryType(this.getGeometryType());
				otherLine.setDimensions(this.getDimensions());
				otherLine.removeListPoints();
				otherLine.setListPoints(listPoints);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Geometry clone() {
		Line line = new Line();
		line.setGeometryType(this.getGeometryType());
		line.setDimensions(this.getDimensions());
		line.removeListPoints();
		line.setListPoints(listPoints);
		return line;
	}

	@Override
	public String toWKT() {
		return GeometryToWKT.write(this);
	}

	@Override
	public byte[] toWKB() {
		return GeometryToWKB.write(this);
	}

	public int getPartCount() {
		return listPoints.size();
	}

	public double[] getPointAtIndex(int index) {
		if (index >= listPoints.size()) {
			return null;
		} else {
			double[] p = listPoints.get(index);
			if (p.length == 2) {
				return new double[] { p[0], p[1] };
			} else if (p.length == 3) {
				return new double[] { p[0], p[1], p[2] };
			} else {
				return null;
			}
		}
	}

	public void addPoints(List<double[]> list) {
		if (this.getDimensions() == 0)
			this.setDimensions(list.get(0).length);
		this.getListPoints().addAll(list);
		isNeedComputeLength = true;
	}

	public double getLength() {
		if (isNeedComputeLength) {
			double len = 0;
			for (int i = 1; i < listPoints.size(); i++) {
				len += MRFUtil.distance(listPoints.get(i),
						listPoints.get(i - 1));
			}
			this.length = len;
			isNeedComputeLength = false;
		}

		return this.length;
	}

	// private List<Point> listPoints;
	// private double length = 0;
	// private boolean isNeedComputeLength = true;
	//
	// public Line() {
	// this.setGeometryType(WKBGeometryType.WKBLINESTRING);
	// this.setDimensions(0);
	// listPoints = new ArrayList<Point>();
	// isNeedComputeLength = true;
	// }
	//
	// public Line(List<Point> points) {
	// this.setGeometryType(WKBGeometryType.WKBLINESTRING);
	// listPoints = new ArrayList<Point>();
	// setListPoints(points);
	// isNeedComputeLength = true;
	// }
	//
	// public void setListPoints(List<Point> points) {
	// try {
	// if (points == null || points.size() == 0) {
	// throw new Exception("Parser Line Parameter points is null");
	// } else {
	// int dimBackup = 0;
	// Envelope env = this.getEnv();
	// int size = points.size();
	// for (int i = 0; i < size; i++) {
	// Point p = points.get(i);
	// if (!(p instanceof Point)) {
	// throw new Exception(
	// "WKBParser Line Parameter points have a class that is not Point");
	// } else {
	// if (i == 0) {
	// dimBackup = p.getDimensions();
	// env.reset();
	// listPoints.add(p);
	// env.expand(p.X, p.Y);
	// } else if (dimBackup != p.getDimensions()) {
	// throw new Exception(
	// "Parser Line Parameter points have Point with different dimensions");
	// } else {
	// listPoints.add(p);
	// env.expand(p.X, p.Y);
	// }
	//
	// }
	// }
	// this.setDimensions(dimBackup);
	// }
	// isNeedComputeLength = true;
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// public void addPoint(Point pt) {
	// if (this.getDimensions() == 0)
	// this.setDimensions(pt.getDimensions());
	// if (listPoints.size() < 1) {
	// listPoints.add(pt);
	// } else if (listPoints.size() > 0
	// && pt.distance(listPoints.get(listPoints.size() - 1)) > 1e-7) {
	// listPoints.add(pt);
	// }
	// isNeedComputeLength = true;
	// }
	//
	// public List<Point> getListPoints() {
	// return listPoints;
	// }
	//
	// public void removeListPoints() {
	// listPoints.clear();
	// this.getEnv().reset();
	// this.setDimensions(0);
	// isNeedComputeLength = true;
	// }
	//
	// public boolean isEqual(Line otherLine) {
	// if (otherLine == null || !(otherLine instanceof Line)) {
	// return false;
	// } else if (this.getDimensions() != otherLine.getDimensions()) {
	// return false;
	// } else if (listPoints.size() != otherLine.getListPoints().size()) {
	// return false;
	// } else {
	// List<Point> listOtherPoints = otherLine.getListPoints();
	// for (int i = 0; i < listPoints.size(); i++) {
	// if (!(listPoints.get(i).isEqual(listOtherPoints.get(i)))) {
	// return false;
	// }
	// }
	// return true;
	// }
	// }
	//
	// public void copyTo(Line otherLine) {
	// try {
	// if (otherLine == null) {
	// throw new Exception(
	// "WKBParser Line [copyTo] Parameter Line is nil");
	// } else {
	// otherLine.setGeometryType(this.getGeometryType());
	// otherLine.setDimensions(this.getDimensions());
	// otherLine.removeListPoints();
	// otherLine.setListPoints(listPoints);
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// @Override
	// public Geometry clone() {
	// Line line = new Line();
	// line.setGeometryType(this.getGeometryType());
	// line.setDimensions(this.getDimensions());
	// line.removeListPoints();
	// line.setListPoints(listPoints);
	// return line;
	// }
	//
	// @Override
	// public String toWKT() {
	// return GeometryToWKT.write(this);
	// }
	//
	// @Override
	// public byte[] toWKB() {
	// return GeometryToWKB.write(this);
	// }
	//
	// public int getPartCount() {
	// return listPoints.size();
	// }
	//
	// public Point GetPointAtIndex(int index) {
	// if (index >= listPoints.size()) {
	// return null;
	// } else {
	// return listPoints.get(index);
	// }
	// }
	//
	// public void addPoints(List<Point> list) {
	// if (this.getDimensions() == 0)
	// this.setDimensions(list.get(0).getDimensions());
	// this.getListPoints().addAll(list);
	// isNeedComputeLength = true;
	// }
	//
	// public double getLength() {
	// if (isNeedComputeLength) {
	// double len = 0;
	// for (int i = 1; i < listPoints.size(); i++) {
	// len += listPoints.get(i).distance(listPoints.get(i - 1));
	// }
	// this.length = len;
	// isNeedComputeLength = false;
	// }
	//
	// return this.length;
	// }
}
