package MRF.common.geometry;

import java.util.ArrayList;
import java.util.List;

public class Polygon extends Geometry{
	   private List<Line> listMultiParts;

		public Polygon() {
			this.setGeometryType(WKBGeometryType.WKBPOLYGON);
			this.setDimensions(0);
			listMultiParts = new ArrayList<Line>();
		}

		public Polygon(List<Line> multiPoints) {
			this.setGeometryType(WKBGeometryType.WKBPOLYGON);
			listMultiParts = new ArrayList<Line>();
			this.setPolygons(multiPoints);
		}

		public void setPolygons(List<Line> multiPoints) {
			try {
				if (multiPoints == null || multiPoints.size() == 0) {
					throw new Exception(
							"WKBParser Polygon Parameter Multi Points list is null");
				} else {
					this.setDimensions(multiPoints.get(0).getDimensions());
					this.getEnv().reset();
					// this.env.expandEnv(line.env);
					for (int j = 0; j < multiPoints.size(); j++)// new
					{
						this.addRing(multiPoints.get(j));
						this.getEnv().expandEnv((multiPoints.get(j).getEnv()));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public List<Line> getPolygon() {
			return listMultiParts;
		}

		public List<Line> getInteriorRings() {
			if (listMultiParts.size() > 1) {
				List<Line> polygonsI = new ArrayList<Line>();
				for (int i = 1; i < listMultiParts.size(); i++) {
					polygonsI.add(listMultiParts.get(i));
				}
				return polygonsI;
			} else {
				return new ArrayList<Line>();
			}
		}

		public Line getExteriorRing() {
			if (listMultiParts.size() > 0) {
				return listMultiParts.get(0);
			} else {
				return new Line();
			}
		}

		public boolean addRing(Line line) {
			if (listMultiParts.size() == 0) {
				this.getEnv().reset();
			} else {
				if (this.getEnv().contains(line.getEnv()))
					return false;
			}
			listMultiParts.add(line);
			this.getEnv().expandEnv(line.getEnv());
			return true;
		}

		public void removePolygons() {
			listMultiParts.clear();
			this.setDimensions(0);
			this.getEnv().reset();
		}

		public boolean isEqual(Polygon otherPolygon) {
			if (otherPolygon == null || !(otherPolygon instanceof Polygon)) {
				return false;
			} else if (this.getDimensions() != otherPolygon.getDimensions()) {
				return false;
	        } else if (listMultiParts.size() != otherPolygon.getPartCount()) {
				return false;
			} else if (!(this.getExteriorRing().equals(otherPolygon
					.getExteriorRing()))) {
				return false;
			} else if (this.getInteriorRings().size() != otherPolygon
					.getInteriorRings().size()) {
				return false;
			} else {
				List<Line> listOtherPoints = otherPolygon.getPolygon();
				for (int i = 0; i < listMultiParts.size(); i++) {
					if (!(listMultiParts.get(i).isEqual(listOtherPoints.get(i)))) {
						return false;
					}
				}
				return true;
			}
		}

		public void copyTo(Polygon otherPolygon)  {
			try {
				if (otherPolygon == null) {
					throw new Exception(
							"WKBParser Polygon [copyTo] Parameter Polygon is null");
				} else {
				    otherPolygon.setGeometryType(this.getGeometryType());
					otherPolygon.setDimensions(this.getDimensions());
					otherPolygon.removePolygons();
					for (int i = 0; i < listMultiParts.size(); i++) {
						otherPolygon.addRing(listMultiParts.get(i));
					}
					otherPolygon.getEnv().reset();
					otherPolygon.getEnv().expandEnv(this.getEnv());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		@Override
	    public String toWKT() {
	        return GeometryToWKT.write(this);
	    }
		@Override
	    public  byte[] toWKB() {
	        return GeometryToWKB.write(this);
	    }
		
		public int getPartCount() {
		    return listMultiParts.size();
		}

		public Line getPartAtIndex(int index) {
		    if (index>=listMultiParts.size()) {
		        return null;
		    } else
		        return listMultiParts.get(index);
		}
}
