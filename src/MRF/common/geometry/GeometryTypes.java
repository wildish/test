package MRF.common.geometry;

public enum GeometryTypes {
	WKBPoint,

	/*
	 * LineString.
	 */
	WKBLineString,

	/*
	 * Polygon.
	 */
	WKBPolygon,

	/*
	 * MultiPoint.
	 */
	WKBMultiPoint,

	/*
	 * MultiLineString.
	 */
	WKBMultiLineString,

	/*
	 * MultiPolygon.
	 */
	WKBMultiPolygon,

	/*
	 * GeometryCollection.
	 */
	WKBGeometryCollection,

	/*
	 * Point with Z coordinate.
	 */
	WKBPointZ,

	/*
	 * LineString with Z coordinate.
	 */
	WKBLineStringZ,

	/*
	 * Polygon with Z coordinate.
	 */
	WKBPolygonZ,

	/*
	 * MultiPoint with Z coordinate.
	 */
	WKBMultiPointZ,

	/*
	 * MultiLineString with Z coordinate.
	 */
	WKBMultiLineStringZ,

	/*
	 * MultiPolygon with Z coordinate.
	 */
	WKBMultiPolygonZ,

	/*
	 * GeometryCollection with Z coordinate.
	 */
	WKBGeometryCollectionZ,

	/*
	 * Point with M ordinate value.
	 */
	WKBPointM,

	/*
	 * LineString with M ordinate value.
	 */
	WKBLineStringM,

	/*
	 * Polygon with M ordinate value.
	 */
	WKBPolygonM,

	/*
	 * MultiPoint with M ordinate value.
	 */
	WKBMultiPointM,

	/*
	 * MultiLineString with M ordinate value.
	 */
	WKBMultiLineStringM,

	/*
	 * MultiPolygon with M ordinate value.
	 */
	WKBMultiPolygonM,

	/*
	 * GeometryCollection with M ordinate value.
	 */
	WKBGeometryCollectionM,

	/*
	 * Point with Z coordinate and M ordinate value.
	 */
	WKBPointZM,

	/*
	 * LineString with Z coordinate and M ordinate value.
	 */
	WKBLineStringZM,

	/*
	 * Polygon with Z coordinate and M ordinate value.
	 */
	WKBPolygonZM,

	/*
	 * MultiPoint with Z coordinate and M ordinate value.
	 */
	WKBMultiPointZM,

	/*
	 * MultiLineString with Z coordinate and M ordinate value.
	 */
	WKBMultiLineStringZM,

	/*
	 * MultiPolygon with Z coordinate and M ordinate value.
	 */
	WKBMultiPolygonZM,

	/*
	 * GeometryCollection with Z coordinate and M ordinate value.
	 */
	WKBGeometryCollectionZM
}
