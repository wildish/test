package MRF.common.geometry;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import MRF.common.Util.MRFUtil;

public class GeometryFromWKB {

	public static Geometry parse(byte[] bytes) throws Exception {
		ByteBuffer bais = ByteBuffer.wrap(bytes);
		bais.order(ByteOrder.LITTLE_ENDIAN);
		return parse(bais);
	}

	public static Geometry parse(ByteBuffer reader) throws Exception {
		// byte byteOrder = reader.get();
		reader.get();

		/*
		 * if (!Enum.IsDefined(typeof(WkbByteOrder), byteOrder)) { throw new
		 * RuntimeException("Byte order not recognized"); }
		 */

		// Get the type of this geometry.

		int type = 1;
		type = reader.getInt(); // readUInt32(reader,
								// (WkbByteOrder.values()[byteOrder]));
		/*
		 * if (!Enum.IsDefined(typeof(WKBGeometryType), type)) throw new
		 * Exception("Geometry type not recognized");
		 */

		int len = WKBGeometryType.values().length;
		if (type >= len) {
			return null;
		}
		switch ((WKBGeometryType.values()[type])) {
		case WKBPOINT:
			return createPoint(reader);
		case WKBLINESTRING:
			return createLineString(reader);
		case WKBPOLYGON:
			return createPolygon(reader);
		case WKBMULTIPOINT:
			return createWKBMultiPoint(reader);
		case WKBMULTILINESTRING: {
			MultiLine line = createWKBMultiLineString(reader);
			if (line.getPartCount() == 1) {
				return line.getPartAtIndex(0);
			} else
				return line;
		}
		case WKBMULTIPOLYGON:
			return createWKBMultiPolygon(reader);
		case WKBGEOMETRYCOLLECTION:
			return createWKBGeometryCollection(reader);
		default:
			throw new Exception("Geometry type '" + type + "' not supported");
		}
	}

	private static Point createPoint(ByteBuffer reader) throws IOException {
		// Create and return the point.
		return getPoint(reader.getDouble(), reader.getDouble());
	}

	private static Point getPoint(double d1, double d2) {
		return new Point(d1, d2);
	}

	// private static List<Point> readCoordinates(ByteBuffer reader)
	// throws IOException {
	// // Get the number of points in this linestring.
	// int numPoints = reader.getInt();
	//
	// // Create a new array of coordinates.
	// List<Point> coords = new ArrayList<Point>();
	//
	// // Loop on the number of points in the ring.
	// for (int i = 0; i < numPoints; i++) {
	// // Add the coordinate.
	// coords.add(getPoint(reader.getDouble(), reader.getDouble()));
	// }
	// return coords;
	// }
	private static List<double[]> readCoordinates(ByteBuffer reader)
			throws IOException {
		// Get the number of points in this linestring.
		int numPoints = reader.getInt();

		// Create a new array of coordinates.
		List<double[]> coords = new ArrayList<double[]>();

		// Loop on the number of points in the ring.
		for (int i = 0; i < numPoints; i++) {
			// Add the coordinate.
			coords.add(new double[] { reader.getDouble(), reader.getDouble() });
		}
		return coords;
	}

	private static Line createLineString(ByteBuffer reader) throws IOException {
		Line line = new Line();
		// l.Vertices.AddRange(ReadCoordinates(reader, byteOrder));
		// Point[] arrPoint = readCoordinates(reader);
		// List<Point> listPoints = new ArrayList<Point>();
		// for (int i = 0; i < arrPoint.length; i++) {
		// listPoints.add(arrPoint[i]);
		// }
		// List<Point> listPoints = readCoordinates(reader);
		List<double[]> listPoints = readCoordinates(reader);
		line.setListPoints(listPoints);
		return line;
	}

	private static Line createLinearRing(ByteBuffer reader) throws IOException {
		Line line = new Line();
		// l.Vertices.AddRange(ReadCoordinates(reader, byteOrder));
		// Point[] arrPoint = readCoordinates(reader);
		// List<Point> listPoints = new ArrayList<Point>();
		// for (int i = 0; i < arrPoint.length; i++) {
		// listPoints.add(arrPoint[i]);
		// }
		// List<Point> listPoints = readCoordinates(reader);
		List<double[]> listPoints = readCoordinates(reader);
		// if polygon isn't closed, add the first point to the end (this
		// shouldn't occur for correct WKB data)
		if (MRFUtil.isEqual(listPoints.get(0),
				listPoints.get(listPoints.size() - 1))) {
			listPoints.add(listPoints.get(0));
		}

		line.setListPoints(listPoints);
		return line;
	}

	private static Polygon createPolygon(ByteBuffer reader) throws IOException {
		// Get the Number of rings in this Polygon.
		int numRings = reader.getInt();
		// Debug.Assert(numRings >= 1,
		// "Number of rings in polygon must be 1 or more.");

		Line line = createLinearRing(reader);
		List<Line> lines = new ArrayList<Line>();
		lines.add(line);
		Polygon shell = new Polygon(lines);

		// Create a new array of linearrings for the interior rings.
		for (int i = 0; i < (numRings - 1); i++) {
			shell.addRing(createLinearRing(reader));
		}

		// Create and return the Poylgon.
		return shell;
	}

	private static MultiPoint createWKBMultiPoint(ByteBuffer reader)
			throws IOException {
		// Get the number of points in this multipoint.
		int numPoints = reader.getInt();

		// Create a new array for the points.
		MultiPoint points = new MultiPoint();
		List<Point> listPoints = new ArrayList<Point>();
		// Loop on the number of points.
		for (int i = 0; i < numPoints; i++) {
			// Read point header
			reader.get();
			reader.getInt();

			// TODO: Validate type

			// Create the next point and add it to the point array.
			listPoints.add(createPoint(reader));
		}
		points.setListPoints(listPoints);
		return points;
	}

	private static MultiLine createWKBMultiLineString(ByteBuffer reader)
			throws IOException {
		// Get the number of linestrings in this multilinestring.
		int numLineStrings = reader.getInt();

		// Create a new array for the linestrings .
		MultiLine mline = new MultiLine();
		List<Line> lines = new ArrayList<Line>();
		// Loop on the number of linestrings.
		for (int i = 0; i < numLineStrings; i++) {
			// Read linestring header
			reader.get();
			reader.getInt();

			// Create the next linestring and add it to the array.
			lines.add(createLineString(reader));
		}
		mline.setListLines(lines);
		// Create and return the MultiLineString.
		return mline;
	}

	private static MultiPolygon createWKBMultiPolygon(ByteBuffer reader)
			throws IOException {
		// Get the number of Polygons.
		int numPolygons = reader.getInt();

		// Create a new array for the Polygons.
		MultiPolygon polygons = new MultiPolygon();
		List<Polygon> listPolygons = new ArrayList<Polygon>();
		// Loop on the number of polygons.
		for (int i = 0; i < numPolygons; i++) {
			// get polygon header
			reader.get();
			reader.getInt();

			// Create the next polygon and add it to the array.
			listPolygons.add(createPolygon(reader));
		}
		polygons.SetPolygons(listPolygons);
		// Create and return the MultiPolygon.
		return polygons;
	}

	private static Geometry createWKBGeometryCollection(ByteBuffer reader)
			throws Exception {
		// The next byte in the array tells the number of geometries in this
		// collection.
		int numGeometries = reader.getInt();

		// Create a new array for the geometries.
		GeometryCollection geometries = new GeometryCollection();

		// Loop on the number of geometries.
		for (int i = 0; i < numGeometries; i++) {
			// Call the main create function with the next geometry.
			geometries.addGeometry(parse(reader));
		}

		// Create and return the next geometry.
		return geometries;
	}

}
