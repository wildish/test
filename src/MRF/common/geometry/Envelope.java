package MRF.common.geometry;

public class Envelope {

	public double minX;
	public double minY;
	public double maxX;
	public double maxY;

	public Envelope() {
		minX = Double.MAX_VALUE / 2.0;
		maxX = -0.5 * Double.MAX_VALUE;
		minY = Double.MAX_VALUE / 2.0;
		maxY = -0.5 * Double.MAX_VALUE;
	}

	public Envelope(Envelope env) {
		minX = env.minX;
		maxX = env.maxX;
		minY = env.minY;
		maxY = env.maxY;
	}

	public Envelope(double minx, double maxx, double miny, double maxy) {
		this.minX = minx;
		this.maxX = maxx;
		this.minY = miny;
		this.maxY = maxy;
	}

	public Envelope(double x, double y) {
		this.minX = x - 0.0000005;
		this.maxX = x + 0.0000005;
		this.minY = y - 0.0000005;
		this.maxY = y + 0.0000005;
	}

	public Envelope(Point pt1, Point pt2) {
		this.minX = Math.min(pt1.X, pt2.X);
		this.maxX = Math.max(pt1.X, pt2.X);
		this.minY = Math.min(pt1.Y, pt2.Y);
		this.maxY = Math.max(pt1.Y, pt2.Y);
	}

	public boolean isEmpty() {
		return (maxX - minX > Double.MAX_VALUE / 4.0 && maxY - minY < Double.MAX_VALUE / 4.0);
	}

	public boolean intersects(Envelope otherEnv) {
		return !(otherEnv.minX > maxX || otherEnv.maxX < minX
				|| otherEnv.minY > maxY || otherEnv.maxY < minY);
	}

	public boolean contains(Envelope otherEnv) {
		if (minY < otherEnv.minY)
			return false;
		if (minX < otherEnv.minX)
			return false;
		if (maxX > otherEnv.maxX)
			return false;
		if (maxY > otherEnv.maxY)
			return false;
		return true;
	}

	public boolean contains(Point pt) {
		if (minY < pt.Y)
			return false;
		if (minX < pt.X)
			return false;
		if (maxX > pt.X)
			return false;
		if (maxY > pt.Y)
			return false;
		return true;
	}

	public void reset() {
		minX = Double.MAX_VALUE / 2.0;
		maxX = -0.5 * Double.MAX_VALUE;
		minY = Double.MAX_VALUE / 2.0;
		maxY = -0.5 * Double.MAX_VALUE;
	}

	public void expand(double x, double y) {
		if (minX > x)
			minX = x;
		if (minY > y)
			minY = y;
		if (maxX < x)
			maxX = x;
		if (maxY < y)
			maxY = y;
	}

	public void expandEnv(Envelope env) {
		if (minX > env.minX)
			minX = env.minX;
		if (minY > env.minY)
			minY = env.minY;
		if (maxX < env.maxX)
			maxX = env.maxX;
		if (maxY < env.maxY)
			maxY = env.maxY;
	}

	public double getWidth() {
		return Math.abs(maxX - minX);
	}

	public double getHeight() {
		return Math.abs(maxY - minY);
	}

	public double getArea() {
		return Math.abs((maxX - minX) * (maxY - minY));
	}

	public static boolean intersects(Point p1, Point p2, Point q1, Point q2) {
		double minp = Math.min(p1.X, p2.X);
		double maxq = Math.max(q1.X, q2.X);
		if (minp > maxq)
			return false;

		double minq = Math.min(q1.X, q2.X);
		double maxp = Math.max(p1.X, p2.X);
		if (maxp < minq)
			return false;

		minp = Math.min(p1.Y, p2.Y);
		maxq = Math.max(q1.Y, q2.Y);
		if (minp > maxq)
			return false;

		minq = Math.min(q1.Y, q2.Y);
		maxp = Math.max(p1.Y, p2.Y);
		if (maxp < minq)
			return false;

		return true;
	}

	public static boolean intersects(double[] p1, double[] p2, double[] q1,
			double[] q2) {
		double minp = Math.min(p1[0], p2[0]);
		double maxq = Math.max(q1[0], q2[0]);
		if (minp > maxq)
			return false;

		double minq = Math.min(q1[0], q2[0]);
		double maxp = Math.max(p1[0], p2[0]);
		if (maxp < minq)
			return false;

		minp = Math.min(p1[1], p2[1]);
		maxq = Math.max(q1[1], q2[1]);
		if (minp > maxq)
			return false;

		minq = Math.min(q1[1], q2[1]);
		maxp = Math.max(p1[1], p2[1]);
		if (maxp < minq)
			return false;

		return true;
	}

	public void expandToInclude(Envelope other) {
		if (other.minX > other.maxX)
			return;
		if (minX > maxX) {
			minX = other.minX;
			maxX = other.maxX;
			minY = other.minY;
			maxY = other.maxY;
		} else {
			if (other.minX < minX)
				minX = other.minX;
			if (other.maxX > maxX)
				maxX = other.maxX;
			if (other.minY < minY)
				minY = other.minY;
			if (other.maxY > maxY)
				maxY = other.maxY;
		}
	}

	public boolean equals2D(Envelope other) {
		if (Math.abs(minX - other.minX) > 1e-5)
			return false;
		if (Math.abs(minY - other.minY) > 1e-5)
			return false;
		if (Math.abs(maxX - other.maxX) > 1e-5)
			return false;
		if (Math.abs(maxY - other.maxY) > 1e-5)
			return false;
		return true;
	}

	public void grow(double width, double height) {
		minX = minX - width/2.0;
		maxX = maxX + width/2.0;
		minY = minY - height/2.0;
		maxY = maxY - height/2.0;
	}
}
