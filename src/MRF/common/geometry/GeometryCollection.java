package MRF.common.geometry;

import java.util.ArrayList;
import java.util.List;

public class GeometryCollection extends Geometry {
	
	private List<Geometry> listGeometry;
	
	public GeometryCollection() {
        this.setGeometryType(WKBGeometryType.WKBGEOMETRYCOLLECTION);
        this.setDimensions(0);
        listGeometry = new ArrayList<Geometry>();
    }

    public void addGeometry(Geometry geometry) throws Exception {
        try {
            if (geometry == null) {
                throw new Exception("WKBParser Geometry Collection Parameter Geometry is null");
            } else {
                if (((Object)geometry instanceof Point) || ((Object)geometry instanceof MultiPoint) ||
                   ((Object)geometry instanceof Line) || ((Object)geometry instanceof MultiLine) ||
                   ((Object)geometry instanceof Polygon) || ((Object)geometry instanceof MultiPolygon)) {
                    listGeometry.add(geometry);
                    getEnv().expandEnv(geometry.getEnv());
                } else {
                    throw new Exception("WKBParser Geometry Collection Geometry is invalid (WKB Geometry not recognised)");
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void removeGeometries() {
        listGeometry = new ArrayList<Geometry>();
        this.setDimensions(0);
        this.getEnv().reset();
    }
    
    public List<Geometry> getGeometries() {
        return listGeometry;
    }

    public boolean isEqual(GeometryCollection otherGeometries) {
        if (otherGeometries == null || !(otherGeometries instanceof GeometryCollection)) {
            return false;
        } else if (this.getDimensions() != otherGeometries.getDimensions()) {
            return false;
        } else if (listGeometry.size() != otherGeometries.getGeometries().size()) {
            return false;
        } else {
            List<Geometry> listOtherGeometries = otherGeometries.getGeometries();
            for (int i = 0; i < listOtherGeometries.size(); i++) {
                Geometry otherGeom = listOtherGeometries.get(i);
                Geometry geom = listGeometry.get(i);
                if (!(geom.isEqual(otherGeom))) {
                    return false;
                }
            }
            return true;
        }
    }

    public void copyTo(GeometryCollection otherGeometries) throws Exception {
        if (otherGeometries == null) {
            throw new Exception("WKBParser Geometry Collection [copyTo] Parameter Geometry Collection is null");
        } else {
            otherGeometries.setGeometryType(this.getGeometryType());
            otherGeometries.setDimensions(this.getDimensions());
            otherGeometries.removeGeometries();
            for (int i = 0; i < listGeometry.size(); i++) {
                otherGeometries.addGeometry(listGeometry.get(i));
            }
            otherGeometries.getEnv().reset();
            otherGeometries.getEnv().expandEnv(this.getEnv());
        }
    }

    public Geometry clone() {
        GeometryCollection geom = new GeometryCollection();
        geom.setGeometryType(this.getGeometryType());
        geom.setDimensions(this.getDimensions());
        geom.removeGeometries();
        try {
	        for (int i = 0; i < listGeometry.size(); i++) {
					geom.addGeometry(listGeometry.get(i));
	        }
        } catch (Exception e) {
        	e.printStackTrace();
        }
        geom.getEnv().reset();
        geom.getEnv().expandEnv(this.getEnv());
        return geom;
    }

    public String toWKT() {
        return GeometryToWKT.write(this);
    }

    public byte[] toWKB() {
        return GeometryToWKB.write(this);
    }

    public int getPartCount()
    {
        return listGeometry.size();
    }
    public Geometry getPartAtIndex(int index)
    {
        if (index >= listGeometry.size()) {
            return null;
        }
        return listGeometry.get(index);
    }
}
