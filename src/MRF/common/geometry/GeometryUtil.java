package MRF.common.geometry;

import java.util.ArrayList;
import java.util.List;

import MRF.common.Util.MRFUtil;
import MRF.common.Util.Algorithm.CGAlgorithms;

public class GeometryUtil {

	public static Point getPointAtLength(Line line, double length, double rotate) {
		rotate = 0;
		List<double[]> listPoints = line.getListPoints();
		if (length <= 0) {
			if (listPoints.size() > 1) {
				rotate = new LineSegment(listPoints.get(0), listPoints.get(1))
						.getAngle();
			}
			double[] d = listPoints.get(0);
			return d == null ? null : new Point(d);
		} else if (length >= line.getLength()) {
			if (listPoints.size() > 1) {
				rotate = new LineSegment(listPoints.get(listPoints.size() - 2),
						listPoints.get(listPoints.size() - 1)).getAngle();
			}
			double[] d = listPoints.get(listPoints.size() - 1);
			return d == null ? null : new Point(d);
		} else {
			double len = 0;
			for (int i = 1; i < listPoints.size(); i++) {
				len += MRFUtil.distance(listPoints.get(i),
						listPoints.get(i - 1));
				if (len >= length) {
					LineSegment seg = new LineSegment(listPoints.get(i - 1),
							listPoints.get(i));
					rotate = seg.getAngle();
					double[] d = seg
							.pointAlong((length - len + seg.getLength())
									/ seg.getLength());
					return d == null ? null : new Point(d);
				}
			}
		}
		double[] d = listPoints.get(0);
		return d == null ? null : new Point(d);
		// List<Point> listPoints = line.getListPoints();
		// if (length <= 0) {
		// if (listPoints.size() > 1) {
		// rotate = new LineSegment(listPoints.get(0), listPoints.get(1))
		// .getAngle();
		// }
		// return listPoints.get(0);
		// } else if (length >= line.getLength()) {
		// if (listPoints.size() > 1) {
		// rotate = new LineSegment(listPoints.get(listPoints.size() - 2),
		// listPoints.get(listPoints.size() - 1)).getAngle();
		// }
		// return listPoints.get(listPoints.size() - 1);
		// } else {
		// double len = 0;
		// for (int i = 1; i < listPoints.size(); i++) {
		// len += listPoints.get(i).distance(listPoints.get(i - 1));
		// if (len >= length) {
		// LineSegment seg = new LineSegment(listPoints.get(i - 1),
		// listPoints.get(i));
		// rotate = seg.getAngle();
		// return seg.pointAlong((length - len + seg.getLength())
		// / seg.getLength());
		// }
		// }
		// }
		// return listPoints.get(0);
	}

	public static List<Point> getPointAtEverySegmentLength(Line line,
			double segLength, List<Double> rotates) {
		double rotate = 0;
		List<Point> result = new ArrayList<Point>();
		// List<Point> listPoints = line.getListPoints();
		List<double[]> listPoints = line.getListPoints();
		if (line.getLength() <= 0) {
			if (listPoints.size() > 1) {
				rotate = new LineSegment(listPoints.get(0), listPoints.get(1))
						.getAngle();
			}
			double[] d = listPoints.get(0);
			if (d != null) {
				result.add(new Point(d));
			}
			rotates.add(rotate);
		} else if (segLength >= line.getLength()) {
			if (listPoints.size() > 1) {
				rotate = new LineSegment(listPoints.get(listPoints.size() - 2),
						listPoints.get(listPoints.size() - 1)).getAngle();
			}
			double[] d = listPoints.get(listPoints.size() - 1);
			if (d != null) {
				result.add(new Point(d));
			}
			rotates.add(rotate);
		} else {
			double len = 0;
			double curPartLen = 0;
			// double surplusLen = 0;
			for (int i = 1; i < listPoints.size(); i++) {
				curPartLen = MRFUtil.distance(listPoints.get(i),
						listPoints.get(i - 1));
				len += curPartLen;
				if (len >= segLength) {
					while (len >= segLength) {
						len -= segLength;
					}
					// surplusLen = len;
				}
				/*
				 * if (len >= length) { LineSegment seg = new
				 * LineSegment(listPoints[i - 1], listPoints[i]); rotate =
				 * seg.Angle; return seg.PointAlong((length - len + seg.Length)
				 * / seg.Length); }
				 */
			}
		}
		return result;
	}

	public static Geometry getBufferGeometry(Geometry geo, double distance) {
		if (geo.getGeometryType() == WKBGeometryType.WKBPOINT) {
			Point pt = (Point) geo;
			List<double[]> pts = new ArrayList<double[]>();
			pts.add(new double[] { pt.X - distance, pt.Y - distance });
			pts.add(new double[] { pt.X - distance, pt.Y + distance });
			pts.add(new double[] { pt.X + distance, pt.Y + distance });
			pts.add(new double[] { pt.X + distance, pt.Y - distance });
			pts.add(new double[] { pt.X - distance, pt.Y - distance });
			Line line = new Line(pts);
			// Point pt = (Point) geo;
			// List<Point> pts = new ArrayList<Point>();
			// pts.add(new Point(pt.X - distance, pt.Y - distance));
			// pts.add(new Point(pt.X - distance, pt.Y + distance));
			// pts.add(new Point(pt.X + distance, pt.Y + distance));
			// pts.add(new Point(pt.X + distance, pt.Y - distance));
			// pts.add(new Point(pt.X - distance, pt.Y - distance));
			// Line line = new Line(pts);
			List<Line> lines = new ArrayList<Line>();
			lines.add(line);
			Polygon polygon = new Polygon(lines);
			return polygon;
		}
		return geo;
	}

	// public static List<Point> getPointsInGeometry(Geometry geom) {
	// List<Point> returnVal = new ArrayList<Point>();
	// if (geom.getGeometryType() == WKBGeometryType.WKBPOINT) {
	// returnVal.add((Point) geom);
	// } else if (geom.getGeometryType() == WKBGeometryType.WKBMULTIPOINT) {
	// List<Point> pts = ((MultiPoint) geom).getListPoints();
	// returnVal.addAll(pts);
	// } else if (geom.getGeometryType() == WKBGeometryType.WKBLINESTRING) {
	// List<Point> pts = ((Line) geom).getListPoints();
	// returnVal.addAll(pts);
	// } else if (geom.getGeometryType() == WKBGeometryType.WKBMULTILINESTRING)
	// {
	// List<Line> lines = ((MultiLine) geom).GetListLines();
	// for (Line line : lines) {
	// returnVal.addAll(line.getListPoints());
	// }
	// } else if (geom.getGeometryType() == WKBGeometryType.WKBPOLYGON) {
	// Polygon polygon = (Polygon) geom;
	// int partCount = polygon.getPartCount();
	// for (int i = 0; i < partCount; i++) {
	// returnVal.addAll(polygon.getPartAtIndex(i).getListPoints());
	// }
	// } else if (geom.getGeometryType() == WKBGeometryType.WKBMULTIPOLYGON) {
	// List<Polygon> polygons = ((MultiPolygon) geom).GetPolygons();
	// for (Polygon polygon : polygons) {
	// int partCount = polygon.getPartCount();
	// for (int i = 0; i < partCount; i++) {
	// returnVal.addAll(polygon.getPartAtIndex(i).getListPoints());
	// }
	// }
	// } else if (geom.getGeometryType() ==
	// WKBGeometryType.WKBGEOMETRYCOLLECTION) {
	// GeometryCollection gc = (GeometryCollection) geom;
	// List<Geometry> geoms = gc.getGeometries();
	// for (int i = 0; i < geoms.size(); i++) {
	// returnVal.addAll(getPointsInGeometry(geoms.get(i)));
	// }
	// }
	// return returnVal;
	// }

	public static List<Line> getLinesInGeometry(Geometry geom) {
		List<Line> returnVal = new ArrayList<Line>();
		if (geom.getGeometryType() == WKBGeometryType.WKBLINESTRING) {
			// List<Point> pts = ((Line)geom).getListPoints();
			returnVal.add((Line) geom);
		} else if (geom.getGeometryType() == WKBGeometryType.WKBMULTILINESTRING) {
			List<Line> lines = ((MultiLine) geom).GetListLines();
			for (Line line : lines) {
				returnVal.add(line);
			}
		} else if (geom.getGeometryType() == WKBGeometryType.WKBPOLYGON) {
			Polygon polygon = (Polygon) geom;
			returnVal.addAll(polygon.getPolygon());
		} else if (geom.getGeometryType() == WKBGeometryType.WKBMULTIPOLYGON) {
			List<Polygon> polygons = ((MultiPolygon) geom).GetPolygons();
			for (Polygon polygon : polygons) {
				returnVal.addAll(polygon.getPolygon());
			}
		} else if (geom.getGeometryType() == WKBGeometryType.WKBGEOMETRYCOLLECTION) {
			GeometryCollection gc = (GeometryCollection) geom;
			List<Geometry> geoms = gc.getGeometries();
			for (int i = 0; i < geoms.size(); i++) {
				returnVal.addAll(getLinesInGeometry(geoms.get(i)));
			}
		}
		return returnVal;
	}

	public static boolean isCCW(List<Point> ring) throws Exception {
		// # of points without closing endpoint
		int nPts = ring.size() - 1;

		// sanity check
		if (nPts < 3)
			throw new Exception(
					"Ring has fewer than 4 points, so orientation cannot be determined");

		// find highest point
		Point hiPt = ring.get(0);
		int hiIndex = 0;
		for (int i = 1; i <= nPts; i++) {
			Point p = ring.get(i);
			if (p.Y > hiPt.Y) {
				hiPt = p;
				hiIndex = i;
			}
		}

		// find distinct point before highest point
		int iPrev = hiIndex;
		do {
			iPrev = iPrev - 1;
			if (iPrev < 0)
				iPrev = nPts;
		} while (ring.get(iPrev).isEqual(hiPt) && iPrev != hiIndex);

		// find distinct point after highest point
		int iNext = hiIndex;
		do
			iNext = (iNext + 1) % nPts;
		while (ring.get(iNext).isEqual(hiPt) && iNext != hiIndex);

		Point prev = ring.get(iPrev);
		Point next = ring.get(iNext);

		/*
		 * This check catches cases where the ring contains an A-B-A
		 * configuration of points. This can happen if the ring does not contain
		 * 3 distinct points (including the case where the input array has fewer
		 * than 4 elements), or it contains coincident line segments.
		 */
		if (prev.isEqual(hiPt) || next.isEqual(hiPt) || prev.isEqual(next))
			return false;

		int disc = computeOrientation(prev, hiPt, next);

		/*
		 * If disc is exactly 0, lines are collinear. There are two possible
		 * cases: (1) the lines lie along the x axis in opposite directions (2)
		 * the lines lie on top of one another
		 * 
		 * (1) is handled by checking if next is left of prev ==> CCW (2) will
		 * never happen if the ring is valid, so don't check for it (Might want
		 * to assert this)
		 */
		boolean isCCW = false;
		if (disc == 0)
			// poly is CCW if prev x is right of next x
			isCCW = (prev.X > next.X);
		else
			// if area is positive, points are ordered CCW
			isCCW = (disc > 0);
		return isCCW;
	}

	public static int computeOrientation(Point p1, Point p2, Point q) {
		// fast filter for orientation index
		// avoids use of slow extended-precision arithmetic in many cases
		int index = orientationIndexFilter(p1, p2, q);
		if (index <= 1)
			return index;

		// normalize coordinates
		double dx1 = p2.X - p1.X;
		double dy1 = p2.Y - p1.Y;
		double dx2 = q.X - p2.X;
		double dy2 = q.Y - p2.Y;

		return signOfDet2x2(dx1, dy1, dx2, dy2);
	}

	public static int signOfDet2x2(double x1, double y1, double x2, double y2) {
		double det = x1 * y2 - y1 * x2;
		if (Math.abs(det) < 0.00000001)
			return 0;
		if (det < -0.00000001)
			return -1;
		return 1;
	}

	private static int orientationIndexFilter(Point pa, Point pb, Point pc) {
		double detsum;

		double detleft = (pa.X - pc.X) * (pb.Y - pc.Y);
		double detright = (pa.Y - pc.Y) * (pb.X - pc.X);
		double det = detleft - detright;

		if (detleft > 0.0) {
			if (detright <= 0.0) {
				return signum(det);
			}
			detsum = detleft + detright;
		} else if (detleft < 0.0) {
			if (detright >= 0.0) {
				return signum(det);
			}
			detsum = -detleft - detright;
		} else {
			return signum(det);
		}

		double errbound = 1e-15 * detsum;
		if ((det >= errbound) || (-det >= errbound)) {
			return signum(det);
		}

		return 2;
	}

	private static int signum(double x) {
		if (x > 0)
			return 1;
		if (x < 0)
			return -1;
		return 0;
	}

	public static double getLength(Geometry geo) {
		double len = 0.0;
		WKBGeometryType type = geo.getGeometryType();
		switch (type) {
		case WKBLINESTRING:
			len = CGAlgorithms.length((Line) geo);
			break;
		case WKBMULTILINESTRING: {
			List<Line> lineList = ((MultiLine) geo).GetListLines();
			for (Line line : lineList) {
				len += CGAlgorithms.length(line);
			}
			break;
		}
		case WKBPOLYGON: {
			List<Line> lineList = ((Polygon) geo).getPolygon();
			for (Line line : lineList) {
				len += CGAlgorithms.length(line);
			}
			break;
		}
		case WKBMULTIPOLYGON: {
			List<Polygon> polygonList = ((MultiPolygon) geo).GetPolygons();
			for (Polygon polygon : polygonList) {
				List<Line> lineList = polygon.getPolygon();
				for (Line line : lineList) {
					len += CGAlgorithms.length(line);
				}
			}
			break;
		}
		default:
			len = 0;
			break;
		}
		return len;
	}

	public static double getArea(Geometry geo) {
		double area = 0.0;
		WKBGeometryType type = geo.getGeometryType();
		switch (type) {
		case WKBPOLYGON: {
			Polygon poly = (Polygon) geo;
			area = CGAlgorithms.signedArea(poly.getExteriorRing());
			List<Line> lineList = poly.getInteriorRings();
			for (Line line : lineList) {
				area -= CGAlgorithms.signedArea(line);
			}
			break;
		}
		case WKBMULTIPOLYGON: {
			List<Polygon> polygonList = ((MultiPolygon) geo).GetPolygons();
			for (Polygon polygon : polygonList) {
				area += CGAlgorithms.signedArea(polygon.getExteriorRing());
				List<Line> lineList = polygon.getInteriorRings();
				for (Line line : lineList) {
					area -= CGAlgorithms.signedArea(line);
				}
			}
			break;
		}
		default:
			area = 0;
			break;
		}
		return area;
	}

	public static boolean isIntersectWithPointBuffer(Geometry geo, Point pt,
			double distance) {
		Envelope env = new Envelope(pt.X - distance, pt.X + distance, pt.Y
				- distance, pt.Y + distance);
		if (env.intersects(geo.getEnv()) == false) {
			return false;
		}
		double[] ptArr = new double[] { pt.X, pt.Y };
		double minLen = Double.MAX_VALUE;
		WKBGeometryType type = geo.getGeometryType();
		switch (type) {
		case WKBPOINT: {
			Point pt1 = (Point) geo;
			minLen = Math.abs(pt.X - pt1.X) + Math.abs(pt.Y - pt1.Y);
			break;
		}
		case WKBMULTIPOINT: {
			List<Point> ptList = ((MultiPoint) geo).getListPoints();
			for (Point pt1 : ptList) {
				minLen = Math.abs(pt.X - pt1.X) + Math.abs(pt.Y - pt1.Y);
				if (minLen < distance) {
					return true;
				}
			}
			break;
		}
		case WKBLINESTRING: {
			List<double[]> ptList = ((Line) geo).getListPoints();
			double[] p1;
			double[] p0 = ptList.get(0);
			for (int i = 1; i < ptList.size(); i++) {
				p0 = ptList.get(i - 1);
				p1 = ptList.get(i);
				minLen = CGAlgorithms.distancePointLine(ptArr, p0, p1);
				if (minLen < distance) {
					return true;
				}
			}
			break;
		}
		case WKBMULTILINESTRING: {

			List<Line> lineList = ((MultiLine) geo).GetListLines();
			for (Line line : lineList) {
				List<double[]> ptList = line.getListPoints();
				double[] p1;
				double[] p0 = ptList.get(0);
				for (int i = 1; i < ptList.size(); i++) {
					p0 = ptList.get(i - 1);
					p1 = ptList.get(i);
					minLen = CGAlgorithms.distancePointLine(ptArr, p0, p1);
					if (minLen < distance) {
						return true;
					}
				}
			}
			break;
		}
		case WKBPOLYGON: {
			Line line = ((Polygon) geo).getExteriorRing();
			if (CGAlgorithms.isPointInRing(pt, line.getListPoints())) {
				return true;
			} else {
				List<double[]> ptList = line.getListPoints();
				double[] p1;
				double[] p0 = ptList.get(0);
				for (int i = 1; i < ptList.size(); i++) {
					p0 = ptList.get(i - 1);
					p1 = ptList.get(i);
					minLen = CGAlgorithms.distancePointLine(ptArr, p0, p1);
					if (minLen < distance) {
						return true;
					}
				}
			}
			break;
		}
		case WKBMULTIPOLYGON: {
			List<Polygon> polygonList = ((MultiPolygon) geo).GetPolygons();
			for (Polygon poly : polygonList) {
				Line line = poly.getExteriorRing();
				if (CGAlgorithms.isPointInRing(pt, line.getListPoints())) {
					return true;
				} else {
					List<double[]> ptList = line.getListPoints();
					double[] p1;
					double[] p0 = ptList.get(0);
					for (int i = 1; i < ptList.size(); i++) {
						p0 = ptList.get(i - 1);
						p1 = ptList.get(i);
						minLen = CGAlgorithms.distancePointLine(ptArr, p0, p1);
						if (minLen < distance) {
							return true;
						}
					}
				}
			}
			break;
		}
		default:
			break;
		}
		return minLen <= distance;
	}
}
