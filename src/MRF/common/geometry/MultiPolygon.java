package MRF.common.geometry;

import java.util.ArrayList;
import java.util.List;

public class MultiPolygon extends Geometry{
	private List<Polygon> listPolygons;
	public MultiPolygon() {
		this.setGeometryType(WKBGeometryType.WKBMULTIPOLYGON);
		this.setDimensions(0);
		listPolygons = new ArrayList<Polygon>();
	}
	public MultiPolygon(List<Polygon> polygons) {
		this.setGeometryType(WKBGeometryType.WKBMULTIPOLYGON);
		listPolygons = new ArrayList<Polygon>();
		try {
			this.SetPolygons(polygons);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void SetPolygons(List<Polygon> polygons)  {
		try {
		if (polygons == null || polygons.size() == 0) {
				throw new Exception(
						"WKBParser Multi Polygon Parameter Polygons is null");
		} else {
			int dimBackup = 0;
			for (int i = 0; i < polygons.size(); i++) {
				Polygon temp = (Polygon) polygons.get(i);
				if (!(polygons.get(i) instanceof Polygon)) {
					throw new Exception(
							"WKBParser Multi Polygon Parameter Polygons have a class that is falset Polygon");
				} else {
					if (i == 0) {
						dimBackup = temp.getDimensions();
						listPolygons.add(temp);
						this.getEnv().reset();
						this.getEnv().expandEnv(temp.getEnv());
					} else if (dimBackup != polygons.get(i).getDimensions()) {
						throw new Exception(
								"WKBParser Multi Polygon Parameter Polygons have Polygon with different dimensions");
					} else {
						listPolygons.add(temp);
						this.getEnv().expandEnv(temp.getEnv());
					}
				}
			}
			this.setDimensions(dimBackup);
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Polygon> GetPolygons() {
		return listPolygons;
	}

	public void RemovePolygons() {
		listPolygons.clear();
		this.setDimensions(0);
	}

	public boolean IsEqual(MultiPolygon otherPolygonM) {
		if (otherPolygonM == null || !(otherPolygonM instanceof MultiPolygon)) {
			return false;
		} else if (this.getDimensions() != otherPolygonM.getDimensions()) {
			return false;
		} else if (listPolygons.size() != otherPolygonM.GetPolygons().size()) {
			return false;
		} else {
			List<Polygon> listOtherPolygons = otherPolygonM
					.GetPolygons();
			for (int i = 0; i < listPolygons.size(); i++) {
				if (!(listPolygons.get(i).isEqual(listOtherPolygons.get(i)))) {
					return false;
				}
			}
			return true;
		}
	}

	public void copyTo(MultiPolygon otherPolygonM)  {
		try {
			if (otherPolygonM == null) {
				throw new Exception(
						"WKBParser Multi Polygon [copyTo] Parameter Multi Polygon is null");
			} else {
			    otherPolygonM.setGeometryType(this.getGeometryType());
				otherPolygonM.setDimensions(this.getDimensions());
				otherPolygonM.RemovePolygons();
				otherPolygonM.SetPolygons(listPolygons);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
    public  Geometry clone()
    {
        MultiPolygon polygons = new MultiPolygon();
        polygons.setGeometryType(this.getGeometryType());
        polygons.setDimensions(this.getDimensions());
        polygons.RemovePolygons();
        try {
			polygons.SetPolygons(listPolygons);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return polygons;
    }
    @Override
    public  String toWKT()
    {
        return GeometryToWKT.write(this);
    }
    @Override
    public  byte[] toWKB()
    {
        return GeometryToWKB.write(this);
	}
	public int GetPartCount()
	{
	    return listPolygons.size();
	}
	public Polygon GetPartAtIndex(int index)
	{
	    if (index >= listPolygons.size())
	    {
	        return null;
	    }
	    return listPolygons.get(index);
	}
}
