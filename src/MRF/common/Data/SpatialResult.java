package MRF.common.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import MRF.common.Util.MRFUtil;
import MRF.common.geometry.Geometry;
import MRF.common.geometry.GeometryToWKT;
import MRF.common.geometry.WKBGeometryType;
import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class SpatialResult {
	private List<GeometryWithData> datas;
	private List<String> columnNames;
	private List<DataType> columnTypes;
	private WKBGeometryType geoType;
	private String tableName;
	private int srid = 0;

	public SpatialResult() {
		datas = new ArrayList<GeometryWithData>();
		columnNames = new ArrayList<String>();
		columnTypes = new ArrayList<DataType>();
		geoType = WKBGeometryType.WKBGEOMETRYNULL;
	}

	public List<GeometryWithData> getDatas() {
		return datas;
	}

	public void addColumn(String colName, DataType type) {
		this.columnNames.add(colName.toLowerCase());
		this.columnTypes.add(type);
	}

	public boolean containsColumn(String name) {
		return columnNames.contains(name.toLowerCase());
	}

	public void setDatas(List<GeometryWithData> datas) {
		this.datas = datas;
		if (this.geoType == WKBGeometryType.WKBGEOMETRYNULL) {
			this.geoType = datas.get(0).getWKBGeometry().getGeometryType();
		}
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public List<DataType> getColumnTypes() {
		return this.columnTypes;
	}

	public WKBGeometryType getGeometryType() {
		return geoType;
	}

	public void addFeature(GeometryWithData data) {
		this.datas.add(data);
		if (this.geoType == WKBGeometryType.WKBGEOMETRYNULL) {
			this.geoType = data.getWKBGeometry().getGeometryType();
		}
	}

	public void setGeometryType(WKBGeometryType geoType) {
		this.geoType = geoType;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getSrid() {
		return srid;
	}

	public void setSrid(int srid) {
		this.srid = srid;
	}

	public SpatialResult Clone() {
		SpatialResult result = new SpatialResult();
		result.setSrid(this.getSrid());
		result.setGeometryType(this.getGeometryType());
		for (int i = 0; i < columnNames.size(); i++) {
			result.addColumn(columnNames.get(i), columnTypes.get(i));
		}
		return result;
	}

	public String toJosn() {
		StringBuilder returnVal = new StringBuilder(256);
		boolean isFirst = true;
		Geometry geo = null;
		for (GeometryWithData data : datas) {
			if (isFirst == false) {
				returnVal.append(",");
			}
			returnVal.append("{");
			geo = data.getWKBGeometry();
			if (geo != null
					&& geo.getGeometryType() != WKBGeometryType.WKBGEOMETRYNULL) {
				returnVal.append("\"geom\":\"")
						.append(GeometryToWKT.write(geo)).append("\"");
				isFirst = false;
			} else {
				isFirst = true;
			}
			Map<String, Object> dict = data.getData();
			for (String key : dict.keySet()) {
				if (isFirst == false) {
					returnVal.append(",");
				}
				Object obj = dict.get(key);
				if (obj instanceof String || obj instanceof Long) {
					returnVal.append("\"");
					returnVal.append(MRFUtil.replaceCharsForQuery(key));
					returnVal.append("\":\"");
					returnVal.append(MRFUtil.replaceCharsForQuery(obj
							.toString()));
					returnVal.append("\"");
				} else {
					returnVal.append("\"");
					returnVal.append(MRFUtil.replaceCharsForQuery(key));
					returnVal.append("\":\"");
					returnVal.append(MRFUtil.replaceCharsForQuery(obj
							.toString()));
					returnVal.append("\"");
				}
				isFirst = false;
			}
			returnVal.append("}");
			isFirst = false;
		}
		return returnVal.toString();
	}

	public enum DataType {
		UnknownType, IntType, DoubleType, BoolType, StringType, DateTimeType, BlobType
	}
}
