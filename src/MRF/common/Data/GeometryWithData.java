package MRF.common.Data;

import java.util.HashMap;
import java.util.Map;
import MRF.common.geometry.Geometry;

public class GeometryWithData {
	private MRF.common.geometry.Geometry geometry;
	private Map<String, Object> data;

	public GeometryWithData() {
		this.geometry = new MRF.common.geometry.Geometry();
		this.data = new HashMap<String, Object>();
	}

	public Geometry getWKBGeometry() {
		return geometry;
	}

	public void setWKBGeometry(Geometry geometry) {
		this.geometry = geometry;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public void addColValue(String name, Object obj) {
    	data.put(name.toLowerCase(), obj);
    }

	public boolean isEqual(GeometryWithData other) {
		if (other == this)
			return true;
		if (other == null)
			return false;
		return isEqualToGeometryWithData(other);
	}

	public boolean isEqualToGeometryWithData(GeometryWithData aGeometryWithData) {
		if (aGeometryWithData == this)
			return true;
		if (!this.data.equals(aGeometryWithData.getData()))
			return false;
		return true;
	}
}
