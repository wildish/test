package MRF.common.Data.sqlite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import DataBaseLib.DBFactory;
import DataBaseLib.DBProvider;
import MRF.common.Data.SpatialResult;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import MRF.common.geometry.Geometry;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.util.Log;

public class SpatialDataAccess {

	private static SpatialDataAccess sharedSpatialDataAccess = null;

	public static SpatialDataAccess getInstance() {
		if (sharedSpatialDataAccess == null) {
			sharedSpatialDataAccess = new SpatialDataAccess();
		}
		return sharedSpatialDataAccess;
	}

	private SpatialDataAccess() {
	}

	@SuppressLint("DefaultLocale")
	public boolean loadSQLsFromString(String databaseName, String sqlString) {
		String[] sqlArray = sqlString.split("\\n");
		StringBuilder buffer = new StringBuilder();
		String line = "";
		ArrayList<String> sqls = new ArrayList<String>();
		boolean isSuccess = true;
		try {
			for (int i = 0; i < sqlArray.length; i++) {
				line = sqlArray[i].trim();
				if (StringUtil.isBlank(line)) {
					continue;
				} else if (line.startsWith("#") || line.startsWith("--")) {
					continue;
				} else if (line.endsWith(");")) {
					buffer.append(line);
					sqls.add(buffer.toString());
					buffer = new StringBuilder();
				} else if (line.toLowerCase().startsWith("begintran")) {
					buffer = new StringBuilder();
					if (sqls.size() > 0) {
						isSuccess = this.update(databaseName, sqls);
						sqls.clear();
					}
				} else if (line.toLowerCase().startsWith("endtran")) {
					buffer = new StringBuilder();
					if (sqls.size() > 0) {
						isSuccess = this.update(databaseName, sqls);
						sqls.clear();
					}
				} else {
					buffer.append(line);
				}
			}
		} catch (Exception exception) {
			Log.i("SpatialDataAccess", exception.getMessage());
		} finally {
		}

		if (sqls.size() > 0) {
			isSuccess = this.update(databaseName, sqls); // CommonContent.database.update(sqls);
		}
		return isSuccess;
	}

	@SuppressLint("DefaultLocale")
	public SpatialResult query(String databaseName, String tableName,
			String strCols, String strIdCol, String strGeomCol,
			String strwhere, Geometry queryShape) {
		SpatialResult result = new SpatialResult();
		try {
			StringBuilder sql = new StringBuilder();
			// boolean hasGeomCol = false;
			boolean useSpatialFilter = false;

			// if (!StringUtil.isBlank(strGeomCol)) {
			// hasGeomCol = true;
			// }
			if (!StringUtil.isBlank(strIdCol) && queryShape != null) {
				useSpatialFilter = true;
			}
			if (tableName.equals("t_task")) {
				useSpatialFilter = false;
			}
			if (!StringUtil.isBlank(strCols)) {
				sql.append("SELECT ");
				sql.append(parseColName(strCols, tableName));
			}

			if (!StringUtil.isBlank(strIdCol)) {
				if (sql.length() > 0) {
					sql.append(",");
				} else {
					sql.append("SELECT ");
				}
				sql.append(" ");
				sql.append(parseColName(strIdCol, tableName));
				sql.append(" ");
			}

			if (!StringUtil.isBlank(strGeomCol)) {
				if (sql.length() > 0) {
					sql.append(",");
				} else {
					sql.append("SELECT");
				}
				sql.append(" ");
				sql.append(parseColName(strGeomCol, tableName));
				sql.append(" ");
			}

			if (useSpatialFilter) {

				// sql.append(String.format(" FROM %s ", tableName));
				// sql.append(String.format(" WHERE %s IN (SELECT %s FROM %s WHERE (minx>%f or maxx<%f or miny>%f or maxy<%f) = 0) ",
				// strIdCol,strIdCol,tableName,queryShape.getEnv().maxX,queryShape.getEnv().minX,queryShape.getEnv().maxY,queryShape.getEnv().minY
				// ));

				sql.append(" FROM ");
				sql.append(tableName);
				sql.append(", ");
				sql.append(tableName);
				sql.append("_rtree");
				sql.append(" WHERE ");
				sql.append(tableName);
				sql.append(".");
				sql.append(strIdCol);
				sql.append("=");
				sql.append(tableName);
				sql.append("_rtree.rtree_id AND ");
				sql.append(tableName);
				sql.append("_rtree.rtree_maxx>=");
				sql.append(queryShape.getEnv().minX);
				sql.append(" AND ");
				sql.append(tableName);
				sql.append("_rtree.rtree_minx<=");
				sql.append(queryShape.getEnv().maxX);
				sql.append(" AND ");
				sql.append(tableName);
				sql.append("_rtree.rtree_maxy>=");
				sql.append(queryShape.getEnv().minY);
				sql.append(" AND ");
				sql.append(tableName);
				sql.append("_rtree.rtree_miny<=");
				sql.append(queryShape.getEnv().maxY);

				if (!StringUtil.isBlank(strwhere)) {
					sql.append(" AND ");
					sql.append(MRFUtil.replaceCharsForQuery(strwhere));
				}
			} else if (!StringUtil.isBlank(strwhere)) {
				sql.append(" FROM ");
				sql.append(tableName);
				sql.append(" WHERE ");
				sql.append(MRFUtil.replaceCharsForQuery(strwhere));
				sql.append(" ");
			} else {
				sql.append(" FROM ");
				sql.append(tableName);
			}
			sql.append(";");
			DBProvider database = DBFactory.getDBProvider(databaseName);
			result = database.query(sql.toString(), null, strGeomCol);
			// JSONArray queryResult = database.queryArray(sql.toString());
			// if (queryResult == null || queryResult.length() == 0) {
			// return null;
			// }
			// int geomColIndex = -1;
			//
			// boolean isFirst = true;
			// for (int k = 0; k < queryResult.length(); k++) {
			// JSONObject obj = queryResult.optJSONObject(k);
			// if (isFirst) {
			// if (obj.length() == 0) {
			// break;
			// }
			// @SuppressWarnings("unchecked")
			// Iterator<String> it = obj.keys();
			// int i = 0;
			// while (it.hasNext()) {
			// String column = it.next();
			// result.getColumnNames().add(column);
			// if (hasGeomCol && strGeomCol.compareTo(column) == 0) {
			// geomColIndex = i;
			// break;
			// }
			// i++;
			// }
			// isFirst = false;
			// }
			// GeometryWithData data = new GeometryWithData();
			// @SuppressWarnings("unchecked")
			// Iterator<String> it = obj.keys();
			// int i = 0;
			// while (it.hasNext()) {
			// String columnName = it.next();
			// if (i == geomColIndex) {
			// data.setWKBGeometry(GeometryFromWKB.parse((byte[])obj
			// .opt(columnName)));
			// if(data.getWKBGeometry() != null) {
			// result.setGeometryType(data.getWKBGeometry().getGeometryType());
			// }
			// }
			// if (obj.get(columnName) != null) {
			// if (columnName.toLowerCase().indexOf("time") >= 0
			// || columnName.toLowerCase().indexOf("date") >= 0) {
			// long date = obj.getLong(columnName);
			// data.getData().put(columnName, date);
			// continue;
			// }
			//
			// Object o = obj.get(columnName);
			// if (o instanceof Long) {
			// data.getData().put(columnName,
			// obj.getLong(columnName));
			// } else if (o instanceof Double) {
			// data.getData().put(columnName,
			// obj.getDouble(columnName));
			// } else if (o instanceof String) {
			// data.getData().put(columnName,
			// obj.getString(columnName));
			// }
			// }
			// i++;
			// }
			// if (data.getData().size() > 0
			// || data.getWKBGeometry() != null) {
			// result.getDatas().add(data);
			// }
			// }

			result.setTableName(tableName);

		} catch (Exception exception) {
			Log.i("SpatialDataAccess", exception.getMessage());
		} finally {

		}
		return result;
	}

	private String parseColName(String strCols, String tableName) {

		String[] cols = strCols.split(",");
		StringBuilder buffer = new StringBuilder();
		for (int i = 0, size = cols.length; i < size; i++) {
			if (i > 0) {
				buffer.append(",");
			}
			String colName = cols[i];
			if (colName.contains(".")) {
				buffer.append(colName);
			} else {
				buffer.append(tableName);
				buffer.append(".");
				buffer.append(colName);
			}
		}

		return buffer.toString();
	}

	public boolean update(String databaseName, List<String> sqls) {
		try {
			DBProvider database = DBFactory.getDBProvider(databaseName);
			return database.executeTransaction(sqls);
		} catch (Exception e) {
			return false;
		}
	}

	@SuppressLint("DefaultLocale")
	private int getColumnType(String text, String columnName) {
		text = text.toLowerCase();
		if (columnName.indexOf("time") >= 0 || columnName.indexOf("date") >= 0) {
			return 5;// second from 1970
		} else if (text.indexOf("int") >= 0) {
			return 1;// integer
		} else if (text.startsWith("double") || text.compareTo("real") == 0
				|| text.compareTo("float") == 0) {
			return 2;// double
		} else if (text.indexOf("char") >= 0 || text.compareTo("text") == 0
				|| text.compareToIgnoreCase("CLOB") == 0) {
			return 3;// text
		} else if (text.compareTo("blob") == 0 || text.compareTo("none") == 0) {
			return 4;// blob
		} else {
			return -1;// unknown
		}
	}

	public boolean insertWithGeometry(String databaseName, String tableName,
			String strCols, HashMap<String, Object> strValues, String strIdCol,
			String strGeomCol, Geometry geomVal) {
		try {

			DBProvider database = DBFactory.getDBProvider(databaseName);

			JSONArray cloumns = getCloumnsType(database, tableName);
			if (cloumns == null || cloumns.length() == 0) {
				return false;
			}
			ContentValues contentValues = new ContentValues();
			boolean isValidVal = true;

			for (int i = 0; i < cloumns.length(); i++) {
				JSONObject tableStatement = cloumns.optJSONObject(i);
				isValidVal = true;
				String colName = tableStatement.optString("name");
				if (!strValues.containsKey(colName)) {
					continue;
				}
				int colType = getColumnType(tableStatement.optString("type"),
						colName);
				switch (colType) {
				case 1: {
					if (colName.equals("id") || colName.equals("gid")
							|| colName.equals("guid")) {
						long val = Long.parseLong(strValues.get(colName)
								.toString());
						contentValues.put(colName, val);
						break;
					}
					int val = Integer.parseInt(strValues.get(colName)
							.toString());
					contentValues.put(colName, val);
					break;
				}
				case 2: {
					double val = Double.parseDouble(strValues.get(colName)
							.toString());
					contentValues.put(colName, val);
					break;
				}
				case 3: {
					contentValues.put(colName, strValues.get(colName)
							.toString());
					break;
				}
				case 4: {
					contentValues.put(colName, strValues.get(colName)
							.toString().getBytes());
					break;
				}
				case 5: {
					Long val = Long
							.parseLong(strValues.get(colName).toString());
					contentValues.put(colName, val);
					break;
				}
				default:
					isValidVal = false;
					break;
				}
				if (!isValidVal) {
					continue;
				}
			}
			if (!StringUtil.isBlank(strGeomCol) && geomVal != null) {
				contentValues.put(strGeomCol, geomVal.toWKB());
			}

			long idVal = 0;
			if (!StringUtil.isBlank(strIdCol)) {
				if (strValues.get(strIdCol) != null) {
					idVal = Long.parseLong(String.valueOf(strValues
							.get(strIdCol)));
				} else {
					StringBuffer buffer = new StringBuffer();
					buffer.append("select max(");
					buffer.append(strIdCol);
					buffer.append(") as id from ");
					buffer.append(tableName);
					buffer.append(";");
					String strIdSql = buffer.toString();
					JSONObject idObj = database.first(strIdSql, null);
					if (idObj == null) {
						return false;
					}
					if (idObj.has("id")) {
						idVal = Long.parseLong(idObj.get("id").toString());
					}
					idVal++;
				}
				if (idVal > -1) {
					contentValues.put(strIdCol, idVal);
				}
			}
			database.insert(tableName, contentValues);
			contentValues.clear();
			if (idVal >= 0 && !StringUtil.isBlank(strGeomCol)
					&& geomVal != null) {
				contentValues.put("rtree_id", idVal);
				contentValues.put("rtree_minx", geomVal.getEnv().minX);
				contentValues.put("rtree_miny", geomVal.getEnv().minY);
				contentValues.put("rtree_maxx", geomVal.getEnv().maxX);
				contentValues.put("rtree_maxy", geomVal.getEnv().maxY);
			}
			database.insert(tableName + "_rtree", contentValues);
			StringBuilder sql = new StringBuilder();
			sql.append("update sqlite_sequence set seq = max(seq,");
			sql.append(idVal);
			sql.append(") where name='");
			sql.append(tableName);
			sql.append("';");
			database.execSQL(sql.toString());
			return true;
		} catch (Exception ex) {
			Log.i("SpatialDataAccess",
					"insertWithGeometry():" + ex.getMessage());
		} finally {
		}
		return false;
	}

	private JSONArray getCloumnsType(DBProvider database, String tableName) {
		StringBuilder sql = new StringBuilder();
		sql.append("PRAGMA table_info('");
		sql.append(tableName);
		sql.append("')");
		JSONArray cloumns = database.queryArray(sql.toString(), null);

		return cloumns;
	}

	public boolean updateWithGeometry(String databaseName, String tableName,
			String strCols, HashMap<String, Object> strValues, String strIdCol,
			String strGeomCol, Geometry geomVal) {
		try {
			DBProvider database = DBFactory.getDBProvider(databaseName);

			JSONArray cloumns = getCloumnsType(database, tableName);
			if (cloumns == null || cloumns.length() == 0) {
				return false;
			}
			ArrayList<Object> bindValues = new ArrayList<Object>();

			ContentValues contentValues = new ContentValues();
			boolean isValidVal = true;
			for (int i = 0; i < cloumns.length(); i++) {
				JSONObject tableStatement = cloumns.optJSONObject(i);
				isValidVal = true;
				String colName = tableStatement.optString("name");
				if (!strValues.containsKey(colName)) {
					continue;
				}
				int colType = getColumnType(tableStatement.optString("type"),
						colName);
				switch (colType) {
				case 1: {
					int val = Integer.parseInt(strValues.get(colName)
							.toString());
					bindValues.add(val);
					contentValues.put(colName, val);
					break;
				}
				case 2: {
					double val = Double.parseDouble(strValues.get(colName)
							.toString());
					bindValues.add(val);
					contentValues.put(colName, val);
					break;
				}
				case 3: {
					bindValues.add(strValues.get(colName));
					contentValues.put(colName, strValues.get(colName)
							.toString());
					break;
				}
				case 4: {
					bindValues.add(strValues.get(colName));
					contentValues.put(colName, strValues.get(colName)
							.toString());
					break;
				}
				case 5: {
					Long val = Long
							.parseLong(strValues.get(colName).toString());
					bindValues.add(val);
					contentValues.put(colName, val);
					break;
				}
				default:
					isValidVal = false;
					break;
				}
				if (isValidVal == false)
					continue;
			}

			if (!StringUtil.isBlank(strGeomCol) && geomVal != null) {
				bindValues.add(geomVal.getEnv().minX);
				contentValues.put(strGeomCol, geomVal.toWKT());

				bindValues.add(geomVal.getEnv().minX);
				contentValues.put("minx", geomVal.getEnv().minX);

				bindValues.add(geomVal.getEnv().minY);
				contentValues.put("miny", geomVal.getEnv().minY);

				bindValues.add(geomVal.getEnv().maxX);
				contentValues.put("maxx", geomVal.getEnv().maxX);

				bindValues.add(geomVal.getEnv().maxY);
				contentValues.put("maxy", geomVal.getEnv().maxY);
			}
			long idVal = -1;
			if (!StringUtil.isBlank(strIdCol)) {
				if (strValues.containsKey(strIdCol)
						&& strValues.get(strIdCol) != null) {
					idVal = Long.parseLong(strValues.get(strIdCol).toString());
				}
			}
			if (idVal > -1) {
				StringBuffer buffer = new StringBuffer();
				buffer.append(" ");
				buffer.append(strIdCol);
				buffer.append("=");
				buffer.append(idVal);
				buffer.append(" ;");
				String where = buffer.toString();
				int ref = database.update(tableName, contentValues, where);
				return (ref > 0);
			} else {
				return false;
			}
		} catch (Exception ex) {
			Log.i("SpatialDataAccess",
					"updateWithGeometry():" + ex.getMessage());
		} finally {
		}
		return false;
	}

	public boolean deleteWithGeometry(String databaseName, String tableName,
			String strIdCol, String strIDs) {
		try {
			DBProvider database = DBFactory.getDBProvider(databaseName);
			StringBuffer buffer = new StringBuffer();
			buffer.append(" ");
			buffer.append(strIdCol);
			buffer.append(" in (");
			buffer.append(strIDs);
			buffer.append(")");
			String where = buffer.toString();
			int result = database.delete(tableName, where);
			return (result > 0);
		} catch (Exception ex) {
			Log.i("SpatialDataAccess",
					"deleteWithGeometry():" + ex.getMessage());
		} finally {
		}
		return false;
	}
}
