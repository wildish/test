/*
 *  XmlFileMapTemplateAccess.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;

import org.dom4j.Document;
import org.dom4j.Element;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import MRF.common.Util.CommonContext;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;

/**
 * 
 * @author Admin
 */
public class XmlFileMapTemplateAccess extends MapTemplateBase {
	public static LayerInfo getLayerInfoById(String mapName, String layerId) throws Exception {
		String filePath = Global.getMapTemplateFilePath(mapName);
		Document xmlDoc = CommonContext.getDocumentByPath(filePath);

		String xPath = "/doc/layers/*";
		List<Element> nodeList = MRFUtil.getNodeListByPath(
				xmlDoc.getDocument(), xPath);
		if (nodeList == null || nodeList.size() == 0)
			return null;
		//
		List<LegendLayer> layerList = new ArrayList<LegendLayer>();
		for (int i = 0; i < layerList.size(); i++) {
			Element tNode = nodeList.get(i);
			if (tNode.getName() == "layer"
					&& tNode.attributeValue("displayIconType") != null)
				continue;
			String lyId = tNode.attributeValue("id");
			if (lyId.equals(layerId)) {
				LayerInfo info = new LayerInfo();
				readLayerInfo(tNode, info);

				return info;
			}
		}

		return null;
	}

	protected static void readLayerInfo(Element tLayerNode, LayerInfo layerInfo) {
		layerInfo.setId(tLayerNode.attributeValue("id"));
		layerInfo.setName(tLayerNode.attributeValue("name"));
		// accessible
		if (!StringUtil.isBlank(tLayerNode.attributeValue(
				"Accessibility"))) {
			layerInfo.setAccessibility(StringUtil.stringToBool(tLayerNode.attributeValue("Accessibility"), true));
		}
		layerInfo.setLayerType(tLayerNode.getName());
		layerInfo.setGroupName(tLayerNode.attributeValue("group"));
		layerInfo.setSubGroupName(tLayerNode.attributeValue("subGroup"));
		layerInfo.setTitle(tLayerNode.attributeValue("title"));
		layerInfo.setSourceCrs(tLayerNode.attributeValue("crs"));
		layerInfo.setUomFactor(StringUtil.StringToDouble(
				tLayerNode.attributeValue("uomFactor")));
		if (Double.isNaN(layerInfo.getUomFactor())) {
			layerInfo.setUomFactor(0);
		}
		layerInfo.setVisible(StringUtil.stringToBool(
				tLayerNode.attributeValue("visible"), true));
		layerInfo.setActive(StringUtil.stringToBool(
				tLayerNode.attributeValue("active"), false));
		layerInfo.setIcon(tLayerNode.attributeValue("displayIconType"));
		layerInfo.setScaleLow(StringUtil.StringToDouble(
				tLayerNode.attributeValue("scaleLow")));
		if (Double.isNaN(layerInfo.getScaleLow())) {
			layerInfo.setScaleLow(0);
		}
		layerInfo.setScaleHigh(StringUtil.StringToDouble(
				tLayerNode.attributeValue("scaleHigh")));
		if (Double.isNaN(layerInfo.getScaleHigh())) {
			layerInfo.setScaleHigh(0);
		}
		layerInfo.setHighlight(tLayerNode.attributeValue("highlight"));

		Element schemaNode;
		try {
			schemaNode = MRFUtil.getNodeListByPath(tLayerNode.getDocument(),
					"dataSource/*/schema").get(0);
		} catch (Exception e) {
			schemaNode = null;
		}
		if (schemaNode != null) {
			layerInfo.setSchemaName(schemaNode.getText());
		} else {
			layerInfo.setSchemaName("");
		}

		Element tableNode;
		try {
			tableNode = MRFUtil.getNodeByPath(tLayerNode.getDocument(),
					"dataSource/*/table");
		} catch (Exception e) {
			tableNode = null;
		}
		if (tableNode != null) {
			layerInfo.setTableName(tableNode.getText());
		} else {
			layerInfo.setTableName("");
		}

		Element filedsNode;
		try {
			filedsNode = MRFUtil.getNodeByPath(tLayerNode.getDocument(),
					"dataSource/*/columnlist");
		} catch (Exception e) {
			filedsNode = null;
		}
		if (filedsNode != null) {
			layerInfo.setFields(filedsNode.getText());
		} else {
			layerInfo.setFields("");
		}
	}

	public static LayerDataSource[] getLayerDataSourceList(String mapName,
			String layerId) throws Exception {
		String filePath = Global.getMapTemplateFilePath(mapName);
		Document xmlDoc = CommonContext.getDocumentByPath(filePath);
		//
		String xPath = String.format("/doc/layers/*[@id=\"%s\"]/dataSource/*",
				layerId);
		List<Element> nodeList = MRFUtil.getNodeListByPath(
				xmlDoc.getDocument(), xPath);
		if (nodeList == null || nodeList.size() == 0) {
			return null;
		}

		//
		int idx = 0;
		LayerDataSource[] result = new LayerDataSource[nodeList.size()];
		for (int k = 0; k < nodeList.size(); k++) {
			Element dsNode = nodeList.get(k);
			LayerDataSource layerDs = new LayerDataSource();
			String agent = dsNode.getName() + "Reader";
			if (!dataSourceAgent(agent, layerDs, dsNode)) {
				return null;
			}
			result[idx++] = layerDs;
		}
		return result;
	}

	public static LayerInfo[] getLayerInfoList(String mapName) throws Exception {
		String filePath = Global.getMapTemplateFilePath(mapName);
		Document xmlDoc = CommonContext.getDocumentByPath(filePath);
		//
		String xPath = "/doc/layers/*";
		List<Element> nodeList = MRFUtil.getNodeListByPath(
				xmlDoc.getDocument(), xPath);
		if (nodeList == null || nodeList.size() == 0) {
			return null;
		}
		//
		LayerInfo[] result = new LayerInfo[nodeList.size()];
		for (int k = 0; k < nodeList.size(); k++) {
			Element tNode = nodeList.get(k);
			LayerInfo layerInfo = new LayerInfo();
			readLayerInfo(tNode, layerInfo);
			if (layerInfo.isAccessibility()) {
				continue;
			}
			result[k] = layerInfo;
		}
		return result;
	}

	public static LayerInfo[] getLayerInfoListBySax(String mapName) {
		String filePath = Global.getMapTemplateFilePath(mapName);
		LayerInfo[] liArray;
		try {
			XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser()
					.getXMLReader();
			FileInputStream fis = new FileInputStream(new File(filePath));
			SaxLayerInfoHandler handler = new SaxLayerInfoHandler();
			xmlReader.setContentHandler(handler);
			xmlReader.parse(new InputSource(fis));

			liArray = handler.getLayerList().toArray(
					new LayerInfo[handler.getLayerList().size()]);
		} catch (Exception e) {
			liArray = null;
		}
		return liArray;
	}
}
