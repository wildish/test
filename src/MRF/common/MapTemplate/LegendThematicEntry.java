/*
  *  LegendThematicEntry.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;


/**
 * 
 * @author Admin
 */
public class LegendThematicEntry extends ThematicMapEntry {
	private String icon;

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
