/*
 *  Theme.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author Admin
 */
public class Theme {
	private String symbolName;
    private String scale;
    private String scaleable;
    private String style;
    private String type;
    private String polygonStroke;
    private String polygonStrokeWidth;
    public Theme() {
    	
    }
    public Theme(String json) {
    	if(json == null || json.trim().length() == 0) {
    		return;
    	}
    	JSONObject obj = null;
    	try {
			obj = new JSONObject(json);
		} catch (JSONException e) {
		}
    	if(obj == null) {
    		return;
    	}
    	if(obj.has("SymbolName")) {
			symbolName = obj.optString("SymbolName");
		}
		if(obj.has("Scale")) {
			scale = obj.optString("Scale");
		}
		if(obj.has("Scaleable")) {
			scaleable = obj.optString("Scaleable");
		}
		if(obj.has("Style")) {
			style = obj.optString("Style");
		}
		if(obj.has("Type")) {
			type = obj.optString("Type");
		}
		if(obj.has("PolygonStroke")) {
			polygonStroke = obj.optString("PolygonStroke");
		}
		if(obj.has("PolygonStrokeWidth")) {
			polygonStrokeWidth = obj.optString("PolygonStrokeWidth");
		}
    }
	public String getSymbolName() {
		return symbolName;
	}
	public void setSymbolName(String symbolName) {
		this.symbolName = symbolName;
	}
	public String getScale() {
		return scale;
	}
	public void setScale(String scale) {
		this.scale = scale;
	}
	public String getScaleable() {
		return scaleable;
	}
	public void setScaleable(String scaleable) {
		this.scaleable = scaleable;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPolygonStroke() {
		return polygonStroke;
	}
	public void setPolygonStroke(String polygonStroke) {
		this.polygonStroke = polygonStroke;
	}
	public String getPolygonStrokeWidth() {
		return polygonStrokeWidth;
	}
	public void setPolygonStrokeWidth(String polygonStrokeWidth) {
		this.polygonStrokeWidth = polygonStrokeWidth;
	}
}
