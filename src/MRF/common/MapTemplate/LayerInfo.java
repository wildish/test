/*
 *  LayerInfo.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

/**
 * 
 * @author Admin
 */
public class LayerInfo {
	
	private String id;

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAccessibility() {
		return accessibility;
	}

	public void setAccessibility(boolean accessibility) {
		this.accessibility = accessibility;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getSubGroupName() {
		return subGroupName;
	}

	public void setSubGroupName(String subGroupName) {
		this.subGroupName = subGroupName;
	}

	public String getLayerType() {
		return layerType;
	}

	public void setLayerType(String layerType) {
		this.layerType = layerType;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public double getScaleLow() {
		return scaleLow;
	}

	public void setScaleLow(double scaleLow) {
		this.scaleLow = scaleLow;
	}

	public double getScaleHigh() {
		return scaleHigh;
	}

	public void setScaleHigh(double scaleHigh) {
		this.scaleHigh = scaleHigh;
	}

	public String getSourceCrs() {
		return sourceCrs;
	}

	public void setSourceCrs(String sourceCrs) {
		this.sourceCrs = sourceCrs;
	}

	public double getUomFactor() {
		return uomFactor;
	}

	public void setUomFactor(double uomFactor) {
		this.uomFactor = uomFactor;
	}

	public String getHighlight() {
		return highlight;
	}

	public void setHighlight(String highlight) {
		this.highlight = highlight;
	}

	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}
	private String name;

    private boolean accessibility;

    private String title;

    private String groupName;

    private String subGroupName;

    private String layerType;

    private String icon;

    private boolean visible;

    private boolean active;

    private double scaleLow;

    private double scaleHigh;

    private String sourceCrs;

    private double uomFactor;

    private String highlight;

    private String schemaName;
    
    private String tableName;

    private String fields;
}
