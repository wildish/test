/*
 *  ThemeAttributeValueType.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

/**
 * 
 * @author Admin
 */
public enum ThemeAttributeValueType {
	STRING, COMPOUND, THEMATIC
}
