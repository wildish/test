/*
 *  MapEntry.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

/**
 * 
 * @author Admin
 */
public class MapEntry {
	private String layerId;
	private String attrTag;
	private String key;
	private String value;

	public String getLayerId() {
		return layerId;
	}

	public void setLayerId(String layerId) {
		this.layerId = layerId;
	}

	public String getAttrTag() {
		return attrTag;
	}

	public void setAttrTag(String attrTag) {
		this.attrTag = attrTag;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
