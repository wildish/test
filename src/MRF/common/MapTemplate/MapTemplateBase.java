/*
 *  MapTemplateBase.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import MRF.common.Util.CommonContext;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import MRF.common.Util.XmlUtil;

/**
 * 
 * @author Admin
 */
public abstract class MapTemplateBase {

	protected static boolean dataSourceAgent(String method,
			LayerDataSource layerDs, Element tDataSoureNode) {
		Method agent = CommonContext.getMethod(MapTemplateBase.class, method,
				new Class[] { MapTemplateBase.class });

		if (agent != null) {
			try {
				agent.setAccessible(true);
				agent.invoke(MapTemplateBase.class, tDataSoureNode);
				return true;
			} catch (Exception ex) {
				return false;
			}
		} else {
			if (method.endsWith("Reader")) {
				return defaultDataSourceReader(layerDs, tDataSoureNode);
			} else if (method.endsWith("Writer")) {
				return defaultDataSourceWriter(layerDs, tDataSoureNode);
			}
			return false;
		}
	}

	protected static boolean defaultDataSourceReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		layerDs.setId(XmlUtil.getAttrValue(tDataSourceNode, "id"));
		layerDs.setType(tDataSourceNode.getName());
		List<Element> tNodeList = MRFUtil.getNodeListByPath(tDataSourceNode, "*");
		if (tNodeList != null && tNodeList.size() > 0) {
			layerDs.setAttributes(new HashMap<String, String>());
			for (int k = 0; k < tNodeList.size(); k++) {
				Element tNode = tNodeList.get(k);
				layerDs.getAttributes().put(tNode.getName(),
						tNode.getText());
			}
		}
		return true;
	}

	protected static boolean defaultDataSourceWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		if (!StringUtil.isBlank(layerDs.getId())) {
			tDataSourceNode.addAttribute("id", layerDs.getId());
		}

		if (layerDs.getAttributes() != null
				&& layerDs.getAttributes().size() > 0) {
			Map<String, String> attrs = layerDs.getAttributes();
			Iterator<String> it = attrs.keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				Element tNode = tDataSourceNode.addElement(key);
				if (tNode == null) {
					return false;
				}
				tNode.setText(attrs.get(key));
			}
		}
		return true;
	}

	protected static boolean imageDataSourceReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		layerDs.setId(XmlUtil.getAttrValue(tDataSourceNode, "id"));
		layerDs.setType(tDataSourceNode.getName());
		layerDs.setAttributes(new HashMap<String, String>());
		layerDs.getAttributes().put("origin",
				XmlUtil.readAttrXY(tDataSourceNode, "origin"));
		layerDs.getAttributes().put("size",
				XmlUtil.readAttrXY(tDataSourceNode, "size"));
		layerDs.getAttributes().put("multiplier",
				XmlUtil.readAttrXY(tDataSourceNode, "multiplier"));
		layerDs.getAttributes().put("quality",
				XmlUtil.getNodeValue(tDataSourceNode, "quality"));
		layerDs.getAttributes().put("path",
				XmlUtil.getNodeValue(tDataSourceNode, "path"));
		return true;
	}

	protected static boolean imageDataSourceWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		if (!StringUtil.isBlank(layerDs.getId())) {
			tDataSourceNode.addAttribute("id", layerDs.getId());
		}

		if (layerDs.getAttributes() == null) {
			return true;
		}
		if (layerDs.getAttributes().containsKey("origin")) {
			XmlUtil.writeAttrXY(tDataSourceNode, "origin", layerDs
					.getAttributes().get("origin"));
		}

		if (layerDs.getAttributes().containsKey("size")) {
			XmlUtil.writeAttrXY(tDataSourceNode, "size", layerDs
					.getAttributes().get("size"));
		}

		if (layerDs.getAttributes().containsKey("multiplier")) {
			XmlUtil.writeAttrXY(tDataSourceNode, "multiplier", layerDs
					.getAttributes().get("multiplier"));
		}
		//
		Element tNode = null;
		if (layerDs.getAttributes().containsKey("quality")) {
			tNode = tDataSourceNode.addElement("quality");
			if (tNode == null) {
				return false;
			}
			tNode.setText(layerDs.getAttributes().get("quality"));
		}

		if (layerDs.getAttributes().containsKey("path")) {
			tNode = tDataSourceNode.addElement("path");
			if (tNode == null) {
				return false;
			}
			tNode.setText(layerDs.getAttributes().get("path"));
		}
		return true;
	}

	protected static boolean ecwimageReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return imageDataSourceReader(layerDs, tDataSourceNode);
	}

	protected static boolean ecwimageWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return imageDataSourceWriter(layerDs, tDataSourceNode);
	}

	protected static boolean dgcswmsimageReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return imageDataSourceReader(layerDs, tDataSourceNode);
	}

	protected static boolean dgcswmsimageWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return imageDataSourceWriter(layerDs, tDataSourceNode);
	}

	protected static boolean mrsidimageReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return imageDataSourceReader(layerDs, tDataSourceNode);
	}

	protected static boolean mrsidimageWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return imageDataSourceWriter(layerDs, tDataSourceNode);
	}

	protected static boolean databaseDataSourceReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		layerDs.setId(XmlUtil.getAttrValue(tDataSourceNode, "id"));
		layerDs.setType(tDataSourceNode.getName());
		layerDs.setAttributes(new HashMap<String, String>());
		layerDs.getAttributes().put("connector",
				XmlUtil.getNodeValue(tDataSourceNode, "connector"));
		layerDs.getAttributes().put("idcol",
				XmlUtil.getNodeValue(tDataSourceNode, "idcol"));
		layerDs.getAttributes().put("geomcol",
				XmlUtil.getNodeValue(tDataSourceNode, "geomcol"));
		layerDs.getAttributes().put("columnlist",
				XmlUtil.getNodeValue(tDataSourceNode, "columnlist"));
		layerDs.getAttributes().put("where",
				XmlUtil.getNodeValue(tDataSourceNode, "where"));
		layerDs.getAttributes().put("table",
				XmlUtil.getNodeValue(tDataSourceNode, "table"));
		layerDs.getAttributes().put("query",
				XmlUtil.getNodeValue(tDataSourceNode, "query"));
		return true;
	}

	protected static boolean databaseDataSourceWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		if (!StringUtil.isBlank(layerDs.getId())) {
			tDataSourceNode.addAttribute("id", layerDs.getId());
		}
		//
		Element tNode = null;
		if (layerDs.getAttributes().containsKey("connector")
				&& !StringUtil
						.isBlank(layerDs.getAttributes().get("connector"))) {
			tNode = tDataSourceNode.addElement("connector");
			if (tNode == null)
				return false;
			tNode.setText(layerDs.getAttributes().get("connector"));
		}

		if (layerDs.getAttributes().containsKey("idcol")
				&& !StringUtil.isBlank(layerDs.getAttributes().get("idcol"))) {
			tNode = tDataSourceNode.addElement("idcol");
			if (tNode == null)
				return false;
			tNode.setText(layerDs.getAttributes().get("idcol"));
		}

		if (layerDs.getAttributes().containsKey("geomcol")
				&& !StringUtil.isBlank(layerDs.getAttributes().get("geomcol"))) {
			tNode = tDataSourceNode.addElement("geomcol");
			if (tNode == null)
				return false;
			tNode.setText(layerDs.getAttributes().get("geomcol"));
		}

		if (layerDs.getAttributes().containsKey("columnlist")
				&& !StringUtil.isBlank(layerDs.getAttributes()
						.get("columnlist"))) {
			tNode = tDataSourceNode.addElement("columnlist");
			if (tNode == null)
				return false;
			tNode.setText(layerDs.getAttributes().get("columnlist"));
		}

		if (layerDs.getAttributes().containsKey("where")
				&& !StringUtil.isBlank(layerDs.getAttributes().get("where"))) {
			tNode = tDataSourceNode.addElement("where");
			if (tNode == null)
				return false;
			tNode.setText(layerDs.getAttributes().get("where"));
		}

		if (layerDs.getAttributes().containsKey("table")
				&& !StringUtil.isBlank(layerDs.getAttributes().get("table"))) {
			tNode = tDataSourceNode.addElement("table");
			if (tNode == null)
				return false;
			tNode.setText(layerDs.getAttributes().get("table"));
		}

		if (layerDs.getAttributes().containsKey("query")
				&& !StringUtil.isBlank(layerDs.getAttributes().get("query"))) {
			tNode = tDataSourceNode.addElement("query");
			if (tNode == null)
				return false;
			tNode.setText(layerDs.getAttributes().get("query"));
		}
		return true;
	}

	protected static boolean oracleDataSourceReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return databaseDataSourceReader(layerDs, tDataSourceNode);
	}

	protected static boolean oracleDataSourceWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return databaseDataSourceWriter(layerDs, tDataSourceNode);
	}

	protected static boolean sdeDataSourceReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return databaseDataSourceReader(layerDs, tDataSourceNode);
	}

	protected static boolean sdeDataSourceWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		return databaseDataSourceWriter(layerDs, tDataSourceNode);
	}

	protected static boolean wmsDataSourceReader(LayerDataSource layerDs,
			Element tDataSourceNode) {
		layerDs.setId(XmlUtil.getAttrValue(tDataSourceNode, "id"));
		layerDs.setType(tDataSourceNode.getName());
		layerDs.setAttributes(new HashMap<String, String>());
		layerDs.getAttributes().put("wmsService",
				XmlUtil.getNodeValue(tDataSourceNode, "wmsService"));

		if (tDataSourceNode.getNodeType() == Element.ELEMENT_NODE) {
			Element el = tDataSourceNode;
			List<Element> nl = MRFUtil.getchildListByName(el, "layers");
			if (nl != null && nl.size() > 0) {
				layerDs.getAttributes().put("layers",
						XmlUtil.getAttrValue(nl.get(0), "name"));
			}
		}
		return true;
	}

	protected static boolean wmsDataSourceWriter(LayerDataSource layerDs,
			Element tDataSourceNode) {
		if (layerDs.getAttributes().containsKey("id")
				&& !StringUtil.isBlank(layerDs.getId())) {
			tDataSourceNode.addAttribute("id", layerDs.getId());
		}
		//
		Element tNode = null;
		if (layerDs.getAttributes().containsKey("wmsService")
				&& !StringUtil.isBlank(layerDs.getAttributes()
						.get("wmsService"))) {
			tNode = tDataSourceNode.addElement("wmsService");
			if (tNode == null)
				return false;
			tNode.setText(layerDs.getAttributes().get("wmsService"));
		}

		if (layerDs.getAttributes().containsKey("layers")
				&& !StringUtil.isBlank(layerDs.getAttributes().get("layers"))) {
			tNode = tDataSourceNode.addElement("layers");
			if (tNode == null) {
				return false;
			}
			tNode.addAttribute("name",
					layerDs.getAttributes().get("layers"));
		}
		return true;
	}
}
