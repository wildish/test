package MRF.common.MapTemplate;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import MRF.common.Util.StringUtil;

public class Global {
	public static String DefaultDataSourceName = "client";
	// public static String MapTemplateDir = "Config/MapTemplate";
	// public static String UserProfileDir = "Config/UserProfile";
	// public static boolean EnableUserProfile = true;
	// public static boolean EnableUserTemplate = false;
	private static HashMap<String, String> mapViews = new HashMap<String, String>();
	public static String MapXmlType = "svg";

	public static String getMapTemplateFilePath(String szMapViewName) {
		if (szMapViewName.endsWith(".map")) {
			return szMapViewName;
		}
		String rtn = "";
		synchronized (mapViews) {
			if (!StringUtil.isBlank(szMapViewName)
					&& mapViews.containsKey(szMapViewName)) {
				rtn = mapViews.get(szMapViewName);
			} else {
				rtn = "";
			}
		}
		if (rtn != null && rtn.trim().length() > 0) {
			rtn = String.format("%s%s%s%s%s%s%s", GlobalApp.getRootPath(),
					File.separator, WebRequestHandle.Util.Global.TaskDir
							.get(WebRequestHandle.Util.Global.CurrentTaskId),
					File.separator, "TempTemplate", File.separator, rtn);
		}
		return rtn;
	}

	public static void addMapTemplate(String szMapViewName, String filePath) {
		synchronized (mapViews) {
			mapViews.put(szMapViewName, filePath);
		}
	}

	private static void initMapTemplate(String dir) {
		// DefaultDataSourceName
		Global.DefaultDataSourceName = ConfigurationManager.AppSettings
				.get("DefaultDataSourceName");

		// MapTemplateDir
		// StringBuffer buffer = new StringBuffer();
		// buffer.append(dir);
		// buffer.append("/");
		// buffer.append(ConfigurationManager.AppSettings.get("MapTemplateDir"));
		// Global.MapTemplateDir = buffer.toString();

		// user profile
		// Global.EnableUserProfile = Boolean
		// .parseBoolean(ConfigurationManager.AppSettings
		// .get("EnableUserProfile"));
		// if (Global.EnableUserProfile) {
		// Global.EnableUserTemplate = Boolean
		// .parseBoolean(ConfigurationManager.AppSettings
		// .get("EnableUserTemplate"));
		// buffer = new StringBuffer();
		// buffer.append(dir);
		// buffer.append("/");
		// buffer.append(ConfigurationManager.AppSettings
		// .get("UserProfileDir"));
		// Global.UserProfileDir = buffer.toString();
		//
		// if (!new File(Global.UserProfileDir).exists()) {
		// new File(Global.UserProfileDir).mkdirs();
		// }
		// }
		//
		Iterator<String> it = ConfigurationManager.TemplateSettings.keySet()
				.iterator();
		while (it.hasNext()) {
			String mapViewName = it.next();
			String path = ConfigurationManager.TemplateSettings
					.get(mapViewName);
			// if (path.length() < 2 || path.charAt(1) != ':') {
			// buffer = new StringBuffer();
			// buffer.append(Global.MapTemplateDir);
			// buffer.append("/");
			// buffer.append(path);
			// path = buffer.toString();
			// }
			Global.addMapTemplate(mapViewName, path);
		}
	}

	public static void applicationStart(String path) {
		// Init Map Templates
		initMapTemplate(path);
	}
}
