package MRF.common.MapTemplate;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import MRF.common.Util.StringUtil;

public class SaxLayerInfoHandler extends DefaultHandler {
    private List<LayerInfo> layerList;
    private String tempVal;
    private LayerInfo tempLayer;

    public SaxLayerInfoHandler() {
        layerList = new ArrayList<LayerInfo>();
    }

    public List<LayerInfo> getLayerList() {
        return layerList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrs) throws SAXException {
        tempVal = "";
        if (qName.equalsIgnoreCase("layer") || qName.equalsIgnoreCase("pointlayer") || qName.equalsIgnoreCase("annotationlayer")
                || qName.equalsIgnoreCase("linestringlayer") || qName.equalsIgnoreCase("polygonlayer") || qName.equalsIgnoreCase("imagelayer")) {
            tempLayer = new LayerInfo();
            tempLayer.setId(getAttrValue(attrs, "id"));
            tempLayer.setId(getAttrValue(attrs, "name"));
            tempLayer.setAccessibility(getAttrValue(attrs, "Accessibility", false));
            tempLayer.setLayerType(qName);
            tempLayer.setGroupName(getAttrValue(attrs, "group"));
            tempLayer.setGroupName(getAttrValue(attrs, "subGroup"));
            tempLayer.setSourceCrs(getAttrValue(attrs, "crs"));
            tempLayer.setUomFactor(getAttrValue(attrs, "uomFactor", 0d));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        tempVal = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
    	if ("layer".equalsIgnoreCase(qName)
				|| "pointlayer".equalsIgnoreCase(qName)
				|| "annotationlayer".equalsIgnoreCase(qName)
				|| "linestringlayer".equalsIgnoreCase(qName)
				|| "polygonlayer".equalsIgnoreCase(qName)
				|| "imagelayer".equalsIgnoreCase(qName))
			layerList.add(tempLayer);
		if ("title".equalsIgnoreCase(qName))
			tempLayer.setTitle(tempVal);

		if ("visible".equalsIgnoreCase(qName))
			tempLayer.setVisible(StringUtil.stringToBool(tempVal, true));

		if ("active".equalsIgnoreCase(qName))
			tempLayer.setActive(StringUtil.stringToBool(tempVal, true));

		if ("displayicontype".equalsIgnoreCase(qName))
			tempLayer.setIcon(tempVal);

		if ("scalelow".equalsIgnoreCase(qName))
			tempLayer.setScaleLow(getNodeValue(tempVal, 0d));

		if ("scalehigh".equalsIgnoreCase(qName))
			tempLayer.setScaleHigh(getNodeValue(tempVal, 0d));

		if ("table".equalsIgnoreCase(qName))
			tempLayer.setTableName(tempVal);

		if ("columnlist".equalsIgnoreCase(qName))
			tempLayer.setFields(tempVal);
//        switch(qName.toLowerCase()) {
//            case "layer":
//            case "pointlayer":
//            case "annotationlayer":
//            case "linestringlayer":
//            case "polygonlayer":
//            case "imagelayer":
//                layerList.add(tempLayer);
//                break;
//            case "title":
//                tempLayer.setTitle(tempVal);
//                break;
//            case "visible":
//                tempLayer.setVisible(StringUtil.stringToBool(tempVal, true));
//                break;
//            case "active":
//                tempLayer.setActive(StringUtil.stringToBool(tempVal, true));
//                break;
//            case "displayicontype":
//                tempLayer.setIcon(tempVal);
//                break;
//            case "scalelow":
//                tempLayer.setScaleLow(getNodeValue(tempVal, 0d));
//                break;
//            case "scalehigh":
//                tempLayer.setScaleHigh(getNodeValue(tempVal, 0d));
//                break;
//            case "table":
//                tempLayer.setTableName(tempVal);
//                break;
//            case "columnlist":
//                tempLayer.setFields(tempVal);
//                break;
//        }
    }

    private Double getNodeValue(String value, Double defaultValue) {
        Double result = Double.NaN;
        if(!StringUtil.isBlank(value)) {
            result = StringUtil.StringToDouble(value);
        }
        if(Double.isNaN(result)) {
            result = defaultValue;
        }
        return result;
    }

    private String getAttrValue(Attributes attrs, String name) {
        String result = "";
        int i = -1;
        if( (i = attrs.getIndex(name)) > -1) {
            result = attrs.getValue(i);
        }

        return result;
    }

    private Boolean getAttrValue(Attributes attrs, String name, boolean defaultValue) {
        boolean result = defaultValue;
        int i = -1;
        if( (i = attrs.getIndex(name)) > -1) {
            result = StringUtil.stringToBool(attrs.getValue(i), defaultValue);
        }
        return result;
    }

    private Double getAttrValue(Attributes attrs, String name, Double defaultValue) {
        Double result = Double.NaN;
        int i = -1;
        if( (i = attrs.getIndex(name)) > -1) {
            result = StringUtil.StringToDouble(attrs.getValue(i));
        }
        if(Double.isNaN(result)) {
            result = defaultValue;
        }
        return result;
    }
}