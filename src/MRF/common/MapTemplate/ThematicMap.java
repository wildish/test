/*
 *  ThematicMap.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

/**
 * 
 * @author Admin
 */
public class ThematicMap {
	private ThemeAttribute keySource;

	private boolean fieldAsNumber;

	private String compareOperator;

	private ThematicMapEntry[] entries;

	public ThemeAttribute getKeySource() {
		return keySource;
	}

	public void setKeySource(ThemeAttribute keySource) {
		this.keySource = keySource;
	}

	public boolean isFieldAsNumber() {
		return fieldAsNumber;
	}

	public void setFieldAsNumber(boolean fieldAsNumber) {
		this.fieldAsNumber = fieldAsNumber;
	}

	public String getCompareOperator() {
		return compareOperator;
	}

	public void setCompareOperator(String compareOperator) {
		this.compareOperator = compareOperator;
	}

	public ThematicMapEntry[] getEntries() {
		return entries;
	}

	public void setEntries(ThematicMapEntry[] entries) {
		this.entries = entries;
	}
}
