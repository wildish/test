/*
 *  LegendLayer.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

/**
 * 
 * @author Admin
 */
public class LegendLayer extends LayerInfo {
	private LegendThematic[] ThematicList;

	public LegendThematic[] getThematicList() {
		return ThematicList;
	}

	public void setThematicList(LegendThematic[] thematicList) {
		ThematicList = thematicList;
	}
}
