/*
 *  LayerTheme.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

/**
 * 
 * @author Admin
 */
public class LayerTheme {
	private String themeName;

	private ThemeAttribute[] themeAttributes;

	private ThemeAttribute style;

	private ThemeAttribute[] featureAttributes;

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public ThemeAttribute[] getThemeAttributes() {
		return themeAttributes;
	}

	public void setThemeAttributes(ThemeAttribute[] themeAttributes) {
		this.themeAttributes = themeAttributes;
	}

	public ThemeAttribute getStyle() {
		return style;
	}

	public void setStyle(ThemeAttribute style) {
		this.style = style;
	}

	public ThemeAttribute[] getFeatureAttributes() {
		return featureAttributes;
	}

	public void setFeatureAttributes(ThemeAttribute[] featureAttributes) {
		this.featureAttributes = featureAttributes;
	}
}
