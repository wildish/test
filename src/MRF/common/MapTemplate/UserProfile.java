package MRF.common.MapTemplate;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;

import MRF.common.Util.CommonContext;
import MRF.common.Util.FileUtil;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import MRF.common.Util.XmlUtil;

public class UserProfile {
	public static String createProfile2(String szMapViewName, String userId,
			String srcMapViewName) {
		// if (!Global.EnableUserProfile)
		// return "";
		//
		// String profileDir = String.format("%s/%s", Global.UserProfileDir,
		// userId);
		// try {
		// File file = new File(profileDir);
		// if (!file.exists()) {
		// file.mkdirs();
		// }
		//
		// String srcPath = Global.getMapTemplateFilePath(szMapViewName);
		// if (StringUtil.isBlank(srcPath)) {
		// //
		// Log.GetInstance().Write(String.Format("Failed to get '%s' map template file!",
		// // srcMapViewName));
		// return "";
		// }
		// String fileName = srcPath.substring(srcPath.lastIndexOf("/") + 1);
		// String profileFile = String.format("%s/%s", profileDir, fileName);
		// file = new File(profileFile);
		// if (file.exists()) {
		// // UpdateAccessibilityByFile(profileFile, srcPath);
		// } else {
		// copyMapTemplate(szMapViewName, profileDir);
		// }
		// //
		// String userProfileName = getUserProfile(szMapViewName, userId);
		// Global.addMapTemplate(userProfileName, profileFile);
		// return userProfileName;
		// } catch (Exception ex) {
		// // _logger.Error(ex.ToString());
		// }
		// //
		return "";
	}

	public static String getUserProfile(String szViewName, String userId) {
		return String.format("p$%s$%s", szViewName, userId);
	}

	@SuppressWarnings("unused")
	private static String copyMapTemplate(String srcMapViewName, String destDir) {
		String srcPath = Global.getMapTemplateFilePath(srcMapViewName);
		String fileName = srcPath.substring(srcPath.lastIndexOf('/') + 1);
		String destPath = String.format("%s/%s", destDir, fileName);
		File file = new File(destPath);
		if (!file.exists()) {
			FileUtil.copyFileTo(new File(srcPath), new File(destPath));
		}

		if (!file.canWrite()) {
			file.setWritable(true);
		}
		// if ((File.GetAttributes(destPath) & FileAttributes.ReadOnly) ==
		// FileAttributes.ReadOnly) {
		// File.SetAttributes(destPath, FileAttributes.Normal);
		// }
		//
		return destPath;
	}

	public static boolean updateAccessiblityByLayers(String mapFile,
			String[] accessibeLayers, String[] visibleLayers) throws Exception {
		// parse accessibeLayers -- get all visibe layer
		HashMap<String, String> visibleLayerDict = null;
		if (visibleLayers != null) {
			visibleLayerDict = new HashMap<String, String>();
			for (String item : visibleLayers) {
				visibleLayerDict.put(item, null);
			}
		}

		HashMap<String, List<String>> userLayers = new HashMap<String, List<String>>();
		if (accessibeLayers != null) {
			for (int i = 0; i < accessibeLayers.length; i++) {
				String layer = accessibeLayers[i];
				String layerId = "";
				String accessTips = "";
				int idx = layer.indexOf(":");
				if (idx != -1) {
					layerId = layer.substring(0, idx);
					accessTips = layer.substring(idx + 1);
				} else {
					layerId = layer;
				}

				List<String> accessTipList = new ArrayList<String>();
				if (!StringUtil.isBlank(accessTips)) {
					String[] accessTipArray = accessTips.split(",");
					for (String s : accessTipArray) {
						if (!StringUtil.isBlank(s)) {
							accessTipList.add(XmlUtil.escapeXmlChars(s));
						}
					}
				}
				if (!userLayers.containsKey(layerId)) {
					userLayers.put(layerId, accessTipList);
				}
			}
		}

		Document xmlDoc = CommonContext.getDocumentByPath(mapFile);
		String xPath = "/doc/layers/*";
		List<Element> nodeList = MRFUtil.getNodeListByPath(
				xmlDoc.getDocument(), xPath);
		final String ALLFLAG = "$All$";
		for (int k = 0; k < nodeList.size(); k++) {
			Element layerNode = nodeList.get(k);
			String layerId = XmlUtil.getAttrValue(layerNode, "id");
			if (userLayers.containsKey(layerId)) {
				List<String> fields = userLayers.get(layerId);
				if (fields != null && fields.size() > 0
						&& !fields.get(0).equals(ALLFLAG)) {
					// accessibe: deal tip
					Element tipFeatureNode = MRFUtil.getNodeByPath(layerNode,
							"featureAttribute[@tag=\"tip\"]");
					if (tipFeatureNode != null) {
						List<Element> tipNodeList = MRFUtil.getchildListByName(
								tipFeatureNode, "field");
						for (int n = 0; n < tipNodeList.size(); n++) {
							Element tipNode = tipNodeList.get(n);
							// unaccessibe tip
							if (!fields.contains(tipNode.getText())) {
								// XmlUtil.createAttribute(
								// tipNode.getPreviousSibling(),
								// "Accessibility", "false");
								tipNode.addAttribute("Accessibility", "false");
							}
						}
					}
				}

				//
				layerNode.addAttribute("Accessibility", "true");

				// layer visible in map
				if (visibleLayerDict != null) {
					Element nodeVisible = MRFUtil.getChildByName(layerNode,
							"visible");
					if (nodeVisible == null) {
						layerNode.addElement("visible");
					}
					if (visibleLayerDict.containsKey(layerId)) {
						nodeVisible.setText("true");
					} else {
						nodeVisible.setText("false");
					}
				}
			} else {
				// unaccessibe:set attribute
				layerNode.addAttribute("Accessibility", "false");
			}
		}

		// TODO xmlDoc.save(mapFile);
		return true;
	}
}
