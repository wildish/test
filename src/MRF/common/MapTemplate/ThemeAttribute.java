/*
 *  ThemeAttribute.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

/**
 * 
 * @author Admin
 */
public class ThemeAttribute {
	private String name;

	private boolean nameAsTag;

	private ThemeAttributeValueType valueType;

	private String stringValue;

	private CompoundToken[] compoundTokens;

	private ThematicMap thematicMap;
	
	public ThemeAttribute() {
		valueType = ThemeAttributeValueType.STRING;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isNameAsTag() {
		return nameAsTag;
	}

	public void setNameAsTag(boolean nameAsTag) {
		this.nameAsTag = nameAsTag;
	}

	public ThemeAttributeValueType getValueType() {
		return valueType;
	}

	public void setValueType(ThemeAttributeValueType valueType) {
		this.valueType = valueType;
	}

	public String getStringValue() {
		return stringValue;
	}

	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}

	public CompoundToken[] getCompoundTokens() {
		return compoundTokens;
	}

	public void setCompoundTokens(CompoundToken[] compoundTokens) {
		this.compoundTokens = compoundTokens;
	}

	public ThematicMap getThematicMap() {
		return thematicMap;
	}

	public void setThematicMap(ThematicMap thematicMap) {
		this.thematicMap = thematicMap;
	}
}
