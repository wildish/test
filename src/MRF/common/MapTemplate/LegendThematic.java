/*
 *  LegendLayer.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.MapTemplate;

/**
 * 
 * @author Admin
 */
public class LegendThematic {
	private String Name;

	private LegendThematicEntry[] Entries;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public LegendThematicEntry[] getEntries() {
		return Entries;
	}

	public void setEntries(LegendThematicEntry[] entries) {
		Entries = entries;
	}
}
