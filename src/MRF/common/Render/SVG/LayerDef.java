package MRF.common.Render.SVG;

import java.io.Serializable;

public class LayerDef implements Serializable{
	private static final long serialVersionUID = 1L;
	public String Tid;
	public String LayerId;
	public String GroupName;
	public String SubGroupName;
	public boolean Accessibility;
	public String Crs;
	public String Type;
	public int DatasetDefId;
	public String Name;
	public boolean EnableHighlight;
	public String Highlight;
	public String DisplayIconType;
	public boolean Visible;
	public String Style;
	public String Symbol;
	public String SymbolStyle;
	public String Thematics;
	public String DomId;
	public String Tip;
	public String DataSource;
	public boolean Active;
	public double ScaleLow;
	public double ScaleHigh;
	public String TextConfigure;
	public String Title;
	public String Images;
	public String GoogleInfor;
	public String Keyfield;
	public String Contents;
}
