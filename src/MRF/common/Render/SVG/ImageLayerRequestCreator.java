package MRF.common.Render.SVG;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.dom4j.Element;

import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import MRF.common.geometry.Point;
import MRF.common.geometry.PointF;
import MRF.common.geometry.Rect;
import MRF.common.geometry.RectF;

public class ImageLayerRequestCreator extends LayerRequestCreator {

	public ImageLayerRequestCreator(BuildMapParam runtimeParam, String szMapId,
			Element tLayer, String strLayerParam) {
		super(runtimeParam, szMapId, tLayer, strLayerParam);
	}

	private double[] resolutions = { 156543.033900000, 78271.516950000,
			39135.758475000, 19567.879237500, 9783.939618750, 4891.969809375,
			2445.984904688, 1222.992452344, 611.496226172, 305.748113086,
			152.874056543, 76.437028271, 38.218514136, 19.109257068,
			9.554628534, 4.777314267, 2.388657133, 1.194328567, 0.597164283 };

	@Override
	public void doBuild() {
		try {
			if (!inScale()) {
				return;
			}

			beginBuild();
			m_tResult.append(RotateUtil.generateRotate(m_tBuildParam));
			m_tResult.append(">");

			if (this.m_tLayer.attribute("id") == null
					|| StringUtil.isBlank(this.m_tLayer.attributeValue("id"))) {
				return;
			}
			Element tTitle = MRFUtil.getChildByName(m_tLayer, "title");
			if (tTitle != null) {
				String title = MRFUtil.xmlToString(tTitle);
				// SVGElementInsertor titleInsertor = new
				// SVGElementInsertor(InsertorType.TextInsertor,
				// String.format("%s", title), "");
				// m_tInsertorArray.Add(titleInsertor);
				m_tResult.append(title);
			}

			List<Element> tImages = null;
			List<Element> tImagesList = MRFUtil.getchildListByName(m_tLayer,
					"images");
			if (tImagesList != null && tImagesList.size() > 0) {
				Element tImagesElement = tImagesList.get(0);
				Element openstreetmap = MRFUtil.getChildByName(tImagesElement,
						"openstreetmap");
				if (openstreetmap != null) {
					initOpenStreetMap(openstreetmap);
				} else {
					List<Element> tScales = MRFUtil.getchildListByName(
							tImagesElement, "scale");
					if (tScales != null && tScales.size() > 0) {
						for (int i = 0; i < tScales.size(); i++) {
							Element tScale = tScales.get(i);
							if (tScale.attribute("scaleHigh") != null
									&& tScale.attribute("scaleLow") != null) {
								String scaleHeightStr = tScale
										.attributeValue("scaleHigh");
								String scaleLowStr = tScale
										.attributeValue("scaleLow");
								double scaleHeight = Double.NaN;
								try {
									scaleHeight = Double
											.parseDouble(scaleHeightStr);
								} catch (Exception e) {
									continue;
								}
								double scaleLow = Double.NaN;
								try {
									scaleLow = Double.parseDouble(scaleLowStr);
								} catch (Exception e) {
									continue;
								}
								if (m_dScale > scaleLow
										&& m_dScale < scaleHeight) {
									tImages = MRFUtil.getchildListByName(
											tScale, "mrftileimage");
								}
							}
						}
					} else {
						tImages = MRFUtil.getchildListByName(tImagesElement,
								"mrftileimage");
					}
				}
			}
			if (tImages != null && tImages.size() > 0) {
				for (int i = 0; i < tImages.size(); i++) {
					Element tImage = tImages.get(i);
					mrftileimage(tImage);
				}
			}
			/*
			 * Rect sourceRect = new Rect((int)m_tBuildParam.left,
			 * (int)m_tBuildParam.top, (int)(m_tBuildParam.right),
			 * (int)m_tBuildParam.bottom); int level = -1; double tScale =
			 * Math.Max(Math.Abs(sourceRect.Width) / 256.0,
			 * Math.Abs(sourceRect.Height) / 256.0); double resultDistance =
			 * Double.MaxValue; for (int i = 0; i < 19; i++) { double
			 * curDistance = Math.Abs(resolutions[i] - tScale); if (curDistance
			 * < resultDistance) { level = i; resultDistance = curDistance; } }
			 * if (level < 0) { level = 18; } double scaleX = resolutions[level]
			 * / m_tBuildParam.m_dWorldPerPixelX; double scaleY =
			 * resolutions[level] / m_tBuildParam.m_dWorldPerPixelY;
			 * OutputOpenStreetMapImageUrl(sourceRect, new Point(
			 * (int)m_tBuildParam.left, (int)m_tBuildParam.top), 256, 256 *
			 * resolutions[level], scaleX, scaleY, level);
			 */
			endBuild();
		} catch (Exception ex) {
			ex.printStackTrace();
			m_tResult.setLength(0);
		}
	}

	private void initOpenStreetMap(Element tImage) {
		List<String> nodes = new ArrayList<String>();
		List<Element> paths = MRFUtil.getchildListByName(tImage, "path");
		for (Element node : paths) {
			nodes.add(node.getTextTrim());
		}

		RectF sourceRect = new RectF((float) m_tBuildParam.left,
				(float) m_tBuildParam.top, (float) m_tBuildParam.right,
				(float) m_tBuildParam.bottom);
		int level = -1;
		double tScale = Math.max(Math.abs(sourceRect.width()) / 256.0,
				Math.abs(sourceRect.height()) / 256.0);
		double resultDistance = Double.MAX_VALUE;
		for (int i = 0; i < 19; i++) {
			double curDistance = Math.abs(resolutions[i] - tScale);
			if (curDistance < resultDistance) {
				level = i;
				resultDistance = curDistance;
			}
		}
		if (level < 0) {
			level = 18;
		}
		double scaleX = resolutions[level] / m_tBuildParam.m_dWorldPerPixelX;
		while (scaleX > 1) {
			level++;
			if (level > 18) {
				level = 18;
				break;
			}
			scaleX = resolutions[level] / m_tBuildParam.m_dWorldPerPixelX;
		}
		double scaleY = resolutions[level] / m_tBuildParam.m_dWorldPerPixelY;
		PointF displayArea = new PointF((float) m_tBuildParam.left,
				(float) m_tBuildParam.top);
		outputImageUrlInfo(sourceRect, displayArea, 256,
				256 * resolutions[level], scaleX, scaleY, level, nodes);
	}

	protected void outputImageUrlInfo(RectF sourceRect, PointF displayArea,
			int nTileSize, double tileUnitWidth, double scaleX, double scaleY,
			int level, List<String> nodes) {
		// rol & col number
		int firstCol = (int) Math.floor((sourceRect.left + 20037508.342789)
				/ tileUnitWidth);
		int firstRow = (int) Math.floor((20037508.342789 - sourceRect.top)
				/ tileUnitWidth);

		int lastCol = (int) Math.ceil((sourceRect.left
				+ Math.abs(sourceRect.width()) + 20037508.342789)
				/ tileUnitWidth);
		int lastRow = (int) Math.ceil((20037508.342789 - sourceRect.top + Math
				.abs(sourceRect.height())) / tileUnitWidth) + 1;

		StringBuilder sb = new StringBuilder();
		// double left = firstCol*tileUnitWidth -20037508.342789;
		// double top =20037508.342789 - firstRow*tileUnitWidth;
		double top = 20037508.342789 - firstRow * tileUnitWidth;
		double left = firstCol * tileUnitWidth - 20037508.342789;

		sb.append(String.format(
				"<g transform=\"translate(%f %f)  scale(%f %f)\" >",
				(left - displayArea.x) / m_tBuildParam.m_dWorldPerPixelX,
				(top - displayArea.y) / m_tBuildParam.m_dWorldPerPixelY,
				Math.abs(scaleX), Math.abs(scaleY)));

		for (int row = firstRow; row <= lastRow; row++) {
			for (int col = firstCol; col <= lastCol; col++) {
				Random r = new Random();
				int num = r.nextInt(nodes.size());
				String sourceStr = String.format("%s/%d/%d/%d.png",
						nodes.get(num), level, col, row);
				sb.append(String.format(
						"<image x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"",
						(col - firstCol) * nTileSize, (row - firstRow)
								* nTileSize, nTileSize + 1, nTileSize + 1));
				sb.append(String.format(" xlink:href=\"%s\"", sourceStr));
				sb.append("></image>");
			}
		}
		sb.append("</g>");
		m_tResult.append(sb.toString()); // MRFUtil.formatString2XML(sb.toString())
	}

	public void outputOpenStreetMapImageUrl(Rect sourceRect, Point displayArea,
			int nTileSize, double tileUnitWidth, double scaleX, double scaleY,
			int level) {
		// rol & col number
		int firstCol = (int) Math.floor((sourceRect.left + 20037508.342789)
				/ tileUnitWidth);
		int firstRow = (int) Math.floor((20037508.342789 - sourceRect.top)
				/ tileUnitWidth);

		int lastCol = (int) Math
				.ceil((sourceRect.left + sourceRect.width() + 20037508.342789)
						/ tileUnitWidth);
		int lastRow = (int) Math.ceil((20037508.342789 - sourceRect.bottom)
				/ tileUnitWidth);

		// double left = firstCol*tileUnitWidth -20037508.342789;
		// double top =20037508.342789 - firstRow*tileUnitWidth;
		double top = 20037508.342789 - firstRow * tileUnitWidth;
		double left = firstCol * tileUnitWidth - 20037508.342789;

		m_tResult.append("<g transform=\"translate(");
		m_tResult.append((left - displayArea.X)
				/ m_tBuildParam.m_dWorldPerPixelX);
		m_tResult.append(" ");
		m_tResult.append((top - displayArea.Y)
				/ m_tBuildParam.m_dWorldPerPixelY);
		m_tResult.append(")  scale(");
		m_tResult.append(Math.abs(scaleX));
		m_tResult.append(" ");
		m_tResult.append(Math.abs(scaleY));
		m_tResult.append(")\" >");
		// (left -displayArea.x)*m_tBuildParam.m_dWorldPerPixelX,
		// (top-displayArea.y)*m_tBuildParam.m_dWorldPerPixelY, scaleX,
		// scaleY));
		for (int row = firstRow; row <= lastRow; row++) {
			for (int col = firstCol; col <= lastCol; col++) {
				String sourceStr = String.format("/localtile/%s/%s/%s.png",
						level, col, row);
				m_tResult.append("<image x=\"");
				m_tResult.append((col - firstCol) * nTileSize);
				m_tResult.append("\" y=\"");
				m_tResult.append((row - firstRow) * nTileSize);
				m_tResult.append("\" width=\"");
				m_tResult.append(nTileSize + 1);
				m_tResult.append("\" height=\"");
				m_tResult.append(nTileSize + 1);
				m_tResult.append("\" xlink:href=\"");
				m_tResult.append(MRFUtil.escapeXMLChars(MRFUtil
						.escapeXMLChars(sourceStr)));
				m_tResult.append("\"></image>");
			}
		}
		m_tResult.append("</g>");
	}

	public void OutputStyle() {
		try {
			Element tStyle = MRFUtil.getChildByName(m_tLayer, "style");

			String strOpacity = getParameter("opacity");
			String strStyle = "";
			if (StringUtil.isBlank(strOpacity)) {
				strOpacity = this.getStyleCreator().getValue("Opacity");
			}

			if (tStyle != null) {
				strStyle = MRFUtil.getText(tStyle);
				// replace original opacity
				if (!StringUtil.isBlank(strOpacity))
					strStyle = ReplaceAttributeValue(strStyle, "opacity",
							strOpacity);
			} else {
				strStyle = String.format("opacity:%s", strOpacity);
			}
			if (!StringUtil.isBlank(strStyle)) {
				m_tResult.append(String.format(" style=\"%s\"", strStyle));
			}

			m_tResult.append(">\n");
		} catch (Exception ex) {
			// Log.i("ImageLayerRequestCreator", ex.getMessage());
			ex.printStackTrace();
		}
	}

	public String ReplaceAttributeValue(String strXml, String strAttributeName,
			String strAttributeValue) {
		int nStartIndex = strXml.toLowerCase().indexOf(
				strAttributeName.toLowerCase());
		if (nStartIndex != -1) {
			int index = strXml.toLowerCase().indexOf(";", nStartIndex);
			int nEndIndex = index + nStartIndex;
			if (nEndIndex < 0) {
				nEndIndex = strXml.length();
			}
			strXml = strXml.replace(
					strXml.substring(nStartIndex, nEndIndex - nStartIndex),
					String.format("%s%s", strAttributeName, strAttributeValue));
		}
		return strXml;
	}

	protected void mrftileimage(Element tImage) {
		Element tPath = MRFUtil.getChildByName(tImage, "path");
		if (tPath == null) {
			return;
		}
		String strPath = MRFUtil.getText(tPath);
		if (StringUtil.isBlank(strPath)) {
			return;
		}
		Double dImageSizeX = 0.0, dImageSizeY = 0.0;
		Double dImageOriginX = 0.0, dImageOriginY = 0.0;
		Double dImageMultiplierX = 0.0, dImageMultiplierY = 0.0;

		Object[] objs = parseElementXYAttributes(tImage, "size");
		boolean valid = (Boolean) objs[0];
		dImageSizeX = (Double) objs[1];
		dImageSizeY = (Double) objs[2];
		if (valid) {
			objs = parseElementXYAttributes(tImage, "origin");
			valid = (Boolean) objs[0];
			dImageOriginX = (Double) objs[1];
			dImageOriginY = (Double) objs[2];
		}
		if (valid) {
			objs = parseElementXYAttributes(tImage, "multiplier");
			valid = (Boolean) objs[0];
			dImageMultiplierX = (Double) objs[1];
			dImageMultiplierY = (Double) objs[2];
			dImageMultiplierY = 0 - Math.abs(dImageMultiplierY);
		}

		if (!valid) {
			Element tDoc = tImage;

			Element tSize = MRFUtil.getChildByName(tDoc, "size");
			dImageSizeX = Double.parseDouble(tSize.attributeValue("x"));
			dImageSizeY = Double.parseDouble(tSize.attributeValue("y"));

			Element tOrigin = MRFUtil.getChildByName(tDoc, "origin");
			dImageOriginX = Double.parseDouble(tOrigin.attributeValue("x"));
			dImageOriginY = Double.parseDouble(tOrigin.attributeValue("y"));

			Element tScale = MRFUtil.getChildByName(tDoc, "scale");
			dImageMultiplierX = Double.parseDouble(tScale.attributeValue("x"));
			dImageMultiplierY = Double.parseDouble(tScale.attributeValue("y"));
		}

		// Get Display and image areas in pixels relative to output
		// For multipliers it is helpful to remember that:
		// -dImageMultiplier? maps image pixels to world coords
		// -m_dWorldPerPixel? maps screen pixels to world coords
		// -image pixels and screen pixels are NOT the same size for most zoom
		// levels - must
		// convert to world coords first (to go either direction)
		RectF tDisplayArea = new RectF(0, 0, m_tBuildParam.m_iScreenWidth,
				m_tBuildParam.m_iScreenHeight);
		RectF tImageArea = new RectF(
				(float) ((dImageOriginX - m_dOriginX) / m_dWorldPerPixelX),
				(float) ((dImageOriginY - m_dOriginY) / m_dWorldPerPixelY),
				(float) ((dImageOriginX - m_dOriginX+dImageSizeX * dImageMultiplierX) / m_dWorldPerPixelX),
				(float) ((dImageOriginY - m_dOriginY+dImageSizeY * dImageMultiplierY) / m_dWorldPerPixelY));
		RectF tIntersect = null;
		if (RectF.intersects(tDisplayArea, tImageArea)) {
			tIntersect = new RectF(
					Math.max(tDisplayArea.left, tImageArea.left), Math.max(
							tDisplayArea.top, tImageArea.top), Math.min(
							tDisplayArea.right, tImageArea.right), Math.min(
							tDisplayArea.bottom, tImageArea.bottom));
		}

		if (tIntersect == null || tIntersect.width() < 1
				|| tIntersect.height() < 1)
			return;

		if (!tIntersect.isEmpty()) {
			int iSrcOriginX = (int) ((tIntersect.left * m_dWorldPerPixelX
					+ m_dOriginX - dImageOriginX) / dImageMultiplierX);
			int iSrcOriginY = (int) ((tIntersect.top * m_dWorldPerPixelY
					+ m_dOriginY - dImageOriginY) / dImageMultiplierY);
			if (iSrcOriginX < 0)
				iSrcOriginX = 0; // B
			if (iSrcOriginY < 0)
				iSrcOriginY = 0; // B

			int iSourceWidth = (int) (tIntersect.width() * m_dWorldPerPixelX / dImageMultiplierX);
			int iSourceHeight = (int) (tIntersect.height() * m_dWorldPerPixelY / dImageMultiplierY);

			iSourceWidth = Math.min(iSourceWidth, dImageSizeX.intValue()
					- iSrcOriginX - 1);
			iSourceHeight = Math.min(iSourceHeight, dImageSizeY.intValue()
					- iSrcOriginY - 1);

			RectF sourceRect = new RectF(iSrcOriginX, iSrcOriginY,
                    iSrcOriginX+iSourceWidth, iSrcOriginY+iSourceHeight);

			String strPixelSize = this.getParameter("pixelsize");
			if (StringUtil.isBlank(strPixelSize)) {
				this.tileImage(tImage, strPath, sourceRect, tIntersect,
						tIntersect);
			} else {
				double dPixelSize = Double.parseDouble(strPixelSize);
				double dPixelWidth = iSourceWidth * dImageMultiplierX
						/ dPixelSize;
				double dPixelHeight = iSourceHeight * dImageMultiplierX
						/ dPixelSize;
				this.tileImage(tImage, strPath, sourceRect, new RectF(0, 0,
						(float) dPixelWidth, (float) dPixelHeight), tIntersect);
			}
			m_tResult
					.append(String
							.format("<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" style=\"fill:white;opacity:0\" />",
									tIntersect.left, tIntersect.top,
									tIntersect.width(), tIntersect.height()));
		}
	}

	private void tileImage(Element tImage, String strPath, RectF sourceRect,
			RectF dstImageSize, RectF displayArea) {
		// Get params from config file
		int nTileSize = Integer.parseInt(MRFUtil.getChildByName(tImage, "tile")
				.attributeValue("size").trim());
		String tileFormat = MRFUtil.getChildByName(tImage, "tile")
				.attributeValue("format").trim();
		Double dImageSizeX = null, dImageSizeY = null;
		Object[] objs = parseElementXYAttributes(tImage, "size");
		dImageSizeX = (Double) objs[1];
		dImageSizeY = (Double) objs[2];
		HashMap<Integer, Double> levelList = this.createLevelList(dImageSizeX,
				dImageSizeY, nTileSize);
		int level = getNearestLevel(dstImageSize.width() / sourceRect.width(),
				levelList);
		double tileScale = levelList.get(level);
		double tileUnitWidth = nTileSize / tileScale;

		double scaleX = displayArea.width() * tileUnitWidth
				/ (sourceRect.width() * nTileSize);
		double scaleY = displayArea.height() * tileUnitWidth
				/ (sourceRect.height() * nTileSize);

		this.outputImageUrlInfo(sourceRect, displayArea,
				MRFUtil.escapeXMLChars(MRFUtil.escapeXMLChars(strPath)),
				tileFormat, nTileSize, tileUnitWidth, scaleX, scaleY, level);
	}

	private void outputImageUrlInfo(RectF sourceRect, RectF displayArea,
			String imagPath, String tileFormat, int nTileSize,
			double tileUnitWidth, double scaleX, double scaleY, int level) {
		// rol & col number
		int firstCol = (int) Math.floor(sourceRect.left / tileUnitWidth);
		int firstRow = (int) Math.floor(sourceRect.top / tileUnitWidth);
		int lastCol = (int) Math.ceil((sourceRect.left + Math
				.abs(sourceRect.width())) / tileUnitWidth);
		int lastRow = (int) Math.ceil((sourceRect.top + Math
				.abs(sourceRect.height())) / tileUnitWidth);

		double top = (sourceRect.top / tileUnitWidth - firstRow)
				* nTileSize * scaleY;
		double left = (sourceRect.left / tileUnitWidth - firstCol)
				* nTileSize * scaleX;
		// define clipPath
		// String clipLayerId = String.format("imageClipPath_%s", m_strLayerId);
		// m_tResult.appendFormat("&lt;clipPath id=\"%s\"&gt;", clipLayerId);
		// m_tResult.appendFormat("&lt;rect x=\"%s\" y=\"%s\" width=\"%s\" height=\"%s\"/&gt;",
		// left, top, sourceRect.Width * nTileSize / tileUnitWidth,
		// sourceRect.Height * nTileSize / tileUnitWidth);
		// m_tResult.appendFormat("&lt;rect width=\"%s\" height=\"%s\"/&gt;",
		// sourceRect.Width * nTileSize / tileUnitWidth, sourceRect.Height *
		// nTileSize / tileUnitWidth);
		// m_tResult.append("&lt;/clipPath&gt;");

		// m_tResult.appendFormat("&lt;g transform=\"translate(%s %s) scale(%s %s)\" style=\"clip-path:url(#%s)\"&gt;",
		// displayArea.X - left, displayArea.Y - top, scaleX, scaleY,
		// clipLayerId);

		m_tResult.append("<g transform=\"translate(");
		m_tResult.append(displayArea.left - left);
		m_tResult.append(" ");
		m_tResult.append(displayArea.top - top);
		m_tResult.append(") scale(");
		m_tResult.append(scaleX);
		m_tResult.append(" ");
		m_tResult.append(scaleY);
		m_tResult.append(")\">");
		for (int row = firstRow; row < lastRow; row++) {
			for (int col = firstCol; col < lastCol; col++) {
				String sourceStr = String.format(imagPath, level+1, col+1, row+1, tileFormat);
				m_tResult.append("<image x=\"");
				m_tResult.append((col - firstCol) * nTileSize);
				m_tResult.append("\" y=\"");
				m_tResult.append((row - firstRow) * nTileSize);
				m_tResult.append("\" width=\"");
				m_tResult.append(nTileSize + 1);
				m_tResult.append("\" height=\"");
				m_tResult.append(nTileSize + 1);
				m_tResult.append("\" xlink:href=\"");
				m_tResult.append(sourceStr);
				m_tResult.append("\" />");
			}
		}
		m_tResult.append("</g>");
	}

	private int getNearestLevel(double scale, HashMap<Integer, Double> levelList) {
		// smaller than smallest
		if (levelList.get(levelList.size() - 1) < scale)
			return levelList.size() - 1;
		// bigger than biggest
		if (levelList.get(0) > scale)
			return 0;

		int result =levelList.size()-1;
		for (result = 0; result < levelList.size(); result++) {
			if (levelList.get(result) < scale) {
				continue;
			} else {
				break;
			}
		}
		return result;
	}

	private HashMap<Integer, Double> createLevelList(double width,
			double height, int tileSize) {
		double maxLength = Math.max(width, height);
		int maxLevel = (int) Math.ceil(Math.log(maxLength / tileSize));

		HashMap<Integer, Double> result = new HashMap<Integer, Double>();

		for (int level = 0; level <= maxLevel; level++) {
			double scale = 1 / (Math.pow(2, maxLevel - level));
			result.put(level, scale);
		}

		return result;
	}

	private Object[] parseElementXYAttributes(Element tImage, String strPath) {
		Object[] rtn = new Object[3];
		rtn[0] = true;
		Element tNode = MRFUtil.getChildByName(tImage, strPath);
		Double xValue = 0.0, yValue = 0.0;
		if (tNode == null || tNode.getNodeType() != Element.ELEMENT_NODE) {
			rtn[0] = false;
		} else {
			Element tEle = tNode;
			if (tEle.attribute("x") != null) {
				try {
					xValue = Double.parseDouble(tEle.attributeValue("x"));
				} catch (NumberFormatException e) {
					rtn[0] = false;
				}
			}
			if (tEle.attribute("y") != null) {
				try {
					yValue = Double.parseDouble(tEle.attributeValue("y"));
				} catch (NumberFormatException e) {
					rtn[0] = false;
				}
			}
		}
		rtn[1] = xValue;
		rtn[2] = yValue;
		return rtn;
	}
}
