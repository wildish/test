package MRF.common.Render.SVG;

import MRF.common.Util.StringUtil;

public class RotateUtil {
	public static String generateRotate(String angle) {
		return generateRotate(0d, 0d, angle, "0", "0");
	}

	public static String generateRotate(double translateX, double translateY,
			String angle, String x, String y) {
		String result = " transform=\" ";

		result += "translate(" + translateX + ", " + translateY + ") ";
		result += " rotate(" + angle;
		if (!StringUtil.isBlank(x) && !StringUtil.isBlank(y)) {
			result += ", " + x + ", " + y;
		}
		result += ")\" ";
		return result;
	}

	public static String generateRotate(BuildMapParam param) {
		String x = (param.getRotateScreenWidth() / 2) + "";
		String y = (param.getRotateScreenHeight() / 2) + "";
		double transX = -param.m_translateX;
		double transY = -param.m_translateY;
		return generateRotate(transX, transY, param.angle, x, y);
	}
}
