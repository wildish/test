package MRF.common.Render.SVG;

import java.util.HashMap;
import java.util.List;

import org.dom4j.Element;
import org.dom4j.Node;

import MRF.common.Render.SVG.SVGElementInsertor.InsertorType;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import android.annotation.SuppressLint;
import android.util.Log;

public class LineStringLayerRequestCreator extends LayerRequestCreator {

	public LineStringLayerRequestCreator(BuildMapParam runtimeParam,
			String szMapId, Element tLayer, String strLayerParam) {
		super(runtimeParam, szMapId, tLayer, strLayerParam);
		super.getStyleCreator().addStyle("fill", "none");
	}

	@SuppressLint("DefaultLocale")
	@Override
	public void doBuild() {
		try {
			if (!this.inScale()) {
				return;
			}
			beginBuild();
			Element tDataSource = MRFUtil.getChildByName(m_tLayer,
					"dataSource");
			if (tDataSource == null) {
				return;
			}

			// m_tLineStyle = MRFUtil.getChildNodeByName(m_tLayer,
			// "lineStyle");
			m_tResult.append(RotateUtil.generateRotate(m_tBuildParam));
			m_tResult.append(">");
			// m_tResult.append(String.format(" style=\"%s\" &gt;",
			// this.getStyleCreator().getformatedText()));
			// m_tResult.append("<Mrx.Emit rollback=\"false\"><format>");
			// m_tResult.append("<path ");

			// SVGElementInsertor insertor1 = new
			// SVGElementInsertor(InsertorType.TextInsertor, "<path ", "");
			// m_tInsertorArray.add(insertor1);

			List<Element> tFeatureList = MRFUtil.getchildListByName(
					m_tLayer, "featureAttribute");
			if (tFeatureList != null && tFeatureList.size() > 0) {
				for (int i = 0; i < tFeatureList.size(); i++) {
					Element tNode = tFeatureList.get(i);
					Element tFeature = tNode;
					String szTagName = tFeature.attributeValue("tag") != null ? tFeature
							.attributeValue("tag") : null;
					if (!StringUtil.isBlank(szTagName)) {
						szTagName = szTagName.toLowerCase();
					}
					if (szTagName.equals("label")) {
						m_tAutoLabelFeature = tFeature;
					} else if (szTagName.equals("tip")) {
						m_tTipFeature = tFeature;
					} else if (szTagName.equals("id")) {
						m_tIdFeature = tFeature;
					} else {
						this.callAgent(tFeature);
					}
				}
			}

			Element tLineStyle = MRFUtil.getChildByName(m_tLayer,
					"lineStyle");
			if (tLineStyle != null) {
				m_tLineStyle = tLineStyle;
				String lineStyleName = getLineStyleName();
				if (!StringUtil.isBlank(lineStyleName)) {
					SVGElementInsertor insertor = getLineStyleInsertor(lineStyleName);
					if (insertor != null) {
						m_tInsertorArray.add(insertor);
					}
				} else {
					tLineStyle = MRFUtil.getChildByName(
							MRFUtil.getChildByName(tLineStyle, "name"), "map");
					if (tLineStyle != null) {
						SVGElementInsertor insertor = getMapedLineStyleInsertor(tLineStyle);
						if (insertor != null) {
							m_tInsertorArray.add(insertor);
						}
					}
				}
			} else {
				Element tStyle = MRFUtil.getChildByName(m_tLayer,
						"style");
				if (tStyle != null) {
					SVGElementInsertor lineStyleInsertorStart = new SVGElementInsertor(
							InsertorType.TextInsertor, "<g t=\"e\">", "");
					m_tInsertorArray.add(lineStyleInsertorStart);

					SVGElementInsertor pathInsertor = new SVGElementInsertor(
							InsertorType.TextInsertor, "<path ", "");
					m_tInsertorArray.add(pathInsertor);

					outputFeatureId();
					outputLineStyleName();
					outputLineStyleType();
					SVGElementInsertor typeInsertor = new SVGElementInsertor(
							InsertorType.TextInsertor, String.format(
									" style=\"%s\"", MRFUtil.getText(tStyle)),
							"");
					m_tInsertorArray.add(typeInsertor);
					SVGElementInsertor insertor2 = new SVGElementInsertor(
							InsertorType.TextInsertor, " d=\"", "", "");
					m_tInsertorArray.add(insertor2);
					SVGElementInsertor insertor3 = new SVGElementInsertor(
							InsertorType.ClipPathInsertor, "", "\"", "");
					m_tInsertorArray.add(insertor3);

					SVGElementInsertor endInsertor = new SVGElementInsertor(
							InsertorType.TextInsertor, "/>", "");
					m_tInsertorArray.add(endInsertor);
					SVGElementInsertor lineStyleInsertorEnd = new SVGElementInsertor(
							InsertorType.TextInsertor, "</g>", "");
					m_tInsertorArray.add(lineStyleInsertorEnd);
				}
			}

			// add "Data" to Path attribute
			// m_tResult.append(" d=\"<MrxSvg.Path");
			// m_tResult.append(String.format(" size=\"%s %s\"%s/>\"",
			// m_tBuildParam.m_iScreenWidth, m_tBuildParam.m_iScreenHeight,
			// m_strOriginAndScale));
			// m_tResult.append("/&gt;");
			// SVGElementInsertor insertor2 = new
			// SVGElementInsertor(InsertorType.TextInsertor, " d=\"", "", "");
			// m_tInsertorArray.add(insertor2);
			// SVGElementInsertor insertor3 = new
			// SVGElementInsertor(InsertorType.ClipPathInsertor, "", "\"/>",
			// "");
			// m_tInsertorArray.add(insertor3);

			outputAutoLabel();

			//
			// this.m_tResult.append("</format>");
			//
			// m_tResult.append("<iterator>");
			this.callAgent(tDataSource);
			// m_tResult.append("</iterator>");
			//
			// m_tResult.append("</Mrx.Emit>");
			this.endBuild();
		} catch (Exception ex) {
			m_tResult.setLength(0);
			Log.i("lineStringLayerRequestCreator",
					"doBuild():" + ex.getMessage());
		}
	}

	private SVGElementInsertor getMapedLineStyleInsertor(Element tMap) {
		try {
			StringBuilder strCompare = new StringBuilder(
					tMap.attributeValue("compare"));
			if (StringUtil.isBlank(strCompare.toString())) {
				return null;
			}
			String strAsNumber = tMap.attributeValue("asNumber");
			if (StringUtil.isBlank(strAsNumber)) {
				return null;
			}
			List<Element> nodeList = MRFUtil.getchildListByName(tMap,
					"keySource");
			if (nodeList == null || nodeList.size() == 0) {
				return null;
			}
			Element tChild = null;
			String thematicField = "";
			int index = 0;
			for (index = 0; index < nodeList.size(); index++) {
				tChild = nodeList.get(index);
				if (tChild.getName().equalsIgnoreCase("keySource"))
					thematicField = MRFUtil.getText(MRFUtil.getChildByName(
							tChild, "field"));
			}
			if (strAsNumber.equalsIgnoreCase("true")) {
				strCompare.append(";1");
			} else {
				strCompare.append(";0");
			}
			SVGElementInsertor insertor = new SVGElementInsertor(
					InsertorType.MapInsertor, strCompare.toString(),
					thematicField);
			for (index = 0; index < tMap.elements().size(); index++) {
				tChild = (Element) tMap.elements().get(index);
				if (tChild == null || tChild.getNodeType() != Element.ELEMENT_NODE
						|| tChild.getName() != null
						|| !tChild.getName().equalsIgnoreCase("mapEntry")) {
					continue;
				}
				Element tMapEntry = tChild;
				String strKey = MRFUtil.getAttrValueWithDefault(tMapEntry,
						"key", "");
				String strValue = MRFUtil.getAttrValueWithDefault(tMapEntry,
						"value", "");
				if (strKey.length() == 0 || strValue.length() == 0) {
					continue;
				}

				if (StringUtil.trim(strValue).startsWith("url")
						&& strValue.length() > 6) {
					strValue = strValue.substring(5, strValue.length() - 6);
					SVGElementInsertor subInsertor = getLineStyleInsertor(strValue);
					if (subInsertor != null) {
						insertor.key.add(strKey);
						insertor.value.add("");
						insertor.addSubInsertor(subInsertor);
					}
				}
			}
			return insertor;
		} catch (Exception ex) {
			// Log.i("LineStringLayerRequestCreator",
			// "getMapedLineStyleInsertor():" + ex.getMessage());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private SVGElementInsertor getLineStyleInsertor(String lineStyleName)
			throws Exception {

		if (MRFUtil.sharedInstance().containsInCachedParameter("linestyles")) {
			HashMap<String, Node> lineStyles = (HashMap<String, Node>) MRFUtil
					.sharedInstance().getCachedParameter("linestyles");
			Element lineStyle = null;
			if (lineStyles.containsKey(lineStyleName)) {
				lineStyle = (Element) lineStyles.get(lineStyleName);
				Element gElement = MRFUtil.getChildByName(lineStyle,
						"g");
				List<Element> pathNodeList = MRFUtil
						.getchildListByName(gElement, "path");
				if (pathNodeList != null && pathNodeList.size() > 0
						&& m_tIdFeature != null) {
					SVGElementInsertor lineStyleInsertor = new SVGElementInsertor(
							InsertorType.MultiInsertor, "<g t=\"e\">", "</g>",
							"");
					for (int i = 0; i < pathNodeList.size(); i++) {
						Element item = pathNodeList.get(i);
						SVGElementInsertor pathInsertor = new SVGElementInsertor(
								InsertorType.TextInsertor, "<path ", "");
						lineStyleInsertor.addSubInsertor(pathInsertor);
						int count = m_tInsertorArray.size();
						outputFeatureId();
						outputLineStyleName();
						outputLineStyleType();
						for (int k = count; k < m_tInsertorArray.size(); k++) {
							lineStyleInsertor.addSubInsertor(m_tInsertorArray
									.get(k));
						}
						// m_tInsertorArray.removeRange(count,
						// m_tInsertorArray.size() - count);
						for (int n = count; n < m_tInsertorArray.size() - count; n++) {
							m_tInsertorArray.remove(n);
						}
						if (item.attribute("style") != null) {
							String type = item.attributeValue("style");
							SVGElementInsertor typeInsertor = new SVGElementInsertor(
									InsertorType.TextInsertor, String.format(
											" style=\"%s\"", type), "");
							lineStyleInsertor.addSubInsertor(typeInsertor);
						}
						SVGElementInsertor insertor2 = new SVGElementInsertor(
								InsertorType.TextInsertor, " d=\"", "", "");
						lineStyleInsertor.addSubInsertor(insertor2);
						SVGElementInsertor insertor3 = new SVGElementInsertor(
								InsertorType.ClipPathInsertor, "", "\"", "");
						lineStyleInsertor.addSubInsertor(insertor3);

						SVGElementInsertor endInsertor = new SVGElementInsertor(
								InsertorType.TextInsertor, "/>", "");
						lineStyleInsertor.addSubInsertor(endInsertor);
					}

					return lineStyleInsertor;
				}
			}
		}
		return null;
	}

	private String getLineStyleName() throws Exception {
		if (m_tLineStyle == null) {
			return null;
		}

		if (m_tLineStyle.getNodeType() != Element.ELEMENT_NODE) {
			return null;
		}
		Element m_eLineStyle = m_tLineStyle;

		String strLSName = null;
		if (m_eLineStyle.attribute("name") != null) {
			strLSName = this.getSymbolyName(m_eLineStyle.attributeValue("name"));
		} else {
			Element tLSName = MRFUtil.getChildByName(m_tLineStyle,
					"name");
			if (tLSName != null) {
				Element tNode = MRFUtil.getFirstChild(tLSName); // MRFUtil.getFirstChildNodeByPath(tLSName,
				// "*");
				if (tNode == null) {
					strLSName = this.getSymbolyName(tLSName.getText());
				}
			} else {
				throw new Exception("Missing name of the linestyle.");
			}
		}

		return strLSName;
	}

	@SuppressWarnings("unchecked")
	private void outputLineStyleName() throws Exception {
		if (m_tLineStyle == null)
			return;

		if (m_tLineStyle.getNodeType() != Element.ELEMENT_NODE) {
			return;
		}
		Element m_eLineStyle = m_tLineStyle;

		String strLSName = null;
		Element tLSName = MRFUtil
				.getChildByName(m_tLineStyle, "name");
		if (m_eLineStyle.attribute("name") != null) {
			strLSName = this
					.getSymbolyName(m_eLineStyle.attributeValue("name"));
		} else if (tLSName != null) {
			Element tNode = MRFUtil.getFirstChild(tLSName); // MRFUtil.getFirstChildNodeByPath(tLSName,
			// "*");
			if (tNode == null) {
				strLSName = this.getSymbolyName(MRFUtil.getText(tLSName));
			}
		} else {
			throw new Exception("Missing name of the linestyle.");
		}

		if (!StringUtil.isBlank(strLSName)) {
			// m_tResult.append(String.format(" name=\"%s\"", strLSName));
			SVGElementInsertor insertor1 = new SVGElementInsertor(
					InsertorType.TextInsertor, String.format(" name=\"%s\"",
							strLSName), "");
			m_tInsertorArray.add(insertor1);
		} else if (tLSName != null) {
			SVGElementInsertor insertor1 = new SVGElementInsertor(
					InsertorType.TextInsertor, " name=\"", "");
			m_tInsertorArray.add(insertor1);
			List<Element> tChilds = tLSName.elements();
			for (int i = 0; i < tChilds.size(); i++) {
				Element tChild = tChilds.get(i);
				if (tChild.getNodeType() != Element.ELEMENT_NODE) {
					callAgent(tChild);
				}
			}
			SVGElementInsertor insertor2 = new SVGElementInsertor(
					InsertorType.TextInsertor, "\"", "");
			m_tInsertorArray.add(insertor2);
		}
	}

	private void outputLineStyleType() {
		if (m_tLineStyle == null) {
			return;
		}
		// linestyle type
		String strLSType = null;
		if (!StringUtil.isBlank(MRFUtil.getAttrValueWithDefault(m_tLineStyle,
				"type", ""))) {
			strLSType = MRFUtil.getAttrValueWithDefault(m_tLineStyle, "type",
					"");
		} else {
			Element tLSType = MRFUtil.getChildByName(m_tLineStyle,
					"type");
			if (tLSType != null) {
				strLSType = MRFUtil.getText(tLSType);
			}
		}
		// line style
		if (!StringUtil.isBlank(strLSType)) {
			SVGElementInsertor lineStyleTypeInsertor = new SVGElementInsertor(
					InsertorType.TextInsertor, String.format(" type=\"%s\" ",
							strLSType), "");
			m_tInsertorArray.add(lineStyleTypeInsertor);
		}
	}
}
