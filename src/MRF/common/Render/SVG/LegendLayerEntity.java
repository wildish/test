package MRF.common.Render.SVG;

import java.io.Serializable;
import java.util.List;

public class LegendLayerEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	public String Id;
	public String Name;
	public String Type;
	public LegendSymbolStyleEntity SymbolStyle;
	public List<LegendThematicEntity> Thematic;
	public String SubGroup;
}
