package MRF.common.Render.SVG;

import java.util.ArrayList;
import java.util.List;

import MRF.common.Render.SVG.SVGElementInsertor.InsertorType;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class AnnotationLayerRequestCreator extends LayerRequestCreator {

	public AnnotationLayerRequestCreator(BuildMapParam runtimeParam,
			String szMapId, org.dom4j.Element tLayer, String strLayerParam) {
		super(runtimeParam, szMapId, tLayer, strLayerParam);
	}

	public void doBuild() {
		StringBuffer buffer = null;
		try {
			if (!inScale())
				return;

			beginBuild();
			boolean bDoNotHaveToLoad = false;

			String tId = "";
			if (m_tLayer.attribute("id") != null) {
				tId = m_tLayer.attributeValue("id");
			}

			String tName = "";
			if (m_tLayer.attribute("name") != null) {
				tName = m_tLayer.attributeValue("name");
			}

			// Element tStyle =
			// (Element)MRFUtil.GetFirstChildNodeByPath(m_tLayer, "style");

			org.dom4j.Element tDataSource = MRFUtil.getChildByName(
					m_tLayer, "dataSource");

			org.dom4j.Element tTextConfigure = MRFUtil
					.getChildByName(m_tLayer, "textConfigure");
			if (StringUtil.isBlank(tId)
					|| StringUtil.isBlank(tName)
					|| tTextConfigure == null)
				return;

			org.dom4j.Element tActive = MRFUtil.getChildByName(
					m_tLayer, "active");
			if (tActive != null) {
				buffer = new StringBuffer();
				buffer.append(" active=\"");
				buffer.append(MRFUtil.getText(tActive));
				buffer.append("\"");
				m_tResult.append(buffer.toString());
			}

			// if (tStyle != null)
			// {
			// m_tResult.AppendFormat(" style=\"%s\"",
			// base.GetStyleCreator().GetFormatedText());
			// }
			m_tResult.append(RotateUtil.generateRotate(m_tBuildParam));
			m_tResult.append(" >");

			String strTextConfigureType = "";
			if (tTextConfigure.attribute("type") != null) {
				strTextConfigureType = tTextConfigure.attributeValue("type");
			}

			if (!bDoNotHaveToLoad) {

				// this.m_tResult.append("<Mrx.Emit rollback=\"false\"><format>");

				org.dom4j.Element tTextField = MRFUtil.getChildByName(
						tTextConfigure, "textField");
				org.dom4j.Element tTextProp = MRFUtil.getChildByName(
						tTextConfigure, "textProp");

				if (strTextConfigureType.equals("byPoint")) {
					// m_tResult.AppendFormat("<text x=\"<MrxSvg.X %s />\" y=\"<MrxSvg.Y %s />\"",
					// m_strOriginAndScale, m_strOriginAndScale);
					SVGElementInsertor insertor1 = new SVGElementInsertor(
							InsertorType.TextInsertor, "<text x=\"", "");
					m_tInsertorArray.add(insertor1);
					SVGElementInsertor insertor2 = new SVGElementInsertor(
							InsertorType.XInsertor, "", "");
					m_tInsertorArray.add(insertor2);
					SVGElementInsertor insertor3 = new SVGElementInsertor(
							InsertorType.YInsertor, "\" y=\"", "\"", "");
					m_tInsertorArray.add(insertor3);
					textConfigure(tTextConfigure);
					//
					SVGElementInsertor insertor4 = new SVGElementInsertor(
							InsertorType.TextInsertor, ">", "", "");
					m_tInsertorArray.add(insertor4);
					if (tTextField != null) {
						// m_tResult.AppendFormat("><Mrx.Field name=\"%s\"/></text>\n",
						// tTextField.Value);
						SVGElementInsertor insertor5 = new SVGElementInsertor(
								InsertorType.FieldInsertor, "", "</text>\n",
								MRFUtil.getText(tTextField));
						m_tInsertorArray.add(insertor5);
					} else {
						// m_tResult.AppendFormat("><Mrx.Prop name=\"%s\"/></text>\n",
						// tTextProp.Value);
						SVGElementInsertor insertor5 = new SVGElementInsertor(
								InsertorType.PropInsertor, "", "</text>\n",
								MRFUtil.getText(tTextProp));
						m_tInsertorArray.add(insertor5);
					}

					// m_tResult.append("</format><iterator>");
					// dataSource iterator
					this.callAgent(tDataSource);
					// m_tResult.append("</iterator></Mrx.Emit>\n");
				} else if (strTextConfigureType.equals("byPath")) {

					org.dom4j.Element tStartOffset = MRFUtil.getChildByName(
							tTextConfigure, "startOffset");

					// <defs>
					// m_tResult.append("\n<defs>");
					SVGElementInsertor insertor1 = new SVGElementInsertor(
							InsertorType.TextInsertor, "\n<defs>", "", "");
					m_tInsertorArray.add(insertor1);

					// <path id=
					// String strIdField =
					// String.format("%s_%s_%sText<Mrx.Prop name=\"id\"/>",
					// m_strMapId, m_strLayerId, tId);
					// m_tResult.AppendFormat("<path id=\"%s\"", strIdField);
					// SVGElementInsertor insertor2 = new
					// SVGElementInsertor(InsertorType.TextInsertor, , "", "");
					// m_tInsertorArray.add(insertor2);

					org.dom4j.Element ds = MRFUtil.getFirstChild(tDataSource);
					org.dom4j.Element idColNode = MRFUtil.getChildByName(ds,
							"idcol");
					// MRFUtil.GetFirstChildNode(tDataSource, "idcol", "id");
					String idCol = "id";
					if (idColNode != null) {
						idCol = MRFUtil.getText(idColNode); // getNodeValue()
					}
					buffer = new StringBuffer();
					buffer.append("<path id=\"");
					buffer.append("m_strMapId");
					buffer.append("_");
					buffer.append("m_strLayerId");
					buffer.append("_");
					buffer.append("tId");
					buffer.append("Text");
					SVGElementInsertor insertor3 = new SVGElementInsertor(
							InsertorType.PropInsertor, buffer.toString(), "\"",
							idCol);
					m_tInsertorArray.add(insertor3);

					// Add "d" path attribute
					// m_tResult.append(" d=\"<MrxSvg.Path");
					// m_tResult.AppendFormat(" size=\"%f %f\"%s/>\"/>",
					// m_dWorldPerPixelX, m_dWorldPerPixelY,
					// m_strOriginAndScale);
					SVGElementInsertor insertor4 = new SVGElementInsertor(
							InsertorType.TextInsertor, " d=\"", "", "");
					m_tInsertorArray.add(insertor4);
					double startOffset = MRFUtil
							.getTextDoubleWithDefault(
									tTextConfigure, "startOffset", 12);
					double fontSize = MRFUtil
							.getTextDoubleWithDefault(
									tTextConfigure, "textSize", 12);
					if (startOffset < fontSize) {
						startOffset = fontSize;
					}
					SVGElementInsertor insertor5 = new SVGElementInsertor(
							InsertorType.TextPathInsertor, "", "\" />",
							MRFUtil.getText(tTextField), startOffset,
							fontSize);
					m_tInsertorArray.add(insertor5);

					// </defs>
					// m_tResult.append("</defs>");
					// m_tResult.append("<text ");
					SVGElementInsertor insertor6 = new SVGElementInsertor(
							InsertorType.TextInsertor, "</defs><text ", "", "");
					m_tInsertorArray.add(insertor6);
					textConfigure(tTextConfigure);
					// m_tResult.append(">");
					SVGElementInsertor insertor7 = new SVGElementInsertor(
							InsertorType.TextInsertor, ">", "", "");
					m_tInsertorArray.add(insertor7);
					// <textPath xlink:href= >
					// String strIdField =
					// String.format("%s_%s_%sText<Mrx.Prop name=\"id\"/>",
					// m_strMapId, m_strLayerId, tId);
					if (tStartOffset == null) {
						// m_tResult.AppendFormat("<textPath method=\"stretch\" xlink:href=\"#%s\">",
						// strIdField);
						// SVGElementInsertor insertor8 = new
						// SVGElementInsertor(InsertorType.TextInsertor,
						// String.format("<textPath method=\"stretch\" xlink:href=\"#%s_%s_%sText",
						// m_strMapId, m_strLayerId, tId), "", "");
						// m_tInsertorArray.add(insertor8);
						buffer = new StringBuffer();
						buffer.append("<textPath method=\"stretch\" xlink:href=\"#");
						buffer.append(m_strMapId);
						buffer.append("_");
						buffer.append(m_strLayerId);
						buffer.append("_");
						buffer.append(tId);
						buffer.append("Text");
						SVGElementInsertor insertor9 = new SVGElementInsertor(
								InsertorType.PropInsertor, buffer.toString(),
								"\">", "id");
						m_tInsertorArray.add(insertor9);
					} else {
						// m_tResult.AppendFormat("<textPath method=\"stretch\" startOffset=\"%s\" xlink:href=\"#%s\">",
						// tStartOffset.Value, strIdField);
						// SVGElementInsertor insertor8 = new
						// SVGElementInsertor(InsertorType.TextInsertor,
						// String.format("<textPath method=\"stretch\" startOffset=\"%s\" xlink:href=\"#%s_%s_%sText",
						// tStartOffset.getNodeValue(), m_strMapId,
						// m_strLayerId, tId), "", "");
						// m_tInsertorArray.add(insertor8);
						buffer = new StringBuffer();
						buffer.append("<textPath method=\"stretch\" startOffset=\"");
						buffer.append(MRFUtil.getText(tStartOffset));
						buffer.append("\" startOffset=\"");
						buffer.append(m_strMapId);
						buffer.append("\" xlink:href=\"#");
						buffer.append(m_strMapId);
						buffer.append("_");
						buffer.append(m_strLayerId);
						buffer.append("_");
						buffer.append(tId);
						buffer.append("Text");
						SVGElementInsertor insertor9 = new SVGElementInsertor(
								InsertorType.PropInsertor, buffer.toString(),
								"\">", idCol);
						m_tInsertorArray.add(insertor9);
					}
					// annotation text
					if (tTextField != null) {
						// m_tResult.AppendFormat("<Mrx.Field name=\"%s\"/></textPath>",
						// tTextField.Value);
						SVGElementInsertor insertor10 = new SVGElementInsertor(
								InsertorType.FieldInsertor, "", "</textPath>",
								MRFUtil.getText(tTextField));
						m_tInsertorArray.add(insertor10);
					} else {
						SVGElementInsertor insertor10 = new SVGElementInsertor(
								InsertorType.PropInsertor, "", "</textPath>",
								MRFUtil.getText(tTextProp));
						m_tInsertorArray.add(insertor10);
					}
					// </text>
					// m_tResult.append("</text>");
					SVGElementInsertor insertor11 = new SVGElementInsertor(
							InsertorType.TextInsertor, "</text>", "", "");
					m_tInsertorArray.add(insertor11);

					// m_tResult.append("</format><iterator>");
					// dataSource iterator
					this.callAgent(tDataSource);
				}

			}
			this.endBuild();
		} catch (Exception ex) {
			m_tResult = new StringBuilder();
			// Log.GetInstance().Write(ex);
		}
	}

	@SuppressWarnings("unchecked")
	public void textConfigure(org.dom4j.Element tTextConfigure) {
		// text-anchor
		org.dom4j.Element tTextAnchor = MRFUtil.getChildByName(tTextConfigure,
				"textAnchor");
		org.dom4j.Element tTextAnchorField = MRFUtil.getChildByName(
				tTextConfigure, "textAnchorField");
		org.dom4j.Element tTextAnchorProp = MRFUtil.getChildByName(
				tTextConfigure, "textAnchorProp");
		SVGElementInsertor insertor1 = new SVGElementInsertor(
				InsertorType.TextInsertor, " text-anchor=\"", "", "");
		m_tInsertorArray.add(insertor1);
		if (tTextAnchorField != null) {
			// m_tResult.AppendFormat(" text-anchor=\"<Mrx.Field name=\"%s\"/>\"",
			// tTextAnchorField.Value);
			SVGElementInsertor insertor2 = new SVGElementInsertor(
					InsertorType.FieldInsertor, "", "\"",
					MRFUtil.getText(tTextAnchorField));
			m_tInsertorArray.add(insertor2);
		} else if (tTextAnchorProp != null) {
			// m_tResult.AppendFormat(" text-anchor=\"<Mrx.Prop name=\"%s\"/>\"",
			// tTextAnchorProp.Value);
			SVGElementInsertor insertor2 = new SVGElementInsertor(
					InsertorType.PropInsertor, "", "\"",
					MRFUtil.getText(tTextAnchorProp));
			m_tInsertorArray.add(insertor2);
		} else if (tTextAnchor != null) {
			// m_tResult.AppendFormat(" text-anchor=\"%s\"", tTextAnchor.Value);
			SVGElementInsertor insertor2 = new SVGElementInsertor(
					InsertorType.TextInsertor,
					MRFUtil.getText(tTextAnchor), "\"", "");
			m_tInsertorArray.add(insertor2);
		}
		// font-size
		// String tStyle = tTextConfigure.getAttribute("style");
		String tTextSize = tTextConfigure.attributeValue("textSize");
		String tTextScale = tTextConfigure.attributeValue("textScale");
		String tScalable = tTextConfigure.attributeValue("scalable");
		List<String> keys = new ArrayList<String>();
		if (StringUtil.isBlank(tTextSize) == false) {
			keys.add("font-size");
			if (StringUtil.isBlank(tTextScale) == false) {
				// m_tResult.append(" font-size=\"<MrxMath.Expr decimals=\"2\">");
				SVGElementInsertor inserter3 = new SVGElementInsertor(
						InsertorType.MathInsertor, " font-size=\"", "", ""); // TODO
																				// TextInsertor
				if (StringUtil.isBlank(tScalable)
						|| tScalable.equals("false")) {
					// m_tResult.AppendFormat("%s*%s</MrxMath.Expr>\"",
					// tTextSize, tTextScale);
					SVGElementInsertor inserter31 = new SVGElementInsertor(
							InsertorType.TextInsertor, String.valueOf(Double
									.parseDouble(tTextSize)
									* Double.parseDouble(tTextScale)), "");
					inserter3.addSubInsertor(inserter31);
				} else {
					double d = Double.NaN;
					try {
						d = Double.parseDouble(tTextScale);
					} catch (Exception e) {
						d = 0;
					}
					double scale = d / Math.abs(m_dWorldPerPixelY);
					// m_tResult.AppendFormat("%s*%f</MrxMath.Expr>\"",
					// tTextSize, scale);
					SVGElementInsertor inserter31 = new SVGElementInsertor(
							InsertorType.TextInsertor, String.valueOf(Double
									.parseDouble(tTextSize) * scale), "");
					inserter3.addSubInsertor(inserter31);
				}
				m_tInsertorArray.add(inserter3);
				// SVGElementInsertor inserter4 = new
				// SVGElementInsertor(InsertorType.TextInsertor, "\"", "");
				// m_tInsertorArray.add(inserter4);
			} else {
				// m_tResult.append(" font-size=\"");
				SVGElementInsertor inserter3 = new SVGElementInsertor(
						InsertorType.TextInsertor, " font-size=\"", "");
				m_tInsertorArray.add(inserter3);
				if (StringUtil.isBlank(tScalable)
						|| tScalable.equals("false")) {
					// m_tResult.AppendFormat("%s\"", tTextSize);
					SVGElementInsertor inserter31 = new SVGElementInsertor(
							InsertorType.TextInsertor, tTextSize, "\"", "");
					m_tInsertorArray.add(inserter31);
				} else {
					double scale = 1.0 / Math.abs(m_dWorldPerPixelY);
					// m_tResult.AppendFormat("<MrxMath.Expr decimals=\"2\">%s*%f</MrxMath.Expr>\"",
					// tTextSize, scale);
					SVGElementInsertor inserter31 = new SVGElementInsertor(
							InsertorType.TextInsertor, String.valueOf(Double
									.parseDouble(tTextSize) * scale), "\"", "");
					m_tInsertorArray.add(inserter31);
				}
			}
		}
		// OutputTextStyle(tStyle, keys);

		// static offset
		String dxOffset = "0";
		String dyOffset = "0";
		org.dom4j.Element tOffset = MRFUtil.getChildByName(tTextConfigure,
				"offset");
		if (tOffset != null) {
			String[] xy = MRFUtil.getText(tOffset).split(",");
			try {
				dxOffset = xy[0].trim();
				dyOffset = xy[1].trim();
			} catch (Exception ex) {
				dxOffset = "0";
				dyOffset = "0";
			}
		}
		// szTranslateX
		// String szTranslateX = "0";
		org.dom4j.Element tOffsetPropX = MRFUtil.getChildByName(tTextConfigure,
				"offsetPropX");
		org.dom4j.Element tOffsetFieldX = MRFUtil.getChildByName(tTextConfigure,
				"offsetFieldX");

		SVGElementInsertor inserterTranslateX = null;
		if (tOffsetFieldX != null) {
			if (dxOffset.equals("0")) {
				// szTranslateX = String.format("<Mrx.Field name=\"%s\"/>",
				// tOffsetFieldX);
				inserterTranslateX = new SVGElementInsertor(
						InsertorType.FieldInsertor, "", "",
						MRFUtil.getText(tOffsetFieldX));
			} else {
				// szTranslateX =
				// String.format("<MrxMath.Expr>%s+<Mrx.Field name=\"%s\"/></MrxMath.Expr>",
				// dxOffset, tOffsetFieldX);
				inserterTranslateX = new SVGElementInsertor(
						InsertorType.MathInsertor, "", "");
				SVGElementInsertor inserter4 = new SVGElementInsertor(
						InsertorType.FieldInsertor, String.format("%s+",
								dxOffset), "",
						MRFUtil.getText(tOffsetFieldX));
				inserterTranslateX.addSubInsertor(inserter4);
			}
		} else if (tOffsetPropX != null) {
			if (dxOffset.equals("0")) {
				// szTranslateX = String.format("<Mrx.Prop name=\"%s\"/>",
				// tOffsetPropX);
				inserterTranslateX = new SVGElementInsertor(
						InsertorType.PropInsertor, "", "",
						MRFUtil.getText(tOffsetPropX));
			} else {
				// szTranslateX =
				// String.format("<MrxMath.Expr>%s+<Mrx.Prop name=\"%s\"/></MrxMath.Expr>",
				// dxOffset, tOffsetPropX);
				inserterTranslateX = new SVGElementInsertor(
						InsertorType.MathInsertor, "", "");
				SVGElementInsertor inserter4 = new SVGElementInsertor(
						InsertorType.PropInsertor, String.format("%s+",
								dxOffset), "",
						MRFUtil.getText(tOffsetPropX));
				inserterTranslateX.addSubInsertor(inserter4);
			}
		} else {
			// szTranslateX = dxOffset;
		}
		// szTranslateY
		// String szTranslateY = "0";
		org.dom4j.Element tOffsetPropY = MRFUtil.getChildByName(tTextConfigure,
				"offsetPropY");
		org.dom4j.Element tOffsetFieldY = MRFUtil.getChildByName(tTextConfigure,
				"offsetFieldY");
		SVGElementInsertor inserterTranslateY = null;
		if (tOffsetFieldY != null) {
			if (dyOffset.equals("0")) {
				// szTranslateY = String.format("<Mrx.Field name=\"%s\"/>",
				// tOffsetFieldY);
				inserterTranslateY = new SVGElementInsertor(
						InsertorType.FieldInsertor, "", "",
						MRFUtil.getText(tOffsetFieldY));
			} else {
				// szTranslateY =
				// String.format("<MrxMath.Expr>%s+<Mrx.Field name=\"%s\"/></MrxMath.Expr>",
				// dyOffset, tOffsetFieldY);
				inserterTranslateY = new SVGElementInsertor(
						InsertorType.MathInsertor, "", "");
				SVGElementInsertor inserter5 = new SVGElementInsertor(
						InsertorType.FieldInsertor, String.format("%s+",
								dyOffset), "",
						MRFUtil.getText(tOffsetFieldY));
				inserterTranslateY.addSubInsertor(inserter5);
			}
		} else if (tOffsetPropY != null) {
			if (dyOffset.equals("0")) {
				// szTranslateY = String.format("<Mrx.Prop name=\"%s\"/>",
				// tOffsetPropY);
				inserterTranslateY = new SVGElementInsertor(
						InsertorType.PropInsertor, "", "",
						MRFUtil.getText(tOffsetPropY));
			} else {
				// szTranslateY =
				// String.format("<MrxMath.Expr>%s+<Mrx.Prop name=\"%s\"/></MrxMath.Expr>",
				// dyOffset, tOffsetPropY);
				inserterTranslateY = new SVGElementInsertor(
						InsertorType.MathInsertor, "", "");
				SVGElementInsertor inserter5 = new SVGElementInsertor(
						InsertorType.PropInsertor, String.format("%s+",
								dyOffset), "",
						MRFUtil.getText(tOffsetPropY));
				inserterTranslateY.addSubInsertor(inserter5);
			}
		} else {
			// szTranslateY = dyOffset;
		}

		boolean bHasTransform = false;
		if (inserterTranslateX != null || inserterTranslateY != null) {
			// m_tResult.AppendFormat(" transform=\"translate(%s %s)",
			// szTranslateX, szTranslateY);
			SVGElementInsertor inserterTranslate = new SVGElementInsertor(
					InsertorType.TextInsertor, " transform=\"translate(", "");
			m_tInsertorArray.add(inserterTranslate);
			if (inserterTranslateX != null && inserterTranslateY == null) {
				SVGElementInsertor inserterRight = new SVGElementInsertor(
						InsertorType.TextInsertor, " 0", "", "");
				m_tInsertorArray.add(inserterTranslateX);
				m_tInsertorArray.add(inserterRight);
			} else if (inserterTranslateX == null && inserterTranslateY != null) {
				SVGElementInsertor inserterLeft = new SVGElementInsertor(
						InsertorType.TextInsertor, "0 ", "", "");
				m_tInsertorArray.add(inserterLeft);
				m_tInsertorArray.add(inserterTranslateY);
			} else {
				inserterTranslate.addSubInsertor(inserterTranslateX);
				SVGElementInsertor inserterEmpty = new SVGElementInsertor(
						InsertorType.TextInsertor, " ", "", "");
				m_tInsertorArray.add(inserterEmpty);
				m_tInsertorArray.add(inserterTranslateY);
			}

			SVGElementInsertor inserterRightQuat = new SVGElementInsertor(
					InsertorType.TextInsertor, ")", "", "");
			m_tInsertorArray.add(inserterRightQuat);
			bHasTransform = true;
		}
		// rotation
		org.dom4j.Element tRotationField = MRFUtil.getChildByName(
				tTextConfigure, "rotationField");
		org.dom4j.Element tRotationProp = MRFUtil.getChildByName(tTextConfigure,
				"rotationProp");
		if (tRotationField != null) {
			// m_tResult.AppendFormat(" transform=\"rotate(<Mrx.Field name=\"%s\"/>,<MrxSvg.X %s />,<MrxSvg.Y %s />)\"",
			// tRotationField, m_strOriginAndScale, m_strOriginAndScale);
			SVGElementInsertor inserter6 = new SVGElementInsertor(
					InsertorType.TextInsertor, " transform=\"rotate(", "");
			SVGElementInsertor inserter61 = new SVGElementInsertor(
					InsertorType.FieldInsertor, "", ",",
					MRFUtil.getText(tRotationField));
			SVGElementInsertor inserter62 = new SVGElementInsertor(
					InsertorType.XInsertor, "", ",", "");
			SVGElementInsertor inserter63 = new SVGElementInsertor(
					InsertorType.YInsertor, "", ")\"", "");
			m_tInsertorArray.add(inserter6);
			m_tInsertorArray.add(inserter61);
			m_tInsertorArray.add(inserter62);
			m_tInsertorArray.add(inserter63);
		} else if (tRotationProp != null) {
			// m_tResult.AppendFormat(" transform=\"rotate(<Mrx.Prop name=\"%s\"/>,<MrxSvg.X %s />,<MrxSvg.Y %s />)\"",
			// tRotationProp, m_strOriginAndScale, m_strOriginAndScale);
			SVGElementInsertor inserter6 = new SVGElementInsertor(
					InsertorType.TextInsertor, " transform=\"rotate(", "");
			SVGElementInsertor inserter61 = new SVGElementInsertor(
					InsertorType.PropInsertor, "", ",",
					MRFUtil.getText(tRotationProp));
			SVGElementInsertor inserter62 = new SVGElementInsertor(
					InsertorType.XInsertor, "", ",", "");
			SVGElementInsertor inserter63 = new SVGElementInsertor(
					InsertorType.YInsertor, "", ")\"", "");
			m_tInsertorArray.add(inserter6);
			m_tInsertorArray.add(inserter61);
			m_tInsertorArray.add(inserter62);
			m_tInsertorArray.add(inserter63);
		}

		if (bHasTransform) {
			// m_tResult.append("\""); //close transform
			SVGElementInsertor inserter7 = new SVGElementInsertor(
					InsertorType.TextInsertor, "\"", "");
			m_tInsertorArray.add(inserter7);
		}
		// featureAttributes
		org.dom4j.Element featureAttributes = MRFUtil.getChildByName(
				tTextConfigure, "featureAttribute");

		if (featureAttributes != null) {
			List<org.dom4j.Node> tLabelStyles = featureAttributes.attributes();
			if (tLabelStyles != null) {
				for (int i = 0; i < tLabelStyles.size(); i++) {
					org.dom4j.Node tLabelStyle = tLabelStyles.get(i);
					String tagName = tLabelStyle.getStringValue()
							.toLowerCase();
					if (tagName.equals("id")) {
						m_tIdFeature = (org.dom4j.Element) tLabelStyle;
					} else if (tagName.equals("tip")) {
						m_tTipFeature = (org.dom4j.Element) tLabelStyle;
					} else {
						callAgent((org.dom4j.Element) tLabelStyles.get(0));
					}
				}
			}
		}
		// this.OutputFeatureId();
		// this.OutputTips();

	}

	public void outputTextStyle(String tStyle, List<String> listKey) {
		if (StringUtil.isBlank(tStyle) == false) {
			String[] arrStyle = tStyle.split(";");
			for (String style : arrStyle) {
				if (!StringUtil.isBlank(style)) {
					String[] arrStyles = style.split(":");
					if (!(listKey.size() > 0 && listKey.contains(arrStyles[0]
							.toLowerCase()))) {
						// m_tResult.AppendFormat(" %s=\"%s\"", arrStyles[0],
						// arrStyles[1]);
						StringBuffer buffer = new StringBuffer();
						buffer.append(" ");
						buffer.append(arrStyles[0]);
						buffer.append("=\"");
						buffer.append(arrStyles[1]);
						buffer.append("\"");
						SVGElementInsertor inserter = new SVGElementInsertor(
								InsertorType.TextInsertor, buffer.toString(),
								"");
						m_tInsertorArray.add(inserter);
					}
				}
			}
		}

	}

}
