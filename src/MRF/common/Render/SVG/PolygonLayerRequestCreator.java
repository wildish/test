package MRF.common.Render.SVG;

import java.util.List;

import org.dom4j.Element;

import MRF.common.Render.SVG.SVGElementInsertor.InsertorType;
import MRF.common.Util.MRFUtil;

public class PolygonLayerRequestCreator extends LayerRequestCreator {

	public PolygonLayerRequestCreator(BuildMapParam runtimeParam,
			String szMapId, Element tLayer, String strLayerParam) {
		super(runtimeParam, szMapId, tLayer, strLayerParam);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void doBuild() {
		try {
			if (!inScale())
				return;

			Element tDataSource = MRFUtil.getChildByName(
					m_tLayer, "dataSource");
			if (tDataSource == null)
				return;

			beginBuild();
			Element tPattern = MRFUtil.getChildByName(m_tLayer, "pattern");

			Element tStroke = null;
			Element tWidth = null;
			m_tResult.append(RotateUtil.generateRotate(m_tBuildParam));
			m_tResult.append(" >");

			SVGElementInsertor insertor1 = new SVGElementInsertor(
					InsertorType.TextInsertor, "<path ", "");
			m_tInsertorArray.add(insertor1);
			if (tPattern != null) {
				tStroke = MRFUtil.getChildByName(tPattern, "stroke");
				tWidth = MRFUtil.getChildByName(tPattern, "width");
				String strStroke = MRFUtil.getText(tStroke);
				String strWidth = MRFUtil.getText(tWidth);
				Element tPattenName = MRFUtil.getChildByName(tPattern,
						"name");
				if (tPattenName != null) {
					StringBuffer buffer = new StringBuffer();
					buffer.append(" style=\"fill:");
					buffer.append(MRFUtil.getText(tPattenName));
					buffer.append("; stroke:");
					buffer.append(strStroke);
					buffer.append("; stroke-width:");
					buffer.append(strWidth);
					buffer.append(";\"");
					SVGElementInsertor insertor2 = new SVGElementInsertor(
							InsertorType.TextInsertor, buffer.toString(), "");
					m_tInsertorArray.add(insertor2);
				} else {
					SVGElementInsertor insertor2 = new SVGElementInsertor(
							InsertorType.TextInsertor, " style=\"fill:", "");
					m_tInsertorArray.add(insertor2);
					// NodeList tChilds = tPattenName.getChildNodes();
					// for (int k = 0; k < tChilds.getLength(); k++)
					// {
					// Node tChild = tChilds.item(k);
					// if (tChild.getNodeType() == Node.ELEMENT_NODE)
					// {
					// callAgent(tChild);
					// }
					// }
					SVGElementInsertor insertor3 = new SVGElementInsertor(
							InsertorType.TextInsertor, String.format(
									"; stroke:%s; stroke-width:%s;\"",
									strStroke, strWidth), "");
					m_tInsertorArray.add(insertor3);
				}
			}

			List<Element> tFeatures = m_tLayer.elements();
			for (int k = 0; k < tFeatures.size(); k++) {
				Element tFeature = tFeatures.get(k);
				if (tFeature.getNodeType() != Element.ELEMENT_NODE
						|| !tFeature.getName().equalsIgnoreCase(
								"featureAttribute")) {
					continue;
				}
				Element element = tFeature;
				String tTag = element.attributeValue("tag");
				if (tTag.equalsIgnoreCase("label")) {
					m_tAutoLabelFeature = element;
				} else if (tTag.equalsIgnoreCase("tip")) {
					m_tTipFeature = element;
				} else if (tTag.equalsIgnoreCase("id")) {
					m_tIdFeature = element;
				} else if (tTag.equalsIgnoreCase("fill")) {
					Element m_tMapFeature = MRFUtil.getChildByName(
							element, "map");
					if (m_tMapFeature != null) {
						SVGElementInsertor insertor5 = new SVGElementInsertor(
								InsertorType.TextInsertor, " fill=\"", "");
						m_tInsertorArray.add(insertor5);
						callAgent(m_tMapFeature);
						SVGElementInsertor insertor6 = new SVGElementInsertor(
								InsertorType.TextInsertor, "\"", "");
						m_tInsertorArray.add(insertor6);
					}
				} else {
					callAgent(element);
				}
			}

			outputFeatureId();
			SVGElementInsertor insertor4 = new SVGElementInsertor(
					InsertorType.ClipPathInsertor, " d=\"", "\"/>", "");
			m_tInsertorArray.add(insertor4);
			// SVGElementInsertor insertor6 = new
			// SVGElementInsertor(InsertorType.TextInsertor, " \"", "");
			// m_tInsertorArray.Add(insertor6);
			// SVGElementInsertor insertor5 = new
			// SVGElementInsertor(InsertorType.TextInsertor, " />", "");
			// m_tInsertorArray.Add(insertor5);
			this.callAgent(tDataSource);
			this.endBuild();
		} catch (Exception ex) {
			// Log.i("PolygonLayerRequestCreator", "doBuild():" + ex.Message);
			m_tResult.setLength(0);
		}
	}

	public void PatternCache(String strPatternName) {
		return;
	}

	public void mapEntryWithStyleName(String styleName, String key, String value) {
		StringBuffer buffer = new StringBuffer();
		if (styleName.equalsIgnoreCase("style")) {
			AbsStyleCreator styleCreator = new AbsStyleCreator(
					this.getStyleCreator());
			styleCreator.formatStyle(value);
			String thematicValue = styleCreator.getFormatedText();
			List<String> styles = styleCreator.getStyles();
			for (String thematicStyleName : styles) {
				getStyleCreator().setStyleStatus(thematicStyleName, true);
			}
			//
			buffer.append("replacer");
			buffer.append(m_strLayerId);
			buffer.append(styleName);
			buffer.append(key);
			String szHolder = buffer.toString();
			m_tBuildParam.cacheEntity(szHolder, thematicValue);
			// m_tResult.Append(String.format("\"%s\":\"%s\";", key, szHolder));
			buffer = new StringBuffer();
			buffer.append("\"");
			buffer.append(key);
			buffer.append("\":\"");
			buffer.append(szHolder);
			buffer.append("\";");
			SVGElementInsertor insertor1 = new SVGElementInsertor(
					InsertorType.TextInsertor, buffer.toString(), "");
			m_tInsertorArray.add(insertor1);
		} else {
			// value = base.GetSymbolyName(value);
			// m_tResult.Append(String.format("\"%s\":\"%s\";", key, value));
			buffer.append("\"");
			buffer.append(key);
			buffer.append("\":\"");
			buffer.append(value);
			buffer.append("\";");
			SVGElementInsertor insertor1 = new SVGElementInsertor(
					InsertorType.TextInsertor, buffer.toString(), "");
			m_tInsertorArray.add(insertor1);
		}
	}
}
