package MRF.common.Render.SVG;

public interface IMapRequestCreator {
	void initialize(String szMapId, String szViewName, double dOriginX,
			double dOriginY, double dScaleX, double dScaleY, int iXamlWidth,
			int iXamlHeight, String strSpGeom, String strSpOperator,
			String strLayerIds, String strLayerParams, String angle,
			MRF.common.geometry.Point begin, MRF.common.geometry.Point end);

	String getResult();
}
