package MRF.common.Render.SVG;

import MRF.common.Util.StringUtil;

public class SvgStyleCreator extends AbsStyleCreator {
	public SvgStyleCreator() {
		this.standardNames.put("id", "id");
		this.standardNames.put("Opacity", "opacity");
	}

	public SvgStyleCreator(AbsStyleCreator styleCreator) {
		super(styleCreator);
	}

	public String getFormatedText() {
		StringBuffer buffer = new StringBuffer();
		if (this.styleValues != null && this.styleValues.size() > 0) {
			for (String key : this.styleValues.keySet()) {
				if (!this.hasOutputStyles.contains(key)) {
					if (buffer.length() > 0) {
						buffer.append(";");
					}
					String value = this.styleValues.get(key);
					buffer.append(key);
					buffer.append(":");
					buffer.append(value);
				}
			}
		}
		//
		return buffer.toString();
	}

	public String getLegendFormatedText() {
		StringBuffer buffer = new StringBuffer();
		if (this.styleValues != null && this.styleValues.size() > 0) {
			for (String key : this.styleValues.keySet()) {
				if (!this.hasOutputStyles.contains(key)) {
					String value = this.styleValues.get(key);
					if (buffer.length() > 0) {
						buffer.append(",");
					}
					buffer.append(key);
					buffer.append(":\"");
					buffer.append(value);
					buffer.append("\"");
				}
			}
		}
		//
		return buffer.toString();
	}

	@Override
	public void formatStyle(String plainText) {
		try {
			if (StringUtil.isBlank(plainText)) {
				return;
			}
			// plainText = StringUtils.convertRGBToHEX(plainText);
			String[] strProps = plainText.split(";");
			for (int i = 0; i < strProps.length; i++) {
				String[] props = strProps[i].split(":");
				if (props.length != 2) {
					continue;
				}
				String standardName = this.getStandardName(props[0].trim());
				if (StringUtil.isBlank(standardName)) {
					continue;
				}
				props[1] = props[1].trim();
				if (!StringUtil.isBlank(props[1])) {
					this.styleValues.put(standardName, props[1]);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
