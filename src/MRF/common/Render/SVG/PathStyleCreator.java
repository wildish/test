package MRF.common.Render.SVG;

public class PathStyleCreator extends SvgStyleCreator {
	public PathStyleCreator() {
		super();
	    this.standardNames.put("fill","fill");
	    this.standardNames.put("stroke","stroke");
	    this.standardNames.put("width","stroke-width");
	    this.standardNames.put("stroke-width","stroke-width");
	    this.standardNames.put("strokethickness","stroke-width");
	    this.standardNames.put("stroke-dasharray","stroke-dasharray");
	    this.standardNames.put("strokedasharray","stroke-dasharray");
	    this.standardNames.put("stroke-dashoffset","stroke-dashoffset");
	    this.standardNames.put("strokedashoffset","stroke-dashoffset");
	    this.standardNames.put("stroke-linejoin","stroke-linejoin");
	    this.standardNames.put("strokelinejoin","stroke-linejoin");
	    this.standardNames.put("stroke-miterlimit","stroke-miterlimit");
	    this.standardNames.put("strokemiterlimit","stroke-miterlimit");
	    this.standardNames.put("fill-opacity","fill-opacity");
	    this.standardNames.put("fillopacity","fill-opacity");
	    this.standardNames.put("stroke-opacity","stroke-opacity");
	    this.standardNames.put("strokeopacity","stroke-opacity");
	}
}
