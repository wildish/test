package MRF.common.Render.SVG;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.sourceforge.jeval.Evaluator;
import MRF.common.Data.GeometryWithData;
import MRF.common.Util.StringUtil;
import MRF.common.geometry.Line;
import MRF.common.geometry.MultiLine;
import MRF.common.geometry.MultiPoint;
import MRF.common.geometry.MultiPolygon;
import MRF.common.geometry.Point;
import MRF.common.geometry.Polygon;
import MRF.common.geometry.WKBGeometryType;
import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class SVGElementInsertor {
	public enum InsertorType {
		TextInsertor, FieldInsertor, PropInsertor, MapInsertor, XYInsertor, MathInsertor, ClipPathInsertor, XInsertor, YInsertor, TextPathInsertor, MultiInsertor
	}

	public InsertorType type; // 1 text 2 field 3 prop 4 map 5 xy 6 math
								// (subInsert); 7 path
	public String prepText, sufixText;
	public String field;
	public List<String> key;
	public List<String> value;
	public List<SVGElementInsertor> subInsertores;
	public double oriX;
	public double oriY;
	public double scaleX;
	public double scaleY;
	public long mapWidth;
	public long mapHeight;
	private double textPathOffset = 10;
	private double textPathFontSize = 12;
	boolean isSuccess = true;

	public SVGElementInsertor(InsertorType type, String text, String field) {
		this.type = type;
		this.field = field;
		this.prepText = text;
		this.key = new ArrayList<String>();
		this.value = new ArrayList<String>();
	}

	public SVGElementInsertor(InsertorType type, String prepText,
			String sufixText, String field) {
		this.type = type;
		this.field = field;
		this.prepText = prepText;
		this.sufixText = sufixText;
		this.key = new ArrayList<String>();
		this.value = new ArrayList<String>();
	}

	public SVGElementInsertor(InsertorType type, String prepText,
			String sufixText, String field, double textPathOffset,
			double textPathFontSize) {
		this.type = type;
		this.field = field;
		this.prepText = prepText;
		this.sufixText = sufixText;
		this.key = new ArrayList<String>();
		this.value = new ArrayList<String>();
		this.textPathOffset = textPathOffset;
		this.textPathFontSize = textPathFontSize;
	}

	public boolean execute(StringBuilder m_tResult, GeometryWithData data,
			BuildMapParam param) {
		try {
			isSuccess = true;
			this.oriX = param.m_dOriginX;
			this.oriY = param.m_dOriginY;
			this.scaleX = param.m_dWorldPerPixelX;
			this.scaleY = param.m_dWorldPerPixelY;
			this.mapWidth = param.m_iScreenWidth;
			this.mapHeight = param.m_iScreenHeight;
			switch (type) {
			case TextInsertor:
				m_tResult.append(prepText);
				if (!StringUtil.isBlank(sufixText)) {
					m_tResult.append(sufixText);
				}
				break;
			case FieldInsertor:
				m_tResult.append(OutputField(data));
				break;
			case PropInsertor:
				m_tResult.append(OutputProp(data));
				break;
			case MapInsertor:
				m_tResult.append(OutputMap(data, param));
				break;
			case XYInsertor:
				m_tResult.append(OutputXY(data));
				break;
			case MathInsertor:
				isSuccess = OutputMathResult(m_tResult, data, param);
				break;
			case ClipPathInsertor:
				m_tResult.append(outputPath(data));
				break;
			case XInsertor:
				m_tResult.append(OutputX(data));
				break;
			case YInsertor:
				m_tResult.append(OutputY(data));
				break;
			case TextPathInsertor:
				m_tResult.append(outputPath(data));
				break;
			case MultiInsertor:
				isSuccess = OutputMultiResult(m_tResult, data, param);
				break;
			default:
				break;
			}
			return isSuccess & true;
		} catch (Exception ex) {
			// Log.i("PointLayerRequestCreator", "execute()" + ex.message());
			return true;
		}

	}

	public void addSubInsertor(SVGElementInsertor subInsertor) {
		if (this.subInsertores == null) {
			this.subInsertores = new ArrayList<SVGElementInsertor>();
		}
		this.subInsertores.add(subInsertor);
	}

	public String OutputField(GeometryWithData data) {
		StringBuilder temp = new StringBuilder(prepText);

		if (data.getData().containsKey(field)
				&& data.getData().get(field) != null) {
			temp.append(data.getData().get(field));
		}
		if (!StringUtil.isBlank(sufixText)) {
			temp.append(sufixText);
		}
		return temp.toString();
	}

	public String outputPath(GeometryWithData data) {
		StringBuilder temp = new StringBuilder(prepText);
		StringBuilder d = new StringBuilder();
		if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBLINESTRING) {
			d.append(outputLinePath((Line) data.getWKBGeometry(), false, data));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTILINESTRING) {
			MultiLine multiLine = (MultiLine) data.getWKBGeometry();
			int count = multiLine.getPartCount();
			for (int i = 0; i < count; i++) {
				d.append(outputLinePath(multiLine.getPartAtIndex(i), false,
						data));
			}
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBPOLYGON) {
			Polygon polygon = (Polygon) data.getWKBGeometry();
			int count = polygon.getPartCount();
			for (int i = 0; i < count; i++) {
				d.append(outputLinePath(polygon.getPartAtIndex(i), true, data));
			}
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTIPOLYGON) {
			MultiPolygon multiPolygon = (MultiPolygon) data.getWKBGeometry();
			int partCount = multiPolygon.GetPartCount();
			for (int j = 0; j < partCount; j++) {
				Polygon polygon = multiPolygon.GetPartAtIndex(j);
				int count = polygon.getPartCount();
				for (int i = 0; i < count; i++) {
					d.append(outputLinePath(polygon.getPartAtIndex(i), true,
							data));
				}
			}
		}

		String path = d.toString().trim();
		if (d.length() == 0) {
			path = "M0 0 L0 0";
		}
		temp.append(path);

		if (!StringUtil.isBlank(sufixText)) {
			temp.append(sufixText);
		}
		return temp.toString();
	}

	public String outputLinePath(Line line, boolean isClose,
			GeometryWithData data) {
		String noneLine = "";
		if (line == null || line.getPartCount() < 2)
			return noneLine;

		double[] pt = line.getPointAtIndex(0);
		int partCount = line.getPartCount();
		StringBuilder temp = new StringBuilder();

		long currentPointX = (long) getMapX(pt[0]);
		long currentPointY = (long) getMapy(pt[1]);
		long lastPointX = currentPointX;
		long lastPointY = currentPointY;

		List<double[]> mapPtList = new ArrayList<double[]>();
		mapPtList.add(new double[] { currentPointX, currentPointY });
		boolean isNeedClip = false;

		for (int i = 1; i < partCount; i++) {
			pt = line.getPointAtIndex(i);
			currentPointX = (long) getMapX(pt[0]);
			currentPointY = (long) getMapy(pt[1]);
			if (i < partCount - 1) {
				if ((Math.abs(lastPointX - currentPointX) + Math.abs(lastPointY
						- currentPointY)) < 1)
					continue;
			}
			mapPtList.add(new double[] { currentPointX, currentPointY });

			if (!isNeedClip
					&& (currentPointX > mapWidth || currentPointX < 0
							|| currentPointY > mapHeight || currentPointY < 0)) {
				isNeedClip = true;
			}

			lastPointX = currentPointX;
			lastPointY = currentPointY;
		}
		if (mapPtList.size() < 2)
			return noneLine;

		if (isNeedClip) {
			List<double[]> clipPath = ClipPath(mapPtList);
			if (clipPath.size() < 2)
				return noneLine;

			mapPtList.clear();
			mapPtList.addAll(clipPath);
		}
		partCount = mapPtList.size();
		if (type == InsertorType.TextPathInsertor) {
			if (data.getData().containsKey(field)
					&& data.getData().get(field) != null) {
				String name = data.getData().get(field).toString();
				if (name == null || "".equals(name)) {
					isSuccess = false;
					return noneLine;
				}
				double textPathLen = (textPathOffset * 2 + textPathFontSize
						* name.length());
				double pathLen = 0;
				pt = mapPtList.get(0);
				lastPointX = (long) pt[0];
				lastPointY = (long) pt[1];
				for (int i = 1; i < partCount; i++) {
					pt = mapPtList.get(i);
					currentPointX = (long) pt[0];
					currentPointY = (long) pt[1];
					pathLen += Math.abs(lastPointX - currentPointX)
							+ Math.abs(lastPointY - currentPointY);
					lastPointX = currentPointX;
					lastPointY = currentPointY;
				}
				if (pathLen < textPathLen) {
					isSuccess = false;
					return noneLine;
				}
				if (mapPtList.get(0)[0] + 20 > mapPtList
						.get(mapPtList.size() - 1)[0]) {
					Collections.reverse(mapPtList);
				}
			} else {
				isSuccess = false;
				return noneLine;
			}
		}
		boolean isFirstSegment = true;
		pt = mapPtList.get(0);
		lastPointX = (long) pt[0];
		lastPointY = (long) pt[1];
		for (int i = 1; i < partCount; i++) {
			pt = mapPtList.get(i);
			currentPointX = (long) pt[0];
			currentPointY = (long) pt[1];
			if (isFirstSegment) {
				temp.append(" M");
				temp.append(lastPointX);
				temp.append(" ");
				temp.append(lastPointY);
				temp.append(" ");
				isFirstSegment = false;
			}
			temp.append("l");
			temp.append(currentPointX - lastPointX);
			temp.append(" ");
			temp.append(currentPointY - lastPointY);
			temp.append(" ");
			lastPointX = currentPointX;
			lastPointY = currentPointY;
		}

		if (temp.length() > 0 && isClose) {
			temp.append("z");
		}
		return temp.toString();
	}

	// public List<Point> ClipPath(List<Point> oriArray) {
	// double NearZero = 0.0000001;
	// List<Point> line = new ArrayList<Point>();
	// if (oriArray.size() <= 1) /* nothing to clip */
	// return oriArray;
	//
	// Point pt1 = oriArray.get(0);
	// oriArray.add(0, new Point(pt1.X + 0.000001, pt1.Y + 0.000001));
	// Point pt2 = oriArray.get(oriArray.size() - 1);
	// oriArray.add(new Point(pt2.X + 0.000001, pt2.Y + 0.000001));
	// for (int i = 0; i < oriArray.size() - 1; i++) {
	// pt1 = (Point) oriArray.get(i);
	// pt2 = (Point) oriArray.get(i + 1);
	// double x1 = pt1.X;
	// double y1 = pt1.Y;
	// double x2 = pt2.X;
	// double y2 = pt2.Y;
	//
	// double deltax = x2 - x1;
	// if (Math.abs(deltax) < NearZero) {
	// // bump off of the vertical
	// deltax = (x1 > 0) ? -NearZero : NearZero;
	// }
	// double deltay = y2 - y1;
	// if (Math.abs(deltay) < NearZero) {
	// // bump off of the horizontal
	// deltay = (y1 > 0) ? -NearZero : NearZero;
	// }
	//
	// double xin;
	// double xout;
	// if (deltax > 0) {
	// // points to right
	// xin = 0;
	// xout = mapWidth;
	// } else {
	// xin = mapWidth;
	// xout = 0;
	// }
	//
	// double yin;
	// double yout;
	// if (deltay > 0) {
	// // points up
	// yin = 0;
	// yout = mapHeight;
	// } else {
	// yin = mapHeight;
	// yout = 0;
	// }
	//
	// double tinx = (xin - x1) / deltax;
	// double tiny = (yin - y1) / deltay;
	//
	// double tin1;
	// double tin2;
	// if (tinx < tiny) {
	// // hits x first
	// tin1 = tinx;
	// tin2 = tiny;
	// } else {
	// // hits y first
	// tin1 = tiny;
	// tin2 = tinx;
	// }
	//
	// if (1 >= tin1) {
	// if (0 < tin1)
	// line.add(new Point(xin, yin));
	//
	// if (1 >= tin2) {
	// double toutx = (xout - x1) / deltax;
	// double touty = (yout - y1) / deltay;
	//
	// double tout = (toutx < touty) ? toutx : touty;
	//
	// if (0 < tin2 || 0 < tout) {
	// if (tin2 <= tout) {
	// if (0 < tin2) {
	// if (tinx > tiny) {
	// line.add(new Point(xin, y1 + tinx * deltay));
	// } else {
	// line.add(new Point(x1 + tiny * deltax, yin));
	// }
	// }
	// if (1 > tout) {
	// if (toutx < touty) {
	// line.add(new Point(xout, y1 + toutx
	// * deltay));
	// } else {
	// line.add(new Point(x1 + touty * deltax,
	// yout));
	// }
	// } else
	// line.add(new Point(x2, y2));
	// } else {
	// if (tinx > tiny) {
	// line.add(new Point(xin, yout));
	// } else {
	// line.add(new Point(xout, yin));
	// }
	// }
	// }
	// }
	// }
	// }
	// return line;
	// }

	public List<double[]> ClipPath(List<double[]> oriArray) {
		double NearZero = 0.0000001;
		List<double[]> line = new ArrayList<double[]>();
		if (oriArray.size() <= 1) /* nothing to clip */
			return oriArray;
		double[] pt1 = oriArray.get(0);
		pt1[0] = pt1[0] + 0.000001;
		pt1[1] = pt1[1] + 0.000001;
		oriArray.add(0, pt1);
		double[] pt2 = oriArray.get(oriArray.size() - 1);
		pt2[0] = pt2[0] + 0.000001;
		pt2[1] = pt2[1] + 0.000001;
		oriArray.add(pt2);
		for (int i = 0; i < oriArray.size() - 1; i++) {
			pt1 = oriArray.get(i);
			pt2 = oriArray.get(i + 1);
			double x1 = pt1[0];
			double y1 = pt1[1];
			double x2 = pt2[0];
			double y2 = pt2[1];

			double deltax = x2 - x1;
			if (Math.abs(deltax) < NearZero) {
				// bump off of the vertical
				deltax = (x1 > 0) ? -NearZero : NearZero;
			}
			double deltay = y2 - y1;
			if (Math.abs(deltay) < NearZero) {
				// bump off of the horizontal
				deltay = (y1 > 0) ? -NearZero : NearZero;
			}

			double xin;
			double xout;
			if (deltax > 0) {
				// points to right
				xin = 0;
				xout = mapWidth;
			} else {
				xin = mapWidth;
				xout = 0;
			}

			double yin;
			double yout;
			if (deltay > 0) {
				// points up
				yin = 0;
				yout = mapHeight;
			} else {
				yin = mapHeight;
				yout = 0;
			}

			double tinx = (xin - x1) / deltax;
			double tiny = (yin - y1) / deltay;

			double tin1;
			double tin2;
			if (tinx < tiny) {
				// hits x first
				tin1 = tinx;
				tin2 = tiny;
			} else {
				// hits y first
				tin1 = tiny;
				tin2 = tinx;
			}

			if (1 >= tin1) {
				if (0 < tin1)
					line.add(new double[] { xin, yin });

				if (1 >= tin2) {
					double toutx = (xout - x1) / deltax;
					double touty = (yout - y1) / deltay;

					double tout = (toutx < touty) ? toutx : touty;

					if (0 < tin2 || 0 < tout) {
						if (tin2 <= tout) {
							if (0 < tin2) {
								if (tinx > tiny) {
									line.add(new double[] { xin,
											y1 + tinx * deltay });
								} else {
									line.add(new double[] { x1 + tiny * deltax,
											yin });
								}
							}
							if (1 > tout) {
								if (toutx < touty) {
									line.add(new double[] { xout,
											y1 + toutx * deltay });
								} else {
									line.add(new double[] {
											x1 + touty * deltax, yout });
								}
							} else
								line.add(new double[] { x2, y2 });
						} else {
							if (tinx > tiny) {
								line.add(new double[] { xin, yout });
							} else {
								line.add(new double[] { xout, yin });
							}
						}
					}
				}
			}
		}
		return line;
	}

	public boolean OutputMathResult(StringBuilder m_tResult,
			GeometryWithData data, BuildMapParam param) {
		boolean isSuccess = true;
		try {
			StringBuilder temp = new StringBuilder(prepText);
			if (this.subInsertores != null) {
				StringBuilder temp1 = new StringBuilder();

				for (int i = 0; i < this.subInsertores.size(); i++) {
					isSuccess = ((SVGElementInsertor) subInsertores.get(i))
							.execute(temp1, data, param);
					if (!isSuccess) {
						break;
					}
				}
				if (isSuccess) {
					Evaluator evaluator = new Evaluator();
					String t = evaluator.evaluate(temp1.toString());
					temp.append(t);
					m_tResult.append(temp);
				}
			}
		} catch (Exception ex) {
			// Log.i("SVGElementInsertor", "outputMathResult():" +
			// ex.message());
			isSuccess = true;
		}
		if (!StringUtil.isBlank(sufixText)) {
			m_tResult.append(sufixText);
		}
		return isSuccess;
	}

	public boolean OutputMultiResult(StringBuilder m_tResult,
			GeometryWithData data, BuildMapParam param) {
		boolean isSuccess = true;
		try {
			StringBuilder temp = new StringBuilder(prepText);
			if (this.subInsertores != null) {
				StringBuilder temp1 = new StringBuilder();

				for (int i = 0; i < this.subInsertores.size(); i++) {
					isSuccess = ((SVGElementInsertor) subInsertores.get(i))
							.execute(temp1, data, param);
					if (!isSuccess) {
						break;
					}
				}
				if (isSuccess) {
					temp.append(temp1);
				}
			}

			m_tResult.append(temp);
		} catch (Exception ex) {
			// Log.i("SVGElementInsertor", "outputMathResult():" +
			// ex.message());
			isSuccess = false;
		}
		if (!StringUtil.isBlank(sufixText)) {
			m_tResult.append(sufixText);
		}
		return isSuccess;
	}

	public String OutputProp(GeometryWithData data) {
		StringBuilder temp = new StringBuilder(prepText);
		if (data.getData().containsKey(field)
				&& data.getData().get(field) != null) {
			temp.append(data.getData().get(field));
		} else if (data.getWKBGeometry() != null
				&& data.getWKBGeometry().getGeometryType() != WKBGeometryType.WKBGEOMETRYNULL) {
			if (field.equalsIgnoreCase("minx")) {
				temp.append(getMapX(data.getWKBGeometry().getEnv().minX));
			} else if (field.equalsIgnoreCase("miny")) {
				temp.append(getMapy(data.getWKBGeometry().getEnv().minY));
			} else if (field.equalsIgnoreCase("maxx")) {
				temp.append(getMapX(data.getWKBGeometry().getEnv().maxX));
			} else if (field.equalsIgnoreCase("maxy")) {
				temp.append(getMapy(data.getWKBGeometry().getEnv().maxY));
			} else if (field.equalsIgnoreCase("center")) {
				temp.append(getMapX(data.getWKBGeometry().getEnv().maxX
						+ data.getWKBGeometry().getEnv().minX / 2.0));
				temp.append(" ");
				temp.append(getMapy(data.getWKBGeometry().getEnv().minY
						+ data.getWKBGeometry().getEnv().maxY / 2.0));
			}
		}
		if (!StringUtil.isBlank(sufixText)) {
			temp.append(sufixText);
		}
		return temp.toString();
	}

	public String OutputXY(GeometryWithData data) {
		StringBuilder temp = new StringBuilder(prepText);
		if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBPOINT) {
			temp.append(getMapX(data.getWKBGeometry().getEnv().maxX));
			temp.append(" ");
			temp.append(getMapy(data.getWKBGeometry().getEnv().minY));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBLINESTRING) {
			double[] pt = ((Line) data.getWKBGeometry()).getPointAtIndex(0);
			temp.append(getMapX(pt[0]));
			temp.append(" ");
			temp.append(getMapy(pt[1]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBPOLYGON) {
			double[] pt = ((Polygon) data.getWKBGeometry()).getPartAtIndex(0)
					.getPointAtIndex(0);
			temp.append(getMapX(pt[0]));
			temp.append(" ");
			temp.append(getMapy(pt[1]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTIPOINT) {
			Point pt = ((MultiPoint) data.getWKBGeometry()).getPartAtIndex(0);
			temp.append(getMapX(pt.X));
			temp.append(" ");
			temp.append(getMapy(pt.Y));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTILINESTRING) {
			double[] pt = ((MultiLine) data.getWKBGeometry()).getPartAtIndex(0)
					.getPointAtIndex(0);
			temp.append(getMapX(pt[0]));
			temp.append(" ");
			temp.append(getMapy(pt[1]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTIPOLYGON) {
			double[] pt = ((MultiPolygon) data.getWKBGeometry())
					.GetPartAtIndex(0).getPartAtIndex(0).getPointAtIndex(0);
			temp.append(getMapX(pt[0]));
			temp.append(" ");
			temp.append(getMapy(pt[1]));
		}
		if (!StringUtil.isBlank(sufixText)) {
			temp.append(sufixText);
		}
		return temp.toString();
	}

	public String OutputX(GeometryWithData data) {
		StringBuilder temp = new StringBuilder(prepText);
		if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBPOINT) {
			temp.append(getMapX(data.getWKBGeometry().getEnv().maxX));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBLINESTRING) {
			double[] pt = ((Line) data.getWKBGeometry()).getPointAtIndex(0);
			temp.append(getMapX(pt[0]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBPOLYGON) {
			double[] pt = ((Polygon) data.getWKBGeometry()).getPartAtIndex(0)
					.getPointAtIndex(0);
			temp.append(getMapX(pt[0]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTIPOINT) {
			Point pt = ((MultiPoint) data.getWKBGeometry()).getPartAtIndex(0);
			temp.append(getMapX(pt.X));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTILINESTRING) {
			double[] pt = ((MultiLine) data.getWKBGeometry()).getPartAtIndex(0)
					.getPointAtIndex(0);
			temp.append(getMapX(pt[0]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTIPOLYGON) {
			double[] pt = ((MultiPolygon) data.getWKBGeometry())
					.GetPartAtIndex(0).getPartAtIndex(0).getPointAtIndex(0);
			temp.append(getMapX(pt[0]));
		}

		if (!StringUtil.isBlank(sufixText)) {
			temp.append(sufixText);
		}
		return temp.toString();
	}

	public String OutputY(GeometryWithData data) {
		StringBuilder temp = new StringBuilder(prepText);
		if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBPOINT) {
			temp.append(getMapy(data.getWKBGeometry().getEnv().minY));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBLINESTRING) {
			double[] pt = ((Line) data.getWKBGeometry()).getPointAtIndex(0);
			temp.append(getMapy(pt[1]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBPOLYGON) {
			double[] pt = ((Polygon) data.getWKBGeometry()).getPartAtIndex(0)
					.getPointAtIndex(0);
			temp.append(getMapy(pt[1]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTIPOINT) {
			Point pt = ((MultiPoint) data.getWKBGeometry()).getPartAtIndex(0);
			temp.append(getMapy(pt.Y));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTILINESTRING) {
			double[] pt = ((MultiLine) data.getWKBGeometry()).getPartAtIndex(0)
					.getPointAtIndex(0);
			temp.append(getMapy(pt[1]));
		} else if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBMULTIPOLYGON) {
			double[] pt = ((MultiPolygon) data.getWKBGeometry())
					.GetPartAtIndex(0).getPartAtIndex(0).getPointAtIndex(0);
			temp.append(getMapy(pt[1]));
		}

		if (!StringUtil.isBlank(sufixText)) {
			temp.append(sufixText);
		}
		return temp.toString();
	}

	public String OutputMap(GeometryWithData data, BuildMapParam param) {
		int n = -1;
		StringBuilder temp = new StringBuilder();
		if (!data.getData().containsKey(field)) {
			return "";
		}
		// NSString *data = [data.data objectForKey:self.field];
		String[] compareParams = prepText.split(";");
		if (compareParams == null || compareParams.length == 0)
			return "";

		int type = 2; // 0 GT 1 LT 2 EQ 3 GE 4 LE 5 NE
		if (compareParams[0].equalsIgnoreCase("GT")) {
			type = 0;
		} else if (compareParams[0].equalsIgnoreCase("LT")) {
			type = 1;
		} else if (compareParams[0].equalsIgnoreCase("GE")) {
			type = 3;
		} else if (compareParams[0].equalsIgnoreCase("LE")) {
			type = 4;
		} else if (compareParams[0].equalsIgnoreCase("NE")) {
			type = 5;
		}
		int dataType = 0;// 0 String //1 number
		if (compareParams.length == 2 && compareParams[1].equals("1")) {
			dataType = 1;
		}

		if (dataType == 0) {
			String strData = key.get(key.size() - 1);
			if (data.getData().containsKey(field)
					&& data.getData().get(field) != null) {
				strData = String.valueOf(data.getData().get(field));
			}
			// String strData = data.getData().get(field).toString();
			// 0 GT 1 LT 2 EQ 3 GE 4 LE 5 NE
			for (int i = 0; i < key.size(); i++) {
				int result = strData.toLowerCase().compareTo(
						key.get(i).toLowerCase());
				switch (type) {
				case 0: {
					if (result > 0) {
						n = i;
					}
					break;
				}
				case 1: {
					if (result < 0) {
						n = i;
					}
					break;
				}
				case 2: {
					if (result == 0) {
						n = i;
					}
					break;
				}
				case 3: {
					if (result > 0 || result == 0) {
						n = i;
					}
					break;
				}
				case 4: {
					if (result < 0 || result == 0) {
						n = i;
					}
					break;
				}
				case 5: {
					if (result != 0) {
						n = i;
					}
					break;
				}
				default:
					break;
				}

				if (n != -1) {
					break;
				}
			}
		} else if (dataType == 1) {
			double dataVal = Double.parseDouble(key.get(key.size() - 1));
			if (data.getData().containsKey(field)
					&& data.getData().get(field) != null) {
				dataVal = Double.parseDouble(String.valueOf(data.getData().get(
						field)));
			}
			// 0 GT 1 LT 2 EQ 3 GE 4 LE 5 NE
			for (int i = 0; i < key.size(); i++) {
				double keyData = Double.parseDouble(key.get(i).toString());
				switch (type) {
				case 0: {
					if (dataVal > keyData) {
						n = i;
					}
					break;
				}
				case 1: {
					if (dataVal < keyData) {
						n = i;
					}
					break;
				}
				case 2: {
					if (dataVal == keyData) {
						n = i;
					}
					break;
				}
				case 3: {
					if (dataVal >= keyData) {
						n = i;
					}
					break;
				}
				case 4: {
					if (dataVal <= keyData) {
						n = i;
					}
					break;
				}
				case 5: {
					if (dataVal != keyData) {
						n = i;
					}
					break;
				}
				default:
					break;
				}

				if (n != -1) {
					break;
				}
			}

		}
		if (n == -1) {
			n = value.size() - 1;
		}

		if (value != null) {
			temp.append(value.get(n));
		}

		if (this.subInsertores != null) {
			this.subInsertores.get(n).execute(temp, data, param);
		}

		if (!StringUtil.isBlank(sufixText)) {
			temp.append(sufixText);
		}
		return temp.toString();
	}

	public double getMapX(double srcX) {
		return (srcX - oriX) / scaleX;
	}

	public double getMapy(double srcY) {
		return (srcY - oriY) / scaleY;
	}
}
