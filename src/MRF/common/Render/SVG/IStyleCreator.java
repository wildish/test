package MRF.common.Render.SVG;

import java.util.List;

public interface IStyleCreator {
	 String getStandardName(String styleName);

     //
     String getValue(String styleName);

     //
     void formatStyle(String plainText);

     //
     String getFormatedText();

     //
     String getPlainText();

     //
     void addStyle(String styleName, String styleValue);

     //
     void removeStyle(String styleName);

     //
     void clear();

     //
     String buildStyledSymbol(String oriSymbol);

     //
     void setStyleStatus(String styleName, boolean bHasOutput);

     boolean getStyleStatus(String styleName);

     //
     List<String> getStyles();
}
