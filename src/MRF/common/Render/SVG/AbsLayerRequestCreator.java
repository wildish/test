package MRF.common.Render.SVG;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;
import org.dom4j.Node;

import MRF.common.Data.SpatialResult;
import MRF.common.Data.sqlite.SpatialDataAccess;
import MRF.common.Render.SVG.SVGElementInsertor.InsertorType;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import MRF.common.geometry.Line;

public abstract class AbsLayerRequestCreator implements ILayerRequestCreator {
	public Element m_tLayer = null;
	public BuildMapParam m_tBuildParam = null;

	public String m_strMapId;
	public String m_strLayerId;
	public String m_strLayerCrs;
	public StringBuilder m_tResult;
	public StringBuilder m_tTemp;

	public double m_dScale;
	public double m_dOriginX;
	public double m_dOriginY;
	public double m_dWorldPerPixelX;
	public double m_dWorldPerPixelY;
	public String m_strCurrLayerParams;
	public String m_strOriginAndScale;
	private AbsStyleCreator absStyleCreator;
	public List<SVGElementInsertor> m_tInsertorArray;
	public SpatialResult result;
	public Map<String, String> m_tSymbols;

	public AbsLayerRequestCreator() {
		m_tResult = new StringBuilder();
		m_tTemp = new StringBuilder();
		m_tInsertorArray = new ArrayList<SVGElementInsertor>();
		m_tSymbols = new HashMap<String, String>();
	}

	public AbsLayerRequestCreator(BuildMapParam runtimeParam, String szMapId,
			Element tLayer, String strLayerParam) {
		super();
		try {
			m_tResult = new StringBuilder();
			m_tTemp = new StringBuilder();
			m_tInsertorArray = new ArrayList<SVGElementInsertor>();
			m_strMapId = szMapId;
			m_tSymbols = new HashMap<String, String>();

			m_strMapId = szMapId;
			m_tLayer = tLayer;
			m_tBuildParam = runtimeParam;
			m_strCurrLayerParams = strLayerParam;

			m_strLayerId = MRFUtil.getAttrValueWithDefault(tLayer, "id", "");
			m_strLayerCrs = MRFUtil.getAttrValueWithDefault(tLayer, "crs", "");
			if (!StringUtil.isBlank(MRFUtil.getAttrValueWithDefault(tLayer,
					"uomFactor", ""))) {
				double dApplyScale = runtimeParam.m_dUomFactor
						/ MRFUtil.getAttrDoubleWidthDefault(tLayer,
								"uomFactor", 1);
				m_dScale = runtimeParam.m_dScale * dApplyScale;
				m_dOriginX = runtimeParam.m_dOriginX * dApplyScale;
				m_dOriginY = runtimeParam.m_dOriginY * dApplyScale;
				m_dWorldPerPixelX = runtimeParam.m_dWorldPerPixelX
						* dApplyScale;
				m_dWorldPerPixelY = runtimeParam.m_dWorldPerPixelY
						* dApplyScale;
			} else {
				m_dScale = runtimeParam.m_dScale;
				m_dOriginX = runtimeParam.m_dOriginX;
				m_dOriginY = runtimeParam.m_dOriginY;
				m_dWorldPerPixelX = runtimeParam.m_dWorldPerPixelX;
				m_dWorldPerPixelY = runtimeParam.m_dWorldPerPixelY;
			}
			StringBuffer buffer = new StringBuffer();
			buffer.append(" origin=\"");
			buffer.append(m_dOriginX);
			buffer.append(" ");
			buffer.append(m_dOriginY);
			buffer.append("\" scale=\"");
			buffer.append(m_dWorldPerPixelX);
			buffer.append(" ");
			buffer.append(m_dWorldPerPixelY);
			buffer.append("\"");
			m_strOriginAndScale = buffer.toString();
			//

		} catch (Exception ex) {
			// Log.i("AbsLayerRequestCreator",
			// "AbsLayerRequestCreator():"+ex.getMessage());
			ex.printStackTrace();
		}
	}

	public String getResult() {
		return this.m_tResult.toString();
	}

	public AbsStyleCreator getStyleCreator() {
		return absStyleCreator;
	}

	public void setStyleCreator(AbsStyleCreator absStyleCreator) {
		this.absStyleCreator = absStyleCreator;
	}

	public void initialize(String szMapId, Element tNode) {
		try {
			m_strMapId = szMapId;
			m_tLayer = tNode;
			this.beginBuild();
			this.doBuild();
			if (StringUtil.isBlank(m_tResult.toString())) {
				return;
			}
			this.endBuild();
		} catch (Exception ex) {
			// Log.i("AbsLayerRequestCreator", "initialize():"+ex.getMessage());
			ex.printStackTrace();
		}
	}

	public void initialize(BuildMapParam runtimeParam, String szMapId,
			Element tLayer, String strLayerParam) {
		try {
			m_strMapId = szMapId;
			m_tLayer = tLayer;
			m_tBuildParam = runtimeParam;
			this.m_strCurrLayerParams = strLayerParam;

			String tId = tLayer.attributeValue("id");
			if (StringUtil.isBlank(tId) == false) {
				m_strLayerId = tId;
			}

			String tCrs = tLayer.attributeValue("crs");
			if (StringUtil.isBlank(tCrs) == false) {
				m_strLayerCrs = tCrs;
			}

			String tUomFactor = tLayer.attributeValue("uomFactor");
			if (StringUtil.isBlank(tUomFactor) == false) {
				double dApplyScale = runtimeParam.m_dUomFactor
						/ Double.parseDouble(tUomFactor);
				m_dScale = runtimeParam.m_dScale * dApplyScale;
				m_dOriginX = runtimeParam.m_dOriginX * dApplyScale;
				m_dOriginY = runtimeParam.m_dOriginY * dApplyScale;
				m_dWorldPerPixelX = runtimeParam.m_dWorldPerPixelX
						* dApplyScale;
				m_dWorldPerPixelY = runtimeParam.m_dWorldPerPixelY
						* dApplyScale;
			} else {
				m_dScale = runtimeParam.m_dScale;
				m_dOriginX = runtimeParam.m_dOriginX;
				m_dOriginY = runtimeParam.m_dOriginY;
				m_dWorldPerPixelX = runtimeParam.m_dWorldPerPixelX;
				m_dWorldPerPixelY = runtimeParam.m_dWorldPerPixelY;
			}

			StringBuffer buffer = new StringBuffer();
			buffer.append(" origin=\"");
			buffer.append(m_dOriginX);
			buffer.append(" ");
			buffer.append(m_dOriginY);
			buffer.append("\" scale=\"");
			buffer.append(m_dWorldPerPixelX);
			buffer.append(" ");
			buffer.append(m_dWorldPerPixelY);
			buffer.append("\"");
			m_strOriginAndScale = buffer.toString();
			//
			if (inScale()) {
				//
				this.beginBuild();
				this.doBuild();
				if (StringUtil.isBlank(m_tResult.toString()))
					return;
				this.endBuild();
			} else {
				m_tResult = new StringBuilder();
			}
		} catch (Exception ex) {
			// Log.i("AbsLayerRequestCreator", "initialize():"+ex.getMessage());
		}

	}

	public abstract void beginBuild();

	public abstract void doBuild();

	public abstract void endBuild();

	public String getParameter(String paramName) {
		return this.m_tBuildParam.getParameter(m_strCurrLayerParams, paramName);
	}

	public boolean inScale() {
		try {
			String strNoScale = this.getParameter("noscale");
			if (strNoScale.equals("1") || "true".equals(strNoScale)) {
				return true;
			}

			List<Element> lowNodeList = MRFUtil.getchildListByName(
					m_tLayer, "scaleLow");
			if (lowNodeList == null || lowNodeList.size() == 0) {
				return true;
			}
			Element tScaleLow = lowNodeList.get(0);
			double scaleLow = Double.parseDouble(MRFUtil.getText(tScaleLow));
			if (scaleLow > m_dScale) {
				return false;
			}

			List<Element> highNodeList = MRFUtil.getchildListByName(
					m_tLayer, "scaleHigh");
			if (highNodeList == null || highNodeList.size() == 0) {
				return true;
			}
			Element tScaleHigh = highNodeList.get(0);

			double scaleHigh = Double.parseDouble(MRFUtil.getText(tScaleHigh));
			if (scaleHigh <= m_dScale) {
				return false;
			}
		} catch (Exception ex) {

		}
		return true;
	}

	// private Method getMethod(Class<?> clazz, String methodName,
	// final Class<?>[] classes) {
	// Method method = null;
	// try {
	// method = clazz.getDeclaredMethod(methodName, classes);
	// } catch (NoSuchMethodException e) {
	// try {
	// method = clazz.getMethod(methodName, classes);
	// } catch (NoSuchMethodException ex) {
	// if (clazz.getSuperclass() == null) {
	// return method;
	// } else {
	// method = getMethod(clazz.getSuperclass(), methodName,
	// classes);
	// }
	// }
	// }
	// return method;
	// }

	public void callAgent(Element tNode) {
		String name = tNode.getName();
		//
		// Method method = getMethod(this.getClass(), name,
		// new Class[] { Element.class });
		// if (method == null) {
		// return;
		// }
		//
		// try {
		// method.setAccessible(true);
		// method.invoke(this, (Element) tNode);
		// } catch (Exception ex) {
		// Log.i("AbsLayerRequestCreator", "callAgent():" + ex.getMessage());
		// }
		if (tNode.getNodeType() != Element.ELEMENT_NODE) {
			return;
		}
		Element ele = tNode;
		if ("active".equals(name)) {
			active(ele);
		} else if ("style".equals(name)) {
			style(ele);
		} else if ("title".equals(name)) {
			title(ele);
		} else if ("contents".equals(name)) {
			contents(ele);
		} else if ("visible".equals(name)) {
			visible(ele);
		} else if ("field".equals(name)) {
			field(ele);
		} else if ("prop".equals(name)) {
			prop(ele);
		} else if ("text".equals(name)) {
			text(ele);
		} else if ("featureAttribute".equals(name)) {
			featureAttribute(ele);
		} else if ("map".equals(name)) {
			map(ele);
		} else if ("dataSource".equals(name)) {
			dataSource(ele);
		}
		// switch (name) {
		// case "active":
		// active(ele);
		// break;
		// case "style":
		// style(ele);
		// break;
		// case "title":
		// title(ele);
		// break;
		// case "contents":
		// contents(ele);
		// break;
		// case "visible":
		// visible(ele);
		// break;
		// case "field":
		// field(ele);
		// break;
		// case "prop":
		// prop(ele);
		// break;
		// case "text":
		// text(ele);
		// break;
		// case "featureAttribute":
		// featureAttribute(ele);
		// break;
		// case "map":
		// map(ele);
		// break;
		// case "dataSource":
		// dataSource(ele);
		// break;
		// }
	}

	public void active(Element tActive) {

	}

	public void style(Element tStyle) {

	}

	public void title(Element tTitle) {
		if (tTitle == null) {
			return;
		}
	}

	public void contents(Element tContents) {
		if (tContents == null)
			return;

		m_tResult.append(tContents.getTextTrim());
	}

	public void visible(Element tVisible) {
		if (tVisible == null) {
			return;
		}
		// m_tResult.append(String.format("<visible>%s</visible>",
		// tVisible.getTextContent()));
	}

	public void field(Element tField) {
		if (MRFUtil.getAttrBoolWithDefault(tField, "Accessibility", true)) {
			SVGElementInsertor insertor = new SVGElementInsertor(
					InsertorType.FieldInsertor, "", MRFUtil.getText(tField));
			m_tInsertorArray.add(insertor);
		}
	}

	/**
	 * prop agent to ouput mrx property
	 * 
	 * @param tProp
	 */
	public void prop(Element tProp) {
		try {
			SVGElementInsertor insertor = new SVGElementInsertor(
					InsertorType.PropInsertor, "", MRFUtil.getText(tProp));
			m_tInsertorArray.add(insertor);
			String fieldName = getParameter("fieldName");
			List<Element> tIdNode = MRFUtil.getNodeListByPath(
					m_tLayer, "featureAttribute[@tag=\"thematic\"]");
			if (tIdNode == null
					&& !StringUtil.isBlank(fieldName)
					&& StringUtil.trim(MRFUtil.getText(tProp))
							.equalsIgnoreCase("id")) {
				SVGElementInsertor insertor1 = new SVGElementInsertor(
						InsertorType.TextInsertor, "\" thematic=\"",
						MRFUtil.getText(tProp));
				m_tInsertorArray.add(insertor1);
				SVGElementInsertor insertor2 = new SVGElementInsertor(
						InsertorType.FieldInsertor, "", fieldName);
				m_tInsertorArray.add(insertor2);
			}
		} catch (Exception ex) {
			// Log.i("AbsLayerRequestCreator", "prop():" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 * text agent to echo text
	 * 
	 * @param tText
	 */
	public void text(Element tText) {
		if (MRFUtil.getAttrBoolWithDefault(tText, "Accessibility", true)) {
			SVGElementInsertor insertor = new SVGElementInsertor(
					InsertorType.TextInsertor, MRFUtil.getText(tText), "");
			m_tInsertorArray.add(insertor);
		}
	}

	/**
	 * featureAttribute agent
	 * 
	 * @param tFeatureAttribute
	 */
	public void featureAttribute(Element tFeatureAttribute) {
		try {
			String strTag = MRFUtil.getAttrValueWithDefault(tFeatureAttribute,
					"tag", "").toLowerCase();
			if (strTag.length() == 0)
				return;
			if ("tip".equals(strTag) || "id".equals(strTag)
					|| "fill".equals(strTag) || "label".equals(strTag)
					|| "stroke".equals(strTag) || "stroke-width".equals(strTag)) {
				return;
			}
			/*
			 * switch (strTag) { case "tip": return; case "id": return; case
			 * "fill": return; case "label": return; case "stroke": return; case
			 * "stroke-width": return; }
			 */
			//
			if (!strTag.equalsIgnoreCase("style")) {
				// [self._styleCreator setStyleStatus:strTag, true];
				// [_m_tResult appendFormat:@" %@=\"", strTag ];
				SVGElementInsertor insertor = new SVGElementInsertor(
						InsertorType.TextInsertor, strTag, "");
				m_tInsertorArray.add(insertor);
			}

			for (int i = 0; i < tFeatureAttribute.elements().size(); i++) {
				Element tChild = (Element) tFeatureAttribute
						.elements().get(i);
				if (tChild.getNodeType() != Node.COMMENT_NODE)
					callAgent(tChild);
			}
			if (!strTag.equalsIgnoreCase("style")) {
				// [_m_tResult appendstring:@"\""];
				SVGElementInsertor insertor = new SVGElementInsertor(
						InsertorType.TextInsertor, "\"", "");
				m_tInsertorArray.add(insertor);
			}
		} catch (Exception ex) {
			// Log.i("AbsLayerRequestCreator", "featureAttribute():" +
			// ex.getMessage());
			ex.printStackTrace();
		}
	}

	public void map(Element tMap) {
		try {
			StringBuilder strCompare = new StringBuilder(
					tMap.attributeValue("compare"));
			if (StringUtil.isBlank(strCompare.toString())) {
				return;
			}
			String strAsNumber = tMap.attributeValue("asNumber");
			if (StringUtil.isBlank(strAsNumber)) {
				return;
			}
			List<Element> nodeList = MRFUtil.getchildListByName(tMap,
					"keySource");
			if (nodeList == null || nodeList.size() == 0) {
				return;
			}
			Element tChild = null;
			String thematicField = "";
			int index = 0;
			for (index = 0; index < nodeList.size(); index++) {
				tChild = nodeList.get(index);
				if (tChild.getName().equalsIgnoreCase("keySource"))
					thematicField = MRFUtil.getText(MRFUtil.getChildByName(
							tChild, "field"));
			}
			if (strAsNumber.endsWith("true")) {
				strCompare.append(";1");
			} else {
				strCompare.append(";0");
			}
			SVGElementInsertor insertor = new SVGElementInsertor(
					InsertorType.MapInsertor, strCompare.toString(),
					thematicField);
			for (index = 0; index < tMap.elements().size(); index++) {
				tChild = (Element) tMap.elements().get(index);
				if (tChild.getNodeType() != Element.ELEMENT_NODE
						|| tChild == null
						|| !tChild.getName().equalsIgnoreCase("mapEntry")) {
					continue;
				}
				Element tMapEntry = tChild;
				String strKey = MRFUtil.getAttrValueWithDefault(tMapEntry,
						"key", "");
				String strValue = MRFUtil.getAttrValueWithDefault(tMapEntry,
						"value", "");
				if (strKey.length() == 0 || strValue.length() == 0) {
					continue;
				}

				if (StringUtil.contains(strValue, "rgb(", true)) {
					strValue = StringUtil.convertRGBToHEX(strValue);
				}
				insertor.key.add(strKey);
				if (StringUtil.trim(strValue).toLowerCase().startsWith("url")) {
					insertor.value.add(getSymbolyName(strValue));
				} else
					insertor.value.add(strValue);
			}
			m_tInsertorArray.add(insertor);
		} catch (Exception ex) {
			// Log.i("AbsLayerRequestCreator", "map():" + ex.getMessage());
		}
	}

	public void mapEntry(String styleName, String key, String value) {
		StringBuffer buffer = new StringBuffer();
		if (styleName == "style") {
			IStyleCreator styleCreator = AbsStyleCreator.CreateInstance(this
					.getStyleCreator());
			styleCreator.formatStyle(value);
			String thematicValue = styleCreator.getFormatedText();
			List<String> styles = styleCreator.getStyles();
			for (String thematicStyleName : styles) {
				this.getStyleCreator().setStyleStatus(thematicStyleName, true);
			}
			//
			buffer = new StringBuffer();
			buffer.append("$$$$");
			buffer.append(m_strLayerId);
			buffer.append("$");
			buffer.append(styleName);
			buffer.append("$");
			buffer.append(key);
			buffer.append("$$$");
			String szHolder = buffer.toString();
			this.m_tBuildParam.cacheEntity(szHolder, thematicValue);
			buffer = new StringBuffer();
			buffer.append("\"");
			buffer.append(key);
			buffer.append("\":\"");
			buffer.append(szHolder);
			buffer.append("\";");
			m_tResult.append(buffer.toString());
		} else {
			buffer = new StringBuffer();
			buffer.append("\"");
			if (value.trim().toLowerCase().startsWith("url")) {
				buffer.append(key);
				buffer.append("\":\"");
				buffer.append(this.getSymbolyName(value));
			} else {
				buffer.append(key);
				buffer.append("\":\"");
				buffer.append(value);
			}
			buffer.append("\";");
			m_tResult.append(buffer.toString());
		}
	}

	public String getSymbolyName(String url) {
		int nPos1 = url.toLowerCase().indexOf("url(#");
		int nPos2 = url.indexOf(")");
		if (nPos2 <= nPos1 + 5)
			return "";

		String symbolName = url.substring(nPos1 + 5, nPos2);
		if (!m_tSymbols.containsKey(symbolName)) {
			if (MRFUtil.sharedInstance().containsInCachedParameter(
					"allsymboldefines")) {
				@SuppressWarnings("unchecked")
				Map<String, String> symbols = (Map<String, String>) MRFUtil
						.sharedInstance()
						.getCachedParameter("allsymboldefines");
				if (symbols == null) {
					return "";
				}

				if (symbols.containsKey(symbolName)) {
					m_tSymbols.put(symbolName, symbols.get(symbolName));
				} else if (symbolName.length() > 8) {
					String tempName2 = symbolName.substring(0,
							symbolName.length() - 8);
					if (StringUtil.isBlank(symbols.get(tempName2)) == false) {
						StringBuffer buffer = new StringBuffer();
						buffer.append(" fill=\"");
						buffer.append(symbolName.substring(symbolName.length() - 7));
						buffer.append("\" ");
						String color = buffer.toString();
						StringBuilder symbol = new StringBuilder(symbols.get(
								tempName2).toString());
						int range = symbol.toString().toLowerCase()
								.indexOf("fill=");
						if (range >= 0) {
							// String oriColor = symbol.substring(range, range +
							// 5+9);
							String sym = symbol.toString().toLowerCase();
							String startSym = sym.substring(0, range);
							String endSym = sym.substring(range + 5 + 9);
							// sym = sym.replaceAll(oriColor, color);
							buffer = new StringBuffer();
							buffer.append(startSym);
							buffer.append(color);
							buffer.append(endSym);
							sym = buffer.toString();
							symbol = new StringBuilder();
							symbol.append(sym);
						} else {
							symbol.insert(2, color);
						}
						String sym1 = symbol.toString().toLowerCase();
						sym1 = sym1.replace(tempName2, symbolName);
						m_tSymbols.put(symbolName, sym1);
					} else {
						StringBuffer buffer = new StringBuffer();
						buffer.append("<g id=\"");
						buffer.append(symbolName);
						buffer.append("\"></g>");
						m_tSymbols.put(symbolName, buffer.toString());
					}
				}
			}
		}
		return symbolName;
	}

	// private readonly ILogger _logger = new ConsoleLogFactory(new
	// LogFilter()).CreateLogger(typeof(AnnotationLayerRequestCreator));

	public void dataSource(Element tDatasource) {
		try {
			Element node = MRFUtil.getFirstChild(tDatasource);
			if (node != null) {
				String strIdCol = MRFUtil.getTextWithDefault(node, "idcol",
						"id");
				String strGeomCol = MRFUtil.getTextWithDefault(node, "geomcol",
						"geometry");
				String strColmnList = MRFUtil.getTextWithDefault(node,
						"columnlist", "");
				String strTableName = MRFUtil.getTextWithDefault(node, "table",
						"");

				String strWhere = MRFUtil.getTextWithDefault(node, "where", "");

				// List<Point> points = new ArrayList<Point>();
				// points.add(new Point(m_tBuildParam.left,
				// m_tBuildParam.bottom));
				// points.add(new Point(m_tBuildParam.right,
				// m_tBuildParam.top));
				// Line queryShape = new Line(points);
				List<double[]> points = new ArrayList<double[]>();
				points.add(new double[] { m_tBuildParam.left,
						m_tBuildParam.bottom });
				points.add(new double[] { m_tBuildParam.right,
						m_tBuildParam.top });
				Line queryShape = new Line(points);
				result = SpatialDataAccess.getInstance().query("client",
						strTableName, strColmnList, strIdCol, strGeomCol,
						strWhere, queryShape);
			}
		} catch (Exception ex) {
			// Log.i("AbsLayerRequestCreator", "datasource():" +
			// ex.getMessage());
		}
	}

	public void Initialize(String szMapId, Element tNode) {
		throw new RuntimeException();
	}

}
