package MRF.common.Render.SVG;

import java.io.Serializable;

public class LegendSymbolStyleEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public String Name;
	public String Style;
}