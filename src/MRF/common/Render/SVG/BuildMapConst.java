package MRF.common.Render.SVG;

public class BuildMapConst {
	public final static String XAML_XMLNS = "http://schemas.microsoft.com/client/2007";
    public final static String XAML_XMLNS_X = "http://schemas.microsoft.com/winfx/2006/xaml";

    public final static double METER_PER_PIXEL = 0.00026458;
    public final static double FEET_PER_PIXEL = 0.000868056;
    public final static double SVY_FEET_PER_PIXEL = 0.000868054;
    public final static double DEGREE_PER_PIXEL = METER_PER_PIXEL / 110000.0;
}
