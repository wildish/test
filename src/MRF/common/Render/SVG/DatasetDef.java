package MRF.common.Render.SVG;

import java.io.Serializable;

public class DatasetDef implements Serializable {
	private static final long serialVersionUID = 1L;
	public int Id;
	public String Database;
	public String Connection;
}
