package MRF.common.Render.SVG;

import java.util.List;

import org.dom4j.Element;
import org.dom4j.Node;

import MRF.common.Data.GeometryWithData;
import MRF.common.Render.SVG.SVGElementInsertor.InsertorType;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;

public class LayerRequestCreator extends AbsLayerRequestCreator implements
		ILayerRequestCreator {
	public Element m_tIdFeature;
	public Element m_tTipFeature;
	public Element m_tAutoLabelFeature;
	public Element m_tLineStyle;
	public Element m_tFillFeature;
	public Element m_tStrokeFeature;

	public LayerRequestCreator(BuildMapParam runtimeParam, String szMapId,
			Element tLayer, String strLayerParam) {
		super(runtimeParam, szMapId, tLayer, strLayerParam);
		this.setStyleCreator(new PathStyleCreator());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void beginBuild() {

		Element tStyle = MRFUtil.getChildByName(m_tLayer, "style");
		if (tStyle != null) {
			this.getStyleCreator().formatStyle(MRFUtil.getText(tStyle));
		}

		m_tResult.append("\r\n<g");

		m_tResult.append(" t=\"l\" type=\"");
		m_tResult.append(m_tLayer.getName());
		m_tResult.append("\"");
		List<Node> namedNodeMap = m_tLayer.attributes();

		Boolean isHideFirst = false;
		for (int i = 0; i < namedNodeMap.size(); i++) {
			Node attr = namedNodeMap.get(i);
			if (attr.getName().toLowerCase().equals("id")) {
				m_tResult.append(" ");
				m_tResult.append(attr.getName());
				m_tResult.append("=\"");
				m_tResult.append(m_strMapId);
				m_tResult.append("_");
				m_tResult.append(attr.getStringValue());
				m_tResult.append("\"");
				if (attr.getStringValue().equals("scaleBar")
						|| attr.getStringValue().equals("NorthArrow")) {
					isHideFirst = true;
				}
			} else {
				m_tResult.append(" ");
				m_tResult.append(attr.getName());
				m_tResult.append("=\"");
				m_tResult.append(attr.getStringValue());
				m_tResult.append("\"");
			}
		}
		String styleStr = this.getStyleCreator().getFormatedText();
		if (isHideFirst) {
			styleStr += ";display:none;";
			isHideFirst = false;
		}
		m_tResult.append(" style=\"");
		m_tResult.append(styleStr);
		m_tResult.append("\" ");

	}

	@SuppressWarnings("unchecked")
	public void doBuild() {
		if (!inScale())
			return;

		beginBuild();
		Element tStyle = MRFUtil.getChildByName(m_tLayer, "style");
		this.callAgent(tStyle);

		if (m_tLayer.attributeValue("name").equalsIgnoreCase("North Arrow")) {
			m_tResult.append("transform=\"rotate(" + m_tBuildParam.angle
					+ ")\"");
		}

		m_tResult.append(">");

		List<Element> tNodes = m_tLayer.elements();
		for (int i = 0; i < tNodes.size(); i++) {
			Element tNode = tNodes.get(i);
			if (tNode.getName().equalsIgnoreCase("style")) {
				continue;
			} else if (tNode.getName().equalsIgnoreCase("transform")) {
				continue;
			}
			callAgent(tNode);
		}
		this.endBuild();
	}

	@Override
	public void endBuild() {
		if (result != null && result.getDatas().size() > 0) {
			int pos = m_tResult.length();
			boolean isRollback = false;

			for (int i = 0; i < result.getDatas().size(); i++) {
				pos = m_tResult.length();
				GeometryWithData data = result.getDatas().get(i);
				pos = m_tResult.length();
				isRollback = false;
				for (int j = 0; j < m_tInsertorArray.size(); j++) {
					isRollback = !((SVGElementInsertor) m_tInsertorArray.get(j))
							.execute(m_tResult, data, m_tBuildParam);
					// some error happen, remove any text
					if (isRollback) {
						m_tResult.delete(pos - 1, m_tResult.length() - pos);
						break;
					}
				}
			}

		}
		m_tResult.append("</g>");

	}

	@SuppressWarnings("unchecked")
	public void outputLineStyle() {

		if (m_tLineStyle == null) {
			return;
		}
		m_tResult.append(" style=\"");
		// linestyle name
		String strLSName = null;
		Element tLSName = MRFUtil
				.getChildByName(m_tLineStyle, "name");
		if (!StringUtil.isBlank(MRFUtil.getAttrValueWithDefault(m_tLineStyle,
				"name", ""))) {
			strLSName = getSymbolyName(MRFUtil.getAttrValueWithDefault(
					m_tLineStyle, "name", ""));
		} else if (tLSName != null) {
			if (tLSName.elements().size() < 1) {
				strLSName = this.getSymbolyName(MRFUtil.getText(tLSName));
			}
		} else {
			return;
		}
		// linestyle type
		String strLSType = "";
		if (!StringUtil.isBlank(MRFUtil.getAttrValueWithDefault(m_tLineStyle,
				"type", ""))) {
			strLSType = MRFUtil.getAttrValueWithDefault(m_tLineStyle, "type",
					"");
		} else {
			Element tLSType = MRFUtil.getChildByName(m_tLineStyle,
					"type");
			if (tLSType != null) {
				strLSType = MRFUtil.getText(tLSType);
			}
		}
		// line style
		m_tResult.append("<Mrx.LineStyle type=\"");
		m_tResult.append(strLSType);
		m_tResult.append("\"");

		if (!StringUtil.isBlank(strLSName)) {
			m_tResult.append(" name=\"");
			m_tResult.append(strLSName);
			m_tResult.append("\">");
		} else if (tLSName != null) {
			m_tResult.append(" ><name>");
			List<Element> tChilds = tLSName.elements();
			for (int i = 0; i < tChilds.size(); i++) {
				Element tChild = tChilds.get(i);
				if (tChild.getNodeType() != Element.COMMENT_NODE) {
					this.callAgent(tChild);
				}
			}
			m_tResult.append("</name>");
		}

		if (m_tStrokeFeature != null || m_tFillFeature != null) {
			m_tResult.append("<default>");
			if (m_tStrokeFeature != null) {
				m_tResult.append(";Stroke:");
				List<Element> tChilds = m_tStrokeFeature.elements();
				for (int i = 0; i < tChilds.size(); i++) {
					Element tChild = tChilds.get(i);
					if (tChild.getNodeType() != Element.COMMENT_NODE)
						this.callAgent(tChild);
				}
				this.getStyleCreator().setStyleStatus("Stroke", true);
			}

			if (m_tFillFeature != null) {
				m_tResult.append(";Fill:");
				List<Element> tChilds = m_tFillFeature.elements();
				for (int i = 0; i < tChilds.size(); i++) {
					Element tChild = tChilds.get(i);
					if (tChild.getNodeType() != Element.COMMENT_NODE) {
						this.callAgent(tChild);
					}
				}
				this.getStyleCreator().setStyleStatus("Fill", true);
			}
			//
			m_tResult.append(this.getStyleCreator().getPlainText());
			m_tResult.append("</default>");
		}
		//
		m_tResult.append("</Mrx.LineStyle>");
		m_tResult.append("\"");

	}

	public void outputAutoLabel() {

		String strFSize = "10";
		String strTextField = "";
		String strOffset = "0.0,0.0";
		if (m_tAutoLabelFeature == null) {
			return;
		}
		List<Element> minFontSizeNodeList = MRFUtil
				.getchildListByName(m_tAutoLabelFeature, "minFontSize");
		if (minFontSizeNodeList != null && minFontSizeNodeList.size() > 0) {
			strFSize = MRFUtil.getText(minFontSizeNodeList.get(0));
		}
		List<Element> textFieldNodeList = MRFUtil.getchildListByName(
				m_tAutoLabelFeature, "textField");
		if (textFieldNodeList != null && textFieldNodeList.size() > 0) {
			strTextField = MRFUtil.getText(textFieldNodeList.get(0));
		}
		List<Element> offsetNodeList = MRFUtil.getchildListByName(
				m_tAutoLabelFeature, "offset");
		if (offsetNodeList != null && offsetNodeList.size() > 0) {
			strOffset = MRFUtil.getText(offsetNodeList.get(0));
		}
		m_tResult.append("<MrxSvg.AutoLabelTexts");
		m_tResult.append(m_strOriginAndScale);
		m_tResult.append(" text-scale=\"1.0\" font-size=\"");
		m_tResult.append(strFSize);
		m_tResult.append("\" offset=\"");
		m_tResult.append(strOffset);
		m_tResult.append("\" ><Mrx.Field name=\"");
		m_tResult.append(strTextField);
		m_tResult.append("\"/></MrxSvg.AutoLabelTexts>");

	}

	@SuppressWarnings("unchecked")
	public void outputTips() {

		if (m_tTipFeature == null) {
			return;
		}
		m_tResult.append(" tip=\"");
		List<Element> tTipChilds = m_tTipFeature.elements();
		for (int i = 0; i < tTipChilds.size(); i++) {
			Element tTipChild = tTipChilds.get(i);
			if (tTipChild.getNodeType() != Element.COMMENT_NODE)
				this.callAgent(tTipChild);
		}
		m_tResult.append("\" ");

	}

	@SuppressWarnings("unchecked")
	public void outputFeatureId() {
		if (m_tIdFeature == null) {
			return;
		}
		SVGElementInsertor insertor1 = new SVGElementInsertor(
				InsertorType.TextInsertor, String.format(" id=\"%s_%s_",
						m_strMapId, m_strLayerId), "");
		m_tInsertorArray.add(insertor1);
		List<Element> tChilds = m_tIdFeature.elements();
		for (int i = 0; i < tChilds.size(); i++) {
			Element tChild = tChilds.get(i);
			if (tChild.getNodeType() != Element.COMMENT_NODE) {
				this.callAgent(tChild);
			}
		}
		SVGElementInsertor insertor2 = new SVGElementInsertor(
				InsertorType.TextInsertor, "\" ", "");
		m_tInsertorArray.add(insertor2);

	}

	public void initialize(String szMapId, Element tNode) {
		super.initialize(szMapId, tNode);
	}

	public void initialize(BuildMapParam buildParam, String szMapId,
			Element tNode, String szLayerParam) {
		super.initialize(buildParam, szMapId, tNode, szLayerParam);
	}

}
