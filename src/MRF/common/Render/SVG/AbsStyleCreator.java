package MRF.common.Render.SVG;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import MRF.common.Util.StringUtil;

public class AbsStyleCreator implements IStyleCreator{

	protected Map<String, String> styleValues = new HashMap<String, String>();
	protected Map<String, String> standardNames = new HashMap<String, String>();
    protected List<String> hasOutputStyles = new ArrayList<String>();
    
    public AbsStyleCreator() {

    }
    
    public AbsStyleCreator(AbsStyleCreator styleCreator) {
        Map<String, String> standardNames = styleCreator.standardNames;
        if (standardNames != null && standardNames.size() > 0) {
            for (String key : standardNames.keySet())
            {
            	String value = standardNames.get(key);
                this.standardNames.put(key, value);
            }
        }
    }
    
    public static AbsStyleCreator createInstance() {
        return new SvgStyleCreator();
    }
    
    public static AbsStyleCreator CreateInstance(AbsStyleCreator styleCreator) {
        return new SvgStyleCreator(styleCreator);
    }
    
	@Override
	public String getStandardName(String styleName) {
		if (this.standardNames.containsKey(styleName)) {
            return this.standardNames.get(styleName);
        } else {
            if (this.standardNames != null && this.standardNames.size() > 0) {
                for (String key : this.standardNames.keySet())
                {
                	String value = this.standardNames.get(key);
                    if (value.equals(styleName)) {
                        return key;
                    }
                }      
            }
        }
        return null;
	}

	@Override
	public String getValue(String styleName) {
		try {
			String standardName = this.getStandardName(styleName);
            if (StringUtil.isBlank(standardName)) {
                return "";
            }
            if (this.styleValues.containsKey(standardName))
            {
                return this.styleValues.get(standardName);
            } else {
                return "";
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return "";
	}

	@Override
	public void formatStyle(String plainText) {
		
	}

	@Override
	public String getFormatedText() {
		StringBuilder styleBuilder = new StringBuilder();
        if (this.styleValues != null && this.styleValues.size() > 0) {
            for (String key : this.styleValues.keySet()) {
                if (!this.hasOutputStyles.contains(key)) {
                	String value = this.styleValues.get(key);
                    styleBuilder.append(" ");
                    styleBuilder.append(key);
                    styleBuilder.append("=\"");
                    styleBuilder.append(value);
                    styleBuilder.append("\"");
                }
            }       
        }
        //
        return styleBuilder.toString();
	}

	@Override
	public String getPlainText() {
		try {
            boolean bFirst = true;
            StringBuilder styleBuilder = new StringBuilder();
            if (this.styleValues != null && this.styleValues.size() > 0) {
                for (String key : this.styleValues.keySet()) {
                    if (!this.hasOutputStyles.contains(key)) {
                    	String value = this.styleValues.get(key);
                        if (!bFirst) {
                            styleBuilder.append(";");
                        }
                        styleBuilder.append(key);
                        styleBuilder.append(":");
                        styleBuilder.append(value);
                        //
                        bFirst = false;
                    }
                }            
            }
            //
            return styleBuilder.toString();
        }
        catch (Exception ex)
        {
            //Log.i("AbsStyleCreator", "getPlainText():" + ex.getMessage());
        }
        return "";
	}

	@Override
	public void addStyle(String styleName, String styleValue) {
		String standardName = this.getStandardName(styleName);
        if (!this.hasOutputStyles.contains(standardName)) {
            this.styleValues.put(standardName, styleValue);
        }
	}

	@Override
	public void removeStyle(String styleName) {
		String standardName = this.getStandardName(styleName);
        if (!standardNames.containsKey(standardName)) {
            return;
        }
        String fixedName = (String)standardNames.get(styleName);
        if (this.styleValues.containsKey(fixedName))
        {
            this.styleValues.remove(fixedName);
        }
	}

	@Override
	public void clear() {
		this.styleValues.clear();
	}

	@Override
	public String buildStyledSymbol(String oriSymbol) {
		return null;
	}

	@Override
	public void setStyleStatus(String styleName, boolean hasOutput) {
		String standardName = this.getStandardName(styleName);
        if (hasOutput) {
            if (this.hasOutputStyles.contains(standardName)) {
                return;
            }
            this.hasOutputStyles.add(standardName);
        } else {

            this.hasOutputStyles.remove(standardName);
        }
	}

	@Override
	public boolean getStyleStatus(String styleName) {
		String standardName = this.getStandardName(styleName);
        if (StringUtil.isBlank(standardName)) {
            return true;
        }
        return this.hasOutputStyles.contains(standardName);
	}

	@Override
	public List<String> getStyles() {
		List<String> styles = new ArrayList<String>();
        if (this.styleValues != null && this.styleValues.size() > 0) {
            for (String key : this.styleValues.keySet()) {
                if (!this.hasOutputStyles.contains(key)) {
                    styles.add(key);
                }
            }      
        }
        return styles;
	}

}
