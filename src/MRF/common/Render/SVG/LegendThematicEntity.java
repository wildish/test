package MRF.common.Render.SVG;

import java.io.Serializable;

public class LegendThematicEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public String Name;
	public String FieldName;
	public String ThematicValue;
	public LegendSymbolStyleEntity SymbolStyle;
}