package MRF.common.Render.SVG;

import java.util.HashSet;
import java.util.List;

import org.dom4j.Element;

import MRF.common.Data.GeometryWithData;
import MRF.common.Render.SVG.SVGElementInsertor.InsertorType;
import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import MRF.common.geometry.Point;
import MRF.common.geometry.WKBGeometryType;

public class PointLayerRequestCreator extends LayerRequestCreator {

	public PointLayerRequestCreator(BuildMapParam runtimeParam, String szMapId,
			Element tLayer, String strLayerParam) {
		super(runtimeParam, szMapId, tLayer, strLayerParam);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void doBuild() {
		try {
			if (!inScale())
				return;

			Element tSymbol = MRFUtil.getChildByName(m_tLayer,
					"symbol");
			if (tSymbol == null)
				return;

			beginBuild();

			Element tDataSource = MRFUtil.getChildByName(m_tLayer,
					"dataSource");
			if (tDataSource == null)
				return;

			Element tRotationField = MRFUtil.getChildByName(m_tLayer,
					"rotationField");
			Element tRotationProp = MRFUtil.getChildByName(m_tLayer,
					"rotationProp");

			String strRotation = "0";
			if (tRotationField != null)
				strRotation = MRFUtil.getText(tRotationField).trim();
			else if (tRotationProp != null)
				strRotation = MRFUtil.getText(tRotationProp).trim();

			String strSymbolName = "";

			String strScale = "1.0";
			String strScalable = "false";
			double rotScale = 180.0 / Math.PI;

			m_tResult.append(RotateUtil.generateRotate(m_tBuildParam));
			m_tResult.append(">");

			boolean bEnableHighlight = false;

			String tSymbolHighlightAttr = "";
			if (m_tLayer.attribute("enableHighlight") != null) {
				tSymbolHighlightAttr = m_tLayer.attributeValue(
						"enableHighlight").trim();
			}

			Element tAutoLabel = MRFUtil.getNodeByPath(m_tLayer,
					"featureAttribute[@tag=\"label\"]");
			boolean bAutoLabel = (tAutoLabel != null);
			if (tSymbolHighlightAttr != "" || tSymbolHighlightAttr != null) {
				if (!tSymbolHighlightAttr.equalsIgnoreCase("false")
						&& !tSymbolHighlightAttr.equalsIgnoreCase("0")) {
					bEnableHighlight = true;
				}
			}
			if (tSymbol != null) {
				if (tSymbol.attribute("scale") != null
						&& !StringUtil.isBlank(tSymbol.attributeValue("scale"))) {
					strScale = tSymbol.attributeValue("scale");
				} else {
					Element scaleSysbol = MRFUtil.getChildByName(
							tSymbol, "scale");
					if (scaleSysbol != null) {
						strScale = MRFUtil.getText(scaleSysbol);
					}
				}

				if (tSymbol.attribute("scalable") != null
						&& !StringUtil.isBlank(tSymbol
								.attributeValue("scalable"))) {
					strScalable = tSymbol.attributeValue("scalable");
				} else {
					Element scaleableSysbol = MRFUtil.getChildByName(
							tSymbol, "scalable");
					if (scaleableSysbol != null) {
						strScalable = MRFUtil.getText(scaleableSysbol);
					}
				}

				// listSymbolName
				SVGElementInsertor insertor1 = new SVGElementInsertor(
						InsertorType.TextInsertor, String.format(
								"<use enableHighlight=\"%s\"",
								(bEnableHighlight ? "true" : "false")), "");
				m_tInsertorArray.add(insertor1);
				// [self.m_tResult appendformat:@"<use enableHighlight=\"%s\"",
				// bEnableHighlight ? "true" : "false"];

				if (tSymbol.attribute("name") != null
						&& !StringUtil.isBlank(tSymbol.attributeValue("name"))) {
					SVGElementInsertor insertor2 = new SVGElementInsertor(
							InsertorType.TextInsertor,
							String.format(
									" xlink:href=\"#%s\" transform=\"translate(",
									getSymbolyName(strSymbolName)), "");
					m_tInsertorArray.add(insertor2);
					SVGElementInsertor insertor3 = new SVGElementInsertor(
							InsertorType.XYInsertor, "", "");
					m_tInsertorArray.add(insertor3);
					// [self.m_tResult
					// appendformat:@" xlink:href=\"#%@\" transform=\"translate(<MrxSvg.XY",
					// strSymbolName];
				} else {
					Element tSymbolName = MRFUtil.getChildByName(
							tSymbol, "name");
					Element tSymbolMap = MRFUtil.getChildByName(
							tSymbolName, "map");
					if (tSymbolName != null) {
						if (tSymbolMap != null) {
							SVGElementInsertor insertor6 = new SVGElementInsertor(
									InsertorType.TextInsertor,
									" xlink:href=\"#", "");
							m_tInsertorArray.add(insertor6);
							List<Element> tChilds = tSymbolName
									.elements();
							for (int k = 0; k < tChilds.size(); k++) {
								Element node = tChilds.get(k);
								if (node.getNodeType() != Element.ELEMENT_NODE) {
									continue;
								}
								callAgent(node);
							}

							SVGElementInsertor insertor7 = new SVGElementInsertor(
									InsertorType.TextInsertor,
									"\" transform=\"translate(", "");
							m_tInsertorArray.add(insertor7);
							SVGElementInsertor insertor8 = new SVGElementInsertor(
									InsertorType.XYInsertor, "", "");
							m_tInsertorArray.add(insertor8);
						} else {
							strSymbolName = tSymbolName.getText();
							// for display manager: formate for symbol
							// <symbol><name>circle</name></symbol>
							// [self.m_tResult
							// appendformat:@" xlink:href=\"#%@\" transform=\"translate(<MrxSvg.XY",
							// strSymbolName];
							SVGElementInsertor insertor4 = new SVGElementInsertor(
									InsertorType.TextInsertor,
									String.format(
											" xlink:href=\"#%s\" transform=\"translate(",
											getSymbolyName(strSymbolName)), "");
							m_tInsertorArray.add(insertor4);
							SVGElementInsertor insertor5 = new SVGElementInsertor(
									InsertorType.XYInsertor, "", "");
							m_tInsertorArray.add(insertor5);
						}
					} else// for Gisnetuser: formate for symbol
							// <symbol><name><text>circle<text><field></field></name></symbol>
					{
						// [self.m_tResult appendString:@" xlink:href=\"#"];
						SVGElementInsertor insertor6 = new SVGElementInsertor(
								InsertorType.TextInsertor, " xlink:href=\"#",
								"");
						m_tInsertorArray.add(insertor6);
						// NodeList tChilds = tSymbolName.getChildNodes();
						// for (int k = 0; k < tChilds.getLength(); k++) {
						// Node node = tChilds.item(k);
						// if (node.getNodeType() != Node.ELEMENT_NODE) {
						// continue;
						// }
						// Element tMapEntry = (Element) node;
						// callAgent(tMapEntry);
						// }

						SVGElementInsertor insertor7 = new SVGElementInsertor(
								InsertorType.TextInsertor,
								"\" transform=\"translate(", "");
						m_tInsertorArray.add(insertor7);
						SVGElementInsertor insertor8 = new SVGElementInsertor(
								InsertorType.XYInsertor, "", "");
						m_tInsertorArray.add(insertor8);
						// [self.m_tResult
						// appendString:@"\" transform=\"translate(<MrxSvg.XY"];
					}
				}

				// [self.m_tResult appendformat:@"%@",
				// self.m_strOriginAndScale];
				if (strScalable.equalsIgnoreCase("true")) {
					Element scaleNode = MRFUtil.getChildByName(
							tSymbol, "scale");
					if (MRFUtil
							.isNonnegativeFloatingpointValidationWith(strScale)) {
						double temDouble = Double.NaN;
						Double.parseDouble(strScale);
						double sScale = temDouble / this.m_dWorldPerPixelX;
						SVGElementInsertor insertor9 = new SVGElementInsertor(
								InsertorType.TextInsertor, String.format(
										") scale(%s)", sScale), "", "");
						m_tInsertorArray.add(insertor9);
					} else {
						List<Element> childs = scaleNode.elements();
						if (childs != null && childs.size() > 1) {
							String scaleText = MRFUtil.getTextWithDefault(
									scaleNode, "text", "");
							String scaleField = MRFUtil.getTextWithDefault(
									scaleNode, "field", "");
							if (!StringUtil.isBlank(scaleText)
									|| !StringUtil.isBlank(scaleField)) {
								SVGElementInsertor insertor71 = new SVGElementInsertor(
										InsertorType.MathInsertor, ") scale(",
										scaleText, "");
								SVGElementInsertor insert72 = new SVGElementInsertor(
										InsertorType.FieldInsertor, "",
										String.format("/%s",
												this.m_dWorldPerPixelX),
										scaleField);
								insertor71.addSubInsertor(insert72);
								this.m_tInsertorArray.add(insertor71);
								SVGElementInsertor insert73 = new SVGElementInsertor(
										InsertorType.TextInsertor, ") ", "");
								this.m_tInsertorArray.add(insert73);
							}
						} else {
							SVGElementInsertor insertor9 = new SVGElementInsertor(
									InsertorType.TextInsertor,
									String.format(") scale(1)"), "");
							m_tInsertorArray.add(insertor9);
						}
					}
					// [self.m_tResult appendformat:@"/>) scale(%f)", sScale];
				} else {
					SVGElementInsertor insertor10 = new SVGElementInsertor(
							InsertorType.TextInsertor, String.format(
									") scale(%s)", strScale), "");
					m_tInsertorArray.add(insertor10);
					// [self.m_tResult appendformat:@"/>) scale(%@)", strScale];
				}
			}
			SVGElementInsertor insertor11 = new SVGElementInsertor(
					InsertorType.MathInsertor, " rotate(", ")\"", "");
			if (tRotationField != null) {
				SVGElementInsertor insert13 = new SVGElementInsertor(
						InsertorType.FieldInsertor, String.format("-%s*",
								rotScale), strRotation);
				insertor11.addSubInsertor(insert13);
				// [self.m_tResult
				// appendformat:@" rotate(<MrxMath.Expr decimals=\"2\">-%f*<Mrx.Field name=\"%@\"/></MrxMath.Expr>)\"",
				// rotScale, strRotation];
			} else if (tRotationProp != null) {
				SVGElementInsertor insert12 = new SVGElementInsertor(
						InsertorType.PropInsertor, String.format("-%s*",
								rotScale), strRotation);
				insertor11.addSubInsertor(insert12);
				// [self.m_tResult
				// appendformat:@" rotate(<MrxMath.Expr decimals=\"2\">-%f*<Mrx.Prop name=\"%@\"/></MrxMath.Expr>)\"",
				// rotScale, strRotation];
			} else {
				SVGElementInsertor insert12 = new SVGElementInsertor(
						InsertorType.TextInsertor, "0", "");
				insertor11.addSubInsertor(insert12);
				// [self.m_tResult appendString:@" rotate(0)\""];
			}
			m_tInsertorArray.add(insertor11);
			// SVGElementInsertor insertor12 = new SVGElementInsertor(6 ,")\""
			// ,"");
			// m_tInsertorArray.add(insertor12);
			// SVGElementInsertor insertor14 = new
			// SVGElementInsertor(InsertorType.TextInsertor,")\"" ,"");
			// m_tInsertorArray.add(insertor14);
			// add feature attributes to path element
			List<Element> tFeatures = m_tLayer.elements();
			// int i = 0;
			// while (i < tFeatures.count)
			for (int k = 0; k < tFeatures.size(); k++) {
				// DDElement *tFeature = [tFeatures objectAtIndex:i];
				Element node = tFeatures.get(k);
				if (node.getNodeType() == Element.ELEMENT_NODE) {
					Element tFeature = node;
					if (tFeature.getName().equalsIgnoreCase(
							"featureAttribute")) {
						if (tFeature.attribute("tag") == null
								|| StringUtil.isBlank(tFeature
										.attributeValue("tag"))) {
							continue;
						}

						String tagName = tFeature.attributeValue("tag");
						if (tagName.equalsIgnoreCase("id")) {
							m_tIdFeature = tFeature;
						} else if (tagName.equalsIgnoreCase("tip")) {
							m_tTipFeature = tFeature;
						} else if (bAutoLabel) {
							m_tAutoLabelFeature = tFeature;
						} else {
							callAgent(tFeature);
						}
					}
				}
			}
			outputFeatureId();
			/*
			 * this.OutputFeatureId(); this.OutputTips();
			 * 
			 * // handle different coordinate systems this.OutputAutoLabel();
			 */

			SVGElementInsertor insertor15 = new SVGElementInsertor(
					InsertorType.TextInsertor, "></use>", "");
			m_tInsertorArray.add(insertor15);

			// [self.m_tResult appendString:@">"]; // close use tag

			// [self.m_tResult appendString:@"</use>"]; // close use tag
			// [self.m_tResult appendString:@"</format><iterator>"];

			if (tDataSource != null) {
				this.callAgent(tDataSource);
			}
			// [self.m_tResult appendString:@"</iterator></Mrx.Emit>"];
			// ClusterFilter();
			this.endBuild();
		} catch (Exception ex) {
			m_tResult = new StringBuilder();
			// Log.i("PointLayerRequestCreator","doBuild():"+ex.getMessage());
		}
	}

	public void ClusterFilter() {
		int symbolWidth = 18;
		double oriX = m_tBuildParam.m_dOriginX;
		double oriY = m_tBuildParam.m_dOriginY;
		double scaleX = m_tBuildParam.m_dWorldPerPixelX * symbolWidth;
		double scaleY = m_tBuildParam.m_dWorldPerPixelY * symbolWidth;
		int mapWith = (int) (m_tBuildParam.m_iScreenWidth / symbolWidth);
		int mapHeight = (int) (m_tBuildParam.m_iScreenHeight / symbolWidth);
		// double mapWith = self.m
		HashSet<Long> set = new HashSet<Long>();
		int mapx = 0;
		int mapy = 0;
		// int mapdouble
		if (result != null && result.getDatas().size() > 0) {
			GeometryWithData data = null;
			Point pt = null;
			for (int i = 0; i < result.getDatas().size(); i++) {
				data = result.getDatas().get(i);
				if (data.getWKBGeometry().getGeometryType() == WKBGeometryType.WKBPOINT) {
					pt = (Point) data.getWKBGeometry();
					mapx = (int) ((pt.X - oriX) / scaleX);
					mapy = (int) ((pt.Y - oriY) / scaleY);
					if (mapx < 0 || mapx > mapWith || mapy < 0
							|| mapy > mapHeight) {
						result.getDatas().remove(i);
						continue;
					}
					long location = mapx + mapy * mapWith;
					if (!set.contains(location)) {
						set.add(location);
					} else {
						result.getDatas().remove(i);
					}
				}
			}
		}
	}
}
