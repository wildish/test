package MRF.common.Render.SVG;

import java.util.HashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;

import MRF.common.Util.MRFUtil;
import MRF.common.Util.StringUtil;
import MRF.common.geometry.Point;

public class BuildSVGMapRequest implements IMapRequestCreator {

	private HashMap<String, String> m_tSymbolDefs = new HashMap<String, String>();
	private Element tDocElement = null;
	private StringBuilder m_tFrontDefs = new StringBuilder();
	private StringBuilder m_tResult = new StringBuilder();
	private BuildMapParam m_tBuildMapPram = new BuildMapParam();

	@Override
	public void initialize(String szMapId, String szMapViewName,
			double dOriginX, double dOriginY, double dScaleX, double dScaleY,
			int iScreenWidth, int iScreenHeght, String strSpGeom,
			String strSpOperator, String strLayerIDs, String strLayerParams,
			String angle, Point begin, Point end) {
		try {
			m_tBuildMapPram = new BuildMapParam();
			m_tBuildMapPram.m_MapID = szMapId;
			m_tBuildMapPram.m_dScale = dScaleX;
			m_tBuildMapPram.m_dOriginX = dOriginX;
			m_tBuildMapPram.m_dOriginY = dOriginY;
			m_tBuildMapPram.m_dWorldPerPixelX = 1.0 / dScaleX;
			m_tBuildMapPram.m_dWorldPerPixelY = 1.0 / dScaleY;
			m_tBuildMapPram.m_tAllLayerParams = strLayerParams.split("\\|");
			m_tBuildMapPram.m_tRequestedLayers = strLayerIDs.split(",");
			m_tBuildMapPram.m_iScreenWidth = iScreenWidth;
			m_tBuildMapPram.m_iScreenHeight = iScreenHeght;

			m_tBuildMapPram.beginP = begin;
			m_tBuildMapPram.endP = end;
			m_tBuildMapPram.angle = angle;
			// DocumentBuilderFactory dbf =
			// DocumentBuilderFactory.newInstance();
			// DocumentBuilder db = dbf.newDocumentBuilder();
			// File xmlFile = new File(szMapViewName);
			// InputStream is = new FileInputStream(xmlFile);
			// Document doc = db.parse(is);
			Document doc = com.wildish.mrfmap.servlet.CommonContext.getDocumentByPath(szMapViewName);

			tDocElement = doc.getRootElement();

			if (tDocElement.attribute("crs") != null) {
				m_tBuildMapPram.m_strMapCrs = tDocElement.attributeValue("crs");
			}
			if (tDocElement.attribute("uomName") != null) {
				m_tBuildMapPram.m_strUomName = tDocElement
						.attributeValue("uomName");

				if (tDocElement.attribute("uomFactor") != null) {
					double factor = 1;
					try {
						factor = Double.parseDouble(tDocElement
								.attributeValue("uomFactor"));
					} catch (Exception e) {
					}
					m_tBuildMapPram.m_dUomFactor = factor;
				}
			}
			m_tBuildMapPram.initialize();
			m_tFrontDefs = new StringBuilder();
			m_tResult = new StringBuilder();
			m_tSymbolDefs = new HashMap<String, String>();
		} catch (Exception ex) {
			// Log.i("BuildSVGMapRequest","initialize():"+ex.getMessage());
		}
	}

	@Override
	public String getResult() {
		tDocElement = null;
		m_tFrontDefs.append("\r\n<defs>");

		for (String key : m_tSymbolDefs.keySet()) {
			String value = m_tSymbolDefs.get(key).toString();
			m_tFrontDefs.append(value.replace("\\", ""));
		}

		m_tFrontDefs.append("</defs>");
		m_tFrontDefs.append(m_tResult);
		return m_tFrontDefs.toString();
	}

	public void buildSVG() {
		buildSVGHeadContent();
		buildSVGLayers();
		m_tResult.append("</g></g></svg>");
	}

	public void buildSVGHeadContent() {
		m_tFrontDefs
				.append("<?xml version=\"1.0\"  encoding=\"UTF-8\" standalone=\"no\"?>\n");
		m_tFrontDefs
				.append("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\" >\n"); // <svg>
		String strOnLoadFunction = "";
		if (StringUtil.isBlank(m_tBuildMapPram.m_MapID) == false) {
			strOnLoadFunction = String.format("window.config['%s_InitMap']",
					m_tBuildMapPram.m_MapID);
		} else {
			strOnLoadFunction = "initPageSVG";
			if (tDocElement.attribute("eventHandler") != null) {
				String value = tDocElement.attributeValue("eventHandler");
				if (!StringUtil.isBlank(value)) {
					strOnLoadFunction = value.trim();
				}
			}
		}
		m_tFrontDefs.append("<svg id=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_MapID);
		m_tFrontDefs.append("_\" zoomAndPan=\"disable\"  width=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_iScreenWidth);
		m_tFrontDefs.append("\" height=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_iScreenHeight);
		m_tFrontDefs.append("\"  onload=\"");
		m_tFrontDefs.append(strOnLoadFunction);
		m_tFrontDefs
				.append("(evt)\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" >\n\r\n<g t=\"l\" id=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_MapID);
		m_tFrontDefs.append("_maps\"><imageSpec>\n<uom name=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_strUomName);
		m_tFrontDefs.append("\" factor=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_dUomFactor);
		m_tFrontDefs.append("\"/>\n<size x=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_iScreenWidth);
		m_tFrontDefs.append("\" y=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_iScreenHeight);
		m_tFrontDefs.append("\"/>\n<origin x=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_dOriginX);
		m_tFrontDefs.append("\" y=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_dOriginY);
		m_tFrontDefs.append("\"/>\n<scale x=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_dWorldPerPixelX);
		m_tFrontDefs.append("\" y=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_dWorldPerPixelY);
		m_tFrontDefs.append("\"/>\n</imageSpec>\n\r\n<g t=\"l\" id=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_MapID);
		m_tFrontDefs.append("_layers\">\r\n<clipPath id=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_MapID);
		m_tFrontDefs.append("_clip\"><rect width=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_iScreenWidth);
		m_tFrontDefs.append("\" height=\"");
		m_tFrontDefs.append(m_tBuildMapPram.m_iScreenHeight);
		m_tFrontDefs.append("\"/></clipPath>\n<g style=\"clip-path:url(#");
		m_tFrontDefs.append(m_tBuildMapPram.m_MapID);
		m_tFrontDefs.append("_clip)\">");
	}

	public void buildSVGLayers() {
		try {
			List<Element> tLayers = MRFUtil.getNodeListByPath(
					tDocElement.getDocument(), "//layers");
			if (tLayers == null || tLayers.size() == 0
					|| tLayers.get(0).elements().size() == 0)
				return;

			int layerIndex = tLayers.get(0).elements().size() - 1;

			Element tChild = (Element) tLayers.get(0).elements()
					.get(layerIndex);

			if (m_tBuildMapPram.m_tRequestedLayers == null) {
				while (tChild != null) {
					layerIndex = layerIndex - 1;
					if (layerIndex < 0)
						break;

					if (tChild.getNodeType() != Element.ELEMENT_NODE) {
						tChild = (Element) tLayers.get(0).elements()
								.get(layerIndex);
						continue;
					}
					m_tResult.append(buildSVGLayer(m_tBuildMapPram.m_MapID,
							tChild, null));
					m_tResult.append("\r\n");
					tChild = (Element) tLayers.get(0).elements()
							.get(layerIndex);
				}
			} else {
				HashMap<String, Object> layerNodes = new HashMap<String, Object>();
				while (tChild != null) {
					layerIndex = layerIndex - 1;

					if (tChild.getNodeType() != Element.ELEMENT_NODE) {
						if (layerIndex < 0) {
							break;
						}
						tChild = (Element) tLayers.get(0).elements()
								.get(layerIndex);
						continue;
					}
					Element element = tChild;
					if (element.attribute("id") == null) {
						if (layerIndex < 0) {
							break;
						}
						tChild = (Element) tLayers.get(0).elements()
								.get(layerIndex);
						continue;
					}
					String tId = element.attributeValue("id");
					if (StringUtil.isBlank(tId)) {
						if (layerIndex < 0) {
							break;
						}
						tChild = (Element) tLayers.get(0).elements()
								.get(layerIndex);
						continue;
					}

					// Accessibility
					if (!MRFUtil.getAttrBoolWithDefault(element,
							"Accessibility", true)) {
						if (layerIndex < 0) {
							break;
						}
						tChild = (Element) tLayers.get(0).elements()
								.get(layerIndex);
						continue;
					}

					layerNodes.put(tId, tChild);// setObject:tChild
												// forKey:(DDElement*)tId.stringValue];
					if (layerIndex < 0) {
						break;
					}
					tChild = (Element) tLayers.get(0).elements()
							.get(layerIndex);
				}

				String szLayerId = "";
				for (layerIndex = 0; layerIndex < m_tBuildMapPram.m_tRequestedLayers.length; layerIndex++) {
					szLayerId = m_tBuildMapPram.m_tRequestedLayers[layerIndex];
					if (layerNodes.containsKey(szLayerId)
							&& layerNodes.get(szLayerId) != null) {
						String szLayerParam = "";
						if (m_tBuildMapPram.m_tRequestedLayerParams.length > layerIndex) {
							szLayerParam = m_tBuildMapPram.m_tRequestedLayerParams[layerIndex];
						}
						Element ele = (Element) layerNodes.get(szLayerId);
						m_tResult.append(buildSVGLayer(m_tBuildMapPram.m_MapID,
								ele, szLayerParam));

						m_tResult.append("\r\n");
					}
				}
			}
		} catch (Exception ex) {
			// Log.e("BuildSVGMapRequest", "buildSVGLayers():" +
			// ex.getMessage());
		}
		m_tResult.append("\r\n</g>\r\n");
	}

	// private readonly ILogger _logger = new ConsoleLogFactory(new
	// LogFilter()).CreateLogger(typeof(AnnotationLayerRequestCreator));

	public String buildSVGLayer(String mapID, Element tLayer,
			String szLayerParam) {
		// _logger.Info("BuildSVGLayer begin");
		String localName = tLayer.getName();
		// tLayer.getQName() == null ? tLayer.getName()
		// : tLayer.getQName().toString();

		if (localName.equalsIgnoreCase("layer")) {
			LayerRequestCreator layer = new LayerRequestCreator(
					m_tBuildMapPram, mapID, tLayer, szLayerParam);
			layer.doBuild();
			// _logger.Info("BuildSVGLayer end");
			return layer.m_tResult.toString();
		} else if (localName.equalsIgnoreCase("imageLayer")) {
			ImageLayerRequestCreator layer = new ImageLayerRequestCreator(
					m_tBuildMapPram, mapID, tLayer, szLayerParam);
			layer.doBuild();
			return layer.m_tResult.toString();
		} else if (localName.equalsIgnoreCase("annotationLayer")) {
			AnnotationLayerRequestCreator layer = new AnnotationLayerRequestCreator(
					m_tBuildMapPram, mapID, tLayer, szLayerParam);
			layer.doBuild();
			return layer.m_tResult.toString();
		} else if (localName.equalsIgnoreCase("pointLayer")) {
			PointLayerRequestCreator layer = new PointLayerRequestCreator(
					m_tBuildMapPram, mapID, tLayer, szLayerParam);
			layer.doBuild();

			for (String key : layer.m_tSymbols.keySet()) {
				if (!m_tSymbolDefs.containsKey(key)) {
					m_tSymbolDefs.put(key, layer.m_tSymbols.get(key));
				}
			}
			// _logger.Info("BuildSVGLayer end");
			return layer.m_tResult.toString();
		} else if (localName.equalsIgnoreCase("lineStringLayer")) {
			LineStringLayerRequestCreator layer = new LineStringLayerRequestCreator(
					m_tBuildMapPram, mapID, tLayer, szLayerParam);
			layer.doBuild();
			// _logger.Info("BuildSVGLayer end");
			return layer.m_tResult.toString();
		} else if (localName.equalsIgnoreCase("polygonLayer")) {
			PolygonLayerRequestCreator layer = new PolygonLayerRequestCreator(
					m_tBuildMapPram, mapID, tLayer, szLayerParam);
			layer.doBuild();
			// _logger.Info("BuildSVGLayer end");
			return layer.m_tResult.toString();
		} else {
			return "";
		}

	}

	public void setRotateAngle(String angle) {
		this.m_tBuildMapPram.angle = angle;
	}
}
