package MRF.common.Render.SVG;

import java.util.HashMap;
import java.util.Map;

import MRF.common.Util.StringUtil;
import MRF.common.geometry.Point;

public class BuildMapParam {
	public String m_MapID;

	public String m_strMapCrs;
	public String m_strUomName;
	public double m_dUomFactor;

	public double m_dScale;
	public double m_dOriginX;
	public double m_dOriginY;
	public double m_dWorldPerPixelX;
	public double m_dWorldPerPixelY;

	public int m_iScreenWidth;
	public int m_iScreenHeight;

	public String[] m_tAllLayerParams;
	public String[] m_tRequestedLayers;
	public String m_strCurrentEnvelope;
	public String[] m_tRequestedLayerParams;

	public Map<String, String> m_tEntityHolderDic = new HashMap<String, String>();
	public double left;
	public double right;
	public double top;
	public double bottom;
	public String angle;
	public Point beginP;
	public Point endP;
	public double m_translateX;
	public double m_translateY;

	public void initialize() {
		if (m_strUomName.toLowerCase().equals("metre")
				|| m_strUomName.toLowerCase().equals("meter")) {
			m_dScale = Math.abs(m_dWorldPerPixelY) * m_dUomFactor
					/ BuildMapConst.METER_PER_PIXEL;
		} else if (m_strUomName.toLowerCase().equals("foot")) {

			m_dScale = Math.abs(m_dWorldPerPixelY) * m_dUomFactor
					/ BuildMapConst.FEET_PER_PIXEL;
		}

		if (m_tAllLayerParams != null && m_tAllLayerParams.length > 0) {
			m_tRequestedLayerParams = m_tAllLayerParams[0].split(",");
		}

		// double dWidth = m_iScreenWidth * m_dWorldPerPixelX;
		// double dHeight = m_iScreenHeight * m_dWorldPerPixelY;

		left = Math.min(endP.X, beginP.X);
		right = Math.max(beginP.X, endP.X);
		bottom = Math.min(beginP.Y, endP.Y);
		top = Math.max(endP.Y, beginP.Y);

		StringBuffer buffer = new StringBuffer();
		buffer.append(left);
		buffer.append(",");
		buffer.append(bottom);
		buffer.append(",");
		buffer.append(right);
		buffer.append(",");
		buffer.append(top);
		m_strCurrentEnvelope = buffer.toString();

		m_translateX = (getRotateScreenWidth() - m_iScreenWidth) / 2;
		m_translateY = (getRotateScreenHeight() - m_iScreenHeight) / 2;
		m_iScreenWidth = (int) getRotateScreenWidth();
		m_iScreenHeight = (int) getRotateScreenHeight();
	}

	public String getParameter(String strParmList, String strParmName) {
		try {
			if (StringUtil.isBlank(strParmList) == false) {
				String[] strParameter = strParmList.split(";");
				for (int i = 0; i < strParameter.length; i++) {
					if (strParameter[i].toLowerCase().indexOf(
							strParmName.toLowerCase() + ":") >= 0)
						return (strParameter[i].substring(
								strParmName.length() + 1,
								strParameter[i].length() - strParmName.length()
										- 1));
				}
			}

			if (m_tAllLayerParams != null) {
				for (int i = 1; i < m_tAllLayerParams.length; i++) {
					if (m_tAllLayerParams[i].toLowerCase().indexOf(
							strParmName.toLowerCase() + ":") >= 0)
						return (m_tAllLayerParams[i].substring(strParmName
								.length() + 1)).trim();
				}
			}
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
		}
		return "";
	}

	public Map<String, String> EntityHolderDict;

	public void cacheEntity(String szHolder, String szValue) {
		m_tEntityHolderDic.put(szHolder, szValue);
	}

	public String replaceResponseHolder(String strResponse) {
		for (String kv : this.m_tEntityHolderDic.keySet()) {
			strResponse = strResponse.replace(kv,
					this.m_tEntityHolderDic.get(kv));
		}
		//
		return strResponse;
	}

	public double getRotateScreenWidth() {
		return Math.abs(right - left) / Math.abs(m_dWorldPerPixelX);
	}

	public double getRotateScreenHeight() {
		return Math.abs(top - bottom) / Math.abs(m_dWorldPerPixelY);
	}
}
