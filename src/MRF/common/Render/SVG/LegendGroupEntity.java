package MRF.common.Render.SVG;

import java.io.Serializable;
import java.util.List;

public class LegendGroupEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public String Name;
	public List<LegendLayerEntity> Layers;
}
