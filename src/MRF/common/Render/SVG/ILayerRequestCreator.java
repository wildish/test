package MRF.common.Render.SVG;

import org.dom4j.Element;


public interface ILayerRequestCreator {
	String getResult();

	void initialize(String szMapId, Element tNode);

	void initialize(BuildMapParam buildParam, String szMapId, Element tNode,
			String szLayerParam);
}
