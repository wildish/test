/*
 *  XmlUtil.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.Util;

import java.util.List;

import org.dom4j.Element;

/**
 * 
 * @author Admin
 */
public class XmlUtil {
	private static String[][] _specialCharMap = new String[][] {
			new String[] { "&", "&amp;" }, new String[] { "\"", "&quot;" },
			new String[] { "<", "&lt;" }, new String[] { ">", "&gt;" } };

//	public static String unEscapeXmlChars(String input) {
//		if (input == null)
//			return null;
//
//		for (int i = 0; i < _specialCharMap.length; i++)
//			input = input.replaceAll(_specialCharMap[i][1],
//					_specialCharMap[i][0]);
//
//		return input;
//	}

	public static String escapeXmlChars(String input) {
		if (input == null)
			return null;

		for (int i = 0; i < _specialCharMap.length; i++)
			input = input.replaceAll(_specialCharMap[i][0],
					_specialCharMap[i][1]);

		return input;
	}

	// public static String EscapeXmlChars(String strText)
	// {
	// StringWriter tStringWriter = new StringWriter();
	// XmlTextWriter tXmlWriter = new XmlTextWriter(tStringWriter);
	// tXmlWriter.WriteString(strText);
	// tXmlWriter.Close();
	// String result = tStringWriter.ToString();
	// return result;
	// }

	public static String getAttrValue(Element tNode, String attrName) {
		String attr = tNode.attributeValue(attrName);
		if (attr == null)
			return "";
		return attr;
	}

	public static String getNodeValue(Element tNode, String nodeName) {
		Element tValueNode = tNode.element(nodeName);
		if (tValueNode == null) {
			return "";
		}
		String value = tValueNode.getTextTrim();
		if(StringUtil.isBlank(value)) {
			return "";
		}
		return value;
	}

//	public static NamedNodeMap createAttribute(Node node, String name,
//			String value) {
//		if(node.getNodeType() == Element.ELEMENT_NODE) {
//			Element e = (Element) node;
//			e.setAttribute(name, value);
//			return e.getAttributes();
//		}
//		
//		return null;
//	}
//
//	public static boolean IsSingleNode(Node tNode) {
//		if (tNode.ChildNodes.Count == 0)
//			return true;
//		if (tNode.ChildNodes.Count == 1) {
//			XmlNodeType nodeType = tNode.FirstChild.NodeType;
//			return nodeType != XmlNodeType.Element
//					&& nodeType != XmlNodeType.CDATA;
//		}
//		return false;
//	}
//
//	public static boolean DeleteElement(Document xmlDoc, String xPath) {
//		Node tNode = xmlDoc.SelectSingleNode(xPath);
//		if (tNode == null)
//			return false;
//		//
//		Node tParent = tNode.ParentNode;
//		if (tParent == null)
//			return false;
//		tParent.RemoveChild(tNode);
//		return true;
//	}
	
//	public static Element appendElement(Element node, String name) {
//		Element xmlDoc = node.addComment(name);
//		node.add(xmlDoc);
//		return xmlDoc;
//	}
	
//	public static Node AppendElement(Node node, String name, String id) {
//		Document xmlDoc = node.OwnerDocument;
//		Node tNode = xmlDoc.CreateElement(name);
//		node.AppendChild(tNode);
//		//
//		XmlAttribute tID = xmlDoc.CreateAttribute("id");
//		tID.Value = id;
//		tNode.Attributes.Append(tID);
//		return tNode;
//	}
//
//	@SuppressWarnings("unchecked")
//	public static Element createElement(Element node, String name) {
//		if(node.getNodeType() == Element.ELEMENT_NODE) {
//			Element e = node;
//			List<Element> nl = e.elements(name);
//			if(nl != null && nl.size() > 0) {
//				return nl.get(0);
//			} else {
//				Element tNode = e.addElement(name);
//				return tNode;
//			}
//		}
//		return null;
//	}

//	public static Element createElementByInnertext(Element node, String name, String innerText)
//    {
//        NodeList tNodeList = node.getAttributes().getNamedItem(name).getChildNodes();
//        for(int i = 0;i<tNodeList.getLength();i++)
//        {
//        	Node Node = tNodeList.item(i);
//            if (Node.getNodeValue() == innerText)
//            {
//                return Node;
//            }
//        }
//
//        Document xmlDoc = node.getOwnerDocument();
//        Node tNode = xmlDoc.createComment(name);
//        node.appendChild(tNode);
//        return tNode;
//    }
	
//	public static Node CreateElement(Node node, String name, String id) {
//		String xPath = String.format("%s[@id=\"%s\"]", name, id);
//		Document xmlDoc = node.OwnerDocument;
//		Node tNode = node.SelectSingleNode(xPath);
//		if (tNode != null)
//			return tNode;
//
//		tNode = xmlDoc.CreateElement(name);
//		node.AppendChild(tNode);
//		//
//		XmlAttribute tID = xmlDoc.CreateAttribute("id");
//		tID.Value = id;
//		tNode.Attributes.Append(tID);
//		return tNode;
//	}
//
//	public static Node CreateElementByTag(Node node, String name, String tag) {
//		String xPath = String.format("%s[@tag=\"%s\"]", name, tag);
//		Document xmlDoc = node.OwnerDocument;
//		Node tNode = node.SelectSingleNode(xPath);
//		if (tNode != null)
//			return tNode;
//
//		tNode = xmlDoc.CreateElement(name);
//		node.AppendChild(tNode);
//		//
//		XmlAttribute tID = xmlDoc.CreateAttribute("tag");
//		tID.Value = tag;
//		tNode.Attributes.Append(tID);
//		return tNode;
//	}
//
//	public static Node InsertFirstElement(Node node, String name, String id) {
//		String xPath = String.format("%s[@id=\"%s\"]", name, id);
//		Document xmlDoc = node.OwnerDocument;
//		Node tNode = node.SelectSingleNode(xPath);
//		if (tNode != null)
//			return tNode;
//
//		tNode = xmlDoc.CreateElement(name);
//		node.InsertBefore(tNode, node.FirstChild);
//		//
//		XmlAttribute tID = xmlDoc.CreateAttribute("id");
//		tID.Value = id;
//		tNode.Attributes.Append(tID);
//		return tNode;
//	}
//
	@SuppressWarnings("unchecked")
	public static String readAttrXY(Element tParent, String strElement) {
		if(tParent.getNodeType() == Element.ELEMENT_NODE) {
			List<Element> nl = tParent.elements(strElement);
			if(nl == null || nl.size() == 0) {
				return "0,0";
			}
			Element tElement =  nl.get(0);
			String x = tElement.attributeValue("x");
			String y = tElement.attributeValue("y");
			if(StringUtil.isBlank(x) || StringUtil.isBlank(y)) {
				return "0,0";
			}
			return String.format("%s,%s", x, y);
		}
		return "0,0";
	}

	public static boolean writeAttrXY(Element tParent, String elementName,
			String strXY) {
		String[] xy = strXY.split(",");
		if (xy.length != 2)
			return false;
		//
		Element tNode = tParent.addElement(elementName);
		if (tNode == null) {
			return false;
		}
		tNode.addAttribute("x", xy[0]);
		tNode.addAttribute("y", xy[1]);
		return true;
	}
}
