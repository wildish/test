//
//  JsonSQLiteDatabase.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package MRF.common.Util;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import org.sqlite.database.sqlite.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public final class JsonSQLiteDatabase {

	private SQLiteDatabase db;

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		if (this.db != null) {
			this.db.close();
			this.db = null;
		}
	}

	public JsonSQLiteDatabase(SQLiteDatabase db) {
		this.db = db;
	}

	public int delete(String table, String whereClause) {
		return this.db.delete(table, whereClause, null);
	}

	public int delete(String table, String whereClause, String[] whereArgs) {
		return this.db.delete(table, whereClause, whereArgs);
	}

	public void execSQL(String sql) {
		this.db.execSQL(sql, new Object[] {});
	}

	public void execSQL(String sql, Object[] params) {
		this.db.execSQL(sql, params);
	}

	public long insert(String table, ContentValues values) {
		return this.db.insert(table, null, replaceCharsForUpdate(values));
	}

	public int update(String table, ContentValues values, String whereClause) {
		return this.db.update(table, replaceCharsForUpdate(values),
				whereClause, null);
	}

	public int update(String table, ContentValues values, String whereClause,
			String[] whereArgs) {
		return this.db.update(table, values, whereClause, whereArgs);
	}

	private ContentValues replaceCharsForUpdate(ContentValues values) {
		if (values == null || values.size() == 0) {
			return values;
		}
		Set<String> set = values.keySet();
		Iterator<String> it = set.iterator();
		while (it.hasNext()) {
			String key = it.next();
			Object obj = values.get(key);
			if (obj instanceof String) {
				String value = values.getAsString(key);
				value = MRFUtil.replaceCharsForUpdate(value);
				values.put(key, value);
			}
		}

		return values;
	}

	public boolean update(List<String> sqls) {
		boolean ret = true;
		this.db.beginTransaction();
		try {
			if (sqls != null && sqls.size() > 0) {
				for (int i = 0; i < sqls.size(); i++) {
					this.execSQL(sqls.get(i));
				}
				this.db.setTransactionSuccessful();
			}
		} catch (Exception e) {
			ret = false;
			e.printStackTrace();
		} finally {
			this.db.endTransaction();
		}

		return ret;
	}

	public JSONObject count(String tableName) {
		String sql = "SELECT count(*) as count FROM " + tableName;
		return first(sql, null);
	}

	public JSONArray query(String sql) {
		return query(sql, null);
	}

	public JSONArray query(String sql, String[] selectionArgs) {
		Cursor cursor = this.db.rawQuery(sql, selectionArgs);
		JSONArray results = new JSONArray();
		cursorToArray(cursor, results);
		cursor.close();
		return results;
	}

	private <T> void cursorToArray(Cursor cursor, JSONArray array) {
		JSONObject item = null;
		while (cursor.moveToNext()) {
			item = new JSONObject();
			for (int i = 0; i < cursor.getColumnCount(); i++) {
				putData(item, cursor, i);
			}
			//
			array.put(item);
		}
	}

	private void putData(JSONObject obj, Cursor cursor, int idx) {
		String field = cursor.getColumnName(idx);
		int type = cursor.getType(idx);
		try {
			switch (type) {
			case Cursor.FIELD_TYPE_STRING:
				// obj.put(field, cursor.getString(idx));
				obj.put(field,
						MRFUtil.replaceCharsForQuery(cursor.getString(idx)));
				break;
			case Cursor.FIELD_TYPE_INTEGER:
				obj.put(field, cursor.getLong(idx));
				break;
			case Cursor.FIELD_TYPE_FLOAT:
				if (field.endsWith("_time") || field.endsWith("_date")) {
					obj.put(field, cursor.getLong(idx));
				} else {
					obj.put(field, cursor.getDouble(idx));
				}
				break;
			case Cursor.FIELD_TYPE_BLOB:
				obj.put(field, new String(cursor.getBlob(idx), "utf-8"));
				break;
			default:
				obj.put(field, null);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public JSONObject first(String sql) {
		return first(sql, null);
	}

	public JSONObject first(String sql, String[] selectionArgs) {
		JSONArray array = query(sql, selectionArgs);
		if (array == null || array.length() == 0) {
			return null;
		} else {
			try {
				return array.getJSONObject(0);
			} catch (JSONException e) {
				return null;
			}
		}
	}

	/*
	 * private static final String[] RAW_TYPES = { "byte", "char", "short",
	 * "int", "long", "float", "double", "java.lang.Byte",
	 * "java.lang.Character", "java.lang.Short", "java.lang.Integer",
	 * "java.lang.Long", "java.lang.Float", "java.lang.Double",
	 * "java.lang.String", "java.util.Date" };
	 * 
	 * public static boolean isRawType(Class<?> dataType) { String valueType =
	 * dataType.getName(); if (valueType == null || valueType.equals("")) {
	 * return false; } // for (String s : RAW_TYPES) { if (s.equals(valueType))
	 * { return true; } } // return false; }
	 * 
	 * @SuppressLint("SimpleDateFormat") public static DateFormat DateFormat =
	 * new SimpleDateFormat( "yyyy-MM-dd hh:mm:ss");
	 * 
	 * public static Object getValue(Cursor cursor, int idx, Class<?>
	 * valueClass) { String valueType = valueClass.getName(); if
	 * (valueType.equals("byte") || valueType.equals("java.lang.Byte")) { return
	 * cursor.getString(idx).charAt(0); } else if (valueType.equals("char") ||
	 * valueType.equals("java.lang.Character")) { return
	 * cursor.getString(idx).charAt(0); } else if (valueType.equals("short") ||
	 * valueType.equals("java.lang.Short")) { return cursor.getShort(idx); }
	 * else if (valueType.equals("int") ||
	 * valueType.equals("java.lang.Integer")) { return cursor.getInt(idx); }
	 * else if (valueType.equals("long") || valueType.equals("java.lang.Long"))
	 * { return cursor.getLong(idx); } else if (valueType.equals("float") ||
	 * valueType.equals("java.lang.Float")) { return cursor.getFloat(idx); }
	 * else if (valueType.equals("double") ||
	 * valueType.equals("java.lang.Double")) { return cursor.getDouble(idx); }
	 * else if (valueType.equals("java.lang.String")) { return
	 * cursor.getString(idx); } else if (valueType.equals("java.util.Date")) {
	 * try { return DateFormat.parse(cursor.getString(idx)); } catch (Exception
	 * e) { return null; } } else { return cursor.getString(idx); } }
	 */

}