package MRF.common.Util.Algorithm;

import java.util.List;

import MRF.common.Util.MRFUtil;
import MRF.common.geometry.Envelope;
import MRF.common.geometry.Line;
import MRF.common.geometry.Location;
import MRF.common.geometry.Point;

/// <summary>
/// Specifies and implements various fundamental Computational Geometric algorithms.
/// The algorithms supplied in this class are robust for double-precision floating point.
/// </summary>
public class CGAlgorithms {

	// / <summary>
	// / A value that indicates an orientation of clockwise, or a right turn.
	// / </summary>
	public static final int Clockwise = -1;
	// / <summary>
	// / A value that indicates an orientation of clockwise, or a right turn.
	// / </summary>
	public static final int Right = Clockwise;

	// / <summary>
	// / A value that indicates an orientation of counterclockwise, or a left
	// turn.
	// / </summary>
	public static final int CounterClockwise = 1;
	// / <summary>
	// / A value that indicates an orientation of counterclockwise, or a left
	// turn.
	// / </summary>
	public static final int Left = CounterClockwise;

	// / <summary>
	// / A value that indicates an orientation of collinear, or no turn
	// (straight).
	// / </summary>
	public static final int Collinear = 0;
	// / <summary>
	// / A value that indicates an orientation of collinear, or no turn
	// (straight).
	// / </summary>
	public static final int Straight = Collinear;

	// / <summary>
	// / Returns the index of the direction of the point <c>q</c> relative to
	// / a vector specified by <c>p1-p2</c>.
	// / </summary>
	// / <param name="p1">The origin point of the vector</param>
	// / <param name="p2">The final point of the vector</param>
	// / <param name="q">the point to compute the direction to</param>
	// / <returns>
	// / <list type="Bullet">
	// / <item>1 if q is counter-clockwise (left) from p1-p2</item>
	// / <item>-1 if q is clockwise (right) from p1-p2</item>
	// / <item>0 if q is collinear with p1-p2</item></list>
	// / </returns>
	public static int orientationIndex(Point p1, Point p2, Point q) {
		// fast filter for orientation index
		// avoids use of slow extended-precision arithmetic in many cases
		int index = orientationIndexFilter(p1, p2, q);
		if (index <= 1)
			return index;

		// normalize coordinates
		double dx1 = p2.X - p1.X;
		double dy1 = p2.Y - p1.Y;
		double dx2 = q.X - p2.X;
		double dy2 = q.Y - p2.Y;

		return signOfDet2x2(dx1, dy1, dx2, dy2);
	}

	public static int orientationIndex(double[] p1, double[] p2, double[] q) {
		// fast filter for orientation index
		// avoids use of slow extended-precision arithmetic in many cases
		int index = orientationIndexFilter(p1, p2, q);
		if (index <= 1)
			return index;

		// normalize coordinates
		double dx1 = p2[0] - p1[0];
		double dy1 = p2[1] - p1[1];
		double dx2 = q[0] - p2[0];
		double dy2 = q[1] - p2[1];

		return signOfDet2x2(dx1, dy1, dx2, dy2);
	}

	// / <summary>
	// / Computes the sign of the determinant of the 2x2 matrix
	// / with the given entries.
	// / </summary>
	// / <param name="x1"></param>
	// / <param name="y1"></param>
	// / <param name="x2"></param>
	// / <param name="y2"></param>
	// / <returns>
	// / <list type="Bullet">
	// / <item>-1 if the determinant is negative,</item>
	// / <item>1 if the determinant is positive,</item>
	// / <item>0 if the determinant is 0.</item>
	// / </list>
	// / </returns>
	public static int signOfDet2x2(double x1, double y1, double x2, double y2) {
		double det = x1 * y2 - y1 * x2;
		if (Math.abs(det) < 1e-7)
			return 0;
		if (det < 0)
			return -1;
		return 1;

	}

	// / <summary>
	// / A value which is safely greater than the
	// / relative round-off error in double-precision numbers
	// / </summary>
	private static final double DoublePrecisionSafeEpsilon = 1e-15;

	// / <summary>
	// / A filter for computing the orientation index of three coordinates.
	// / <para/>
	// / If the orientation can be computed safely using standard DP
	// / arithmetic, this routine returns the orientation index.
	// / Otherwise, a value i > 1 is returned.
	// / In this case the orientation index must
	// / be computed using some other more robust method.
	// / The filter is fast to compute, so can be used to
	// / avoid the use of slower robust methods except when they are really
	// needed,
	// / thus providing better average performance.
	// / <para/>
	// / Uses an approach due to Jonathan Shewchuk, which is in the public
	// domain.
	// / </summary>
	// / <returns>
	// / <list type="Bullet">
	// / <item>The orientation index if it can be computed safely</item>
	// / <item>&gt; 1 if the orientation index cannot be computed safely</item>>
	// / </list>
	// / </returns>
	private static int orientationIndexFilter(Point pa, Point pb, Point pc) {
		double detsum;

		double detleft = (pa.X - pc.X) * (pb.Y - pc.Y);
		double detright = (pa.Y - pc.Y) * (pb.X - pc.X);
		double det = detleft - detright;

		if (detleft > 0.0) {
			if (detright <= 0.0) {
				return Signum(det);
			}
			detsum = detleft + detright;
		} else if (detleft < 0.0) {
			if (detright >= 0.0) {
				return Signum(det);
			}
			detsum = -detleft - detright;
		} else {
			return Signum(det);
		}

		double errbound = DoublePrecisionSafeEpsilon * detsum;
		if ((det >= errbound) || (-det >= errbound)) {
			return Signum(det);
		}

		return 2;
	}

	private static int orientationIndexFilter(double[] pa, double[] pb,
			double[] pc) {
		double detsum;

		double detleft = (pa[0] - pc[0]) * (pb[1] - pc[1]);
		double detright = (pa[1] - pc[1]) * (pb[0] - pc[0]);
		double det = detleft - detright;

		if (detleft > 0.0) {
			if (detright <= 0.0) {
				return Signum(det);
			}
			detsum = detleft + detright;
		} else if (detleft < 0.0) {
			if (detright >= 0.0) {
				return Signum(det);
			}
			detsum = -detleft - detright;
		} else {
			return Signum(det);
		}

		double errbound = DoublePrecisionSafeEpsilon * detsum;
		if ((det >= errbound) || (-det >= errbound)) {
			return Signum(det);
		}

		return 2;
	}

	private static int Signum(double x) {
		if (x > 0)
			return 1;
		if (x < 0)
			return -1;
		return 0;
	}

	// / <summary>
	// / Tests whether a point lies inside or on a ring.
	// / </summary>
	// / <remarks>
	// / <para>The ring may be oriented in either direction.</para>
	// / <para>A point lying exactly on the ring boundary is considered to be
	// inside the ring.</para>
	// / <para>This method does <i>not</i> first check the point against the
	// envelope
	// / of the ring.</para>
	// / </remarks>
	// / <param name="p">Point to check for ring inclusion.</param>
	// / <param name="ring">An array of <see cref="Point"/>s representing the
	// ring (which must have first point identical to last point)</param>
	// / <returns>true if p is inside ring.</returns>
	// / <see cref="IPointInRing"/>
	public static boolean isPointInRing(Point p, Point[] ring) {
		return locatePointInRing(p, ring) != Location.Exterior;
	}

	public static boolean isPointInRing(Point p, List<double[]> ring) {
		return locatePointInRing(p, ring) != Location.Exterior;
	}

	public static Location locatePointInRing(Point p, List<double[]> ring) {
		int crossingCount = 0, tempVal = 0;

		for (int i = 1; i < ring.size(); i++) {
			tempVal = countSegment(ring.get(i), ring.get(i - 1), p);
			if (tempVal == -1)
				return Location.Boundary;
			else if (tempVal > 0) {
				crossingCount += tempVal;
			}
		}
		if ((crossingCount % 2) == 1) {
			return Location.Interior;
		}
		return Location.Exterior;
	}

	/**
	 * 
	 * @param p1
	 *            first point in line segment
	 * @param p2
	 *            second point in line segment
	 * @param p
	 *            test point
	 * @param isPointOnSegment
	 * @param crossingCount
	 * @return -2 out side of line segment, -1 on line segment, >0 crossing
	 *         count
	 */
	public static int countSegment(double[] p1, double[] p2, Point p) {
		int crossingCount = 0;
		/*
		 * For each segment, check if it crosses a horizontal ray running from
		 * the test point in the positive x direction.
		 */

		// check if the segment is strictly to the left of the test point
		if (p1[0] < p.X && p2[0] < p.X)
			return -2;

		// check if the point is equal to the current ring vertex
		if (p.X == p2[0] && p.Y == p2[1]) {
			// isPointOnSegment = true;
			return -1;
		}
		/*
		 * For horizontal segments, check if the point is on the segment.
		 * Otherwise, horizontal segments are not counted.
		 */
		if (p1[1] == p.Y && p2[1] == p.Y) {
			double minx = p1[0];
			double maxx = p2[0];
			if (minx > maxx) {
				minx = p2[0];
				maxx = p1[0];
			}
			if (p.X >= minx && p.X <= maxx) {
				// isPointOnSegment = true;
				return -1;
			}
			return -2;
		}
		/*
		 * Evaluate all non-horizontal segments which cross a horizontal ray to
		 * the right of the test pt. To avoid double-counting shared vertices,
		 * we use the convention that <ul> <li>an upward edge includes its
		 * starting endpoint, and excludes its final endpoint <li>a downward
		 * edge excludes its starting endpoint, and includes its final endpoint
		 * </ul>
		 */
		if (((p1[1] > p.Y) && (p2[1] <= p.Y))
				|| ((p2[1] > p.Y) && (p1[1] <= p.Y))) {
			// translate the segment so that the test point lies on the origin
			double x1 = p1[0] - p.X;
			double y1 = p1[1] - p.Y;
			double x2 = p2[0] - p.X;
			double y2 = p2[1] - p.Y;

			/*
			 * The translated segment straddles the x-axis. Compute the sign of
			 * the ordinate of intersection with the x-axis. (y2 != y1, so
			 * denominator will never be 0.0)
			 */
			// double xIntSign = RobustDeterminant.signOfDet2x2(x1, y1, x2, y2)
			// / (y2
			// - y1);
			// MD - faster & more robust computation?
			double xIntSign = signOfDet2x2(x1, y1, x2, y2);
			if (xIntSign == 0.0) {
				return -1;// isPointOnSegment = true;
			}
			if (y2 < y1)
				xIntSign = -xIntSign;
			// xsave = xInt;

			// The segment crosses the ray if the sign is strictly positive.
			if (xIntSign > 0.0) {
				crossingCount++;
			}
		}
		return crossingCount;
	}

	// /<summary>
	// / Determines whether a point lies in the interior, on the boundary, or in
	// the exterior of a ring.
	// /</summary>
	// / <remarks>
	// / <para>The ring may be oriented in either direction.</para>
	// / <para>This method does <i>not</i> first check the point against the
	// envelope of the ring.</para>
	// / </remarks>
	// / <param name="p">Point to check for ring inclusion</param>
	// / <param name="ring">An array of coordinates representing the ring (which
	// must have first point identical to last point)</param>
	// / <returns>The <see cref="Location"/> of p relative to the ring</returns>
	public static Location locatePointInRing(Point p, Point[] ring) {
		int crossingCount = 0;
		boolean isPointInsegment = false;
		for (int i = 1; i < ring.length; i++) {
			isPointInsegment = false;
			Point p1 = ring[i];
			Point p2 = ring[i - 1];
			countSegment(p1, p2, p, isPointInsegment, crossingCount);
			if (isPointInsegment)
				return Location.Boundary;
		}
		if ((crossingCount % 2) == 1) {
			return Location.Interior;
		}
		return Location.Exterior;
	}

	// /<summary>
	// / Counts a segment
	// /</summary>
	// / <param name="p1">An endpoint of the segment</param>
	// / <param name="p2">Another endpoint of the segment</param>
	public static void countSegment(Point p1, Point p2, Point p,
			boolean isPointOnSegment, int crossingCount) {

		/*
		 * For each segment, check if it crosses a horizontal ray running from
		 * the test point in the positive x direction.
		 */

		// check if the segment is strictly to the left of the test point
		if (p1.X < p.X && p2.X < p.X)
			return;

		// check if the point is equal to the current ring vertex
		if (p.X == p2.X && p.Y == p2.Y) {
			isPointOnSegment = true;
			return;
		}
		/*
		 * For horizontal segments, check if the point is on the segment.
		 * Otherwise, horizontal segments are not counted.
		 */
		if (p1.Y == p.Y && p2.Y == p.Y) {
			double minx = p1.X;
			double maxx = p2.X;
			if (minx > maxx) {
				minx = p2.X;
				maxx = p1.X;
			}
			if (p.X >= minx && p.X <= maxx) {
				isPointOnSegment = true;
			}
			return;
		}
		/*
		 * Evaluate all non-horizontal segments which cross a horizontal ray to
		 * the right of the test pt. To avoid double-counting shared vertices,
		 * we use the convention that <ul> <li>an upward edge includes its
		 * starting endpoint, and excludes its final endpoint <li>a downward
		 * edge excludes its starting endpoint, and includes its final endpoint
		 * </ul>
		 */
		if (((p1.Y > p.Y) && (p2.Y <= p.Y)) || ((p2.Y > p.Y) && (p1.Y <= p.Y))) {
			// translate the segment so that the test point lies on the origin
			double x1 = p1.X - p.X;
			double y1 = p1.Y - p.Y;
			double x2 = p2.X - p.X;
			double y2 = p2.Y - p.Y;

			/*
			 * The translated segment straddles the x-axis. Compute the sign of
			 * the ordinate of intersection with the x-axis. (y2 != y1, so
			 * denominator will never be 0.0)
			 */
			// double xIntSign = RobustDeterminant.signOfDet2x2(x1, y1, x2, y2)
			// / (y2
			// - y1);
			// MD - faster & more robust computation?
			double xIntSign = signOfDet2x2(x1, y1, x2, y2);
			if (xIntSign == 0.0) {
				isPointOnSegment = true;
				return;
			}
			if (y2 < y1)
				xIntSign = -xIntSign;
			// xsave = xInt;

			// The segment crosses the ray if the sign is strictly positive.
			if (xIntSign > 0.0) {
				crossingCount++;
			}
		}
	}

	// / <summary>
	// / Tests whether a point lies on the line segments defined by a
	// / list of coordinates.
	// / </summary>
	// / <param name="p"></param>
	// / <param name="pt"></param>
	// / <returns>true if the point is a vertex of the line
	// / or lies in the interior of a line segment in the linestring
	// / </returns>
	public static boolean isOnLine(Point p, Point[] pt) {
		int crossingCount = 0;
		boolean isPointInsegment = false;
		for (int i = 1; i < pt.length; i++) {
			Point p0 = pt[i - 1];
			Point p1 = pt[i];
			countSegment(p0, p1, p, isPointInsegment, crossingCount);
			if (isPointInsegment)
				return true;
		}
		return false;
	}

	// / <summary>
	// / Computes whether a ring defined by an array of <see cref="Point" />s is
	// oriented counter-clockwise.
	// / </summary>>
	// / <remarks>
	// / <list type="Bullet">
	// / <item>The list of points is assumed to have the first and last points
	// equal.</item>
	// / <item>This will handle coordinate lists which contain repeated
	// points.</item>
	// / </list>
	// / <para>This algorithm is only guaranteed to work with valid rings. If
	// the ring is invalid (e.g. self-crosses or touches), the computed result
	// may not be correct.</para>
	// / </remarks>
	// / <param name="ring">An array of <see cref="Point"/>s froming a
	// ring</param>
	// / <returns>true if the ring is oriented <see
	// cref="Orientation.CounterClockwise"/></returns>
	// / <exception cref="ArgumentException">If there are too few points to
	// determine orientation (&lt;4)</exception>
	// public static boolean isCCW(List<Point> ring) throws Exception {
	// // # of points without closing endpoint
	// // sanity check
	// int nPts = 0;
	// if (ring == null || ring.size() < 4) {
	// throw new Exception(
	// "Ring has fewer than 4 points, so orientation cannot be determined");
	// } else {
	// nPts = ring.size() - 1;
	// }
	//
	// // find highest point
	// Point hiPt = ring.get(0);
	// int hiIndex = 0;
	// for (int i = 1; i <= nPts; i++) {
	// Point p = ring.get(i);
	// if (p.Y > hiPt.Y) {
	// hiPt = p;
	// hiIndex = i;
	// }
	// }
	//
	// // find distinct point before highest point
	// int iPrev = hiIndex;
	// do {
	// iPrev = iPrev - 1;
	// if (iPrev < 0)
	// iPrev = nPts;
	// } while (ring.get(iPrev).equals(hiPt) && iPrev != hiIndex);
	//
	// // find distinct point after highest point
	// int iNext = hiIndex;
	// do
	// iNext = (iNext + 1) % nPts;
	// while (ring.get(iNext).equals(hiPt) && iNext != hiIndex);
	//
	// Point prev = ring.get(iPrev);
	// Point next = ring.get(iNext);
	//
	// /*
	// * This check catches cases where the ring contains an A-B-A
	// * configuration of points. This can happen if the ring does not contain
	// * 3 distinct points (including the case where the input array has fewer
	// * than 4 elements), or it contains coincident line segments.
	// */
	// if (prev.equals(hiPt) || next.equals(hiPt) || prev.equals(next))
	// return false;
	//
	// int disc = computeOrientation(prev, hiPt, next);
	//
	// /*
	// * If disc is exactly 0, lines are collinear. There are two possible
	// * cases: (1) the lines lie along the x axis in opposite directions (2)
	// * the lines lie on top of one another
	// *
	// * (1) is handled by checking if next is left of prev ==> CCW (2) will
	// * never happen if the ring is valid, so don't check for it (Might want
	// * to assert this)
	// */
	// boolean isCCW = false;
	// if (disc == 0)
	// // poly is CCW if prev x is right of next x
	// isCCW = (prev.X > next.X);
	// else
	// // if area is positive, points are ordered CCW
	// isCCW = (disc > 0);
	// return isCCW;
	// }

	public static boolean isCCW(List<double[]> ring) throws Exception {
		// # of points without closing endpoint
		// sanity check
		int nPts = 0;
		if (ring == null || ring.size() < 4) {
			throw new Exception(
					"Ring has fewer than 4 points, so orientation cannot be determined");
		} else {
			nPts = ring.size() - 1;
		}

		// find highest point
		double[] hiPt = ring.get(0);
		int hiIndex = 0;
		for (int i = 1; i <= nPts; i++) {
			double[] p = ring.get(i);
			if (p[1] > hiPt[1]) {
				hiPt = p;
				hiIndex = i;
			}
		}

		// find distinct point before highest point
		int iPrev = hiIndex;
		do {
			iPrev = iPrev - 1;
			if (iPrev < 0)
				iPrev = nPts;
		} while (ring.get(iPrev).equals(hiPt) && iPrev != hiIndex);

		// find distinct point after highest point
		int iNext = hiIndex;
		do
			iNext = (iNext + 1) % nPts;
		while (ring.get(iNext).equals(hiPt) && iNext != hiIndex);

		double[] prev = ring.get(iPrev);
		double[] next = ring.get(iNext);

		/*
		 * This check catches cases where the ring contains an A-B-A
		 * configuration of points. This can happen if the ring does not contain
		 * 3 distinct points (including the case where the input array has fewer
		 * than 4 elements), or it contains coincident line segments.
		 */
		if (MRFUtil.isEqual(prev, hiPt) || MRFUtil.isEqual(next, hiPt)
				|| MRFUtil.isEqual(prev, next))
			return false;

		int disc = computeOrientation(prev, hiPt, next);

		/*
		 * If disc is exactly 0, lines are collinear. There are two possible
		 * cases: (1) the lines lie along the x axis in opposite directions (2)
		 * the lines lie on top of one another
		 * 
		 * (1) is handled by checking if next is left of prev ==> CCW (2) will
		 * never happen if the ring is valid, so don't check for it (Might want
		 * to assert this)
		 */
		boolean isCCW = false;
		if (disc == 0)
			// poly is CCW if prev x is right of next x
			isCCW = (prev[0] > next[0]);
		else
			// if area is positive, points are ordered CCW
			isCCW = (disc > 0);
		return isCCW;
	}

	// / <summary>
	// / Computes whether a ring defined by a coordinate sequence is oriented
	// counter-clockwise.
	// / </summary>>
	// / <remarks>
	// / <list type="Bullet">
	// / <item>The list of points is assumed to have the first and last points
	// equal.</item>
	// / <item>This will handle coordinate lists which contain repeated
	// points.</item>
	// / </list>
	// / <para>This algorithm is only guaranteed to work with valid rings. If
	// the ring is invalid (e.g. self-crosses or touches), the computed result
	// may not be correct.</para>
	// / </remarks>
	// / <param name="ring">A coordinate sequence froming a ring</param>
	// / <returns>true if the ring is oriented <see
	// cref="Orientation.CounterClockwise"/></returns>
	// / <exception cref="ArgumentException">If there are too few points to
	// determine orientation (&lt;4)</exception>
	public static boolean IsCCW(Line ring) throws Exception {
		return isCCW(ring.getListPoints());
	}

	// / <summary>
	// / Computes the orientation of a point q to the directed line segment
	// p1-p2.
	// / The orientation of a point relative to a directed line segment
	// indicates
	// / which way you turn to get to q after travelling from p1 to p2.
	// / </summary>
	// / <param name="p1">The first vertex of the line segment</param>
	// / <param name="p2">The second vertex of the line segment</param>
	// / <param name="q">The point to compute the relative orientation
	// of</param>
	// / <returns>
	// / 1 if q is counter-clockwise from p1-p2,
	// / or -1 if q is clockwise from p1-p2,
	// / or 0 if q is collinear with p1-p2
	// / </returns>
	public static int computeOrientation(Point p1, Point p2, Point q) {
		return orientationIndex(p1, p2, q);
	}

	public static int computeOrientation(double[] p1, double[] p2, double[] q) {
		return orientationIndex(p1, p2, q);
	}

	// / <summary>
	// / Computes the distance from a point p to a line segment AB.
	// / Note: NON-ROBUST!
	// / </summary>
	// / <param name="p">The point to compute the distance for.</param>
	// / <param name="A">One point of the line.</param>
	// / <param name="B">Another point of the line (must be different to
	// A).</param>
	// / <returns> The distance from p to line segment AB.</returns>
	public static double distancePointLine(Point p, Point A, Point B) {
		// if start = end, then just compute distance to one of the endpoints
		if (A.X == B.X && A.Y == B.Y)
			return p.distance(A);

		// otherwise use comp.graphics.algorithms Frequently Asked Questions
		// method
		/*
		 * (1) AC dot AB r = --------- ||AB||^2
		 * 
		 * r has the following meaning: r=0 Point = A r=1 Point = B r<0 Point is
		 * on the backward extension of AB r>1 Point is on the forward extension
		 * of AB 0<r<1 Point is interior to AB
		 */

		double len2 = ((B.X - A.X) * (B.X - A.X) + (B.Y - A.Y) * (B.Y - A.Y));
		double r = ((p.X - A.X) * (B.X - A.X) + (p.Y - A.Y) * (B.Y - A.Y))
				/ len2;

		if (r <= 0.0)
			return p.distance(A);
		if (r >= 1.0)
			return p.distance(B);

		/*
		 * (2) (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay) s = -----------------------------
		 * Curve^2
		 * 
		 * Then the distance from C to Point = |s|*Curve.
		 * 
		 * This is the same calculation as {@link
		 * #distancePointLinePerpendicular}. Unrolled here for performance.
		 */

		double s = ((A.Y - p.Y) * (B.X - A.X) - (A.X - p.X) * (B.Y - A.Y))
				/ len2;

		return Math.abs(s) * Math.sqrt(len2);
	}

	public static double distancePointLine(double[] p, double[] A, double[] B) {
		// if start = end, then just compute distance to one of the endpoints
		if (A[0] == B[0] && A[1] == B[1])
			return MRFUtil.distance(p, A);

		// otherwise use comp.graphics.algorithms Frequently Asked Questions
		// method
		/*
		 * (1) AC dot AB r = --------- ||AB||^2
		 * 
		 * r has the following meaning: r=0 Point = A r=1 Point = B r<0 Point is
		 * on the backward extension of AB r>1 Point is on the forward extension
		 * of AB 0<r<1 Point is interior to AB
		 */

		double len2 = ((B[0] - A[0]) * (B[0] - A[0]) + (B[1] - A[1])
				* (B[1] - A[1]));
		double r = ((p[0] - A[0]) * (B[0] - A[0]) + (p[1] - A[1])
				* (B[1] - A[1]))
				/ len2;

		if (r <= 0.0)
			return MRFUtil.distance(p, A);
		if (r >= 1.0)
			return MRFUtil.distance(p, B);

		/*
		 * (2) (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay) s = -----------------------------
		 * Curve^2
		 * 
		 * Then the distance from C to Point = |s|*Curve.
		 * 
		 * This is the same calculation as {@link
		 * #distancePointLinePerpendicular}. Unrolled here for performance.
		 */

		double s = ((A[1] - p[1]) * (B[0] - A[0]) - (A[0] - p[0])
				* (B[1] - A[1]))
				/ len2;

		return Math.abs(s) * Math.sqrt(len2);
	}

	// / <summary>
	// / Computes the perpendicular distance from a point p
	// / to the (infinite) line containing the points AB
	// / </summary>
	// / <param name="p">The point to compute the distance for.</param>
	// / <param name="A">One point of the line.</param>
	// / <param name="B">Another point of the line (must be different to
	// A).</param>
	// / <returns>The perpendicular distance from p to line AB.</returns>
	public static double distancePointLinePerpendicular(Point p, Point A,
			Point B) {
		// use comp.graphics.algorithms Frequently Asked Questions method
		/*
		 * (2) (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay) s = -----------------------------
		 * Curve^2
		 * 
		 * Then the distance from C to Point = |s|*Curve.
		 */
		double len2 = ((B.X - A.X) * (B.X - A.X) + (B.Y - A.Y) * (B.Y - A.Y));
		double s = ((A.Y - p.Y) * (B.X - A.X) - (A.X - p.X) * (B.Y - A.Y))
				/ len2;

		return Math.abs(s) * Math.sqrt(len2);
	}

	public static double distancePointLinePerpendicular(Point p, double[] A,
			double[] B) {
		return distancePointLinePerpendicular(new double[] { p.X, p.Y }, A, B);
	}

	public static double distancePointLinePerpendicular(double[] p, double[] A,
			double[] B) {
		// use comp.graphics.algorithms Frequently Asked Questions method
		/*
		 * (2) (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay) s = -----------------------------
		 * Curve^2
		 * 
		 * Then the distance from C to Point = |s|*Curve.
		 */
		double len2 = ((B[0] - A[0]) * (B[0] - A[0]) + (B[1] - A[1])
				* (B[1] - A[1]));
		double s = ((A[1] - p[1]) * (B[0] - A[0]) - (A[0] - p[0])
				* (B[1] - A[1]))
				/ len2;

		return Math.abs(s) * Math.sqrt(len2);
	}

	// / <summary>
	// / Computes the distance from a point to a sequence of line segments.
	// / </summary>
	// / <param name="p">A point</param>
	// / <param name="line">A sequence of contiguous line segments defined by
	// their vertices</param>
	// / <returns>The minimum distance between the point and the line
	// segments</returns>
	// / <exception cref="ArgumentException">If there are too few points to make
	// up a line (at least one?)</exception>
	public static double distancePointLine(Point p, Point[] line)
			throws Exception {
		if (line.length == 0)
			throw new Exception("Line array must contain at least one vertex");

		// this handles the case of length = 1
		double minDistance = p.distance(line[0]);
		for (int i = 0; i < line.length - 1; i++) {
			double dist = CGAlgorithms.distancePointLine(p, line[i],
					line[i + 1]);
			if (dist < minDistance) {
				minDistance = dist;
			}
		}
		return minDistance;
	}

	// / <summary>
	// / Computes the distance from a line segment AB to a line segment CD.
	// / Note: NON-ROBUST!
	// / </summary>
	// / <param name="A">A point of one line.</param>
	// / <param name="B">The second point of the line (must be different to
	// A).</param>
	// / <param name="C">One point of the line.</param>
	// / <param name="D">Another point of the line (must be different to
	// A).</param>
	// / <returns>The distance from line segment AB to line segment
	// CD.</returns>
	public static double distanceLineLine(Point A, Point B, Point C, Point D) {
		// check for zero-length segments
		if (A.equals(B))
			return distancePointLine(A, C, D);
		if (C.equals(D))
			return distancePointLine(D, A, B);

		// AB and CD are line segments
		/*
		 * from comp.graphics.algo
		 * 
		 * Solving the above for r and s yields
		 * 
		 * (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy) r = ----------------------------- (eqn
		 * 1) (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
		 * 
		 * (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay) s = ----------------------------- (eqn
		 * 2) (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
		 * 
		 * Let P be the position vector of the intersection point, then
		 * P=A+r(B-A) or Px=Ax+r(Bx-Ax) Py=Ay+r(By-Ay) By examining the values
		 * of r & s, you can also determine some other limiting conditions: If
		 * 0<=r<=1 & 0<=s<=1, intersection exists r<0 or r>1 or s<0 or s>1 line
		 * segments do not intersect If the denominator in eqn 1 is zero, AB &
		 * CD are parallel If the numerator in eqn 1 is also zero, AB & CD are
		 * collinear.
		 */
		boolean noIntersection = false;
		if (!Envelope.intersects(A, B, C, D)) {
			noIntersection = true;
		} else {
			double denom = (B.X - A.X) * (D.Y - C.Y) - (B.Y - A.Y)
					* (D.X - C.X);

			if (denom == 0) {
				noIntersection = true;
			} else {
				double r_num = (A.Y - C.Y) * (D.X - C.X) - (A.X - C.X)
						* (D.Y - C.Y);
				double s_num = (A.Y - C.Y) * (B.X - A.X) - (A.X - C.X)
						* (B.Y - A.Y);

				double s = s_num / denom;
				double r = r_num / denom;

				if ((r < 0) || (r > 1) || (s < 0) || (s > 1)) {
					noIntersection = true;
				}
			}
		}
		if (noIntersection) {
			double min = distancePointLine(A, C, D);
			double v1 = distancePointLine(B, C, D);
			double v2 = distancePointLine(C, A, B);
			double v3 = distancePointLine(D, A, B);
			if (min > v2)
				min = v2;
			if (min > v1)
				min = v1;
			if (min > v3)
				min = v3;
			return min;
		}
		// segments intersect
		return 0.0;
	}

	public static double distanceLineLine(double[] A, double[] B, double[] C,
			double[] D) {
		// check for zero-length segments
		if (A.equals(B))
			return distancePointLine(A, C, D);
		if (C.equals(D))
			return distancePointLine(D, A, B);

		// AB and CD are line segments
		/*
		 * from comp.graphics.algo
		 * 
		 * Solving the above for r and s yields
		 * 
		 * (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy) r = ----------------------------- (eqn
		 * 1) (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
		 * 
		 * (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay) s = ----------------------------- (eqn
		 * 2) (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
		 * 
		 * Let P be the position vector of the intersection point, then
		 * P=A+r(B-A) or Px=Ax+r(Bx-Ax) Py=Ay+r(By-Ay) By examining the values
		 * of r & s, you can also determine some other limiting conditions: If
		 * 0<=r<=1 & 0<=s<=1, intersection exists r<0 or r>1 or s<0 or s>1 line
		 * segments do not intersect If the denominator in eqn 1 is zero, AB &
		 * CD are parallel If the numerator in eqn 1 is also zero, AB & CD are
		 * collinear.
		 */
		boolean noIntersection = false;
		if (!Envelope.intersects(A, B, C, D)) {
			noIntersection = true;
		} else {
			double denom = (B[0] - A[0]) * (D[1] - C[1]) - (B[1] - A[1])
					* (D[0] - C[0]);

			if (denom == 0) {
				noIntersection = true;
			} else {
				double r_num = (A[1] - C[1]) * (D[0] - C[0]) - (A[0] - C[0])
						* (D[1] - C[1]);
				double s_num = (A[1] - C[1]) * (B[0] - A[0]) - (A[0] - C[0])
						* (B[1] - A[1]);

				double s = s_num / denom;
				double r = r_num / denom;

				if ((r < 0) || (r > 1) || (s < 0) || (s > 1)) {
					noIntersection = true;
				}
			}
		}
		if (noIntersection) {
			double min = distancePointLine(A, C, D);
			double v1 = distancePointLine(B, C, D);
			double v2 = distancePointLine(C, A, B);
			double v3 = distancePointLine(D, A, B);
			if (min > v2)
				min = v2;
			if (min > v1)
				min = v1;
			if (min > v3)
				min = v3;
			return min;
		}
		// segments intersect
		return 0.0;
	}

	// / <summary>
	// / Computes the signed area for a ring.
	// / <remarks>
	// / <para>
	// / The signed area is
	// / </para>
	// / <list type="Table">
	// / <item>positive</item><description>if the ring is oriented
	// CW</description>
	// / <item>negative</item><description>if the ring is oriented
	// CCW</description>
	// / <item>zero</item><description>if the ring is degenerate or
	// flat</description>
	// / </list>
	// / </remarks>
	// / </summary>
	// / <param name="ring">The coordinates of the ring</param>
	// / <returns>The signed area of the ring</returns>
	public static double signedArea(Point[] ring) {
		if (ring.length < 3)
			return 0.0;

		double sum = 0.0;
		/**
		 * Based on the Shoelace formula.
		 * http://en.wikipedia.org/wiki/Shoelace_formula
		 */
		double x0 = ring[0].X;
		for (int i = 1; i < ring.length - 1; i++) {
			double x = ring[i].X - x0;
			double y1 = ring[i + 1].Y;
			double y2 = ring[i - 1].Y;
			sum += x * (y2 - y1);
		}
		return sum / 2.0;
	}

	// / <summary>
	// / Computes the signed area for a ring.
	// / <remarks>
	// / <para>
	// / The signed area is
	// / </para>
	// / <list type="Table">
	// / <item>positive</item><description>if the ring is oriented
	// CW</description>
	// / <item>negative</item><description>if the ring is oriented
	// CCW</description>
	// / <item>zero</item><description>if the ring is degenerate or
	// flat</description>
	// / </list>
	// / </remarks>
	// / </summary>
	// / <param name="ring">The coordinates forming the ring</param>
	// / <returns>The signed area of the ring</returns>
	public static double signedArea(Line line) {
		List<double[]> ring = line.getListPoints();

		if (ring == null || ring.size() < 3) {
			return 0.0;
		}
		int n = ring.size();

		/**
		 * Based on the Shoelace formula.
		 * http://en.wikipedia.org/wiki/Shoelace_formula
		 */

		double x, y1, y2;
		double x0 = ring.get(0)[0];
		double sum = 0.0;
		for (int i = 1; i < n - 1; i++) {
			x = ring.get(i)[0] - x0;
			y1 = ring.get(i + 1)[1];
			y2 = ring.get(i - 1)[1];
			sum += x * (y2 - y1);
		}
		return Math.abs(sum / 2.0);
	}

	// / <summary>
	// / Computes the length of a linestring specified by a sequence of points.
	// / </summary>
	// / <param name="pts">The points specifying the linestring</param>
	// / <returns>The length of the linestring</returns>
	public static double length(Line line) {
		// optimized for processing CoordinateSequences
		List<double[]> ring = line.getListPoints();

		if (ring == null || ring.size() < 2) {
			return 0.0;
		}
		int n = ring.size();
		/**
		 * Based on the Shoelace formula.
		 * http://en.wikipedia.org/wiki/Shoelace_formula
		 */
		double[] p0 = ring.get(0);
		double[] p1;
		double sum = 0.0;
		for (int i = 0; i < n - 1; i++) {
			p1 = ring.get(i + 1);
			sum += Math.sqrt((p1[0] - p0[0]) * (p1[0] - p0[0])
					+ (p1[1] - p0[1]) * (p1[1] - p0[1]));
			p0[0] = p1[0];
			p0[1] = p1[1];
		}
		return sum;
	}
}
