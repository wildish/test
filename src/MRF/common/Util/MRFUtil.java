package MRF.common.Util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import com.mrf.proj.Point2D;

import MRF.common.Data.SpatialResult;
import MRF.common.Data.sqlite.SpatialDataAccess;
import MRF.common.geometry.Geometry;
import MRF.common.geometry.Point;
import MRF.common.geometry.WKBGeometryType;

public class MRFUtil {

	private static MRFUtil sharedInstance = null;
	private HashMap<String, Object> parameters;

	public MRFUtil() {
		parameters = new HashMap<String, Object>();
	}

	public static MRFUtil sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new MRFUtil();
		}
		return sharedInstance;
	}

	public boolean containsInCachedParameter(String key) {
		if (key != null && parameters.containsKey(key)
				&& parameters.get(key) != null) {
			return true;
		} else {
			return false;
		}
	}

	public Object getCachedParameter(String key) {
		if (parameters.containsKey(key)) {
			return parameters.get(key);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static Element getFirstChild(Element ele) {
		if (ele == null) {
			return null;
		}
		List<Element> childs = ele.elements();
		if (childs == null || childs.size() == 0) {
			return null;
		}
		return childs.get(0);
	}

	@SuppressWarnings("unchecked")
	public static Element getChildByName(Element parent, String name) {
		if (parent == null) {
			return null;
		}
		List<Element> childs = parent.elements(name);
		if (childs == null || childs.size() == 0) {
			return null;
		}
		return childs.get(0);
	}

	@SuppressWarnings("unchecked")
	public static List<Element> getchildListByName(Element parent, String string) {
		if (parent == null) {
			return null;
		}
		List<Element> childs = parent.elements(string);
		if (childs == null || childs.size() == 0) {
			return null;
		}
		return childs;
	}

	public static String getText(Element ele) {
		if (ele == null) {
			return null;
		}
		return ele.getTextTrim();
	}

	public static String getTextWithDefault(Element ele, String string,
			String string2) {
		Element child = getChildByName(ele, string);
		if (child == null) {
			return string2;
		}
		return child.getText();
	}

	public static double getTextDoubleWithDefault(Element ele, String string,
			double i) {
		Element child = getChildByName(ele, string);
		if (child == null) {
			return i;
		}
		return Double.parseDouble(child.getText());
	}

	public static boolean getTextBooleanWithDefault(Element ele, String string,
			boolean value) {
		Element child = getChildByName(ele, string);
		if (child == null) {
			return value;
		}
		return Boolean.parseBoolean(child.getText());
	}

	@SuppressWarnings("unchecked")
	public static List<Element> getNodeListByPath(Document rootElement,
			String string) {
		if (rootElement == null) {
			return null;
		}
		return rootElement.selectNodes(string);
	}

	@SuppressWarnings("unchecked")
	public static List<Element> getNodeListByPath(Element ele, String string) {
		if (ele == null) {
			return null;
		}
		return ele.selectNodes(string);
	}

	public static Element getNodeByPath(Document document, String string) {
		if (document == null) {
			return null;
		}
		return (Element) document.selectNodes(string).get(0);
	}

	@SuppressWarnings("unchecked")
	public static Element getNodeByPath(Element layerNode, String string) {
		List<Element> list = layerNode.selectNodes(string);
		if (list == null || list.size() == 0) {
			return null;
		}

		return list.get(0);
	}

	public static String getAttrValueWithDefault(Element ele, String string,
			String string2) {
		if (ele == null) {
			return null;
		}
		String attr = ele.attributeValue(string);
		if (attr == null) {
			return string2;
		}
		return attr;
	}

	public static int getAttrIntWithDefault(Element element, String attrName,
			int value) {
		if (element == null) {
			return value;
		}
		String attr = element.attributeValue(attrName);
		if (StringUtil.isBlank(attr)) {
			return value;
		}
		return Integer.parseInt(attr);
	}

	public static boolean getAttrBoolWithDefault(Element ele, String string,
			boolean b) {
		if (ele == null) {
			return b;
		}
		String attr = ele.attributeValue(string);
		if (StringUtil.isBlank(attr)) {
			return b;
		}
		return Boolean.parseBoolean(attr);
	}

	public static double getAttrDoubleWidthDefault(Element ele, String string,
			double i) {
		if (ele == null) {
			return i;
		}
		String attr = ele.attributeValue(string);
		if (StringUtil.isBlank(attr)) {
			return i;
		}
		return Double.parseDouble(attr);
	}

	public static String xmlToString(List<Element> elements) {
		if (elements == null) {
			return "";
		}
		StringBuffer value = new StringBuffer();
		for (int i = 0; i < elements.size(); i++) {
			Element n = elements.get(i);
			value.append(xmlToString(n));
		}
		return value.toString();
	}

	@SuppressWarnings("unchecked")
	public static String xmlToString(Element node) {
		if (node == null) {
			return "";
		}
		StringBuffer buffer = new StringBuffer();
		String nodeName = node.getName();
		if (node.elements().size() > 0) {
			buffer.append("<");
			buffer.append(nodeName);
			buffer.append(attrsToString(node));
			buffer.append(">");
			buffer.append(xmlToString(node.elements()));
			buffer.append("</");
			buffer.append(nodeName);
			buffer.append(">");
			return buffer.toString();
		} else {
			if (node.getNodeType() == Node.TEXT_NODE) {
				return node.getText().replaceAll("\n", "").replaceAll("\t", "");
			} else {
				buffer.append("<");
				buffer.append(node.getName());
				buffer.append(attrsToString(node));
				buffer.append("></");
				buffer.append(node.getName());
				buffer.append(">");
				return buffer.toString();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static Object attrsToString(Element node) {
		StringBuffer attsStr = new StringBuffer();
		List<Node> attrs = node.attributes();
		if (attrs == null || attrs.size() == 0) {
			return "";
		}
		for (int i = 0; i < attrs.size(); i++) {
			Node n = attrs.get(i);
			attsStr.append(" ");
			attsStr.append(n.getName());
			attsStr.append("=\"");
			attsStr.append(n.getStringValue());
			attsStr.append("\"");
		}
		return attsStr.toString();
	}

	public static boolean isNonnegativeFloatingpointValidationWith(String data) {
		Pattern pattern = Pattern
				.compile("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$");
		Matcher matcher = pattern.matcher(data);
		int numberOfMatches = matcher.groupCount();
		if (data == null || data.equals("") || numberOfMatches != 1) {
			return false;
		}
		return true;
	}

	public static String getStringParameter(HashMap<String, Object> parameter,
			String key) {
		Object obj = getParameter(parameter, key);
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}

	public static Object getParameter(HashMap<String, Object> parameter,
			String key) {
		if (parameter.containsKey(key)) {
			return parameter.get(key);
		}
		return null;
	}

	public static int getIntParameter(HashMap<String, Object> parameter,
			String key) {
		Object obj = getParameter(parameter, key);
		if (obj != null) {
			return Integer.parseInt(obj.toString());
		}
		return 0;
	}

	public static String replaceCharsForUpdate(String srcString) {
		if (srcString == null) {
			return srcString;
		}
		srcString = srcString.replaceAll("&", "&amp;")
				.replaceAll("\"", "&quot;").replaceAll("\'", "&apos;")
				.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

		return srcString;
	}

	public static String replaceCharsForQuery(String srcString) {
		if (srcString == null) {
			return srcString;
		}
		srcString = srcString.replaceAll("&quot;", "\"")
				.replaceAll("&apos;", "\'").replaceAll("&lt;", "<")
				.replaceAll("&gt;", ">").replaceAll("&amp;", "&");

		return srcString;
	}

	public static boolean isEqual(double[] p1, double[] p2) {
		if (p1 != null || p2 != null) {
			if (p1.length != 0 && p2.length != 0 && p1.length != p2.length) {
				return false;
			} else {
				if (p1.length == 2) {
					return p1[0] == p2[0] && p1[1] == p2[1];
				} else {
					return p1[0] == p2[0] && p1[1] == p2[1] && p1[2] == p2[2];
				}
			}
		} else {
			return false;
		}
	}

	public static double distance(double[] p1, Point p2) {
		return distance(p1, new double[] { p2.X, p2.Y });
	}

	public static double distance(double[] p1, double[] p2) {
		return Math.sqrt(Math.pow(p1[0] - p2[0], 2)
				+ Math.pow(p1[1] - p2[1], 2));
	}

	public static int compareTo(double[] p1, double[] p2) {
		if (p1[0] < p2[0])
			return -1;
		if (p1[0] > p2[0])
			return 1;
		if (p1[1] < p2[1])
			return -1;
		return p1[1] > p2[1] ? 1 : 0;
	}

	public static String escapeXMLChars(String input) {
		if (StringUtil.isBlank(input))
			return "";

		input = formatString2XML(input);
		return input;
	}

	public static String formatString2XML(String strData) {
		strData = strData.replaceAll("&", "&amp;");
		strData = strData.replaceAll("<", "&lt;");
		strData = strData.replaceAll(">", "&gt;");
		strData = strData.replaceAll("'", "&apos;");
		strData = strData.replaceAll("\"", "&quot;");
		return strData;
	}

	public static String formatXML2String(String strData) {
		strData = strData.replaceAll("&lt;", "<");
		strData = strData.replaceAll("&gt;", ">");
		strData = strData.replaceAll("&apos;", "'");
		strData = strData.replaceAll("&quot;", "\"");
		strData = strData.replaceAll("&amp;", "&");
		return strData;
	}

	public static Long getGUID() {
		UUID uuid = UUID.randomUUID();
		String guid = uuid.toString();
		guid = guid.toUpperCase();
		guid = guid.replaceAll("-", "");
		return getBKDRKey(guid);
	}

	public static Long getBKDRKey(String key) {
		long hash = 0;
		try {
			byte[] md = key.getBytes("UTF-8");
			long seed = 131;
			for (int i = 0; i < md.length; i++) {
				hash = hash * seed + md[i];
			}

			if (hash < 0) {
				hash *= (-1);
			}
			while (hash > 99944594895626L) {
				hash = hash / 10;
			}
		} catch (Exception ex) {
		}
		return Long.valueOf(hash);
	}

	public static String jsonStringFilter(String s) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			switch (c) {
			case '\"':
				builder.append("\\\"");
				break;
			case '\\':
				builder.append("\\\\");
				break;
			case '/':
				builder.append("\\/");
				break;
			case '\b':
				builder.append("\\b");
				break;
			case '\f':
				builder.append("\\f");
				break;
			case '\n':
				builder.append("\\n");
				break;
			case '\r':
				builder.append("\\r");
				break;
			case '\t':
				builder.append("\\t");
				break;
			default:
				builder.append(c);
				break;
			}
		}
		return builder.toString();
	}

	public void putCachedParameter(String key, Object obj) {
		if (obj != null) {
			parameters.put(key, obj);
		}
	}

	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv.toUpperCase());
		}
		return stringBuilder.toString();
	}

	public static byte[] hexStringToBytes(String hexString) {
		if (hexString == null || hexString.equals("")) {
			return null;
		}
		int length = hexString.length() / 2;
		if (hexString.length() % 2 != 0) {
			return null;
		}
		byte[] d = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			d[i] = (byte) (charToByte(hexString.charAt(pos)) << 4 | charToByte(hexString
					.charAt(pos + 1)));
		}
		return d;
	}

	private static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

	public static String AESEncrypt(String content) {
		try {
			byte[] raw = "OfCsePVPmbdslnu4".getBytes();
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			byte[] byteContent = content.getBytes();
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			byte[] bytes = cipher.doFinal(byteContent);
			return bytesToHexString(bytes);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String AESDecrypt(String hexString) {
		try {
			byte[] raw = "OfCsePVPmbdslnu4".getBytes();
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			byte[] content = hexStringToBytes(hexString);
			byte[] bytes = cipher.doFinal(content);
			return new String(bytes);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Point2D getCurrentTaskCenter(String taskId) {
		SpatialResult result = SpatialDataAccess.getInstance().query(
				CommonContext.CLIENT_DATABASE_NAME, "t_task", null, null,
				"range_geom", String.format("id=%s", taskId), null);
		if (result != null && result.getDatas().size() > 0) {
			Geometry geo = result.getDatas().get(0).getWKBGeometry();
			if (geo != null
					&& geo.getGeometryType() != WKBGeometryType.WKBUNKNOWN) {
				return new Point2D(
						(float) ((geo.getEnv().minX + geo.getEnv().maxX) / 2.0),
						(float) ((geo.getEnv().maxY + geo.getEnv().minY) / 2.0));
			}
		}
		return new Point2D();
	}
}
