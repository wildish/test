/*
 *  CoordinateUtil.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import MRF.common.geometry.Point;
import android.annotation.SuppressLint;

import com.mrf.proj.Point2D;
import com.mrf.proj.Projection;
import com.mrf.proj.ProjectionFactory;

/**
 * 
 * @author Admin
 */
public class CoordinateUtil {

	private static HashMap<String, String> epsgs = new HashMap<String, String>();

	public static boolean transformCoordinate(String sourceCoordinate,
			String targetCoordinate, List<Point> pts) {
		if (StringUtil.isBlank(sourceCoordinate)
				|| StringUtil.isBlank(targetCoordinate)) {
			return false;
		}
		String sourceProj = getProjection(sourceCoordinate);
		String targetProj = getProjection(targetCoordinate);
		if (StringUtil.isBlank(sourceProj) || StringUtil.isBlank(targetProj)) {
			return false;
		}
		Projection sourceCS = ProjectionFactory
				.fromPROJ4Specification(sourceProj);
		Projection targetCS = ProjectionFactory
				.fromPROJ4Specification(targetProj);
		if (sourceCS == null || targetCS == null) {
			return false;
		}
		// TODO
		// ICoordinateTransformation trans = new
		// CoordinateTransformationFactory()
		// .createFromCoordinateSystems(sourceCS, targetCS);
		// trans.getMathTransform().transformListPoint(pts);
		for (Point p : pts) {
			Point2D src = new Point2D(p.X, p.Y);
			Point2D dst = new Point2D();
			Point2D temp = new Point2D();
			sourceCS.transform(src, temp);
			targetCS.inverseTransform(temp, dst);
			p.X = dst.getX();
			p.Y = dst.getY();
		}
		return true;
	}

	@SuppressLint("DefaultLocale")
	public static String getProjection(String epsg) {
		if (StringUtil.IsNullOrEmpty(epsg)) {
			return "";
		}
		epsg = epsg.toLowerCase();
		String[] parameter = epsg.split(" |:");
		if (parameter.length < 2) {
			return "";
		}

		if (epsgs.containsKey(parameter[1].trim())) {
			return epsgs.get(parameter[1].trim());
		} else {
			return "";
		}
	}

	public static void initEpsgs(String filePath) {
		File file = new File(filePath);
		if (file.exists()) {
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(file));

				String line = null;
				while ((line = reader.readLine()) != null) {
					if (line.startsWith("#")) {
						continue;
					} else {
						parseLine(line);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	private static void parseLine(String line) {
		int start = line.indexOf("<");
		int end = line.indexOf(">");
		String key = line.substring(start + 1, end).trim();
		String value = line.substring(end + 1).trim();
		epsgs.put(key, value);
	}
}
