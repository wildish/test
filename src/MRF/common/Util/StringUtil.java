package MRF.common.Util;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

public class StringUtil {
	public static double StringToDouble(String str) {
		if (str == "" || str == null) {
			return Double.NaN;
		} else {
			return Double.parseDouble(str);
		}
	}

	public static int StringToInt(String str, int defaultValue) {
		if (str == null || str.length() == 0) {
			return defaultValue;
		} else {
			return Integer.parseInt(str);
		}
	}

	public static boolean stringToBool(String str, boolean defaultValue) {
		if (str == null || str.length() == 0) {
			return defaultValue;
		}
		str = str.trim().toLowerCase();
		if (str.equals("false") || str.equals("true")) {
			return Boolean.parseBoolean(str);
		}
		return defaultValue;
	}

	public static String convertRGBToHEX(String rgbString) {
		if (rgbString == null || rgbString.trim().length() == 0) {
			return "";
		}

		String result = "#";
		String regex = "(?:\\(|\\)|rgb|RGB)*";
		String[] aColor = rgbString.replaceAll(regex, "").split(",");
		for (int i = 0; i < aColor.length; i++) {
			int r = Integer.parseInt(aColor[i]);
			String hexColor = Integer.toHexString(r);
			if (hexColor.length() < 2) {
				hexColor += "0";
			}
			result += hexColor;
		}
		if (result.length() != 7) {
			result = rgbString;
		}
		return rgbString;
	}

	public static byte[] hexStrToByte(String hexString) {
		try {
			if (hexString == null || hexString.equals("")) {
				return null;
			}
			hexString = hexString.toUpperCase();
			int length = hexString.length() / 2;
			char[] hexChars = hexString.toCharArray();
			byte[] d = new byte[length];
			for (int i = 0; i < length; i++) {
				int pos = i * 2;
				d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
			}
			return d;
		} catch (Exception ex) {
			return null;
		}
	}

	private static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

	public static String byteToHexStr(byte[] src) {
		try {
			StringBuilder stringBuilder = new StringBuilder("");
			if (src == null || src.length <= 0) {
				return null;
			}
			for (int i = 0; i < src.length; i++) {
				int v = src[i] & 0xFF;
				String hv = Integer.toHexString(v);
				if (hv.length() < 2) {
					stringBuilder.append(0);
				}
				stringBuilder.append(hv);
			}
			return stringBuilder.toString();
		} catch (Exception ex) {

			return null;
		}
	}

	public static byte[] readZipFile(String ZipedFileName) {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			FileInputStream fin = new FileInputStream(ZipedFileName);
			int read;
			byte[] bytes = new byte[1024];
			while ((read = fin.read(bytes)) > 0) {
				out.write(bytes, 0, read);
			}
			fin.close();
			bytes = out.toByteArray(); 
			out.close();
			return bytes;
		} catch (Exception ex) {
			return null;
		}
	}

	public static boolean isEmpty(String string) {
		if (string == null || string.length() == 0) {
			return true;
		}
		return false;
	}

	public static boolean isBlank(String string) {
		if (string == null || string.trim().length() == 0) {
			return true;
		}
		return false;
	}
	
	public static boolean IsNullOrEmpty(String str) {
		return isEmpty(str);
	}

	public static String trim(String text) {
		return text.trim();
	}
	
	public static boolean contains(String sourceString, String indexString,
			boolean ignoreCase) {
		if (ignoreCase) {
			int index = sourceString.toLowerCase().indexOf(
					indexString.toLowerCase());
			return (index > -1);
		} else {
			int index = sourceString.indexOf(indexString);
			return (index > -1);
		}
	}
}
