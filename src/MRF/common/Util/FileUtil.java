//
//  FileUtil.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package MRF.common.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;

public class FileUtil {
	private static String SDPATH;
    private static String FILESPATH;
    private static Context mContext;
    public String getSDPATH() {
        return SDPATH;
    }
    public void setSDPATH(String value) {
        SDPATH = value;
    }
    public String getFILESPATH() {
        return FILESPATH;
    }
    public FileUtil() {
        SDPATH = Environment.getExternalStorageDirectory() + File.separator;
    }
    public FileUtil(Context context) {
        this();
        mContext = context;
        FILESPATH = mContext.getFilesDir().getPath() + File.separator;
    }
    public static boolean isFileExists(String fileName) {
        if (fileName == null || (fileName = fileName.trim()).equals("")) {
            return false;
        }
        File file = new File(fileName);
        return isFileExists(file);
    }
    public static boolean isFileExists(File file) {
        if (file == null)
            return false;
        return file.exists();
    }
    
    public static boolean isDirectory(File file) {
        if (file == null)
            return false;
        return file.isDirectory();
    }
    public static boolean isDirectory(String fileName) {
        if (fileName == null || (fileName = fileName.trim()).equals("")) {
            return false;
        }
        File file = new File(fileName);
        return isDirectory(file);
    }
    
    public static boolean isFile(File file) {
        if (file == null)
            return false;
        return file.isFile();
    }
    public static boolean isFile(String fileName) {
        if (fileName == null || (fileName = fileName.trim()).equals("")) {
            return false;
        }
        File file = new File(fileName);
        return isFile(file);
    }
    
    public static boolean createOrExistsFolder(File file) {
        if (file == null)
            return false;
        boolean result = false;

        if (isFileExists(file) && isDirectory(file)) {
            return true;
        }
        if (file.mkdirs()) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }
    public static boolean createOrExistsFolder(String fileName) {
        if (fileName == null || (fileName = fileName.trim()).equals("")) {
            return false;
        }
        File file = new File(fileName);
        return createOrExistsFolder(file);
    }
    
    public static boolean createOrExistsFile(File file) {
        if (file == null)
            return false;
        boolean result = false;
        if (isFileExists(file) && isFile(file)) {
            return true;
        }
        File parentFile = file.getParentFile();
        if (!createOrExistsFolder(parentFile)) {
            return false;
        }
        try {
            if (file.createNewFile()) {
                result = true;
            } else {
                result = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }
    public static boolean createOrExistsFile(String fileName) {
        if (fileName == null || (fileName = fileName.trim()).equals("")) {
            return false;
        }
        File file = new File(fileName);
        return createOrExistsFile(file);
    }
    
    public static boolean createFileByDeleteOldFileIfNeeded(File file) {
        if (file == null)
            return false;
        if (isFileExists(file) && isFile(file)) {
            if (!file.delete()) {
                return false;
            }
        }
        return createOrExistsFile(file);
    }
    public static boolean createFileByDeleteOldFileIfNeeded(String fileName) {
        if (fileName == null || (fileName = fileName.trim()).equals("")) {
            return false;        }
        File file = new File(fileName);
        return createFileByDeleteOldFileIfNeeded(file);
    }
    
    public static boolean moveFilesTo(File srcDir, File destDir) {
        if (srcDir == null)
            return false;
        if (destDir == null)
            return false;
        if (!isFileExists(srcDir) || !isDirectory(srcDir)) {
            return false;
        }
        if (!isFileExists(destDir) || !isDirectory(destDir)) {
            if (!createOrExistsFolder(destDir)) {
                return false;
            }
        }
        File[] srcDirFiles = srcDir.listFiles();
        for (int i = 0; i < srcDirFiles.length; i++) {
            if (srcDirFiles[i].isFile()) {
                File oneDestFile = new File(destDir.getPath() + "//"
                        + srcDirFiles[i].getName());
                moveFileTo(srcDirFiles[i], oneDestFile);
                delFile(srcDirFiles[i]);
            } else if (srcDirFiles[i].isDirectory()) {
                File oneDestFile = new File(destDir.getPath() + "//"
                        + srcDirFiles[i].getName());
                moveFilesTo(srcDirFiles[i], oneDestFile);
                delDir(srcDirFiles[i]);
            }
        }
        return true;
    }
    
    public static boolean moveFileTo(File srcFile, File destFile) {
        if (srcFile == null)
            return false;
        if (destFile == null)
            return false;
        boolean iscopy = copyFileTo(srcFile, destFile);
        if (!iscopy)
            return false;
        if (!delFile(srcFile)) {
            return false;
        }
        return true;
    }
    
    public static boolean delFile(File file) {
        if (file == null)
            return false;
        if (!isFileExists(file)) {
            return true;
        }
        if (file.isDirectory())
            return false;
        return file.delete();
    }
    
    public static boolean delDir(File dir) {
        if (dir == null)
            return false;
        if (!isFileExists(dir)) {
            return true;
        }
        if (!isDirectory(dir)) {
            return false;
        }
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                if (!delFile(file)) {
                    return false;                }
            } else if (file.isDirectory()) {
                if (!delDir(file)) {
                    return false;
                }
            }
        }
        if (!dir.delete()) {
            return false;
        }
        return true;
    }
    
    public static boolean copyFileTo(File srcFile, File destFile) {
        if (srcFile == null)
            return false;
        if (destFile == null)
            return false;
        if (!isFileExists(srcFile)) {
            return false;
        }

        if (!isFile(srcFile)) {
            return false;
        }
        if (isFileExists(destFile) && !isFile(destFile))
            return false;

        File parentFile = destFile.getParentFile();
        if (!createOrExistsFolder(parentFile)) {
            return false;
        }

        FileInputStream fis = null;
        FileOutputStream fos = null;
        boolean result = false;
        try {
            fis = new FileInputStream(srcFile);
            fos = new FileOutputStream(destFile);
            int readLen = 0;
            byte[] buf = new byte[1024];
            while ((readLen = fis.read(buf)) != -1) {
                fos.write(buf, 0, readLen);
            }
            fos.flush();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            try {
                fos.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
                result = false;
            }

        }

        return result;
    }

    
    public static boolean copyFilesTo(File srcDir, File destDir) {
        if (srcDir == null)
            return false;
        if (destDir == null)
            return false;

        if (!isFileExists(srcDir) || !isDirectory(srcDir)) {
            return false;
        }

        if (!isFileExists(destDir) || !isDirectory(destDir)) {
            if (!createOrExistsFolder(destDir)) {
                return false;
            }
        }

        File[] srcFiles = srcDir.listFiles();
        for (int i = 0; i < srcFiles.length; i++) {
            if (srcFiles[i].isFile()) {
                File destFile = new File(destDir.getPath() + "/"
                        + srcFiles[i].getName());
                copyFileTo(srcFiles[i], destFile);
            } else if (srcFiles[i].isDirectory()) {
                File theDestDir = new File(destDir.getPath() + "/"
                        + srcFiles[i].getName());
                copyFilesTo(srcFiles[i], theDestDir);
            }
        }
        return true;
    }


    public static boolean writeToFileFromInputStream(String fileName, InputStream is) {
        if (fileName == null || (fileName = fileName.trim()).equals("")) {
            return false;
        }

        File parentFile = new File(fileName).getParentFile();
        if (!createOrExistsFolder(parentFile)) {
            return false;
        }

        boolean result = false;
        File file = null;
        OutputStream os = null;
        try {
            file = new File(fileName);
            os = new FileOutputStream(file);
            byte buffer[] = new byte[4 * 1024];
            int length = 0;
            while ((length = is.read(buffer)) != -1) {
                os.write(buffer, 0, length);
            }
            os.flush();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            file = null;
            result = false;
        } finally {
            try {
                os.close();

            } catch (Exception e2) {
                e2.printStackTrace();
                file = null;

            }
        }
        return result;
    }


    public static OutputStream writeFile(String fileName) throws IOException {
        File file = new File(fileName);

        File parentFile = file.getParentFile();
        createOrExistsFolder(parentFile);
        FileOutputStream fos = new FileOutputStream(file);
        return fos;
    }

  
    public static OutputStream appendFile(String fileName) throws IOException {
        File file = new File(fileName);
        File parentFile = file.getParentFile();
        createOrExistsFolder(parentFile);
        FileOutputStream fos = new FileOutputStream(file, true);
        return fos;
    }

    public static InputStream readFile(String fileName) throws IOException {
        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
        return fis;
    }


    public static boolean isSDCardMounted() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }


    public static boolean isSDFileExists(String fileName) {
        File file = new File(SDPATH + fileName);
        return file.exists();
    }

    public static boolean createOrExistsSDFile(String fileName) {
        File file = new File(SDPATH + fileName);
        return createOrExistsFile(file);
    }

    public static boolean createSDDir(String dirName) {
        File file = new File(SDPATH + dirName);
        return createOrExistsFolder(file);
    }


    public static boolean Write2SDFromInputStream(String fileName, InputStream is) {
        String fileName2 = SDPATH + fileName;
        return writeToFileFromInputStream(fileName2, is);
    }


    public static boolean delSDFile(String fileName) {
        File file = new File(SDPATH + fileName);
        return delFile(file);
    }


    public static boolean delSDDir(String dirName) {
        File dir = new File(SDPATH + dirName);
        return delDir(dir);
    }


    public static boolean renameSDFile(String oldfileName, String newFileName) {
        File oleFile = new File(SDPATH + oldfileName);
        File newFile = new File(SDPATH + newFileName);
        return moveFileTo(oleFile, newFile);
    }

  
    public static boolean copySDFileTo(String srcFileName, String destFileName) {
        File srcFile = new File(SDPATH + srcFileName);
        File destFile = new File(SDPATH + destFileName);
        return copyFileTo(srcFile, destFile);
    }


    public static boolean copySDFilesTo(String srcDirName, String destDirName) {
        File srcDir = new File(SDPATH + srcDirName);
        File destDir = new File(SDPATH + destDirName);
        return copyFilesTo(srcDir, destDir);
    }


    public static boolean moveSDFileTo(String srcFileName, String destFileName)
            throws IOException {
        File srcFile = new File(SDPATH + srcFileName);
        File destFile = new File(SDPATH + destFileName);
        return moveFileTo(srcFile, destFile);
    }


    public static boolean moveSDFilesTo(String srcDirName, String destDirName) {
        File srcDir = new File(SDPATH + srcDirName);
        File destDir = new File(SDPATH + destDirName);
        return moveFilesTo(srcDir, destDir);
    }


    public static OutputStream writeSDFile(String fileName) throws IOException {
        String filename2 = SDPATH + fileName;
        return writeFile(filename2);
    }


    public static OutputStream appendSDFile(String fileName) throws IOException {
        String filename2 = SDPATH + fileName;
        return appendFile(filename2);
    }

  
    public static InputStream readSDFile(String fileName) throws IOException {
        String filename2 = SDPATH + fileName;
        return readFile(filename2);
    }

  

    public static boolean createOrExistsPrivateFile(String fileName) {
        File file = new File(FILESPATH + fileName);
        return createOrExistsFile(file);
    }


    public static boolean createOrExistsPrivateFolder(String dirName) {
        File dir = new File(FILESPATH + dirName);
        return createOrExistsFolder(dir);
    }


    public static boolean delDataFile(String fileName) {
        File file = new File(FILESPATH + fileName);
        return delFile(file);
    }


    public static boolean delDataDir(String dirName) {
        File file = new File(FILESPATH + dirName);
        return delDir(file);
    }


    public static boolean copyDataFileTo(String srcFileName, String destFileName) {
        File srcFile = new File(FILESPATH + srcFileName);
        File destFile = new File(FILESPATH + destFileName);
        return copyFileTo(srcFile, destFile);
    }

 
    public static boolean copyDataFilesTo(String srcDirName, String destDirName) {
        File srcDir = new File(FILESPATH + srcDirName);
        File destDir = new File(FILESPATH + destDirName);
        return copyFilesTo(srcDir, destDir);
    }


    public static boolean moveDataFileTo(String srcFileName, String destFileName) {
        File srcFile = new File(FILESPATH + srcFileName);
        File destFile = new File(FILESPATH + destFileName);
        return moveFileTo(srcFile, destFile);
    }


    public static boolean moveDataFilesTo(String srcDirName, String destDirName) {
        File srcDir = new File(FILESPATH + srcDirName);
        File destDir = new File(FILESPATH + destDirName);
        return moveFilesTo(srcDir, destDir);
    }


    @SuppressLint("WorldWriteableFiles")
	@SuppressWarnings("deprecation")
	public static OutputStream wirtePrivateFile(String fileName) throws IOException {
        OutputStream os = mContext.openFileOutput(fileName,
                Context.MODE_WORLD_WRITEABLE);
        return os;
    }

 
    public static OutputStream appendPrivateFile(String fileName) throws IOException {
        OutputStream os = mContext.openFileOutput(fileName, Context.MODE_APPEND);
        return os;
    }

  
    public static InputStream readPrivateFile(String fileName) throws IOException {
        InputStream is = mContext.openFileInput(fileName);
        return is;
    }
}
