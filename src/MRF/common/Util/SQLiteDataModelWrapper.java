//
//  SQLiteDataModelWrapper.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package MRF.common.Util;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import android.database.Cursor;

public class SQLiteDataModelWrapper<T> {
	public SQLiteDataModelWrapper(Class<?> dataClass) {
		this.dataClass = dataClass;
	}

	private static final String[] RAW_TYPES = { "byte", "char", "short", "int",
			"long", "float", "double", "java.lang.Byte", "java.lang.Character",
			"java.lang.Short", "java.lang.Integer", "java.lang.Long",
			"java.lang.Float", "java.lang.Double", "java.lang.String",
			"java.util.Date" };
	private Class<?> dataClass;
	private final HashMap<String, Method> setters = new HashMap<String, Method>();

	public static boolean isRawType(Class<?> dataType) {
		String valueType = dataType.getName();
		if (valueType == null || valueType.equals("")) {
			return false;
		}
		//
		for (String s : RAW_TYPES) {
			if (s.equals(valueType)) {
				return true;
			}
		}
		//
		return false;
	}

	@SuppressWarnings("unchecked")
	public T newInstance() {
		try {
			return (T) this.dataClass.newInstance();
		} catch (Exception e) {
			return null;
		}
	}

	public void setProperty(Object t, Cursor cursor, int i) {
		try {
			this.setPropertyInternal(t, cursor, i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setPropertyInternal(Object t, Cursor cursor, int i)
			throws Exception {
		Method setter = null;
		String colName = cursor.getColumnName(i).replaceAll("_", "");
		String methodKey = "set" + colName.toLowerCase();
		String methodKeyEx = "setProperty." + colName.toLowerCase();
		if (this.setters.containsKey(methodKey)) {
			setter = this.setters.get(methodKey);
			if (setter == null) {
				return;
			}
			Object value = getValue(cursor, i, setter.getParameterTypes()[0]);
			setter.invoke(t, value);
		} else if (this.setters.containsKey(methodKeyEx)) {
			setter = this.setters.get(methodKeyEx);
			if (setter == null) {
				return;
			}
			Object value = getValue(cursor, i, String.class);
			setter.invoke(t, colName, value);
		} else {
			setter = this.getPropertySetter(this.dataClass, colName);
			if (setter != null) {
				this.setters.put(methodKey, setter);
				Object value = getValue(cursor, i,
						setter.getParameterTypes()[0]);
				setter.invoke(t, value);
			} else {
				setter = this.getPropertySetterEx(this.dataClass);
				if (setter != null) {
					this.setters.put(methodKeyEx, setter);
					Object value = getValue(cursor, i, String.class);
					setter.invoke(t, colName, value);
				} else {
					this.setters.put(methodKey, null);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public T getValue(Cursor cursor, int idx) {
		return (T) getValue(cursor, idx, this.dataClass);
	}

	public boolean isRawType() {
		return isRawType(this.dataClass);
	}

	public static Object getValue(Cursor cursor, int idx, Class<?> valueClass) {
		String valueType = valueClass.getName();
		if (valueType.equals("byte") || valueType.equals("java.lang.Byte")) {
			return cursor.getString(idx).charAt(0);
		} else if (valueType.equals("char")
				|| valueType.equals("java.lang.Character")) {
			return cursor.getString(idx).charAt(0);
		} else if (valueType.equals("short")
				|| valueType.equals("java.lang.Short")) {
			return cursor.getShort(idx);
		} else if (valueType.equals("int")
				|| valueType.equals("java.lang.Integer")) {
			return cursor.getInt(idx);
		} else if (valueType.equals("long")
				|| valueType.equals("java.lang.Long")) {
			return cursor.getLong(idx);
		} else if (valueType.equals("float")
				|| valueType.equals("java.lang.Float")) {
			return cursor.getFloat(idx);
		} else if (valueType.equals("double")
				|| valueType.equals("java.lang.Double")) {
			return cursor.getDouble(idx);
		} else if (valueType.equals("java.lang.String")) {
			return cursor.getString(idx);
		} else if (valueType.equals("java.util.Date")) {
			try {
				return DateFormat.parse(cursor.getString(idx));
			} catch (Exception e) {
				return null;
			}
		} else {
			return cursor.getString(idx);
		}
	}

	public static DateFormat DateFormat = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private Method getPropertySetter(Class<?> hostClass, String propertyName) {
		Method[] methods = hostClass.getMethods();
		for (Method method : methods) {
			if (!method.getName().equalsIgnoreCase("set" + propertyName)
					|| method.getParameterTypes().length != 1) {
				continue;
			}
			//
			return method;
		}
		return null;
	}

	private Method getPropertySetterEx(Class<?> hostClass) {
		try {
			return hostClass.getDeclaredMethod("setProperty", new Class<?>[] {
					String.class, String.class });
		} catch (SecurityException e) {
			return null;
		} catch (NoSuchMethodException e) {
			return null;
		}
	}
}
