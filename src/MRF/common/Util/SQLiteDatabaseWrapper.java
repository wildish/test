//
//  SQLiteDatabaseWrapper.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package MRF.common.Util;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public final class SQLiteDatabaseWrapper {
	private SQLiteDatabase db;

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		if (this.db != null) {
			this.db.close();
			this.db = null;
		}
	}

	public SQLiteDatabaseWrapper(SQLiteDatabase db) {
		this.db = db;
	}

	public int delete(String table, String whereClause) {
		return this.db.delete(table, whereClause, null);
	}

	public int delete(String table, String whereClause, String[] whereArgs) {
		return this.db.delete(table, whereClause, whereArgs);
	}

	public void execSQL(String sql) {
		this.db.execSQL(sql, new Object[]{});
	}

	public void execSQL(String sql, Object[] params) {
		this.db.execSQL(sql, params);
	}

	public long insert(String table, ContentValues values) {
		return this.db.insert(table, null, values);
	}

	public int update(String table, ContentValues values, String whereClause) {
		return this.db.update(table, values, whereClause, null);
	}

	public int update(String table, ContentValues values, String whereClause,
			String[] whereArgs) {
		return this.db.update(table, values, whereClause, whereArgs);
	}
	
	public boolean update(List<String> sqls) {
		boolean ret = true;
		this.db.beginTransaction();
		try {
			if(sqls != null && sqls.size() > 0) {
				for(int i=0; i<sqls.size(); i++) {
					this.execSQL(sqls.get(i));
				}
				this.db.setTransactionSuccessful();
			}
		} catch (Exception e) {
			ret = false;
			e.printStackTrace();
		} finally {
			this.db.endTransaction();
		}
		
		return ret;
	}

	public int count(String tableName) {
		String sql = "SELECT count(*) as count FROM " + tableName;
		return first(sql, Integer.class);
	}

	public <T> List<T> query(String sql, Class<T> fClass) {
		return query(sql, null, fClass);
	}

	public <T> List<T> query(String sql, String[] selectionArgs, Class<T> fClass) {
		Cursor cursor = this.db.rawQuery(sql, selectionArgs);
		List<T> results = new ArrayList<T>();
		cursorToList(cursor, results, fClass);
		cursor.close();
		return results;
	}

	private <T> void cursorToList(Cursor cursor, List<T> list, Class<T> fClass) {
		SQLiteDataModelWrapper<T> dataWrapper = new SQLiteDataModelWrapper<T>(
				fClass);
		if (dataWrapper.isRawType()) {
			while (cursor.moveToNext()) {
				list.add(dataWrapper.getValue(cursor, 0));
			}
		} else {
			while (cursor.moveToNext()) {
				T t = dataWrapper.newInstance();
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					dataWrapper.setProperty(t, cursor, i);
				}
				//
				list.add(t);
			}
		}
	}

	public <T> T first(String sql, Class<T> fClass) {
		return first(sql, null, fClass);
	}

	public <T> T first(String sql, String[] selectionArgs, Class<T> fClass) {
		List<T> list = query(sql, selectionArgs, fClass);
		if (list == null || list.size() == 0) {
			return null;
		} else {
			return list.get(0);
		}
	}
	
}