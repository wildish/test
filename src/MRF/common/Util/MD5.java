/*
 *  MD5.java
 *
 *  Created by admin on 4/8/15.
 *  Copyright (c) 2015 MRF. All rights reserved.
 */
package MRF.common.Util;

import java.security.MessageDigest;

import android.util.Log;

/**
 * 
 * @author Admin
 */
public class MD5 {
	public static String md5Hash(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(s.getBytes());
			byte[] digest = md.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < digest.length; i++) {
				int v = digest[i] & 0xFF;
				String hv = Integer.toHexString(v);
				if (hv.length() < 2) {
					sb.append(0);
				}
				sb.append(hv);
			}
			//
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("MD5.md5Hash", e.getClass().getName());
			return "";
		}
	}
}
