package MRF.common.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import DataBaseLib.ConnCfg;
import DataBaseLib.DBFactory;
import MRF.common.Data.sqlite.SpatialDataAccess;
import WebRequestHandle.Util.ConfigurationManager;
import WebRequestHandle.Util.Global;
import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;

import com.mrf.finland.core.GlobalApp;
import com.mrf.finland.util.GPSBluetoothManager;
import com.mrf.finland.util.Logger;
import com.mrf.finland.util.SyncManager;

public class CommonContext {
	// Server Address
	public static final String SERVER_COOKIE_NAME = "server_address";
	public static final String GLOBAL_COOKIE_NAME = "global";
	public static final String CONFIG_COOKIE_NAME = "config";
	public static final String CLIENT_COOKIE_NAME = "company_";
	public static final String CLIENT_USER_COOKIE_NAME = "user_";
	public static final String TASK_COOKIE_NAME = "task_%s_%s";
	public static final String TASK_ALL_NAME = "task_all";
	public static final String CLIENT_UPLOAD_COOKIE_NAME = "upload_%s_%s";
	public static final String CREATE_FEATURE_COOKIE_NAME = "create_features_%s";

	private static final String BASE_FORMAT_URL = "http://%s/FinlandWeb/Services/Download.svc";
	private static final String BASE_FILE_FORMAT_URL = "http://%s/FinlandWeb";
	private static String BASE_URL = null;
	private static String BASE_FILE_URL = null;
	public static final String SYNC_GLOBAL_SERVICE_NAME = "syncglobal"; // syncuser
	public static final String SYNC_CONFIG_SERVICE_NAME = "syncconfig";
	public static final String SYNC_DATA_SERVICE_NAME = "syncclient";
	public static final String SYNC_TASK_FILE_LIST_SERVICE_NAME = "requestfileinfo";
	public static final String SYNC_TASK_SERVICE_NAME = "synctaskbyid";
	public static final String SYNC_CLIENT_SERVICE_NAME = "syncclientserverdata";
	public static final String LOGON_CLIENT_SERVICE_NAME = "logon";
	public static final String ACCOUNT = "account";
	public static final String OPERATOR_ID = "account"; // login user id
	public static final String LOGON_NAME = "logonname";
	public static final String COMPANY_ID = "company_id";
	public static final String COMPANY_DIR = "dir";

	public static final String TASKIDS = "taskids";

	// global database name
	public static final String GLOBAL_DATABASE_NAME = "global";
	// client database name
	public static final String CLIENT_DATABASE_NAME = "client";
	// save the sync config temp file
	private static final String GLOBAL_TEMP_FOLDER = "globaltemp";
	// save the sync config file
	private static final String CONFIG_FILE_FOLDER = "config";
	// File Root Dir
	public static String ROOT_DIR = Environment.getExternalStorageDirectory()
			.getPath() + File.separator + "finland";

	public static final String ADMINISTRATOR_ACCOUNT = "admin";
	public static final String ADMINISTRATOR_PASSWORD = "de279b40e90f11d04dcf8617bb87324f";

	// sync data setting
	// initialize sync date
	public static final String DEFAULT_DATE = "1970-01-01 01:01:01.000";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String MODIFY_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	// initialize sync file parameter
	public static final String CONFIG_FILE_INIT_PARAMETER = String.format(
			"%s|0", new Object[] { "19700101010101000" });
	// sync config list
	public static final String[] CONFIG_FILE_ARRAY = new String[] { "app.xml",
			"config.xml", "configservice.xml", "Identify.xml", "LineStyle.xml",
			"Patterns.xml", "Permissions.xml", "Search.xml", "Symbols.xml" };
	public static final String COMPANY_TABLE = "{\"modify_time\":\"%s\", \"account\":\"%s\"}";

	public static final String CONFIG_SYMBOLS_NAME = "Symbols.xml";
	public static final String CONFIG_CONFIG_NAME = "config.xml";
	// public static final String CONFIG_PATTERNS_NAME = "Patterns.xml";
	public static final String CONFIG_LINESTYLE_NAME = "lineStyle.style";
	// public static final String CONFIG_GLYPHS_NAME = "Glyphs.xml";
	// public static final String CONFIG_MORINVILLE_NAME = "Morinville.map";

	public static final String[] FILTER_FIELDS = new String[] { "id", "uid",
			"gid", "d_type", "status", "create_user", "modify_time",
			"create_time", "dir", "company_id" };

	// task info
	public static final String LAST_DOWNLOADING_MAP = "last_downloading_map_";
	public static final String LAST_DOWNLOADING_DOTS = "last_downloading_dots";
	public static final String LAST_UPLOADING_DOTS = "last_uploading_dots";
	public static final String LAST_TIME_DEFAULT_FORMAT = "MM/dd/yyyy HH:mm:ss";

	// Logger
	public static Logger logger;
	// Thread Pool
	public static ExecutorService threadPool;
	// Task Manager
	// public static TaskManager taskManager;
	// Synchronous Manager
	public static SyncManager syncManager;
	// GPS Bluetooth Manager
	public static GPSBluetoothManager gpsBluetoothManager;

	public static final int PAGE_SIZE = 10;

	public static boolean appOnBackground = false;
	// It has been created
	public static boolean created = false;
	// public static JsonSQLiteDatabase database;
	// Global Database
	// public static JsonSQLiteDatabase globalDatabase;

	public static Map<String, Document> elements = new HashMap<String, Document>();

	private static Application app;

	public static SharedPreferences getCookie(String cookieName) {
		return getCookie(app.getApplicationContext(), cookieName);
	}

	public static SharedPreferences getCookie(Context context) {
		return getCookie(context, GLOBAL_COOKIE_NAME);
	}

	public static SharedPreferences getCookie(Context context, String cookieName) {
		return context.getSharedPreferences(cookieName, 0);
	}

	public static String getBaseUrl(Context context) {
		String server = getCookie(context, SERVER_COOKIE_NAME).getString(
				"server", null);
		if (server == null || server.length() == 0) {
			server = "184.70.195.134";
			getCookie(context, SERVER_COOKIE_NAME).edit()
					.putString("server", server).commit();
		}
		if (server != null) {
			BASE_URL = String.format(BASE_FORMAT_URL, server);
		}
		return BASE_URL;
	}

	public static String getBaseUrl(String server) {
		if (server != null) {
			BASE_URL = String.format(BASE_FORMAT_URL, server);
		}
		return BASE_URL;
	}

	public static String getBaseFileUrl(Context context) {
		String server = getCookie(context, SERVER_COOKIE_NAME).getString(
				"server", null);
		if (server != null) {
			BASE_FILE_URL = String.format(BASE_FILE_FORMAT_URL, server);
		}
		return BASE_FILE_URL;
	}

	public static void onCreate(Application app, int globalResId) {
		if (CommonContext.created) {
			return;
		}
		CommonContext.created = true;

		CommonContext.app = app;

		ROOT_DIR = Environment.getExternalStorageDirectory().getPath()
				+ File.separator + app.getApplicationContext().getPackageName();

		FileUtil.createOrExistsFolder(ROOT_DIR);

		createGlobalDatabase(app.getApplicationContext(), globalResId);

		CoordinateUtil.initEpsgs(GlobalApp.getRootPath() + File.separator
				+ "Config/epsg");

		threadPool = Executors.newFixedThreadPool(5);

		logger = new Logger(app.getApplicationContext());

		// taskManager = new TaskManager(app.getApplicationContext());

		syncManager = new SyncManager(app.getApplicationContext());

		gpsBluetoothManager = new GPSBluetoothManager(
				app.getApplicationContext());
	}

	// public static InputStream getInputStreamForConfigName(Context context,
	// String configName) {
	// InputStream is;
	// String configPath = GlobalApp.getRootPath() + File.separator +
	// configName;
	// try {
	// is = new FileInputStream(configPath);
	// } catch (FileNotFoundException e) {
	// is = null;
	// }
	// return is;
	// }

	public static Document getDocument(InputStream is) {
		SAXReader reader = new SAXReader();
		Document doc = null;
		try {
			doc = reader.read(is);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return doc;
	}

	public static Document getDocument(String fileName) {
		return getDocumentByPath(getConfigFolder() + File.separator + fileName);
	}

	public static Document getDocumentByPath(String path) {
		if (MRFUtil.sharedInstance().containsInCachedParameter(path)) {
			return (Document) MRFUtil.sharedInstance().getCachedParameter(path);
		} else {
			SAXReader reader = new SAXReader();
			Document doc = null;
			try {
				doc = reader.read(new File(path));
				MRFUtil.sharedInstance().putCachedParameter(path, doc);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			return doc;
		}
	}

	private static final String format = "MM/dd/yyyy";

	public static String getDateString(long times) {
		return getDateString(times, format);
	}

	@SuppressLint("SimpleDateFormat")
	public static String getDateString(long times, String format) {
		Date d = new Date(times);
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(d);
	}

	public static Method getMethod(Class<?> clazz, String methodName,
			final Class<?>[] classes) {
		Method method = null;
		try {
			method = clazz.getDeclaredMethod(methodName, classes);
		} catch (NoSuchMethodException e) {
			try {
				method = clazz.getMethod(methodName, classes);
			} catch (NoSuchMethodException ex) {
				if (clazz.getSuperclass() == null) {
					return method;
				} else {
					method = getMethod(clazz.getSuperclass(), methodName,
							classes);
				}
			}
		}
		return method;
	}

	public static byte[] getBytesByInputStream(InputStream inputStream) {
		if (inputStream == null) {
			return null;
		}
		try {
			int count = inputStream.available();
			byte[] bytes = new byte[count];
			int readCount = 0;
			while (readCount < count) {
				readCount += inputStream.read(bytes, readCount, count
						- readCount);
			}
			return bytes;
		} catch (IOException e) {
			return null;
		}
	}

	public static String getStringByInputStream(InputStream inputStream) {
		byte[] bytes = getBytesByInputStream(inputStream);
		if (bytes != null) {
			return new String(bytes);
		}
		return null;
	}

	public static long getUTCTime() {
		return new Date().getTime();
	}

	public static void onBackground() {
		appOnBackground = true;
		if (syncManager.isStartSynData) {
			syncManager.pause();
			syncManager.cancelSyncTaskServer();
		}
	}

	public static void onForeground() {
		appOnBackground = false;
		if (syncManager.isStartSynData) {
			syncManager.startSyncData();
			syncManager.startSyncTaskServer();
		}
	}

	public static boolean isNetworkConnnected(Context context) {
		WifiManager wifiManager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		if (null == wifiManager) {
			return false;
		}

		int status = wifiManager.getWifiState();
		if (status == WifiManager.WIFI_STATE_ENABLED) {
			WifiInfo info = wifiManager.getConnectionInfo();
			if (info != null) {
				return true;
			}
		}

		return false;
	}

	public static boolean isConnnected(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (null == connectivityManager) {
			return false;
		}

		NetworkInfo networkInfo[] = connectivityManager.getAllNetworkInfo();

		if (null == networkInfo) {
			return false;
		}

		for (NetworkInfo info : networkInfo) {
			if (info.getState() == NetworkInfo.State.CONNECTED) {
				return true;
			}
		}

		return false;
	}

	public static String getFileInputStream(String path) {
		File file = new File(path);
		if (!file.exists()) {
			return "";
		}
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			byte[] buf = new byte[1024];
			StringBuffer sb = new StringBuffer();
			while ((fis.read(buf)) != -1) {
				sb.append(new String(buf));
				buf = new byte[1024];
			}
			return sb.toString();
		} catch (IOException e) {
			return "";
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				return "";
			}
		}
	}

	public static boolean createClientDatabase(Context context,
			String databaseFilePath, int clientResId) {
		String dbPath = String.format("%s%s%s", ROOT_DIR, File.separator,
				databaseFilePath);
		FileUtil.createOrExistsFolder(dbPath);
		String companyPath = String.format("%s/client.sqlite", dbPath);
		File company = new File(companyPath);
		if (!company.exists()) {
			try {
				company.createNewFile();
			} catch (Exception e) {
				company.delete();
				return false;
			}
			// database = new JsonSQLiteDatabase(SQLiteDatabase.openDatabase(
			// company.getAbsolutePath(), null,
			// SQLiteDatabase.OPEN_READWRITE));
			String sqlDatabase = getStringByInputStream(context.getResources()
					.openRawResource(clientResId));
			DBFactory.putConnConfig(CLIENT_DATABASE_NAME, new ConnCfg(
					CLIENT_DATABASE_NAME, company.getAbsolutePath()));
			SpatialDataAccess.getInstance().loadSQLsFromString(
					CLIENT_DATABASE_NAME, sqlDatabase);
		} else {
			// database = new JsonSQLiteDatabase(SQLiteDatabase.openDatabase(
			// company.getAbsolutePath(), null,
			// SQLiteDatabase.OPEN_READWRITE));
			DBFactory.putConnConfig(CLIENT_DATABASE_NAME, new ConnCfg(
					CLIENT_DATABASE_NAME, company.getAbsolutePath()));
		}
		return true;
	}

	/**
	 * Create Global Database
	 * 
	 * @param context
	 * @param globalResId
	 * @return boolean
	 */
	private static boolean createGlobalDatabase(Context context, int globalResId) {
		String rootDir = GlobalApp.getRootPath();
		FileUtil.createOrExistsFolder(rootDir);
		String globalDatabasePath = String.format("%s/global.sqlite", rootDir);
		File globle = new File(globalDatabasePath);
		DBFactory.setDefaultDataSource(CLIENT_DATABASE_NAME);
		if (!globle.exists()) {
			try {
				globle.createNewFile();
			} catch (Exception e) {
				globle.delete();
				return false;
			}
			// globalDatabase = new JsonSQLiteDatabase(
			// SQLiteDatabase.openDatabase(globle.getAbsolutePath(), null,
			// SQLiteDatabase.OPEN_READWRITE));
			String sqlDatabase = getStringByInputStream(context.getResources()
					.openRawResource(globalResId));
			DBFactory.putConnConfig(GLOBAL_DATABASE_NAME, new ConnCfg(
					GLOBAL_DATABASE_NAME, globle.getAbsolutePath()));
			SpatialDataAccess.getInstance().loadSQLsFromString(
					GLOBAL_DATABASE_NAME, sqlDatabase);
		} else {
			// globalDatabase = new JsonSQLiteDatabase(
			// SQLiteDatabase.openDatabase(globle.getAbsolutePath(), null,
			// SQLiteDatabase.OPEN_READWRITE));
			DBFactory.putConnConfig(GLOBAL_DATABASE_NAME, new ConnCfg(
					GLOBAL_DATABASE_NAME, globle.getAbsolutePath()));
		}
		return true;
	}

	public static void exit(Context context) {
		// taskManager.pause();
		syncManager.pause();
		getCookie(context).edit().clear().commit();
	}

	public static String getConfigFolder() {
		return String.format("%s%s%s", CommonContext.ROOT_DIR, File.separator,
				CommonContext.CONFIG_FILE_FOLDER);
	}

	public static String getGlobalTempFolder() {
		return String.format("%s%s%s", CommonContext.ROOT_DIR, File.separator,
				CommonContext.GLOBAL_TEMP_FOLDER);
	}

	public static void initDBFactory() {
		String appPath = getConfigFolder() + File.separator + "app.xml";
		ConfigurationManager.init(appPath);
		Global.DefaultDataSourceName = ConfigurationManager.AppSettings
				.get("DefaultDataSourceName");
		Global.TabletOS = ConfigurationManager.AppSettings.get("TabletOS");
		Global.EnableServerStorage = ConfigurationManager.AppSettings
				.get("EnableServerStorage");
		Global.GPSUploadURL = ConfigurationManager.AppSettings
				.get("GPSUploadURL");
		Global.PublicUserName = ConfigurationManager.AppSettings
				.get("PublicUserName");

		MRF.common.MapTemplate.Global.applicationStart(ROOT_DIR);
	}

	public static void unZip(Context context, String path, String uiFileName) {
		File root = new File(path);
		if (!root.exists()) {
			root.mkdirs();
		}

		// File FileName = new File(String.format("%s/%s.zip", rootPath,
		// uiFileName));
		// if (!FileName.exists()) {
		// return;
		// }

		unZipFolder(context, uiFileName, path);
	}

	private static void unZipFolder(Context context, String zipFileString,
			String outPathString) {
		try {
			ZipInputStream inZip = new ZipInputStream(context.getAssets().open(
					zipFileString + ".zip"));
			ZipEntry zipEntry;
			String szName = "";

			while ((zipEntry = inZip.getNextEntry()) != null) {
				szName = zipEntry.getName();
				if (zipEntry.isDirectory()) {
					// get the folder name of the widget
					szName = szName.substring(0, szName.length() - 1);
					java.io.File folder = new java.io.File(outPathString
							+ java.io.File.separator + szName);
					folder.mkdirs();
				} else {
					java.io.File file = new java.io.File(outPathString
							+ java.io.File.separator + szName);
					if (file.exists()
							&& zipEntry.getTime() == file.lastModified()) {
						continue;
					}
					if (!file.exists()) {
						file.createNewFile();
					} else if (zipEntry.getTime() != file.lastModified()) {
						file.delete();
						file.createNewFile();
					}
					// get the output stream of the file
					java.io.FileOutputStream out = new java.io.FileOutputStream(
							file);
					int len;
					byte[] buffer = new byte[1024];
					// read (len) bytes into buffer
					while ((len = inZip.read(buffer)) != -1) {
						// write (len) byte from buffer at the position 0
						out.write(buffer, 0, len);
						out.flush();
					}
					out.close();
				}
			}// end of while
			inZip.close();
		} catch (IOException e) {
		}
	}
}
