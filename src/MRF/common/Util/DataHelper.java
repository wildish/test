package MRF.common.Util;

import android.annotation.SuppressLint;

@SuppressLint("UseValueOf")
public class DataHelper {

	public static byte[] reverse(byte[] array) {
		byte temp;
		for (int i = 0; i < array.length / 2; i++) {
			temp = array[i];
			array[i] = array[array.length - 1 - i];
			array[array.length - 1 - i] = temp;
		}
		return array;
	}

	public static byte[] intToBytes(int value) {
		int temp = value;
		byte[] b = new byte[4];

		for (int i = 0; i < b.length; i++) {
			b[i] = new Integer(temp & 0xff).byteValue();
			temp = temp >> 8;
		}
		return b;
	}

	public static int bytesToInt(byte[] src, int offset) {
		int s0 = src[offset + 0] & 0xff;
		int s1 = (src[offset + 1] & 0xff) << (8 * 1);
		int s2 = (src[offset + 2] & 0xff) << (8 * 2);
		int s3 = (src[offset + 3] & 0xff) << (8 * 3);
		return s0 | s1 | s2 | s3;
	}

	public static byte[] longToBytes(int value) {
		long temp = value;
		byte[] b = new byte[8];
		for (int i = 0; i < b.length; i++) {
			b[i] = new Long(temp & 0xff).byteValue();
			temp = temp >> 8;
		}
		return b;
	}

	public static long bytesToLong(byte[] src, int offset) {
		long s0 = src[offset + 0] & 0xff;
		long s1 = (src[offset + 1] & 0xff) << (8 * 1);
		long s2 = (src[offset + 2] & 0xff) << (8 * 2);
		long s3 = (src[offset + 3] & 0xff) << (8 * 3);
		long s4 = (src[offset + 4] & 0xff) << (8 * 4);
		long s5 = (src[offset + 5] & 0xff) << (8 * 5);
		long s6 = (src[offset + 6] & 0xff) << (8 * 6);
		long s7 = (src[offset + 7] & 0xff) << (8 * 7);

		return s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7;
	}

	public static byte[] doubleToBytes(double d) {
		byte dst[] = new byte[8];
		long v = Double.doubleToLongBits(d);
		dst[0] = (byte) (v & 0xff);
		dst[1] = (byte) ((v >> 8) & 0xff);
		dst[2] = (byte) ((v >> 16) & 0xff);
		dst[3] = (byte) ((v >> 24) & 0xff);
		dst[4] = (byte) ((v >> 32) & 0xff);
		dst[5] = (byte) ((v >> 40) & 0xff);
		dst[6] = (byte) ((v >> 48) & 0xff);
		dst[7] = (byte) ((v >> 56) & 0xff);
		return dst;
	}

	public static double bytesToDouble(byte[] src, int offset) {
		return Double.longBitsToDouble(((long) src[offset + 0] << (8 * 7))
				+ ((long) (src[offset + 1] & 255) << (8 * 6))
				+ ((long) (src[offset + 2] & 255) << (8 * 5))
				+ ((long) (src[offset + 3] & 255) << (8 * 4))
				+ ((long) (src[offset + 4] & 255) << (8 * 3))
				+ ((src[offset + 5] & 255) << (8 * 2))
				+ ((src[offset + 6] & 255) << (8 * 1))
				+ ((src[offset + 7] & 255) << (8 * 0)));
	}

}
