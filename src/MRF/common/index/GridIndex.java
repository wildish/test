package MRF.common.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import MRF.common.geometry.Envelope;
import MRF.common.geometry.GeometryTypes;
import MRF.common.geometry.Line;
import MRF.common.geometry.LineSegment;
import MRF.common.geometry.Point;
import android.annotation.SuppressLint;

@SuppressLint("UseSparseArrays")
public class GridIndex {
	double cellWidth = 100;
	double originX = -30000000;
	double originY = -30000000;
	long rowNum = 600000;
	GeometryTypes type = GeometryTypes.WKBPoint;
	private HashMap<Long, GridIndexNode> gridIndexDic = new HashMap<Long, GridIndexNode>();

	public void addElement(int id, MRF.common.geometry.Geometry geo) {
		if (type == GeometryTypes.WKBPoint) {
			long gridIndex = ((long) ((geo.getEnv().minX - originX) / cellWidth))
					* rowNum
					+ ((long) ((geo.getEnv().minY - originY) / cellWidth));
			if (!gridIndexDic.containsKey(gridIndex)) {
				GridIndexNode node = new GridIndexNode(gridIndex, id, geo);
				gridIndexDic.put(gridIndex, node);
			} else {
				GridIndexNode node = gridIndexDic.get(gridIndex);
				node.addElement(id, geo);
				gridIndexDic.put(gridIndex, node);
			}
		} else if (type == GeometryTypes.WKBLineString) {
			List<double[]> pts = ((Line) geo).getListPoints();
			long colIndex = (long) ((pts.get(0)[0] - originX) / cellWidth);
			long rowIndex = (long) ((pts.get(0)[1] - originY) / cellWidth);
			// long gridIndex = colIndex * rowNum + rowIndex;
			LineSegment seg = new LineSegment();

			// Point startPoint = new Point(originX + colIndex * cellWidth,
			// originY + rowIndex * cellWidth);
			double[] startPoint = new double[] {
					originX + colIndex * cellWidth,
					originY + rowIndex * cellWidth };
			LineSegment lastSeg = null;
			// int sideIndex = -1;
			for (int i = 0; i < pts.size() - 1; i++) {
				seg.setP0(pts.get(i));
				seg.setP1(pts.get(i + 1));
				List<LineSegment> segs = getGridLineSegment(startPoint, lastSeg);
				// sideIndex = -1;
				for (int j = 0; j < segs.size(); j++) {
					double[] temp = segs.get(i).lineIntersection(seg);
					if (temp != null) {
						if (i == 0) {
							startPoint[0] -= cellWidth;
						} else if (i == 1) {
							startPoint[0] += cellWidth;
						} else if (i == 2) {
							startPoint[1] -= cellWidth;
						} else if (i == 3) {
							startPoint[1] += cellWidth;
						}
						// if (i == 0) {
						// startPoint.X -= cellWidth;
						// } else if (i == 1) {
						// startPoint.X += cellWidth;
						// } else if (i == 2) {
						// startPoint.Y -= cellWidth;
						// } else if (i == 3) {
						// startPoint.Y += cellWidth;
						// }
						// sideIndex = i;
						break;
					}
				}
			}
		}
	}

	private List<LineSegment> getGridLineSegment(double[] startPoint,
			LineSegment seg) {
		LineSegment leftSeg = new LineSegment();
		LineSegment rightSeg = new LineSegment();
		LineSegment bottomSeg = new LineSegment();
		LineSegment topSeg = new LineSegment();
		leftSeg.setP0(startPoint);
		leftSeg.setP1(new double[] { startPoint[0], startPoint[1] + cellWidth });
		rightSeg.setP0(new double[] { startPoint[0] + cellWidth, startPoint[1] });
		rightSeg.setP1(new double[] { startPoint[0] + cellWidth,
				startPoint[1] + cellWidth });
		bottomSeg.setP0(startPoint);
		bottomSeg
				.setP1(new double[] { startPoint[0] + cellWidth, startPoint[1] });
		topSeg.setP0(new double[] { startPoint[0], startPoint[1] + cellWidth });
		topSeg.setP1(new double[] { startPoint[0] + cellWidth,
				startPoint[1] + cellWidth });
		List<LineSegment> segs = new ArrayList<LineSegment>(4);
		if (leftSeg.equals(seg)) {
			leftSeg.setP0(new double[] { -300000, -300000 });
			leftSeg.setP1(new double[] { -300001, -300001 });
		}
		if (rightSeg.equals(seg)) {
			rightSeg.setP0(new double[] { -300000, -300000 });
			rightSeg.setP1(new double[] { -300001, -300001 });
		}
		if (bottomSeg.equals(seg)) {
			bottomSeg.setP0(new double[] { -300000, -300000 });
			bottomSeg.setP1(new double[] { -300001, -300001 });
		}
		if (topSeg.equals(seg)) {
			topSeg.setP0(new double[] { -300000, -300000 });
			topSeg.setP1(new double[] { -300001, -300001 });
		}
		segs.add(leftSeg);
		segs.add(rightSeg);
		segs.add(bottomSeg);
		segs.add(topSeg);
		return segs;
		// LineSegment leftSeg = new LineSegment();
		// LineSegment rightSeg = new LineSegment();
		// LineSegment bottomSeg = new LineSegment();
		// LineSegment topSeg = new LineSegment();
		// leftSeg.setP0(new Point(startPoint));
		// leftSeg.setP1(new Point(startPoint.X, startPoint.Y + cellWidth));
		// rightSeg.setP0(new Point(startPoint.X + cellWidth, startPoint.Y));
		// rightSeg.setP1(new Point(startPoint.X + cellWidth, startPoint.Y
		// + cellWidth));
		// bottomSeg.setP0(new Point(startPoint));
		// bottomSeg.setP1(new Point(startPoint.X + cellWidth, startPoint.Y));
		// topSeg.setP0(new Point(startPoint.X, startPoint.Y + cellWidth));
		// topSeg.setP1(new Point(startPoint.X + cellWidth, startPoint.Y
		// + cellWidth));
		// List<LineSegment> segs = new ArrayList<LineSegment>(4);
		// if (leftSeg.equals(seg)) {
		// leftSeg.setP0(new Point(-300000, -300000));
		// leftSeg.setP1(new Point(-300001, -300001));
		// }
		// if (rightSeg.equals(seg)) {
		// rightSeg.setP0(new Point(-300000, -300000));
		// rightSeg.setP1(new Point(-300001, -300001));
		// }
		// if (bottomSeg.equals(seg)) {
		// bottomSeg.setP0(new Point(-300000, -300000));
		// bottomSeg.setP1(new Point(-300001, -300001));
		// }
		// if (topSeg.equals(seg)) {
		// topSeg.setP0(new Point(-300000, -300000));
		// topSeg.setP1(new Point(-300001, -300001));
		// }
		// segs.add(leftSeg);
		// segs.add(rightSeg);
		// segs.add(bottomSeg);
		// segs.add(topSeg);
		// return segs;
	}

	public List<Integer> queryByEnvelope(Envelope getEnv) {
		ArrayList<Integer> resultSet = new ArrayList<Integer>();
		long left = (long) ((getEnv.minX - originX) / cellWidth);
		long right = (long) ((getEnv.maxX - originX) / cellWidth) + 1;
		long bottom = (long) ((getEnv.minY - originY) / cellWidth);
		long top = (long) ((getEnv.maxY - originY) / cellWidth + 1);
		Long gridIndex = 0L;
		for (long i = left; i <= right; i++) {
			for (long j = bottom; j <= top; j++) {
				gridIndex = i * rowNum + j;
				if (gridIndexDic.containsKey(gridIndex)) {
					for (int id : gridIndexDic.get(gridIndex).getElementList()) {
						if (!resultSet.contains(id)) {
							resultSet.add(id);
						}
					}
				}
			}
		}
		return resultSet;
	}

	private boolean checkGridIndexExist(long index, HashSet<Long> resultSet) {
		if (gridIndexDic.containsKey(index)) {
			resultSet.add(index);
			return true;
		}
		return false;
	}

	public int getNearstElementId(Point pt) {
		long col = (long) ((pt.getEnv().minX - originX) / cellWidth);
		long row = (long) ((pt.getEnv().minY - originY) / cellWidth);
		HashSet<Long> resultSet = new HashSet<Long>();
		int step = 0;
		long gridIndex = -1;
		gridIndex = col * rowNum + row;
		boolean isSuccess = checkGridIndexExist(gridIndex, resultSet);
		if (isSuccess) {
			// check neighborhood
			step = 1;
			for (long i = col - step; i <= col + step; i++) {
				gridIndex = i * rowNum + row + 1;
				checkGridIndexExist(gridIndex, resultSet);
				gridIndex = i * rowNum + row - 1;
				checkGridIndexExist(gridIndex, resultSet);
			}
			gridIndex = (col - 1) * rowNum + row;
			checkGridIndexExist(gridIndex, resultSet);
			gridIndex = (col + 1) * rowNum + row;
			checkGridIndexExist(gridIndex, resultSet);
		} else {
			for (Long key : gridIndexDic.keySet())
				resultSet.add(key);
		}
		List<Double> lens = new ArrayList<Double>();
		List<Integer> ids = new ArrayList<Integer>();
		for (Long index : resultSet) {
			GridIndexNode node = gridIndexDic.get(index);
			if (node.getElementCount() > 0) {
				for (int i = 0; i < node.getElementCount(); i++) {
					if (type == GeometryTypes.WKBPoint) {
						Tuple<Integer, MRF.common.geometry.Geometry> tu = node
								.getElement(i);
						ids.add(tu.getT1());
						Point pt2 = (Point) tu.getT2();
						lens.add(Math.sqrt(Math.pow((pt2.X - pt.X), 2)
								+ Math.pow((pt2.Y - pt.Y), 2)));
					}
				}
			}
		}
		double minLen = lens.get(0);
		int posIndex = 0;
		for (int i = 1; i < lens.size(); i++) {
			if (lens.get(i) < minLen) {
				posIndex = i;
				minLen = lens.get(i);
			}
		}
		return ids.get(posIndex);
	}

	public GridIndex(double cellWidth, GeometryTypes type) {
		if (cellWidth > 0) {
			this.cellWidth = cellWidth;
		}
		this.type = type;
		rowNum = (int) (Math.abs(originX * 2) / this.cellWidth) + 1;
	}
}
