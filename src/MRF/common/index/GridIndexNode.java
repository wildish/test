package MRF.common.index;

import java.util.ArrayList;
import java.util.List;

import MRF.common.geometry.Geometry;
import android.annotation.SuppressLint;

public class GridIndexNode {
	long gridId;
	List<Integer> elementIdList;
	List<Geometry> elementList;

	public GridIndexNode(long gridid, int elementId, Geometry geo) {
		this.gridId = gridid;
		this.elementIdList = new ArrayList<Integer>();
		this.elementList = new ArrayList<Geometry>();
		addElement(elementId, geo);
	}

	public GridIndexNode(long gridid) {
		this.gridId = gridid;
		this.elementList = new ArrayList<Geometry>();
		this.elementIdList = new ArrayList<Integer>();
	}

	public void addElement(int elementId, Geometry geo) {
		if (!this.elementIdList.contains(elementId)) {
			this.elementIdList.add(elementId);
			this.elementList.add(geo);
		}
	}

	public void removeElement(Integer elementId) {
		this.elementIdList.remove(elementId);
	}

	public void removeElementByIndex(int index) {
		if (index < this.elementIdList.size() && index > -1) {
			this.elementList.remove(index);
			this.elementIdList.remove(index);
		}
	}

	@SuppressLint("UseSparseArrays")
	public Tuple<Integer, Geometry> getElement(int index) {
		Tuple<Integer, Geometry> value = null;
		if (index < this.elementIdList.size() && index > -1) {
			value = new Tuple<Integer, Geometry>(this.elementIdList.get(index),
					this.elementList.get(index));
		}
		return value;
	}

	public List<Integer> getElementList() {
		return this.elementIdList;
	}

	public int getElementCount() {
		return this.elementIdList.size();
	}
}
