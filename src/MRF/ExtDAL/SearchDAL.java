package MRF.ExtDAL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.dom4j.Element;
import org.json.JSONArray;
import org.json.JSONObject;

import DataBaseLib.DBFactory;
import DataBaseLib.DBProvider;
import MRF.ExtDAL.DataModel.QueryCondition;
import MRF.ExtDAL.Util.SpatialSearchHelper;
import MRF.common.Util.StringUtil;
import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class SearchDAL extends SearchBase implements ISearch {
	public String ConfigPath;

	public SearchDAL(String configPath) {
		this.ConfigPath = configPath;
	}

	public String configPath() {
		return super.configPath;
	}

	@SuppressWarnings("unchecked")
	public String parcelSearchCondition(String nodeName, String cols,
			Object values, String buffer) {
		String where = "";
		String sql = "";
		try {
			Element node = getConfig(String.format(
					"//ParcelSearch/%s", nodeName));
			Element fkNode = node.element("FKQuery");
			Element nodeQuery = node.element("UnionQuery").element(
					"BBOXQuery");
			if (values instanceof List) {
				sql = getSQLWithValues((List<String>) values,
						getPlainText(fkNode.getText()));
			} else if (values instanceof List) {
				sql = String.format(getPlainText(fkNode.getText()),
						getWhereStr((List<QueryCondition>) values));
			} else if (values instanceof String) {
				sql = String.format(getPlainText(fkNode.getText()),
						(String) values);
			}
			if (StringUtil.isBlank(buffer)) {
				where = String.format(
						getPlainText(nodeQuery.elementText("NoBuffer")), sql)
						.trim();
			} else {
				where = String.format(
						getPlainText(nodeQuery.elementText("HasBuffer")), sql,
						buffer).trim();
			}
			if (where.toLowerCase().startsWith("where")) {
				where = where.substring(6);
			}
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
		}
		return where;
	}

	@Override
	public JSONArray parcelSearch(String nodeName, String cols, Object values,
			String buffer, String pagesize, String pageindex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String queryOwnerInfor(String FK) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String queryLevyInfor(String FK) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String queryTaxInfor(String FK) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String queryAssessment(String FK) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String queryPropertyInfor(String FK, boolean isPublicUser) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getOwnerNames(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getStreetNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getZoneTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getOCPs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getQuaters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getTownships() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getRanges() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray getMeridians() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray queryFeatureSelection(String pointsStr, String fieldList,
			String drawType, String searchType, String buffer) {
		// TODO Auto-generated method stub
		return null;
	}

	public JSONArray queryParcleBBOX(String nodeName, String cols,
			Object values, String buffer) {
		JSONArray dt = new JSONArray();
		try {
			Element node = getConfig(String.format(
					"//ParcelSearch/%s", nodeName));
			List<String> fks = getQueryForeignKey(node, values);
			if (fks.size() == 0)
				return dt;
			dt = getBBOXWithFK(node, cols, fks, buffer);
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
		}
		return dt;
	}

	private JSONArray getBBOXWithFK(Element node, String cols,
			List<String> fks, String buffer) {
		try {
			// String fk = node.getAttributes().getNamedItem("ForeignKey")
			// .getTextContent();
			JSONArray dt = this.queryWithBuffer(node, "UnionQuery/BBOXQuery",
					fks, cols, buffer);
			return dt;
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
		}
		return null;
	}

	private JSONArray queryWithBuffer(Element node, String xpath,
			List<String> fks, String cols, String buffer) {
		JSONArray dt = new JSONArray();
		try {
			if (fks.size() == 0)
				return dt;
			Element nodeQuery = node.element(xpath);
			Collection<String> fks_max = maxForeignKeys(node, fks);
			// IDBOperator dbOperator =
			// base.GetDBOperator(nodeQuery.Attributes["DBType"].Value,
			// nodeQuery.Attributes["connector"].Value);
			if (StringUtil.isBlank(buffer)) {
				String sql = getPlainText(nodeQuery.elementText("Query"))
						.replace("$COLS$", cols);
				String temp = "";
				for (String fk_ : fks_max) {
					temp += String.format(" '%s',", fk_);
				}
				if (temp.endsWith(",")) {
					temp = temp.substring(0, temp.trim().length());
				}
				sql += " "
						+ String.format(
								getPlainText(nodeQuery.elementText("NoBuffer")),
								temp);
				DBProvider database = DBFactory.getDBProvider();
				dt = database.getTable(sql);
			} else {
				String temp = "";
				for (String fk_ : fks_max) {
					temp += String.format(" '%s',", fk_);
				}
				if (temp.endsWith(",")) {
					temp = temp.substring(0, temp.trim().length());
				}

				String sql = getPlainText(nodeQuery.elementText("Query"))
						.replace("$COLS$", cols);
				sql += " "
						+ String.format(
								getPlainText(nodeQuery.elementText("HasBuffer")),
								temp, buffer);
				// JSONArray dtTemp = daaccess.GetDataTable(sql);
				DBProvider database = DBFactory.getDBProvider();
				JSONArray dtTemp = database.getTable(sql);
				dt = dtTemp;
			}
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
			ex.printStackTrace();
		}
		return dt;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<String> getQueryForeignKey(Element node,
			Object values) {
		List<String> list = new ArrayList<String>();
		String sql = "";
		try {
			Element fkNode = node.element("FKQuery");
			// Skip query foreign keys
			if (fkNode == null) {
				if (values instanceof List) {
					if (((List) values).get(0) instanceof String)
						return (List<String>) values;
				} else if (values instanceof List) {
					if (((List) values).get(0) instanceof QueryCondition) {
						String fk = node.elementText("ForeignKey");
						for (QueryCondition condition : (List<QueryCondition>) values) {
							if (condition.Field.equalsIgnoreCase(fk)) {
								list.add(condition.Value);
							}
						}
					}
				} else if (values instanceof String) {
					list.add((String) values);
				}
				return list;
			}
			if (values instanceof List) {
				if (((List) values).get(0) instanceof String) {
					sql = getSQLWithValues((List<String>) values,
							getPlainText(fkNode.getText()));
				}
			} else if (values instanceof List) {
				if (((List) values).get(0) instanceof QueryCondition) {
					sql = String.format(getPlainText(fkNode.getText()),
							getWhereStr((List<QueryCondition>) values));
				}
			} else if (values instanceof String) {
				sql = String.format(getPlainText(fkNode.getText()),
						(String) values);
			}
			// IDBOperator dbOperator =
			// base.GetDBOperator(fkNode.Attributes["DBType"].Value,
			// fkNode.Attributes["connector"].Value);
			DBProvider database = DBFactory.getDBProvider();
			JSONArray dt = database.getTable(sql);
			for (int i = 0; i < dt.length(); i++) {

				JSONObject dr = dt.getJSONObject(i);
				if (!list.contains(dr.toString()))
					list.add(dr.toString());
			}
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
			// Log.GetInstance().Write(LogLevel.Error, "SQL: " + sql);
		}
		return list;
	}

	public JSONArray featureAttributeQuery(String sql) {
		JSONArray dt = new JSONArray();
		try {
			// Node nodeQuery = getConfig("//FeatureQuery");
			// IDBOperator dbOperator =
			// base.GetDBOperator(nodeQuery.Attributes["DBType"].Value,
			// nodeQuery.Attributes["connector"].Value);
			DBProvider database = DBFactory.getDBProvider();
			dt = database.getTable(sql);
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
		}
		return dt;
	}

	public JSONArray queryFeatureSelectionPage(String pointsStr,
			String fieldList, String drawType, String searchType,
			String buffer, String pagesize, String pageindex) {
		JSONArray dt = new JSONArray();
		try {
			Element node = super.getConfig("//SearchLayer/"
					+ searchType);
			String sql = "";
			Element attrNode = node.element("AttrQuery");
			sql = SpatialSearchHelper.getQuerySQL(node
					.attributeValue("geomCol"),
					attrNode.getTextTrim().replace("\r\n", " "), pointsStr,
					drawType, buffer, pagesize, pageindex);
			sql = sql.replace("$COLS$", fieldList);
			DBProvider database = DBFactory.getDBProvider();
			dt = database.getTable(sql);
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
			ex.printStackTrace();
		}
		return dt;
	}

	public JSONArray queryFeatureSelectionBBOX(String pointsStr,
			String fieldList, String drawType, String searchType, String buffer) {
		JSONArray dt = new JSONArray();
		try {
			Element node = super.getConfig("//SearchLayer/"
					+ searchType);
			String sql = "";
			Element fkNode = node.element("BBOXQuery");
			sql = SpatialSearchHelper.getQuerySQL(node
					.attributeValue("geomCol"),
					fkNode.getTextTrim().replace("\r\n", " "), pointsStr,
					drawType, buffer);
			sql = sql.replace("$COLS$", fieldList);
			DBProvider database = DBFactory.getDBProvider();
			dt = database.getTable(sql);
		} catch (Exception ex) {
		}
		return dt;
	}
}
