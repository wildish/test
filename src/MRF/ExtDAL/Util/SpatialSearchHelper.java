package MRF.ExtDAL.Util;

import java.util.ArrayList;
import java.util.List;

import MRF.common.Util.StringUtil;

public class SpatialSearchHelper {
	static class PointD {
		private double X;
		private double Y;

		public PointD(double x, double y) {
			this.X = x;
			this.Y = y;
		}
	}

	public static String getQuerySQL(String geomCol, String sqlFMT, String pointsStr, String drawType, String buffer) {
		String where = getWhereCondition(drawType, geomCol, pointsStr, buffer);
		if (StringUtil.isBlank(where))
			return "";
		return String.format(sqlFMT.replace("\r\n", " "), where);
	}

	public static String getQuerySQL(String geomCol, String sqlFMT, String pointsStr, String drawType, String buffer, String pagesize, String pageindex) {
		String where = getWhereCondition(drawType, geomCol, pointsStr, buffer);
		if (StringUtil.isBlank(where))
			return "";
		return String.format(sqlFMT.replace("\r\n", " "), where, pagesize, (Integer.parseInt(pageindex) - 1) * Integer.parseInt(pagesize));
	}

	public static String getWhereCondition(String drawType, String geomCol, String pointsStr, String buffer) {
		List<PointD> points = new ArrayList<PointD>();
		String[] ListPoints = pointsStr.split(";");
		for (int i = 0; i < ListPoints.length; i++) {
			double x = Double.parseDouble(ListPoints[i].split("$")[0]);
			double y = Double.parseDouble(ListPoints[i].split("$")[1]);
			PointD point = new PointD(x, y);
			points.add(point);
		}
		return getWhereCondition(points, drawType, geomCol, buffer);
	}

	private static String getWhereCondition(List<PointD> points, String drawType, String geomCol, String bufferStr) {
		String geomStr = "";
		if (points.size() == 0)
			return "";
		if ("POINT".equalsIgnoreCase(drawType)) {
			double radius = Math.min(points.get(1).X * points.get(1).Y * 20, 2);
			PointD pointCenter = points.get(0);
			geomStr = String.format("POINT(%f %f)=%f", pointCenter.X, pointCenter.Y, radius);
		} else if ("RECTANGLE".equalsIgnoreCase(drawType)) {
			if (points.size() >= 2) {
				PointD pt0, pt1, pt2, pt3, pt4;
				pt0 = points.get(0);
				pt2 = points.get(1);
				pt1 = new PointD(pt2.X, pt0.Y);
				pt3 = new PointD(pt0.X, pt2.Y);
				pt4 = new PointD(pt0.X, pt0.Y);
				geomStr = String.format("POLYGON((%f %f,%f %f,%f %f,%f %f,%f %f))", pt0.X, pt0.Y, pt1.X, pt1.Y, pt2.X, pt2.Y, pt3.X, pt3.Y, pt4.X, pt4.Y);
			}
		} else if ("CIRCLE".equalsIgnoreCase(drawType)) {
			if (points.size() >= 2) {
				PointD ptCenter = points.get(0);
				geomStr = String.format("POINT(%f %f)=%f", ptCenter.X, ptCenter.Y, points.get(1).X);
			}
		} else if ("POLYGON".equalsIgnoreCase(drawType)) {
			if (points.size() >= 4) {
				StringBuilder sbGeom = new StringBuilder("POLYGON((");
				for (PointD pt : points) {
					sbGeom.append(String.format("%f %f", pt.X, pt.Y));
					sbGeom.append(",");
				}
				geomStr = sbGeom.toString().substring(0, sbGeom.toString().lastIndexOf(','));
				geomStr += "))";
			}
		}

		/*
		 * switch (drawType.toUpperCase()) { case "POINT": double radius =
		 * Math.min(points.get(1).X * points.get(1).Y * 20, 2); PointD
		 * pointCenter = points.get(0); geomStr =
		 * String.format("POINT(%f %f)=%f", pointCenter.X, pointCenter.Y,
		 * radius);
		 * 
		 * break; case "RECTANGLE": if (points.size() < 2) break; PointD pt0,
		 * pt1, pt2, pt3, pt4; pt0 = points.get(0); pt2 = points.get(1); pt1 =
		 * new PointD(pt2.X, pt0.Y); pt3 = new PointD(pt0.X, pt2.Y); pt4 = new
		 * PointD(pt0.X, pt0.Y); geomStr = String.format(
		 * "POLYGON((%f %f,%f %f,%f %f,%f %f,%f %f))", pt0.X, pt0.Y,
		 * pt1.X, pt1.Y, pt2.X, pt2.Y, pt3.X, pt3.Y, pt4.X, pt4.Y);
		 * 
		 * break; case "CIRCLE": if (points.size() < 2) break; PointD ptCenter =
		 * points.get(0);// First point is the center of a // circle // Second
		 * point's X,Y is radius geomStr = String.format("POINT(%s %s)=%s",
		 * ptCenter.X, ptCenter.Y, points.get(1).X); break; case "POLYGON": if
		 * (points.size() < 4) break; StringBuilder sbGeom = new
		 * StringBuilder("POLYGON(("); for (PointD pt : points) {
		 * sbGeom.append(String.format("%s %s", pt.X, pt.Y));
		 * sbGeom.append(","); } geomStr = sbGeom.toString().substring(0,
		 * sbGeom.toString().lastIndexOf(',')); geomStr += "))"; break; }
		 */
		if (StringUtil.isBlank(geomStr))
			return "";
		String where = "";
		int buffer = 0;
		Integer.parseInt(bufferStr, buffer);

		if (geomStr.toUpperCase().contains("POINT") && geomStr.contains("="))
			where = String.format("st_intersects(%s, st_setsrid(ST_Buffer(ST_GeomFromText('%s'),%s), st_srid(%s)))", geomCol, geomStr.split("=")[0], Double.parseDouble(geomStr.split("=")[1]) + buffer);
		else {
			if (buffer > 0)
				where = String.format("st_intersects(%s, st_setsrid(ST_Buffer(ST_GeomFromText('%s'), %s), st_srid(%s)))", geomCol, geomStr, buffer);
			else
				where = String.format("st_intersects(%s, st_setsrid(ST_GeomFromText('%s'), st_srid(%s)))", geomCol, geomStr);
		}
		return where;
	}
}
