package MRF.ExtDAL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Element;
import org.json.JSONArray;
import org.json.JSONException;

import DataBaseLib.DBFactory;
import DataBaseLib.DBProvider;
import MRF.ExtDAL.DataModel.ConditionOperator;
import MRF.ExtDAL.DataModel.QueryCondition;
import MRF.common.Util.StringUtil;

public class SearchBase extends DALBase {

	@SuppressWarnings("incomplete-switch")
	public String getWhereStr(List<QueryCondition> conditions) {
		StringBuffer where = new StringBuffer();
		try {
			for (QueryCondition item : conditions) {
				if (StringUtil.isBlank(item.Value))
					continue;
				if (where.length() != 0) {
					where.append(" ");
					where.append(item.Relation);
					where.append(" ");
				}
				where.append(" [");
				where.append(item.Field);
				where.append("] ");
				switch (item.Operator) {
				case EQ:
					where.append(" = ");
					break;
				case GT:
					where.append(" > ");
					break;
				case LT:
					where.append(" < ");
					break;
				case NEQ:
					where.append(" <> ");
					break;
				}
				switch (item.Type) {
				case String:
					if (item.Operator == ConditionOperator.LIKER) {
						where.append(" like '");
						where.append(item.Value);
						where.append("%' ");
					} else if (item.Operator == ConditionOperator.LIKEL) {
						where.append(" like '%");
						where.append(item.Value);
						where.append("' ");
					} else if (item.Operator == ConditionOperator.LIKE) {
						where.append(" like '%");
						where.append(item.Value);
						where.append("%' ");
					} else {
						where.append("'");
						where.append(item.Value);
						where.append("'");
					}
					break;
				case Number:
					where.append(item.Value);
					break;
				case Date:
					//
					break;
				}
			}
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
		}
		return where.toString();
	}

	public List<String> getSQLConditionParameters(String sqlFMT) {
		Pattern reg = Pattern.compile("(where|on)\\s+[\\w\\W]+",
				Pattern.CASE_INSENSITIVE);
		String where = reg.matcher(sqlFMT).group();
		String[] conditions = where.split("(and|AND|And)");
		List<String> paramsList = new ArrayList<String>();
		Pattern reg_Parame = Pattern.compile("\\{\\d+\\}");
		// Pattern reg_Order =
		// Pattern.compile("order by [ \\w\\d,]+ (desc|asc)?",
		// Pattern.CASE_INSENSITIVE);
		// int count = 0;
		for (String c : conditions) {
			if (!reg_Parame.matcher(c).find())
				continue;
			Matcher orderMatch = reg_Parame.matcher(c);
			if (orderMatch != null && !StringUtil.isBlank(orderMatch.group()))
				paramsList.add(c.replace(orderMatch.group(), ""));
			else
				paramsList.add(c);
			// paramsList.Add(c.Replace(orderMatch.Value, count.ToString()));
			// count++;
		}
		return paramsList;
	}

	public String getSQLWithValues(List<String> values, String sqlFMT)
			throws Exception {
		Pattern reg = Pattern.compile("(where|on)\\s+[\\w\\W]+",
				Pattern.CASE_INSENSITIVE);
		String where = reg.matcher(sqlFMT).group();
		String temp = where;
		List<String> paramsList = this.getSQLConditionParameters(sqlFMT);
		if (values.size() < 1 || values.size() > paramsList.size())
			throw new Exception("Parameters Error.");
		for (int i = 0; i < values.size(); i++) {
			if (StringUtil.isBlank(values.get(i))) {
				String regStr = "(and)?[ ]+"
						+ paramsList.get(i).trim().replace("(", "\\(")
								.replace(")", "\\)").replace("{", "\\{")
								.replace("}", "\\}").replace("[", "\\[")
								.replace("]", "\\]");
				reg = Pattern.compile(regStr, Pattern.CASE_INSENSITIVE);
				String match = reg.matcher(where).group();
				if (!StringUtil.isBlank(match))
					where = where.replace(
							match.replace("(", "").replace(")", ""), "");
				continue;
			}
			where = where.replace("{" + i + "}", values.get(i));
		}
		sqlFMT = sqlFMT.replace(temp, where);
		return sqlFMT;
	}

	public JSONArray getBindQuery(String xPath) {
		JSONArray dt = new JSONArray();
		try {
			Element node = getConfig(xPath);
			// String dbType = node.Attributes["DBType"].Value;
			// String connector = node.Attributes["connector"].Value;
			String sql = node.getText();
			DBProvider database = DBFactory.getDBProvider();
			dt = database.getTable(sql);
			int max = Integer.MIN_VALUE;
			if (node.attributeValue("Max") != null)
				max = Integer.parseInt(node.attributeValue("Max"));
			dt = getMaxRecord(dt, max);
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
		}
		return dt;
	}

	public JSONArray getBindQueryForFuzzy(String xPath, String value) {
		JSONArray dt = new JSONArray();
		try {
			Element node = getConfig(xPath);
			// String dbType = node.Attributes["DBType"].Value;
			// String connector = node.Attributes["connector"].Value;
			String sql = String.format(node.getText(), value);
			DBProvider database = DBFactory.getDBProvider();
			dt = database.getTable(sql);
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
		}
		return dt;
	}

	public JSONArray getBindQuery(String xPath, List<String> values) {
		JSONArray dt = new JSONArray();
		try {
			Element node = getConfig(xPath);
			// String dbType = node.Attributes["DBType"].Value;
			// String connector = node.Attributes["connector"].Value;
			String sql = node.getText();
			sql = this.getSQLWithValues(values, sql);
			DBProvider database = DBFactory.getDBProvider();
			dt = database.getTable(sql);
			int max = Integer.MIN_VALUE;
			if (node.attributeValue("Max") != null)
				max = Integer.parseInt(node.attributeValue("Max"));
			dt = getMaxRecord(dt, max);
		} catch (Exception ex) {
			// Log.GetInstance().Write(ex);
			ex.printStackTrace();
		}
		return dt;
	}

	private JSONArray getMaxRecord(JSONArray dt, int max) throws JSONException {
		if (max == Integer.MIN_VALUE || max == Integer.MAX_VALUE)
			return dt;
		if (max > dt.length())
			max = dt.length();
		JSONArray dtNew = new JSONArray();
		for (int i = 0; i < max; i++) {
			dtNew.put(dt.get(i));
		}
		return dtNew;
	}

	public Collection<String> maxForeignKeys(Element node, List<String> fks) {
		int count = 301;
		if (node.attributeValue("MaxRecord") != null) {
			count = Integer.parseInt(node.attributeValue("MaxRecord")) + 1;
			return fks.subList(0, count);
		} else
			return fks;
	}
}
