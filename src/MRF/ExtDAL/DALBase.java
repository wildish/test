package MRF.ExtDAL;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class DALBase {
	public String configPath = "";

	public Element getConfig(String xpath) {
		Element node = null;
		try {
			File xmlFile = new File(configPath);
			InputStream is = new FileInputStream(xmlFile);
			node = new SAXReader().read(is).getRootElement();
			if (node == null)
				throw new Exception(String.format("Node is not exist.\r\n xPath: %s\r\nConfig Path:{1}", xpath, configPath));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return node;
	}

	public String getConfigFilePath(String xPath, String attrName) throws Exception {
		Element node = this.getConfig(xPath);
		String path = node.attributeValue(attrName);
		File dirInfo = new File(this.configPath);
		return dirInfo.getParent() + path;
	}

	public String getPlainText(String source) {
		return source.replace("\n", " ").replace("<", "&lt;").replace(">", "&gt;").replace("\t", " ");
	}
}
