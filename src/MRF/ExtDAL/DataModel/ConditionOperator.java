package MRF.ExtDAL.DataModel;

public enum ConditionOperator {
	LT,
    GT,
    EQ,
    NEQ,
    LIKEL,
    LIKER,
    LIKE
}
