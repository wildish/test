package MRF.ExtDAL.DataModel;

public class QueryCondition {
	 public String Field;
     public String Value;
     public ConditionRelation Relation;
     public ConditionOperator Operator;
     public ConditionType Type;
}
