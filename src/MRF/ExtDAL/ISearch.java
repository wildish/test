package MRF.ExtDAL;

import org.json.JSONArray;

public interface ISearch {

    JSONArray parcelSearch(String nodeName, String cols, Object values, String buffer, String pagesize, String pageindex);


    String queryOwnerInfor(String FK);

    String queryLevyInfor(String FK);

    String queryTaxInfor(String FK);

    String queryAssessment(String FK);

    String queryPropertyInfor(String FK, boolean isPublicUser);


    JSONArray getOwnerNames(String name);

    JSONArray getStreetNames();

    JSONArray getZoneTypes();

    JSONArray getOCPs();

    JSONArray getQuaters();

    JSONArray getSections();

    JSONArray getTownships();

    JSONArray getRanges();

    JSONArray getMeridians();


    JSONArray queryFeatureSelection(String pointsStr, String fieldList, String drawType, String searchType, String buffer);
}
