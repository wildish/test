package com.longtu.utils;

import java.util.HashMap;
import java.util.Map;

public class GeoHashUtil {
	private static final char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

	private static final Map<Character, Integer> map = new HashMap<Character, Integer>();
	private static final int precision = 12;
	private static final int[] bits = { 16, 8, 4, 2, 1 };

	GeoHashUtil() {
		for (int i = 0; i < chars.length; i++)
			map.put(chars[i], i);
	}

	public static String Encode(double latitude, double longitude) {
		double[] latInterval = { -90.0, 90.0 };
		double[] lonInterval = { -180.0, 180.0 };

		StringBuffer geohash = new StringBuffer();
		boolean isEven = true;
		int bit = 0, ch = 0;

		while (geohash.length() < precision) {
			double mid;
			if (isEven) {
				mid = (lonInterval[0] + lonInterval[1]) / 2;
				if (longitude > mid) {
					ch |= bits[bit];
					lonInterval[0] = mid;
				} else {
					lonInterval[1] = mid;
				}

			} else {
				mid = (latInterval[0] + latInterval[1]) / 2;
				if (latitude > mid) {
					ch |= bits[bit];
					latInterval[0] = mid;
				} else {
					latInterval[1] = mid;
				}
			}

			isEven = isEven ? false : true;

			if (bit < 4)
				bit++;
			else {
				geohash.append(chars[ch]);
				bit = 0;
				ch = 0;
			}
		}

		return geohash.toString();
	}

	public static double[] Decode(String geohash) {
		double[] ge = DecodeExactly(geohash);
		double lat = ge[0];
		double lon = ge[1];
		double latErr = ge[2];
		double lonErr = ge[3];

		double latPrecision = Math.max(1, Math.round(-Math.log10(latErr))) - 1;
		double lonPrecision = Math.max(1, Math.round(-Math.log10(lonErr))) - 1;

		lat = GetPrecision(lat, latPrecision);
		lon = GetPrecision(lon, lonPrecision);

		double[] result = new double[2];
		result[0] = lat;
		result[1] = lon;
		return result;
	}

	public static double[] DecodeExactly(String geohash) {
		double[] latInterval = { -90.0, 90.0 };
		double[] lonInterval = { -180.0, 180.0 };

		double latErr = 90.0;
		double lonErr = 180.0;
		boolean isEven = true;
		int sz = geohash.length();
		int bsz = bits.length;
		for (int i = 0; i < sz; i++) {

			int cd = map.get(geohash.toCharArray()[i]);

			for (int z = 0; z < bsz; z++) {
				int mask = bits[z];
				if (isEven) {
					lonErr /= 2;
					if ((cd & mask) != 0)
						lonInterval[0] = (lonInterval[0] + lonInterval[1]) / 2;
					else
						lonInterval[1] = (lonInterval[0] + lonInterval[1]) / 2;

				} else {
					latErr /= 2;

					if ((cd & mask) != 0)
						latInterval[0] = (latInterval[0] + latInterval[1]) / 2;
					else
						latInterval[1] = (latInterval[0] + latInterval[1]) / 2;
				}
				isEven = isEven ? false : true;
			}

		}
		double latitude = (latInterval[0] + latInterval[1]) / 2;
		double longitude = (lonInterval[0] + lonInterval[1]) / 2;

		double[] result = new double[4];
		result[0] = latitude;
		result[1] = longitude;
		result[2] = latErr;
		result[3] = lonErr;
		return result;
	}

	public static double GetPrecision(double x, double precision) {
		double base = Math.pow(10, -precision);
		double diff = x % base;
		return x - diff;
	}
	
	public static void main(String[] args) {
		System.out.println(Encode(8.036958d, 85.369558d));
	}
}