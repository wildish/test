package com.kuaiyun.privatecloud.hawkeye;

import java.lang.reflect.InvocationTargetException;

public class Test {
	public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {

		Argument argument = new Argument();
		argument.append("disk_uuid", "12");
		argument.append("instance_name", 12);
		argument.append("current_max_disk_index", 1);
		argument.append("is_bootable", false);

		Rule rule1 = new Rule();
		rule1.create("disk_uuid", "uuid", x -> x + "_lamba");
		rule1.create("instance_name", "instanceID", null);
		rule1.create("current_max_disk_index", "keyIndex", null);
		// rule1.create("is_bootable", "bootable", x -> System.currentTimeMillis());

		Method addDisk = new DatabaseAddDiskMethod();
		// System.out.println(calfExample.getParameterDeclaration());

		Result result1 = addDisk.invoke(argument, rule1);
		// System.out.println(result1.success());
		// ----------------------------

		// KeyValuePair nextLoopArgument = result1.data();
		//
		// Rule rule2 = new Rule();
		// rule2.create("disk_uuid", "uuid", null);
		// rule2.create("instance_name", "instanceID", null);
		// rule2.create("current_max_disk_index", "keyIndex", null);
		// rule2.create("is_bootable", "bootable", null);
		//
		// Method layer3 = new DatabaseAddDiskMethod();
		// Result result2 = layer3.invoke((Argument)nextLoopArgument, rule2);
		// System.out.println(result2.success());
	}

	public static void testGetInstanceInfoMethod() throws IllegalAccessException, InvocationTargetException {
		Argument argument = new Argument();
		argument.append("instance_name", "KXQSLd4nkTJp");
		argument.append("tenantKey", "");
		argument.append("tenantSecret", "");

		Rule rule = new Rule();
		rule.create("instance_name", "instanceName", null);
		rule.create("tenantKey", "key", null);
		rule.create("tenantSecret", "secret", null);

		Method getInstanceInfo = new GetInstaceInfoMethod();
		// System.out.println(calfExample.getParameterDeclaration());

		Result result1 = getInstanceInfo.invoke(argument, rule);
		// System.out.println(result1.success());
	}
}
