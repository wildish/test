package com.kuaiyun.privatecloud.hawkeye;

import java.util.HashMap;
import java.util.Map;

public class Argument /* implements KeyValuePair */ {
	private Map<String, KeyValuePair<String, Object>> content = new HashMap<String, KeyValuePair<String, Object>>(0);

	public void append(String key, Object value) {
		if(null == key) {
			throw new IllegalArgumentException("Argument key can not be null.");
		}

		if(content.containsKey(key)) {
			throw new IllegalArgumentException(String.format("Duplicate argument key: %s.", key));
		}

		content.put(key, new KeyValuePair<String, Object>(key, value));
	}

	public Object get(String key) {
		return content.get(key).getValue();
	}
}
