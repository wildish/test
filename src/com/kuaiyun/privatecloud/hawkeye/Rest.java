package com.kuaiyun.privatecloud.hawkeye;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import com.google.gson.JsonElement;

public interface Rest {

  /**
   * 默认字符集。
   */
  public static final String CHARSET_NAME = "UTF-8";

  /**
   * 设置要请求的目的路径。
   * 
   * @param url
   *          请求的目的路径。
   * @return
   * @throws MalformedURLException
   */
  public abstract Rest url(String url) throws MalformedURLException;

  /**
   * 设置要请求的目的路径。
   * 
   * @param url
   *          请求的目的路径。
   * @return
   * @throws UnsupportedOperationException
   *           请求的目的路径使用了本程序支持范围（HTTP、HTTPS）外的协议。
   */
  public abstract Rest url(URL url) throws UnsupportedOperationException;

  /**
   * 设置请求头。
   * 
   * @param headers
   *          请求头。
   * @return
   */
  public abstract Rest header(Map<String, String> headers);

  /**
   * 设置请求的内容。
   * 
   * @param data
   *          请求内容。
   * @return
   */
  public abstract Rest data(byte[] data);

  /**
   * 设置请求的内容。
   * 
   * @param data
   *          请求内容。
   * @return
   */
  public abstract Rest data(String data);

  /**
   * 设置请求的内容。
   * 
   * @param data
   *          请求内容。
   * @param encode
   *          当为真时，会额外对 data 进行编码。
   * @return
   */
  public abstract Rest data(String data, boolean encode);

  /**
   * 设置请求的内容。
   * 
   * @param data
   *          请求内容。
   * @return
   */
  public abstract Rest data(JsonElement data);

  /**
   * 设置请求的内容。<br>
   * 内部会将 Map 的内容进行拼接，最终形成一个字符串。该字符串最终会进行转义。
   * 
   * @param data
   *          请求内容。
   * @return
   * @throws UnsupportedEncodingException
   */
  public abstract Rest data(Map<String, String> data) throws UnsupportedEncodingException;

  public abstract Rest handle(ConnectionResponseHandler handler);

  /**
   * 以 GET 请求来获取数据。
   * 
   * @return
   * @throws IOException
   *           I/O 异常
   * @throws IllegalArgumentException
   *           请求的目的地址未指定
   * @throws IllegalStateException
   *           服务器的返回值表明其处于非法状态
   */
  public abstract byte[] get() throws IllegalArgumentException, IllegalStateException, IOException;

  /**
   * 以 POST 请求来获取数据。
   * 
   * @return
   * @throws IOException
   *           I/O 异常
   * @throws IllegalArgumentException
   *           请求的目的地址未指定
   * @throws IllegalStateException
   *           服务器的返回值表明其处于非法状态
   */
  public abstract byte[] post() throws IllegalArgumentException, IllegalStateException, IOException;

  /**
   * 以 PUT 请求来获取数据。
   * 
   * @return
   * @throws IOException
   *           I/O 异常
   * @throws IllegalArgumentException
   *           请求的目的地址未指定
   * @throws IllegalStateException
   *           服务器的返回值表明其处于非法状态
   */
  public abstract byte[] put() throws IllegalArgumentException, IllegalStateException, IOException;

  /**
   * 以 DELETE 请求来获取数据。
   * 
   * @return
   * @throws IOException
   *           I/O 异常
   * @throws IllegalArgumentException
   *           请求的目的地址未指定
   * @throws IllegalStateException
   *           服务器的返回值表明其处于非法状态
   */
  public abstract byte[] delete() throws IllegalArgumentException, IllegalStateException, IOException;

  /**
   * 以 PATCH 请求来获取数据。
   * 
   * @return
   * @throws IOException
   *           I/O 异常
   * @throws IllegalArgumentException
   *           请求的目的地址未指定
   * @throws IllegalStateException
   *           服务器的返回值表明其处于非法状态
   */
  public abstract byte[] patch() throws IllegalArgumentException, IllegalStateException, IOException;

}