package com.kuaiyun.privatecloud.hawkeye;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface Method {
	List<Parameter> getParameterDeclaration();

	Result invoke(Argument argument, Rule bindingRule) throws IllegalAccessException, InvocationTargetException;

	// void parameterDeclorationSort();
}
