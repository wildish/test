package com.kuaiyun.privatecloud.hawkeye;

import java.util.List;

public abstract class Result {

	abstract boolean success();

	abstract List<List<KeyValuePair<String, Object>>> data();
}
