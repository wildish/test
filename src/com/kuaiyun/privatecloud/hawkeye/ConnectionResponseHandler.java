package com.kuaiyun.privatecloud.hawkeye;

import java.net.URL;

/**
 * 处理连接的返回信息。
 * 
 * @author 王子超
 * @since 0.6
 *
 */
public interface ConnectionResponseHandler {

  /**
   * 处理网站连接的返回信息。
   * 
   * @param uri
   *          网站连接的地址
   * @param dataSent
   *          发送给网站交互的内容。<br>
   *          这里可能会传递 NULL 值，如果需要转换为 String 时请注意。
   * @param startTime
   *          开始访问的时间点
   * @param endTime
   *          结束访问的时间点
   * @param statusCode
   *          网站连接的 HTTP 状态码
   * @param responseByteArray
   *          网站返回的数据内容
   * @throws IllegalStateException
   *           非法状态
   */
  void handle(URL uri, byte[] dataSent, long startTime, long endTime, int statusCode, byte[] responseByteArray)
      throws IllegalStateException;
}
