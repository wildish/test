package com.kuaiyun.privatecloud.hawkeye;

import java.util.function.Function;

public class Binding {

  public Binding(String argumentName, String parameterName, Function<Object, Object> function) {
    setArgumentName(argumentName);
    setParameterName(parameterName);
    setFunction(function);
  }

  public Object process(Object argumentValue) {
    // 没有处理函数时
    if (null == getFunction()) {
      return argumentValue;
    }

    // 有处理函数时
    Object result = getFunction().apply(argumentValue);
    return result;
  }

  @Override
  public String toString() {
    return "Binding [argumentName=" + argumentName + ", parameterName=" + parameterName + ", function=" + function
        + "]";
  }

  public String getArgumentName() {
    return null == argumentName ? parameterName : argumentName;
  }

  public void setArgumentName(String argumentName) {
    this.argumentName = argumentName;
  }

  public String getParameterName() {
    return parameterName;
  }

  public void setParameterName(String parameterName) {
    if (null == parameterName) {
      throw new IllegalArgumentException("Parameter key can not be null.");
    }

    this.parameterName = parameterName;
  }

  public Function<Object, Object> getFunction() {
    return function;
  }

  public void setFunction(Function<Object, Object> function) {
    this.function = function;
  }

  private String argumentName = null;
  private String parameterName = null;
  private Function<Object, Object> function = null;
}
