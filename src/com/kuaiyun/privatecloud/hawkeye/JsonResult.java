package com.kuaiyun.privatecloud.hawkeye;

import java.util.List;

import com.google.gson.JsonObject;

/**
 * [说明/描述]
 * 
 * @author LiKai
 * @date 2017年6月16日 上午9:26:58
 * @company Gainet
 * @version 1.0
 * @copyright copyright (c) 2017
 */
public class JsonResult extends Result {
	private JsonObject json = null;

	private boolean isSuccess;

	private List<List<KeyValuePair<String, Object>>> data;

	public JsonResult(JsonObject json) {
		setJson(json);
		execute();
	}

	private void execute() {
		isSuccess = json != null && json.has("code") && json.get("code").getAsInt() == 1;

		json.get("message");
	}

	@Override
	boolean success() {
		return isSuccess;
	}

	@Override
	public List<List<KeyValuePair<String, Object>>> data() {
		return data;
	}

	public void setJson(JsonObject json) {
		this.json = json;
	}

}
