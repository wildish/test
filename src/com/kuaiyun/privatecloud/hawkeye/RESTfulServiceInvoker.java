package com.kuaiyun.privatecloud.hawkeye;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class RESTfulServiceInvoker extends Invoker {
	private String endpoint = null;

	public RESTfulServiceInvoker() {

	}

	public RESTfulServiceInvoker(String endpoint) {
		setEndpoint(endpoint);
	}

	@Override
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	@Override
	public JsonObject execute(List<KeyValuePair<String, Object>> parameterList) {
		Rest req = new HttpClientImpl();
		byte[] resp = null;
		try {
			// 处理参数
			resp = req.data(parameterList.toString()).url(endpoint).handle(new ConnectionResponseHandler() {

				@Override
				public void handle(URL uri, byte[] dataSent, long startTime, long endTime, int statusCode,
						byte[] responseByteArray) throws IllegalStateException {
					// 读取返回值
					String serverReturned = (null == responseByteArray) ? null : new String(responseByteArray);
					// 执行结果。未来可以把这个值加入到日志中去
					// boolean success = ((200 == statusCode) && (null !=
					// serverReturned));

					// log2Console(null, null, startTime, endTime, statusCode, serverReturned);

					// // 抛出异常
					// if (!success) {
					// // 请求调用的方法名称
					// String module =
					// message.getAsJsonObject().get("module").getAsString();
					// String action =
					// message.getAsJsonObject().get("action").getAsString();
					// String methodName = String.format("python.%s.%s", module, action);
					//
					// // 数据库记录，HTTP 状态异常的请求不返回给上级，所以必须在此记录日志
					// CalfOperation.addFunction(target, methodName, message.toString(),
					// startTime, endTime, serverReturned, false);
					// }
				}
			}).post();

		} catch (Exception e) {
			// invokeResult.occur(e);

			// ExceptionLogger.log(e);

		}

		JsonElement respElement = getResult(resp);
		return respElement.getAsJsonObject();

		/*
		 * // String transactionId = UUID.randomUUID().toString();
		 * 
		 * RESTfulServiceInvokeResult invokeResult = new
		 * RESTfulServiceInvokeResult(); return invokeResult;
		 */

	}

	private JsonElement getResult(byte[] byteArrayResult) {
		// 当byteArrayResult为空时，默认返回字符串{}的byte数组
		String strResult;
		try {
			strResult = (null == byteArrayResult) ? "" : new String(byteArrayResult, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			strResult = "";
		}
		strResult = "" == strResult.trim() ? "{}" : strResult;
		JsonElement result = new JsonParser().parse(strResult);
		return result;
	}

	// private void log2Console(String transactionId, InvokeParameter methodParameter, long startTime, long endTime,
	// int statusCode, String serverReturned) {
	// if (logger.isDebugEnabled()) {
	// logger.debug("[{}] send message: {}, to: {}", transactionId, methodParameter, endpoint);
	// logger.debug("[{}] status code: {}", transactionId, statusCode);
	// logger.debug("[{}] server returned: {}", transactionId, serverReturned);
	// logger.debug("[{}] execute in {} ms", transactionId, endTime - startTime);
	// // logger.debug("[{}] success: {}", transactionId, success);
	// }
	// }
}
