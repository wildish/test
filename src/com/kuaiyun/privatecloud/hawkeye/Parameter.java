package com.kuaiyun.privatecloud.hawkeye;

public class Parameter {
	public Parameter(Integer order, String key, Class<?> type, String note, Boolean required) {
		setKey(key);
		setType(type);

		setOrder(order);
		setNote(note);
		setRequired(required);
	}

	@Override
	public String toString() {
		return "Parameter [order=" + order + ", key=" + key + ", type=" + type + ", note=" + note + ", required="
				+ getRequired() + "]";
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		if(null == key) {
			throw new IllegalArgumentException("Key can not be null.");
		}

		this.key = key;
	}

	public Class<?> getType() {
		return type;
	}

	public void setType(Class<?> type) {
		if(null == type) {
			throw new IllegalArgumentException("Type can not be null.");
		}

		this.type = type;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Boolean getRequired() {
		return null == required ? false : required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	/**
	 * 参数的顺序
	 */
	private Integer order = null;
	/**
	 * 参数的名称
	 */
	private String key = null;
	/**
	 * 参数的类型
	 */
	private Class<?> type = null;
	/**
	 * 参数的描述
	 */
	private String note = null;
	/**
	 * 是否必须
	 */
	private Boolean required = null;
}
