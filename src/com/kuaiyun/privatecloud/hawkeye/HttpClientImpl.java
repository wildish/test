package com.kuaiyun.privatecloud.hawkeye;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;

/**
 * REST 客户端的 HttpClient 实现。
 * 
 * @author 孙强
 *
 */
public class HttpClientImpl implements Rest {
  /**
   * 请求的数据类型。
   * 
   * @author 王子超
   *
   */
  private enum DataType {
    /**
     * 键值对
     */
    NAME_VALUE_PAIR,
    /**
     * 字符串
     */
    STRING,
    /**
     * 数组
     */
    BYTE_ARRAY
  }

  /**
   * 默认的 Json 请求头。
   */
  private static Header jsonRequestHeader = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");

  /**
   * 请求的链接地址。
   */
  private URL url = null;

  /**
   * 请求头。
   */
  private List<Header> headers = new ArrayList<Header>(0);

  /**
   * 键值对内容。
   */
  private List<NameValuePair> dataNameValuePairs = new ArrayList<NameValuePair>(0);
  /**
   * 数组内容。<br>
   * 暂无使用需要。
   */
  @SuppressWarnings("unused")
  private byte[] dataByteArray = null;
  /**
   * 字符串内容。
   */
  private String dataString = null;
  /**
   * 数据类型。
   */
  private DataType dataType = null;
  /**
   * 是否是 Json 字符串。
   */
  private boolean isJsonString = false;
  private ConnectionResponseHandler handler = null;

  @Override
  public Rest url(String url) throws MalformedURLException {
    URL uri = new URL(url);
    return url(uri);
  }

  @Override
  public Rest url(URL url) throws UnsupportedOperationException {
    this.url = Preconditions.checkNotNull(url);
    return this;
  }

  @Override
  public Rest header(Map<String, String> headers) {
    // 清除 请求头 设定
    if (this.headers.size() > 0) {
      this.headers = new ArrayList<Header>(0);
    }

    if (null == headers) {
      return this;
    }

    for (String key : headers.keySet()) {
      Header header = new BasicHeader(key, headers.get(key));
      this.headers.add(header);
    }
    return this;
  }

  @Override
  public Rest data(byte[] data) {
    dataType = DataType.BYTE_ARRAY;
    dataByteArray = data;
    isJsonString = false;
    return this;
  }

  @Override
  public Rest data(String data) {
    dataType = DataType.STRING;
    dataString = data;
    isJsonString = false;
    return this;
  }

  @Override
  public Rest data(String data, boolean encode) {
    // encode 无用
    return data(data);
  }

  @Override
  public Rest data(JsonElement data) {
    dataType = DataType.STRING;
    dataString = (null == data) ? null : data.toString();
    isJsonString = true;
    return this;
  }

  @Override
  public Rest data(Map<String, String> data) throws UnsupportedEncodingException {
    dataType = DataType.NAME_VALUE_PAIR;
    if (dataNameValuePairs.size() > 0) {
      dataNameValuePairs = new ArrayList<NameValuePair>(0);
    }
    isJsonString = false;

    if (null == data) {
      return this;
    }

    for (String key : data.keySet()) {
      dataNameValuePairs.add(new BasicNameValuePair(key, data.get(key)));
    }
    return this;
  }

  @Override
  public Rest handle(ConnectionResponseHandler handler) {
    this.handler = handler;
    return this;
  }

  @Override
  public byte[] get() throws IllegalArgumentException, IllegalStateException, IOException {
    HttpGet request = new HttpGet(getUri());
    return send(request, null);
  }

  @Override
  public byte[] post() throws IllegalArgumentException, IllegalStateException, IOException {
    HttpPost request = new HttpPost(getUri());
    HttpEntity entity = getHttpEntity();
    request.setEntity(entity);
    return send(request, entity);
  }

  @Override
  public byte[] put() throws IllegalArgumentException, IllegalStateException, IOException {
    HttpPut request = new HttpPut(getUri());
    HttpEntity entity = getHttpEntity();
    request.setEntity(entity);
    return send(request, entity);
  }

  @Override
  public byte[] delete() throws IllegalArgumentException, IllegalStateException, IOException {
    HttpDelete request = new HttpDelete(getUri());
    // 貌似看来 DELETE 方法不能添加内容
    return send(request, null);
  }

  @Override
  public byte[] patch() throws IllegalArgumentException, IllegalStateException, IOException {
    HttpPatch request = new HttpPatch(getUri());
    HttpEntity entity = getHttpEntity();
    request.setEntity(entity);
    return send(request, entity);
  }

  /**
   * 取得发送的内容。
   * 
   * @return
   */
  private HttpEntity getHttpEntity() {
    if (null == dataType) {
      return null;
    }

    HttpEntity result = null;
    try {

      switch (dataType) {
      case NAME_VALUE_PAIR:
        result = new UrlEncodedFormEntity(dataNameValuePairs, CHARSET_NAME);
        break;
      case STRING:
        result = (null == dataString) ? null : new StringEntity(dataString.toString());
        break;
      case BYTE_ARRAY:
        // 暂时未遇到此需求
        // 如果有此需要时，请在 pom.xml 中引入 httpmime，然后使用 MultipartEntityBuilder
        break;
      default:
        break;
      }
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return result;
  }

  /**
   * 转换 URL 到 URI。
   * 
   * @return
   */
  private URI getUri() {
    URI uri = null;
    try {
      uri = this.url.toURI();
    } catch (URISyntaxException e) {
      // e.printStackTrace();
    }
    return uri;
  }

  private byte[] send(HttpUriRequest request, HttpEntity requestEntity)
      throws IllegalStateException, ClientProtocolException, IOException {
    CloseableHttpClient client = null;
    if (headers.size() == 0) {
      client = HttpClients.createDefault();
    } else {
      client = HttpClients.custom().setDefaultHeaders(headers).build();
    }

    if (dataType == DataType.STRING && isJsonString) {
      request.addHeader(jsonRequestHeader);
    }

    long start = System.currentTimeMillis();
    CloseableHttpResponse response = client.execute(request);
    long end = System.currentTimeMillis();

    int responseCode = response.getStatusLine().getStatusCode();

    HttpEntity entity = response.getEntity();
    int length = (int) entity.getContentLength();
    byte[] responseByteArray = null;
    if (length < 0) {
      // 当不知道返回的内容长度时，length 为 -1
      if (entity != null) {
        // 尝试用这种方法读取
        String responseString = EntityUtils.toString(entity, CHARSET_NAME);
        responseByteArray = responseString.getBytes(CHARSET_NAME);
      }
    } else {
      responseByteArray = new byte[length];
      entity.getContent().read(responseByteArray, 0, length);
    }

    // 自定义处理方式
    if (null != handler) {
      byte[] requestByteArray = null;
      if (null != requestEntity) {
        int sentLength = (int) requestEntity.getContentLength();
        // 暂时未遇到 sentLength = -1 的情况
        requestByteArray = new byte[sentLength];
        requestEntity.getContent().read(requestByteArray, 0, sentLength);
      } else {
        // 暂时不明确调用者的需求，不知道这里是否转为 "" 空字符串会好一点？
      }

      handler.handle(url, requestByteArray, start, end, responseCode, responseByteArray);
    }

    EntityUtils.consume(entity);
    return responseByteArray;
  }
}
