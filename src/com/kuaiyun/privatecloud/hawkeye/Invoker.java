package com.kuaiyun.privatecloud.hawkeye;

import java.util.List;

import com.google.gson.JsonObject;

public abstract class Invoker {

	public abstract void setEndpoint(String endpoint);

	public abstract JsonObject execute(List<KeyValuePair<String, Object>> parameterList);

}
