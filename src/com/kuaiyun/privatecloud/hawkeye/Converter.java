package com.kuaiyun.privatecloud.hawkeye;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Converter {
  public static final int INFINITY = 1000000;

  private static List<Edge> edges = new ArrayList<Edge>(0);
  private static Map<Class<?>, Integer> vertices = null;
  private static int[][] distance = null;
  private static Integer[][] path = null;

  static {
    for (Method method : Converter.class.getDeclaredMethods()) {
      Edge edge = new Edge(method);
      if (null != edge.getMethod()) {
        System.out.println(edge);
        edges.add(edge);
      }
    }

    findAllVerticesByEdges();
    initDistanceArray();
    relax();
  }

  public static String convertInteger2String(Integer object) {
    return Integer.toString(object);
  }

  public static int convertInteger2int(Integer object) {
    return object.intValue();
  }
  
  public static boolean convertint2boolean(int object) {
    return !(object == 0);
  }
  
  public static int convertString2int(String object) {
    return Integer.parseInt(object);
  }
  
  public static Integer convertint2Integer(int object) {
    return new Integer(object);
  }
  
  public static Integer convertString2Integer(String object) {
    return new Integer(object);
  }

  @SuppressWarnings("unchecked")
  public static <T> T convert(Object object, Class<T> targetClass) throws IllegalAccessException,
      InvocationTargetException {
    List<Method> methodList = getConvertMethods(object.getClass(), targetClass);

    Object lastInvokeResult = null;
    if (null == methodList || methodList.size() == 0) {
      throw new IllegalStateException("No convert method was found.");
    }

    lastInvokeResult = object;
    try {
      for (int i = 0; i < methodList.size(); i++) {
        lastInvokeResult = methodList.get(i).invoke(null, new Object[] { lastInvokeResult });
      }
    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      e.printStackTrace();
      throw e;
    }
    return (T) lastInvokeResult;
  }

  private static List<Method> getConvertMethods(Class<?> parameterType, Class<?> returnType) {
    System.out.println("parameterType: " + parameterType);
    System.out.println("returnType: " + returnType);

    List<Method> result = null;
    if (vertices.size() == 0) {
      return new ArrayList<Method>(0);
    }

    /*
    // ////////////////


    // ////////////////

    // Optional<Method> matchingMethod = methods.stream().filter(m ->
    // m.getName().startsWith("convert")).findFirst();
    Method matchingMethod = null;
    for (int i = 0; i < methods.size(); i++) {
      Method currentMethod = methods.get(i);
      Class<?>[] parameterTypes = currentMethod.getParameterTypes();

      if (parameterTypes.length == 1 && parameterTypes[0] == parameterType
          && currentMethod.getReturnType() == returnType) {
        matchingMethod = currentMethod;
        break;
      }
    }

    if (null != matchingMethod) {
      result = new ArrayList<Method>(0);
      result.add(matchingMethod);
    }
    */
    return result;
  }

  private static void relax() {
    for (Class<?> middle : vertices.keySet()) {
      for (int origin = 0; origin < distance.length; origin++) {
        for (int destination = 0; destination < distance[origin].length; destination++) {
          if (distance[origin][destination] > distance[origin][vertices.get(middle)] + distance[vertices.get(middle)][destination]) {
            distance[origin][destination] = distance[origin][vertices.get(middle)] + distance[vertices.get(middle)][destination];
            printDistanceArray();
          }
        }
      }
    }
  }

  private static void initDistanceArray() {
    if (null == vertices) {
      findAllVerticesByEdges();
    }

    if (vertices.size() > 0) {
      distance = new int[vertices.size()][vertices.size()];
      path = new Integer[vertices.size()][vertices.size()];
      
      for (int origin = 0; origin < distance.length; origin++) {
        for (int destination = 0; destination < distance[origin].length; destination++) {
          distance[origin][destination] = (origin == destination) ? 0 : INFINITY;
        }
      }
      
      printDistanceArray();
      for (Class<?> clazz : vertices.keySet()) {
        edges.stream().filter(e -> e.getOrigin() == clazz).forEach(e -> {
          distance[vertices.get(e.getOrigin())][vertices.get(e.getDestination())] = e.getWeight();
//          path[vertices.get(e.getOrigin())][vertices.get(e.getDestination())] = e.getMethod();
        });
        printDistanceArray();
      }
    }
    System.out.println("Distance array initialized.");
    System.out.println(vertices);
  }
  
  private static void printDistanceArray() {
    for (int origin = 0; origin < distance.length; origin++) {
      for (int destination = 0; destination < distance[origin].length; destination++) {
        System.out.print(distance[origin][destination] + "\t");
      }
      System.out.println();
    }
    System.out.println("------------------");
  }

  private static void findAllVerticesByEdges() {
    vertices = new HashMap<Class<?>, Integer>(0);
    for (Edge edge : edges) {
      if (null == edge.getMethod()) {
        continue;
      }

      if (!vertices.keySet().contains(edge.getOrigin())) {
        vertices.put(edge.getOrigin(), vertices.size());
      }
      if (!vertices.keySet().contains(edge.getDestination())) {
        vertices.put(edge.getDestination(), vertices.size());
      }
    }
    System.out.print("Edge Count: " + edges.size() + "\t");
    System.out.println("Vertex Count: " + vertices.size());
  }

  static class Edge {
    private Method method = null;

    public Edge(Method method) {
      Class<?>[] parameterTypes = method.getParameterTypes();
      if (parameterTypes.length == 1 && method.getName().startsWith("convert")
          && Modifier.isStatic(method.getModifiers())) {
        this.method = method;
      }
    }

    public Class<?> getOrigin() {
      return null == method ? null : method.getParameterTypes()[0];
    }

    public Class<?> getDestination() {
      return null == method ? null : method.getReturnType();
    }

    public Method getMethod() {
      return method;
    }

    public int getWeight() {
      return 1;
    }

    @Override
    public String toString() {
      return "Edge [getOrigin()=" + getOrigin() + ", getDestination()=" + getDestination() + ", getMethod()="
          + getMethod() + ", getWeight()=" + getWeight() + "]";
    }
  }

  public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
    convert(new Integer(2), int.class);
  }
}
