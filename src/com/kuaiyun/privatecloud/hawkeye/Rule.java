package com.kuaiyun.privatecloud.hawkeye;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Rule {
  // 用于通过形参的名称来查找绑定规则
  private Map<String, Binding> parameterBinding = new HashMap<String, Binding>(0);

  // 用于通过参数的名称来查找绑定规则
  // private Map<String, Binding> argumentBinding = new HashMap<>(0);

  public void create(String argumentName, String parameterName, Function<Object, Object> function) {
    Binding binding = new Binding(argumentName, parameterName, function);

    parameterBinding.put(parameterName, binding);
    // argumentBinding.put(argumentName, binding);
  }

  /**
   * 根据绑定规则来查找真实参数的对应值。
   * 
   * @param argument
   * 
   * @param parameter
   * @return
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   */
  public Object getArgumentValue(Argument argument, Parameter parameter) throws IllegalAccessException,
      InvocationTargetException {
    System.out.println("-------------------");

    System.out.println("Parameter: " + parameter);
    // 如果形参需求非法，则报错
    if (null == parameter) {
      return null;
    }

    String parameterName = parameter.getKey();
    Binding binding = findBindingByParameterName(parameterName);
    verifyBindingNonNullable(parameter, binding);

    if (null == binding) {
      return null; // FIXME 需要再商榷
    }

    String argumentName = binding.getArgumentName();
    Object argumentValue = argument.get(argumentName);

    Object finalArgumentValue = binding.process(argumentValue);
    System.out.println("Final Argument Value: " + finalArgumentValue);

    System.out.println(finalArgumentValue.getClass());
    return finalArgumentValue.getClass() == parameter.getType() ? finalArgumentValue : Converter.convert(
        finalArgumentValue, parameter.getType());
  }

  /**
   * 通过形参名称查找绑定规则。
   * 
   * @param parameterName
   * @return
   */
  private Binding findBindingByParameterName(String parameterName) {
    if (parameterBinding.containsKey(parameterName)) {
      return parameterBinding.get(parameterName);
    }
    return null;
  }

  /**
   * 验证为 NULL 的绑定规则是否被允许。
   * 
   * @param parameter
   * @param binding
   */
  private void verifyBindingNonNullable(Parameter parameter, Binding binding) {
    if (parameter.getRequired() && null == binding) {
      throw new IllegalStateException(String.format("Parameter %s is required.", parameter.getKey()));
    }
  }
}
