package com.kuaiyun.privatecloud.hawkeye;

import java.util.Map;

//public interface KeyValuePair {
//  Object get(String key);
//}

public class KeyValuePair<K, V> implements Map.Entry<K, V> {
	private K key;
	private V value;

	public KeyValuePair(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public K getKey() {
		return this.key;
	}

	public V getValue() {
		return this.value;
	}

	public K setKey(K key) {
		return this.key = key;
	}

	public V setValue(V value) {
		return this.value = value;
	}

	@Override
	public String toString() {
		return "KeyValuePair [key=" + key + ", value=" + value + "]";
	}

}
