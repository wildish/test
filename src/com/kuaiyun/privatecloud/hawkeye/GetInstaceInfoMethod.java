package com.kuaiyun.privatecloud.hawkeye;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonObject;

public class GetInstaceInfoMethod implements Method {

	private List<Parameter> declaration = null;
	private Invoker invoker = new RESTfulServiceInvoker();

	@Override
	public List<Parameter> getParameterDeclaration() {
		// TODO 此步骤数据到时反射搞进来
		if(null == this.declaration) {
			this.declaration = new ArrayList<Parameter>(0);
			declaration.add(new Parameter(0, "instanceName", String.class, "虚拟机名称", true));
			declaration.add(new Parameter(1, "key", String.class, "租户key", true));
			declaration.add(new Parameter(2, "secret", String.class, "租户secret", true));
		}

		detectDuplicateParameterName(declaration);

		parameterDeclorationSort();

		return declaration;
	}

	/**
	 * 重复参数名称检测。
	 * 
	 * @param parameters
	 */
	private void detectDuplicateParameterName(List<Parameter> parameters) {
		Set<String> parameterNameSet = new HashSet<String>(0);
		for(Parameter parameter : parameters) {
			if(parameterNameSet.contains(parameter.getKey())) {
				throw new IllegalArgumentException(String.format("Duplicate parameter key: %s.", parameter.getKey()));
			}

			parameterNameSet.add(parameter.getKey());
		}
	}

	public void setInvoker(Invoker invoker) {
		this.invoker = invoker;
	}

	public void parameterDeclorationSort() {
		// 此方法需要优化
		// 对声明的参数进行排序
		if(this.declaration == null && this.declaration.size() == 0) {
			return;
		}

		// 判断最大的序号[从0开始], 默认最大序号为声明参数列表个数 - 1
		int maxOrder = this.declaration.size() - 1;
		// 序号数组
		List<Integer> orders = new ArrayList<>();
		for(Parameter parameter : this.declaration) {
			// 当前声明参数的序号不为空并且大于maxOrder时，替换maxOrder
			if(parameter.getOrder() != null) {
				int order = parameter.getOrder().intValue();
				orders.add(order);
				if(parameter.getOrder().intValue() > maxOrder) {
					maxOrder = parameter.getOrder().intValue();
				}
			}
		}

		int curOrder = 0;
		Parameter[] parameters = new Parameter[maxOrder + 1];
		for(Parameter parameter : this.declaration) {
			if(parameter.getOrder() == null) {
				while(!orders.contains(curOrder) && curOrder <= maxOrder) {
					parameters[curOrder++] = parameter;
				}
			} else {
				int order = parameter.getOrder().intValue();
				parameters[order] = parameter;
			}
		}

		this.declaration.clear();
		for(Parameter parameter : parameters) {
			this.declaration.add(parameter);
		}

	}

	@Override
	public Result invoke(Argument argument, Rule bindingRule) throws IllegalAccessException, InvocationTargetException {

		List<Parameter> parameters = getParameterDeclaration();

		List<KeyValuePair<String, Object>> list = new ArrayList<>();
		for(int i = 0; i < parameters.size(); i++) {
			Parameter parameter = parameters.get(i);
			if(parameter != null) {
				// 处理单个参数的值
				list.add(new KeyValuePair<String, Object>(parameter.getKey(),
						bindingRule.getArgumentValue(argument, parameter)));
			} else {
				list.add(null);
			}
		}

		// 汇总参数的值(参数排序)
		invoker.setEndpoint(String.format("http://10.112.0.44:8081/restfulapi-testenv/v2/instances/%s?key=%s&secret=%s",
				list.get(0).getValue(), list.get(1).getValue(), list.get(2).getValue()));
		JsonObject ret = invoker.execute(list);
		// 处理数据生成Result对象

		return null;
	}
}
