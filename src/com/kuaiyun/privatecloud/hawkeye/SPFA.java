package com.kuaiyun.privatecloud.hawkeye;

public class SPFA {
  public static final int INFINITY = 1000000;

  private int[][] distance = null;
  private int vertexCount = 4;

  private int[][] init() {
    distance = new int[vertexCount][vertexCount];
    for (int origin = 0; origin < distance.length; origin++) {
      for (int destination = 0; destination < distance[origin].length; destination++) {
        distance[origin][destination] = (origin == destination) ? 0 : INFINITY;
      }
    }
    return distance;
  }

  private void relax() {
    boolean hasNativeLoop = false;

    for (int vertex = 0; vertex < vertexCount; vertex++) {
      for (int origin = 1; origin < distance.length; origin++) {
        for (int destination = 0; destination < distance[origin].length; destination++) {
          // if (distance[v[origin]] > distance[u[origin]] + w[origin])
          // distance[v[origin]] = distance[u[origin]] + w[origin];
        }
      }
    }
  }

  public static void main(String[] args) {
    SPFA spfa = new SPFA();
    spfa.init();
    spfa.relax();
  }
}
