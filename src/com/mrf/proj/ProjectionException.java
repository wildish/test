//
//  ProjectionException.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class ProjectionException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3505768755183589016L;

	public ProjectionException() {
		super();
	}

	public ProjectionException(String message) {
		super(message);
	}
}
