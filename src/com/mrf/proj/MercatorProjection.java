//
//  MercatorProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class MercatorProjection extends CylindricalProjection {
	
	public MercatorProjection() {
		minLatitude = MapMath.degToRad(-85);
		maxLatitude = MapMath.degToRad(85);
	}
	
	public Point2D project(double lam, double phi, Point2D out) {
		if (spherical) {
			out.x = scaleFactor * lam;
			out.y = scaleFactor * Math.log(Math.tan(MapMath.QUARTERPI + 0.5 * phi));
		} else {
			out.x = scaleFactor * lam;
			out.y = -scaleFactor * Math.log(MapMath.tsfn(phi, Math.sin(phi), e));
		}
		return out;
	}

	public Point2D projectInverse(double x, double y, Point2D out) {
		if (spherical) {
			out.y = MapMath.HALFPI - 2. * Math.atan(Math.exp(-y / scaleFactor));
			out.x = x / scaleFactor;
		} else {
			out.y = MapMath.phi2(Math.exp(-y / scaleFactor), e);
			out.x = x / scaleFactor;
		}
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public boolean isRectilinear() {
		return true;
	}

	/**
	 * Returns the ESPG code for this projection, or 0 if unknown.
	 */
	public int getEPSGCode() {
		return 9804;
	}

	public String toString() {
		return "Mercator";
	}

}
