//
//  TCEAProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class TCEAProjection extends Projection {

	private double rk0;

	public TCEAProjection() {
		initialize();
	}
	
	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.x = rk0 * Math.cos(lpphi) * Math.sin(lplam);
		out.y = scaleFactor * (Math.atan2(Math.tan(lpphi), Math.cos(lplam)) - projectionLatitude);
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		double t;

		out.y = xyy * rk0 + projectionLatitude;
		out.x *= scaleFactor;
		t = Math.sqrt(1. - xyx * xyx);
		out.y = Math.asin(t * Math.sin(xyy));
		out.x = Math.atan2(xyx, t * Math.cos(xyy));
		return out;
	}

	public void initialize() { // tcea
		super.initialize();
		rk0 = 1 / scaleFactor;
	}

	public boolean isRectilinear() {
		return false;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Transverse Cylindrical Equal Area";
	}

}
