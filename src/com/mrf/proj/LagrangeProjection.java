//
//  LagrangeProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class LagrangeProjection extends Projection {

	// Parameters
	private double hrw;
	private double rw = 1.4;
	private double a1;
	private double phi1;

	private final static double TOL = 1e-10;

	public Point2D project(double lplam, double lpphi, Point2D xy) {
		double v, c;

		if ( Math.abs(Math.abs(lpphi) - MapMath.HALFPI) < TOL) {
			xy.x = 0;
			xy.y = lpphi < 0 ? -2. : 2.;
		} else {
			lpphi = Math.sin(lpphi);
			v = a1 * Math.pow((1. + lpphi)/(1. - lpphi), hrw);
			if ((c = 0.5 * (v + 1./v) + Math.cos(lplam *= rw)) < TOL)
				throw new ProjectionException();
			xy.x = 2. * Math.sin(lplam) / c;
			xy.y = (v - 1./v) / c;
		}
		return xy;
	}

	public void setW( double w ) {
		this.rw = w;
	}
	
	public double getW() {
		return rw;
	}

	public void initialize() {
		super.initialize();
		if (rw <= 0)
			throw new ProjectionException("-27");
		hrw = 0.5 * (rw = 1. / rw);
		phi1 = projectionLatitude1;
		if (Math.abs(Math.abs(phi1 = Math.sin(phi1)) - 1.) < TOL)
			throw new ProjectionException("-22");
		a1 = Math.pow((1. - phi1)/(1. + phi1), hrw);
	}

	/**
	 * Returns true if this projection is conformal
	 */
	public boolean isConformal() {
		return true;
	}
	
	public boolean hasInverse() {
		return false;
	}

	public String toString() {
		return "Lagrange";
	}

}
