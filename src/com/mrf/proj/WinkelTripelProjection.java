//
//  WinkelTripelProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class WinkelTripelProjection extends AitoffProjection {

	public WinkelTripelProjection() {
		super( WINKEL, 0.636619772367581343 );
	}

	public String toString() {
		return "Winkel Tripel";
	}

}
