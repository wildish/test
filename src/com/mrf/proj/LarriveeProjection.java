//
//  LarriveeProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class LarriveeProjection extends Projection {

	private final static double SIXTH = .16666666666666666;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.x = 0.5 * lplam * (1. + Math.sqrt(Math.cos(lpphi)));
		out.y = lpphi / (Math.cos(0.5 * lpphi) * Math.cos(SIXTH * lplam));
		return out;
	}

	public String toString() {
		return "Larrivee";
	}

}
