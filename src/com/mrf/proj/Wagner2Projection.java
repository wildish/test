//
//  Wagner2Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class Wagner2Projection extends Projection {

	private final static double C_x = 0.92483;
	private final static double C_y = 1.38725;
	private final static double C_p1 = 0.88022;
	private final static double C_p2 = 0.88550;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.y = MapMath.asin(C_p1 * Math.sin(C_p2 * lpphi));
		out.x = C_x * lplam * Math.cos(lpphi);
		out.y = C_y * lpphi;
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.y = xyy / C_y;
		out.x = xyx / (C_x * Math.cos(out.y));
		out.y = MapMath.asin(Math.sin(out.y) / C_p1) / C_p2;
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Wagner II";
	}
}
