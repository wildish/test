//
//  LoximuthalProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class LoximuthalProjection extends PseudoCylindricalProjection {

	@SuppressWarnings("unused")
	private final static double FC = .92131773192356127802;
	@SuppressWarnings("unused")
	private final static double RP = .31830988618379067154;
	private final static double EPS = 1e-8;
	
	private double phi1;
	private double cosphi1;
	private double tanphi1;

	public LoximuthalProjection() {
		phi1 = Math.toRadians(40.0);
		cosphi1 = Math.cos(phi1);
		tanphi1 = Math.tan(MapMath.QUARTERPI + 0.5 * phi1);
	}

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double x;
		double y = lpphi - phi1;
		if (y < EPS)
			x = lplam * cosphi1;
		else {
			x = MapMath.QUARTERPI + 0.5 * lpphi;
			if (Math.abs(x) < EPS || Math.abs(Math.abs(x) - MapMath.HALFPI) < EPS)
				x = 0.;
			else
				x = lplam * y / Math.log( Math.tan(x) / tanphi1 );
		}
		out.x = x;
		out.y = y;
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		double latitude = xyy + phi1;
		double longitude;
		if (Math.abs(xyy) < EPS)
			longitude = xyx / cosphi1;
		else if (Math.abs( longitude = MapMath.QUARTERPI + 0.5 * xyy ) < EPS ||
			Math.abs(Math.abs(xyx) -MapMath.HALFPI) < EPS)
			longitude = 0.;
		else
			longitude = xyx * Math.log( Math.tan(longitude) / tanphi1 ) / xyy;

		out.x = longitude;
		out.y = latitude;
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Loximuthal";
	}

}
