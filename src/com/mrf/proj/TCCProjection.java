//
//  TCCProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class TCCProjection extends CylindricalProjection {

	public TCCProjection() {
		minLongitude = MapMath.degToRad(-60);
		maxLongitude = MapMath.degToRad(60);
	}
	
	public Point2D project(double lplam, double lpphi, Point2D out) {
		double b, bt;

		b = Math.cos(lpphi) * Math.sin(lplam);
		if ((bt = 1. - b * b) < EPS10)
			throw new ProjectionException("F");
		out.x = b / Math.sqrt(bt);
		out.y = Math.atan2(Math.tan(lpphi), Math.cos(lplam));
		return out;
	}

	public boolean isRectilinear() {
		return false;
	}

	public String toString() {
		return "Transverse Central Cylindrical";
	}

}
