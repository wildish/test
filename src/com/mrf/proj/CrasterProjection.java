//
//  CrasterProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class CrasterProjection extends Projection {

	private final static double XM = 0.97720502380583984317;
	private final static double RXM = 1.02332670794648848847;
	private final static double YM = 3.06998012383946546542;
	private final static double RYM = 0.32573500793527994772;
	private final static double THIRD = 0.333333333333333333;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		lpphi *= THIRD;
		out.x = XM * lplam * (2. * Math.cos(lpphi + lpphi) - 1.);
		out.y = YM * Math.sin(lpphi);
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.y = 3. * Math.asin(xyy * RYM);
		out.x = xyx * RXM / (2. * Math.cos((out.y + out.y) * THIRD) - 1);
		return out;
	}

	/**
	 * Returns true if this projection is equal area
	 */
	public boolean isEqualArea() {
		return true;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Craster Parabolic (Putnins P4)";
	}

}
