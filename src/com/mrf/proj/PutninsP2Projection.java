//
//  PutninsP2Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class PutninsP2Projection extends Projection {

	private final static double C_x = 1.89490;
	private final static double C_y = 1.71848;
	private final static double C_p = 0.6141848493043784;
	private final static double EPS = 1e-10;
	private final static int NITER = 10;
	private final static double PI_DIV_3 = 1.0471975511965977;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double p, c, s, V;
		int i;

		p = C_p * Math.sin(lpphi);
		s = lpphi * lpphi;
		out.y *= 0.615709 + s * ( 0.00909953 + s * 0.0046292 );
		for (i = NITER; i > 0; --i) {
			c = Math.cos(lpphi);
			s = Math.sin(lpphi);
			out.y -= V = (lpphi + s * (c - 1.) - p) /
				(1. + c * (c - 1.) - s * s);
			if (Math.abs(V) < EPS)
				break;
		}
		if (i == 0)
			out.y = lpphi < 0 ? - PI_DIV_3 : PI_DIV_3;
		out.x = C_x * lplam * (Math.cos(lpphi) - 0.5);
		out.y = C_y * Math.sin(lpphi);
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		double c;

		out.y = MapMath.asin(xyy / C_y);
		out.x = xyx / (C_x * ((c = Math.cos(out.y)) - 0.5));
		out.y = MapMath.asin((out.y + Math.sin(out.y) * (c - 1.)) / C_p);
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Putnins P2";
	}

}
