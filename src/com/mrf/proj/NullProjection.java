//
//  NullProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;




public class NullProjection extends Projection {
	
	public Point2D transform( Point2D src, Point2D dst ) {
		dst.x = src.x;
		dst.y = src.y;
		return dst;
	}
	
	
	
	public Rectangle2D getBoundingShape() {
		return null;
	}
	
	public boolean isRectilinear() {
		return true;
	}

	public String toString() {
		return "Null";
	}
}
