//
//  QuarticAuthalicProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class QuarticAuthalicProjection extends STSProjection {

	public QuarticAuthalicProjection() {
		super( 2., 2., false );
	}
	
	public String toString() {
		return "Quartic Authalic";
	}

}
