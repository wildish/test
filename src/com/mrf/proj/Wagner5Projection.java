//
//  Wagner5Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class Wagner5Projection extends MolleweideProjection {

	public Wagner5Projection() {
		super( MolleweideProjection.WAGNER5 );
	}

}
