//
//  BoggsProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class BoggsProjection extends PseudoCylindricalProjection {

	private final static int NITER = 20;
	private final static double EPS = 1e-7;
	@SuppressWarnings("unused")
	private final static double ONETOL = 1.000001;
	private final static double FXC = 2.00276;
	private final static double FXC2 = 1.11072;
	private final static double FYC = 0.49931;
	private final static double FYC2 = 1.41421356237309504880;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double theta, th1, c;
		int i;

		theta = lpphi;
		if (Math.abs(Math.abs(lpphi) - MapMath.HALFPI) < EPS)
			out.x = 0.;
		else {
			c = Math.sin(theta) * Math.PI;
			for (i = NITER; i > 0; --i) {
				theta -= th1 = (theta + Math.sin(theta) - c) /
					(1. + Math.cos(theta));
				if (Math.abs(th1) < EPS) break;
			}
			theta *= 0.5;
			out.x = FXC * lplam / (1. / Math.cos(lpphi) + FXC2 / Math.cos(theta));
		}
		out.y = FYC * (lpphi + FYC2 * Math.sin(theta));
		return out;
	}

	/**
	 * Returns true if this projection is equal area
	 */
	public boolean isEqualArea() {
		return true;
	}

	public boolean hasInverse() {
		return false;
	}

	public String toString() {
		return "Boggs Eumorphic";
	}

}
