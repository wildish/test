//
//  DegreeUnit.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class DegreeUnit extends Unit {
	
	static final long serialVersionUID = -3212757578604686538L;
	
	private static AngleFormat format = new AngleFormat(AngleFormat.ddmmssPattern, true);
	
	public DegreeUnit() {
		super("degree", "degrees", "deg", 1);
	}
	
	public double parse(String s) throws NumberFormatException {
		try {
			return format.parse(s).doubleValue();
		}
		catch (java.text.ParseException e) {
			throw new NumberFormatException(e.getMessage());
		}
	}
	
	public String format(double n) {
		return format.format(n)+" "+abbreviation;
	}
	
	public String format(double n, boolean abbrev) {
		if (abbrev)
			return format.format(n)+" "+abbreviation;
		return format.format(n);
	}
	
	public String format(double x, double y, boolean abbrev) {
		if (abbrev)
			return format.format(x)+"/"+format.format(y)+" "+abbreviation;
		return format.format(x)+"/"+format.format(y);
	}
}

