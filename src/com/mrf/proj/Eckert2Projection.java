//
//  Eckert2Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class Eckert2Projection extends Projection {

	private final static double FXC = 0.46065886596178063902;
	private final static double FYC = 1.44720250911653531871;
	private final static double C13 = 0.33333333333333333333;
	private final static double ONEEPS = 1.0000001;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.x = FXC * lplam * (out.y = Math.sqrt(4. - 3. * Math.sin(Math.abs(lpphi))));
		out.y = FYC * (2. - out.y);
		if ( lpphi < 0.) out.y = -out.y;
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.x = xyx / (FXC * ( out.y = 2. - Math.abs(xyy) / FYC) );
		out.y = (4. - out.y * out.y) * C13;
		if (Math.abs(out.y) >= 1.) {
			if (Math.abs(out.y) > ONEEPS)	throw new ProjectionException("I");
			else
				out.y = out.y < 0. ? -MapMath.HALFPI : MapMath.HALFPI;
		} else
			out.y = Math.asin(out.y);
		if (xyy < 0)
			out.y = -out.y;
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Eckert II";
	}

}
