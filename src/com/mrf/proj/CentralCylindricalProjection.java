//
//  CentralCylindricalProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class CentralCylindricalProjection extends CylindricalProjection {

	@SuppressWarnings("unused")
	private double ap;

	private final static double EPS10 = 1.e-10;

	public CentralCylindricalProjection() {
		minLatitude = Math.toRadians(-80);
		maxLatitude = Math.toRadians(80);
	}
	
	public Point2D project(double lplam, double lpphi, Point2D out) {
		if (Math.abs(Math.abs(lpphi) - MapMath.HALFPI) <= EPS10) throw new ProjectionException("F");
		out.x = lplam;
		out.y = Math.tan(lpphi);
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.y = Math.atan(xyy);
		out.x = xyx;
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Central Cylindrical";
	}

}
