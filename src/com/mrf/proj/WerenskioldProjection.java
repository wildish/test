//
//  WerenskioldProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class WerenskioldProjection extends PutninsP4Projection {

	public WerenskioldProjection() {
		C_x = 1;
		C_y = 4.442882938;
	}

	public String toString() {
		return "Werenskiold I";
	}

}
