//
//  GoodeProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class GoodeProjection extends Projection {

	private final static double Y_COR = 0.05280;
	private final static double PHI_LIM = .71093078197902358062;

	private SinusoidalProjection sinu = new SinusoidalProjection();
	private MolleweideProjection moll = new MolleweideProjection();

	public Point2D project(double lplam, double lpphi, Point2D out) {
		if (Math.abs(lpphi) <= PHI_LIM)
			out = sinu.project(lplam, lpphi, out);
		else {
			out = moll.project(lplam, lpphi, out);
			out.y -= lpphi >= 0.0 ? Y_COR : -Y_COR;
		}
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		if (Math.abs(xyy) <= PHI_LIM)
			out = sinu.projectInverse(xyx, xyy, out);
		else {
			xyy += xyy >= 0.0 ? Y_COR : -Y_COR;
			out = moll.projectInverse(xyx, xyy, out);
		}
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Goode Homolosine";
	}

}
