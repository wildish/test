//
//  Murdoch3Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class Murdoch3Projection extends SimpleConicProjection {

	public Murdoch3Projection() {
		super( SimpleConicProjection.MURD3 );
	}

	public String toString() {
		return "Murdoch III";
	}

}
