//
//  Eckert1Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class Eckert1Projection extends Projection {

	private final static double FC = .92131773192356127802;
	private final static double RP = .31830988618379067154;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.x = FC * lplam * (1. - RP * Math.abs(lpphi));
		out.y = FC * lpphi;
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.y = xyy / FC;
		out.x = xyx / (FC * (1. - RP * Math.abs(out.y)));
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Eckert I";
	}

}
