//
//  CylindricalProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

/**
 * The superclass for all cylindrical projections.
 */
public class CylindricalProjection extends Projection {
	
	public boolean isRectilinear() {
		return true;
	}

	public String toString() {
		return "Cylindrical";
	}

}
