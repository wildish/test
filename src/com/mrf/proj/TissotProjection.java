//
//  TissotProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class TissotProjection extends SimpleConicProjection {

	public TissotProjection() {
		super( SimpleConicProjection.TISSOT );
	}

	public String toString() {
		return "Tissot";
	}

}
