//
//  ConicProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

/**
 * The superclass for all Conic projections.
 */
public class ConicProjection extends Projection {
	
	public String toString() {
		return "Conic";
	}

}
