//
//  Rectangle2D.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class Rectangle2D {
	public static final int OUT_LEFT = 1;
	public static final int OUT_TOP = 2;
	public static final int OUT_RIGHT = 4;
	public static final int OUT_BOTTOM = 8;

	public double x;

	public double y;

	public double width;

	public double height;

	public Rectangle2D(double x, double y, double w, double h) {
		setRect(x, y, w, h);
	}

	public Rectangle2D() {
		setRect(0, 0, 0, 0);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public boolean isEmpty() {
		return (width <= 0.0) || (height <= 0.0);
	}

	public void setRect(double x, double y, double w, double h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
	}

	public void setRect(Rectangle2D r) {
		this.x = r.getX();
		this.y = r.getY();
		this.width = r.getWidth();
		this.height = r.getHeight();
	}

	public int outcode(double x, double y) {
		int out = 0;
		if (this.width <= 0) {
			out |= OUT_LEFT | OUT_RIGHT;
		} else if (x < this.x) {
			out |= OUT_LEFT;
		} else if (x > this.x + this.width) {
			out |= OUT_RIGHT;
		}
		if (this.height <= 0) {
			out |= OUT_TOP | OUT_BOTTOM;
		} else if (y < this.y) {
			out |= OUT_TOP;
		} else if (y > this.y + this.height) {
			out |= OUT_BOTTOM;
		}
		return out;
	}

	
	public Rectangle2D getBounds2D() {
		return new Rectangle2D(x, y, width, height);
	}

	
	public String toString() {
		return getClass().getName() + "[x=" + x + ",y=" + y + ",w=" + width + ",h=" + height + "]";
	}

	
	public void add(double newx, double newy) {
        double x1 = Math.min(getMinX(), newx);
        double x2 = Math.max(getMaxX(), newx);
        double y1 = Math.min(getMinY(), newy);
        double y2 = Math.max(getMaxY(), newy);
        setRect(x1, y1, x2 - x1, y2 - y1);
    }

   public void add(Point2D pt) {
        add(pt.getX(), pt.getY());
    }
    
    
    public void add(Rectangle2D r) {
        double x1 = Math.min(getMinX(), r.getMinX());
        double x2 = Math.max(getMaxX(), r.getMaxX());
        double y1 = Math.min(getMinY(), r.getMinY());
        double y2 = Math.max(getMaxY(), r.getMaxY());
        setRect(x1, y1, x2 - x1, y2 - y1);
    }
    
    public double getMinX()
    {
    	return this.x;
    }
    public double getMinY()
    {
    	return this.y;
    }
    
    public double getMaxX()
    {
    	return this.x+width;
    }
    
    public double getMaxY()
    {
    	return this.y + height;
    }
}
