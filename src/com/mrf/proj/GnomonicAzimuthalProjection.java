//
//  GnomonicAzimuthalProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class GnomonicAzimuthalProjection extends AzimuthalProjection {
	
	public GnomonicAzimuthalProjection() {
		this(Math.toRadians(90.0), Math.toRadians(0.0));
	}

	public GnomonicAzimuthalProjection(double projectionLatitude, double projectionLongitude) {
		super(projectionLatitude, projectionLongitude);
		minLatitude = Math.toRadians(0);
		maxLatitude = Math.toRadians(90);
		initialize();
	}
	
	public void initialize() {
		super.initialize();
	}

	public Point2D project(double lam, double phi, Point2D xy) {
		double sinphi = Math.sin(phi);
		double cosphi = Math.cos(phi);
		double coslam = Math.cos(lam);

		switch (mode) {
		case EQUATOR:
			xy.y = cosphi * coslam;
			break;
		case OBLIQUE:
			xy.y = sinphi0 * sinphi + cosphi0 * cosphi * coslam;
			break;
		case SOUTH_POLE:
			xy.y = -sinphi;
			break;
		case NORTH_POLE:
			xy.y = sinphi;
			break;
		}
		if (Math.abs(xy.y) <= EPS10)
			throw new ProjectionException();
		xy.x = (xy.y = 1. / xy.y) * cosphi * Math.sin(lam);
		switch (mode) {
		case EQUATOR:
			xy.y *= sinphi;
			break;
		case OBLIQUE:
			xy.y *= cosphi0 * sinphi - sinphi0 * cosphi * coslam;
			break;
		case NORTH_POLE:
			coslam = -coslam;
		case SOUTH_POLE:
			xy.y *= cosphi * coslam;
			break;
		}
		return xy;
	}

	public Point2D projectInverse(double x, double y, Point2D lp) {
		double  rh, cosz, sinz;

		rh = MapMath.distance(x, y);
		sinz = Math.sin(lp.y = Math.atan(rh));
		cosz = Math.sqrt(1. - sinz * sinz);
		if (Math.abs(rh) <= EPS10) {
			lp.y = projectionLatitude;
			lp.x = 0.;
		} else {
			switch (mode) {
			case OBLIQUE:
				lp.y = cosz * sinphi0 + y * sinz * cosphi0 / rh;
				if (Math.abs(lp.y) >= 1.)
					lp.y = lp.y > 0. ? MapMath.HALFPI : - MapMath.HALFPI;
				else
					lp.y = Math.asin(lp.y);
				y = (cosz - sinphi0 * Math.sin(lp.y)) * rh;
				x *= sinz * cosphi0;
				break;
			case EQUATOR:
				lp.y = y * sinz / rh;
				if (Math.abs(lp.y) >= 1.)
					lp.y = lp.y > 0. ? MapMath.HALFPI : - MapMath.HALFPI;
				else
					lp.y = Math.asin(lp.y);
				y = cosz * rh;
				x *= sinz;
				break;
			case SOUTH_POLE:
				lp.y -= MapMath.HALFPI;
				break;
			case NORTH_POLE:
				lp.y = MapMath.HALFPI - lp.y;
				y = -y;
				break;
			}
			lp.x = Math.atan2(x, y);
		}
		return lp;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Gnomonic Azimuthal";
	}

}
