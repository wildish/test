//
//  MBTFPPProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class MBTFPPProjection extends Projection {

	private final static double CS = .95257934441568037152;
	private final static double FXC = .92582009977255146156;
	private final static double FYC = 3.40168025708304504493;
	private final static double C23 = .66666666666666666666;
	private final static double C13 = .33333333333333333333;
	private final static double ONEEPS = 1.0000001;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.y = Math.asin(CS * Math.sin(lpphi));
		out.x = FXC * lplam * (2. * Math.cos(C23 * lpphi) - 1.);
		out.y = FYC * Math.sin(C13 * lpphi);
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.y = xyy / FYC;
		if (Math.abs(out.y) >= 1.) {
			if (Math.abs(out.y) > ONEEPS)	throw new ProjectionException("I");
			else	out.y = (out.y < 0.) ? -MapMath.HALFPI : MapMath.HALFPI;
		} else
			out.y = Math.asin(out.y);
		out.x = xyx / ( FXC * (2. * Math.cos(C23 * (out.y *= 3.)) - 1.) );
		if (Math.abs(out.y = Math.sin(out.y) / CS) >= 1.) {
			if (Math.abs(out.y) > ONEEPS)	throw new ProjectionException("I");
			else	out.y = (out.y < 0.) ? -MapMath.HALFPI : MapMath.HALFPI;
		} else
			out.y = Math.asin(out.y);
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "McBride-Thomas Flat-Polar Parabolic";
	}

}
