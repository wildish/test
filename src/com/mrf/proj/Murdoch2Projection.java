//
//  Murdoch2Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class Murdoch2Projection extends SimpleConicProjection {

	public Murdoch2Projection() {
		super( SimpleConicProjection.MURD2 );
	}

	public String toString() {
		return "Murdoch II";
	}

}
