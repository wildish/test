//
//  Ginsburg8Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class Ginsburg8Projection extends Projection {

	private final static double Cl = 0.000952426;
	private final static double Cp = 0.162388;
	private final static double C12 = 0.08333333333333333;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double t = lpphi * lpphi;

		out.y = lpphi * (1. + t * C12);
		out.x = lplam * (1. - Cp * t);
		t = lplam * lplam;
		out.x *= (0.87 - Cl * t * t);
		return out;
	}

	public String toString() {
		return "Ginsburg VIII (TsNIIGAiK)";
	}

}
