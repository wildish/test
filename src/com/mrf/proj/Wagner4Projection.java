//
//  Wagner4Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class Wagner4Projection extends MolleweideProjection {

	public Wagner4Projection() {
		super( MolleweideProjection.WAGNER4 );
	}

}
