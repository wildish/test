//
//  PutninsP5Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class PutninsP5Projection extends Projection {

	protected double A;
	protected double B;

	private final static double C = 1.01346;
	private final static double D = 1.2158542;

	public PutninsP5Projection() {
		A = 2;
		B = 1;
	}

	public Point2D project(double lplam, double lpphi, Point2D xy) {
		xy.x = C * lplam * (A - B * Math.sqrt(1. + D * lpphi * lpphi));
		xy.y = C * lpphi;
		return xy;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D lp) {
		lp.y = xyy / C;
		lp.x = xyx / (C * (A - B * Math.sqrt(1. + D * lp.y * lp.y)));
		return lp;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Putnins P5";
	}

}
