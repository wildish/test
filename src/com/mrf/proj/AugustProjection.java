//
//  AugustProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;




public class AugustProjection extends Projection {

	private final static double M = 1.333333333333333;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double t, c1, c, x1, x12, y1, y12;

		t = Math.tan(.5 * lpphi);
		c1 = Math.sqrt(1. - t * t);
		c = 1. + c1 * Math.cos(lplam *= .5);
		x1 = Math.sin(lplam) *  c1 / c;
		y1 =  t / c;
		out.x = M * x1 * (3. + (x12 = x1 * x1) - 3. * (y12 = y1 *  y1));
		out.y = M * y1 * (3. + 3. * x12 - y12);
		return out;
	}

	/**
	 * Returns true if this projection is conformal
	 */
	public boolean isConformal() {
		return true;
	}

	public boolean hasInverse() {
		return false;
	}

	public String toString() {
		return "August Epicycloidal";
	}

}
