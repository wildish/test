//
//  Wagner3Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class Wagner3Projection extends PseudoCylindricalProjection {
	
	private final static double TWOTHIRD = 0.6666666666666666666667;

	private double C_x;

	public Point2D project(double lplam, double lpphi, Point2D xy) {
		xy.x = C_x * lplam * Math.cos(TWOTHIRD * lpphi);
		xy.y = lpphi;
		return xy;
	}

	public Point2D projectInverse(double x, double y, Point2D lp) {
		lp.y = y;
		lp.x = x / (C_x * Math.cos(TWOTHIRD * lp.y));
		return lp;
	}

	public void initialize() {
		super.initialize();
		C_x = Math.cos(trueScaleLatitude) / Math.cos(2.*trueScaleLatitude/3.);
		es = 0.;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Wagner III";
	}

}
