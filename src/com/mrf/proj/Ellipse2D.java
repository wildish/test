//
//  Ellipse2D.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class Ellipse2D {
	 /**
     * The X coordinate of the upper-left corner of the
     * framing rectangle of this {@code Ellipse2D}.     
     */
    public double x;

    /**
     * The Y coordinate of the upper-left corner of the
     * framing rectangle of this {@code Ellipse2D}.     
     */
    public double y;

    /**
     * The overall width of this <code>Ellipse2D</code>.   
     */
    public double width;

    /**
     * The overall height of the <code>Ellipse2D</code>.    
     */
    public double height;

    /**
     * Constructs a new <code>Ellipse2D</code>, initialized to
     * location (0,&nbsp;0) and size (0,&nbsp;0).    
     */
    public Ellipse2D() {
    }

    /**
     * Constructs and initializes an <code>Ellipse2D</code> from the
     * specified coordinates.
     *
     * @param x the X coordinate of the upper-left corner
     *        of the framing rectangle
     * @param y the Y coordinate of the upper-left corner
     *        of the framing rectangle
     * @param w the width of the framing rectangle
     * @param h the height of the framing rectangle   
     */
    public Ellipse2D(double x, double y, double w, double h) {
        setFrame(x, y, w, h);
    }

    /**
     * {@inheritDoc}
     * @since 1.2
     */
    public double getX() {
        return x;
    }

    /**
     * {@inheritDoc}
     * @since 1.2
     */
    public double getY() {
        return y;
    }

    /**
     * {@inheritDoc}
     * @since 1.2
     */
    public double getWidth() {
        return width;
    }

    /**
     * {@inheritDoc}
     * @since 1.2
     */
    public double getHeight() {
        return height;
    }

    /**
     * {@inheritDoc}
     * @since 1.2
     */
    public boolean isEmpty() {
        return (width <= 0.0 || height <= 0.0);
    }

    /**
     * {@inheritDoc}
     * @since 1.2
     */
    public void setFrame(double x, double y, double w, double h) {
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
    }

    /**
     * {@inheritDoc}
     * @since 1.2
     */
    public Rectangle2D getBounds2D() {
        return new Rectangle2D(x, y, width, height);
    }
}
