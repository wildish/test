//
//  PerspectiveConicProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class PerspectiveConicProjection extends SimpleConicProjection {

	public PerspectiveConicProjection() {
		super( SimpleConicProjection.PCONIC );
	}

	public String toString() {
		return "Perspective Conic";
	}

}
