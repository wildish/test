//
//  VitkovskyProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class VitkovskyProjection extends SimpleConicProjection {

	public VitkovskyProjection() {
		super( SimpleConicProjection.VITK1 );
	}

}
