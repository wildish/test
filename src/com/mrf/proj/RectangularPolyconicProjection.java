//
//  RectangularPolyconicProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class RectangularPolyconicProjection extends Projection {

	private double phi0;
	@SuppressWarnings("unused")
	private double phi1;
	private double fxa;
	private double fxb;
	private boolean mode;

	private final static double EPS = 1e-9;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double fa;

		if (mode)
			fa = Math.tan(lplam * fxb) * fxa;
		else
			fa = 0.5 * lplam;
		if (Math.abs(lpphi) < EPS) {
			out.x = fa + fa;
			out.y = - phi0;
		} else {
			out.y = 1. / Math.tan(lpphi);
			out.x = Math.sin(fa = 2. * Math.atan(fa * Math.sin(lpphi))) * out.y;
			out.y = lpphi - phi0 + (1. - Math.cos(fa)) * out.y;
		}
		return out;
	}

	public void initialize() { // rpoly
		super.initialize();
/*
		if ((mode = (phi1 = Math.abs(pj_param(params, "rlat_ts").f)) > EPS)) {
			fxb = 0.5 * Math.sin(phi1);
			fxa = 0.5 / fxb;
		}
*/
	}

	public String toString() {
		return "Rectangular Polyconic";
	}

}
