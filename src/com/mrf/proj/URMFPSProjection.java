//
//  URMFPSProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class URMFPSProjection extends Projection {

	private final static double C_x = 0.8773826753;
	private final static double Cy = 1.139753528477;

	private double n = 0.8660254037844386467637231707;// wag1
	private double C_y;

	public URMFPSProjection() {
	}
	
	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.y = MapMath.asin(n * Math.sin(lpphi));
		out.x = C_x * lplam * Math.cos(lpphi);
		out.y = C_y * lpphi;
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		xyy /= C_y;
		out.y = MapMath.asin(Math.sin(xyy) / n);
		out.x = xyx / (C_x * Math.cos(xyy));
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public void initialize() { // urmfps
		super.initialize();
		if (n <= 0. || n > 1.)
			throw new ProjectionException("-40");
		C_y = Cy / n;
	}

	// Properties
	public void setN( double n ) {
		this.n = n;
	}
	
	public double getN() {
		return n;
	}
	
	public String toString() {
		return "Urmaev Flat-Polar Sinusoidal";
	}

}
