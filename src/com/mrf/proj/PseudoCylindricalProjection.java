//
//  PseudoCylindricalProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

/**
 * The superclass for all pseudo-cylindrical projections - eg. sinusoidal
 * These are projections where parallels are straight, but meridians aren't
 */
public class PseudoCylindricalProjection extends CylindricalProjection {

	public boolean isRectilinear() {
		return false;
	}

	public String toString() {
		return "Pseudo Cylindrical";
	}

}
