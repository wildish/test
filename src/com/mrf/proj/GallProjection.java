//
//  GallProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class GallProjection extends Projection {

	private final static double YF = 1.70710678118654752440;
	private final static double XF = 0.70710678118654752440;
	private final static double RYF = 0.58578643762690495119;
	private final static double RXF = 1.41421356237309504880;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.x = XF * lplam;
		out.y = YF * Math.tan(.5 * lpphi);
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.x = RXF * xyx;
		out.y = 2. * Math.atan(xyy * RYF);
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Gall (Gall Stereographic)";
	}

}
