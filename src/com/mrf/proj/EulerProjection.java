//
//  EulerProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class EulerProjection extends SimpleConicProjection {

	public EulerProjection() {
		super( SimpleConicProjection.EULER );
	}

	public String toString() {
		return "Euler";
	}

}
