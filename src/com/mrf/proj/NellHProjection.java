//
//  NellHProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;




public class NellHProjection extends Projection {

	private final static int NITER = 9;
	private final static double EPS = 1e-7;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.x = 0.5 * lplam * (1. + Math.cos(lpphi));
		out.y = 2.0 * (lpphi - Math.tan(0.5 *lpphi));
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		double V, c, p;
		int i;

		p = 0.5 * xyy;
		for (i = NITER; i > 0 ; --i) {
			c = Math.cos(0.5 * xyy);
			out.y -= V = (xyy - Math.tan(xyy/2) - p)/(1. - 0.5/(c*c));
			if (Math.abs(V) < EPS)
				break;
		}
		if (i == 0) {
			out.y = p < 0. ? -MapMath.HALFPI : MapMath.HALFPI;
			out.x = 2. * xyx;
		} else
			out.x = 2. * xyx / (1. + Math.cos(xyy));
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Nell-Hammer";
	}

}
