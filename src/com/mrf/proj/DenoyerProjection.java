//
//  DenoyerProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class DenoyerProjection extends Projection {

	public final static double C0 = 0.95;
	public final static double C1 = -.08333333333333333333;
	public final static double C3 = 0.00166666666666666666;
	public final static double D1 = 0.9;
	public final static double D5 = 0.03;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.y = lpphi;
		out.x = lplam;
		double aphi = Math.abs(lplam);
		out.x *= Math.cos((C0 + aphi * (C1 + aphi * aphi * C3)) *
			(lpphi * (D1 + D5 * lpphi * lpphi * lpphi * lpphi)));
		return out;
	}

	public boolean parallelsAreParallel() {
		return true;
	}

	public boolean hasInverse() {
		return false;
	}

	public String toString() {
		return "Denoyer Semi-elliptical";
	}

}
