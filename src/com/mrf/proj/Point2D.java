//
//  Point2D.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public  class Point2D {
	public double x;
	public double y;
	 public Point2D() {
     }

     /**
      * Constructs and initializes a <code>Point2D</code> with the
      * specified coordinates.
      *
      * @param x the X coordinate of the newly
      *          constructed <code>Point2D</code>
      * @param y the Y coordinate of the newly
      *          constructed <code>Point2D</code>
      * @since 1.2
      */
     public Point2D(double x, double y) {
         this.x = x;
         this.y = y;
     }

  
     public double getX() {
         return x;
     }

   
     public double getY() {
         return y;
     }

    
     public void setLocation(double x, double y) {
         this.x = x;
         this.y = y;
     }

    
     public String toString() {
         return "Point2D["+x+", "+y+"]";
     }
}
