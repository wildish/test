//
//  FaheyProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class FaheyProjection extends Projection {

	private final static double TOL = 1e-6;
	
	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.y = 1.819152 * ( out.x = Math.tan(0.5 * lpphi) );
		out.x = 0.819152 * lplam * asqrt(1 - out.x * out.x);
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.y = 2. * Math.atan(out.y /= 1.819152);
		out.x = Math.abs(out.y = 1. - xyy * xyy) < TOL ? 0. :
			xyx / (0.819152 * Math.sqrt(xyy));
		return out;
	}

	private double asqrt(double v) {
		return (v <= 0) ? 0. : Math.sqrt(v);
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Fahey";
	}

}
