//
//  NicolosiProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class NicolosiProjection extends Projection {

	private final static double EPS = 1e-10;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		if (Math.abs(lplam) < EPS) {
			out.x = 0;
			out.y = lpphi;
		} else if (Math.abs(lpphi) < EPS) {
			out.x = lplam;
			out.y = 0.;
		} else if (Math.abs(Math.abs(lplam) - MapMath.HALFPI) < EPS) {
			out.x = lplam * Math.cos(lpphi);
			out.y = MapMath.HALFPI * Math.sin(lpphi);
		} else if (Math.abs(Math.abs(lpphi) - MapMath.HALFPI) < EPS) {
			out.x = 0;
			out.y = lpphi;
		} else {
			double tb, c, d, m, n, r2, sp;

			tb = MapMath.HALFPI / lplam - lplam / MapMath.HALFPI;
			c = lpphi / MapMath.HALFPI;
			d = (1 - c * c)/((sp = Math.sin(lpphi)) - c);
			r2 = tb / d;
			r2 *= r2;
			m = (tb * sp / d - 0.5 * tb)/(1. + r2);
			n = (sp / r2 + 0.5 * d)/(1. + 1./r2);
			double x = Math.cos(lpphi);
			x = Math.sqrt(m * m + x * x / (1. + r2));
			out.x = MapMath.HALFPI * ( m + (lplam < 0. ? -x : x));
			double y = Math.sqrt(n * n - (sp * sp / r2 + d * sp - 1.) /
				(1. + 1./r2));
			out.y = MapMath.HALFPI * ( n + (lpphi < 0. ? y : -y ));
		}
		return out;
	}

	public String toString() {
		return "Nicolosi Globular";
	}

}
