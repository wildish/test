//
//  FoucautProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class FoucautProjection extends STSProjection {

	public FoucautProjection() {
		super( 2., 2., true );
	}
	
	public String toString() {
		return "Foucaut";
	}

}
