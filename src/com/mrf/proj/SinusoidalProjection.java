//
//  SinusoidalProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class SinusoidalProjection extends PseudoCylindricalProjection {
	
	public Point2D project(double lam, double phi, Point2D xy) {
		xy.x = lam * Math.cos(phi);
		xy.y = phi;
		return xy;
	}

	public Point2D projectInverse(double x, double y, Point2D lp) {
		lp.x = x / Math.cos(y);
		lp.y = y;
		return lp;
	}

	public double getWidth(double y) {
		return MapMath.normalizeLongitude(Math.PI) * Math.cos(y);
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Sinusoidal";
	}

}
