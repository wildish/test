//
//  Wagner7Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class Wagner7Projection extends Projection {

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double theta, ct, D;

		theta = Math.asin(out.y = 0.90630778703664996 * Math.sin(lpphi));
		out.x = 2.66723 * (ct = Math.cos(theta)) * Math.sin(lplam /= 3.);
		out.y *= 1.24104 * (D = 1/(Math.sqrt(0.5 * (1 + ct * Math.cos(lplam)))));
		out.x *= D;
		return out;
	}

	/**
	 * Returns true if this projection is equal area
	 */
	public boolean isEqualArea() {
		return true;
	}

	public String toString() {
		return "Wagner VII";
	}

}
