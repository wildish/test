//
//  KavraiskyVProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class KavraiskyVProjection extends STSProjection {

	public KavraiskyVProjection() {
		super( 1.50488, 1.35439, false );
	}
	
	public String toString() {
		return "Kavraisky V";
	}

}
