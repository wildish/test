//
//  NellProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class NellProjection extends Projection {

	private final static int MAX_ITER = 10;
	private final static double LOOP_TOL = 1e-7;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double k, V;
		int i;

		k = 2. * Math.sin(lpphi);
		V = lpphi * lpphi;
		out.y *= 1.00371 + V * (-0.0935382 + V * -0.011412);
		for (i = MAX_ITER; i > 0 ; --i) {
			out.y -= V = (lpphi + Math.sin(lpphi) - k) /
				(1. + Math.cos(lpphi));
			if (Math.abs(V) < LOOP_TOL)
				break;
		}
		out.x = 0.5 * lplam * (1. + Math.cos(lpphi));
		out.y = lpphi;
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.x = 2. * xyx / (1. + Math.cos(xyy));
		out.y = MapMath.asin(0.5 * (xyy + Math.sin(xyy)));
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Nell";
	}

}
