//
//  PutninsP5PProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class PutninsP5PProjection extends PutninsP5Projection {

	public PutninsP5PProjection() {
		A = 1.5;
		B = 0.5;
	}

	public String toString() {
		return "Putnins P5P";
	}

}
