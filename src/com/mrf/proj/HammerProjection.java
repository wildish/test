//
//  HammerProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class HammerProjection extends PseudoCylindricalProjection {

	private double w = 0.5;
	private double m = 1;
	private double rm;

	public HammerProjection() {
	}
	
	public Point2D project(double lplam, double lpphi, Point2D xy) {
		double cosphi, d;

		d = Math.sqrt(2./(1. + (cosphi = Math.cos(lpphi)) * Math.cos(lplam *= w)));
		xy.x = m * d * cosphi * Math.sin(lplam);
		xy.y = rm * d * Math.sin(lpphi);
		return xy;
	}

	public void initialize() {
		super.initialize();
		if ((w = Math.abs(w)) <= 0.)
			throw new ProjectionException("-27");
		else
			w = .5;
		if ((m = Math.abs(m)) <= 0.)
			throw new ProjectionException("-27");
		else
			m = 1.;
		rm = 1. / m;
		m /= w;
		es = 0.;
	}

	/**
	 * Returns true if this projection is equal area
	 */
	public boolean isEqualArea() {
		return true;
	}

	// Properties
	public void setW( double w ) {
		this.w = w;
	}
	
	public double getW() {
		return w;
	}
	
	public void setM( double m ) {
		this.m = m;
	}
	
	public double getM() {
		return m;
	}
	
	public String toString() {
		return "Hammer & Eckert-Greifendorff";
	}

}
