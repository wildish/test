//
//  Eckert5Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class Eckert5Projection extends Projection {

	private final static double XF = 0.44101277172455148219;
	private final static double RXF = 2.26750802723822639137;
	private final static double YF = 0.88202554344910296438;
	private final static double RYF = 1.13375401361911319568;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.x = XF * (1. + Math.cos(lpphi)) * lplam;
		out.y = YF * lpphi;
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.x = RXF * xyx / (1. + Math.cos( out.y = RYF * xyy));
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Eckert V";
	}

}
