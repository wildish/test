//
//  Murdoch1Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class Murdoch1Projection extends SimpleConicProjection {

	public Murdoch1Projection() {
		super( SimpleConicProjection.MURD1 );
	}

	public String toString() {
		return "Murdoch I";
	}

}
