//
//  LambertEqualAreaConicProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class LambertEqualAreaConicProjection extends AlbersProjection {

	public LambertEqualAreaConicProjection() {
		this( false );
	}

	public LambertEqualAreaConicProjection( boolean south ) {
		minLatitude = Math.toRadians(0);
		maxLatitude = Math.toRadians(90);
		projectionLatitude1 = south ? -MapMath.QUARTERPI : MapMath.QUARTERPI;
		projectionLatitude2 = south ? -MapMath.HALFPI : MapMath.HALFPI;
		initialize();
	}

	public String toString() {
		return "Lambert Equal Area Conic";
	}

}
