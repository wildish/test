//
//  STSProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





class STSProjection extends ConicProjection {
	private double C_x;
	private double C_y;
	private double C_p;
	private boolean tan_mode;

	protected STSProjection( double p, double q, boolean mode ) {
		es = 0.;
		C_x = q / p;
		C_y = p;
		C_p = 1/ q;
		tan_mode = mode;
		initialize();
	}
	
	public Point2D project(double lplam, double lpphi, Point2D xy) {
		double c;

		xy.x = C_x * lplam * Math.cos(lpphi);
		xy.y = C_y;
		lpphi *= C_p;
		c = Math.cos(lpphi);
		if (tan_mode) {
			xy.x *= c * c;
			xy.y *= Math.tan(lpphi);
		} else {
			xy.x /= c;
			xy.y *= Math.sin(lpphi);
		}
		return xy;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D lp) {
		double c;
		
		xyy /= C_y;
		c = Math.cos(lp.y = tan_mode ? Math.atan(xyy) : MapMath.asin(xyy));
		lp.y /= C_p;
		lp.x = xyx / (C_x * Math.cos(lp.y /= C_p));
		if (tan_mode)
			lp.x /= c * c;
		else
			lp.x *= c;
		return lp;
	}

	public boolean hasInverse() {
		return true;
	}

}
