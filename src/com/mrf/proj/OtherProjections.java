//
//  OtherProjections.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class OtherProjections {
	public Object createPlugin() {
		return new Object[] {
			new SimpleConicProjection(SimpleConicProjection.TISSOT),
			new SimpleConicProjection(SimpleConicProjection.MURD1),
			new SimpleConicProjection(SimpleConicProjection.MURD2),
			new SimpleConicProjection(SimpleConicProjection.MURD3),
			new SimpleConicProjection(SimpleConicProjection.PCONIC),
			new SimpleConicProjection(SimpleConicProjection.VITK1),
			new MolleweideProjection(MolleweideProjection.WAGNER4),
			new MolleweideProjection(MolleweideProjection.WAGNER5),
		};
	}
}
