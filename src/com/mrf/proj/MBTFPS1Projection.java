//
//  MBTFPS1Projection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;

public class MBTFPS1Projection extends STSProjection {

	public MBTFPS1Projection() {
		super( 1.48875, 1.36509, false );
	}
	
	public String toString() {
		return "McBryde-Thomas Flat-Polar Sine (No. 1)";
	}

}
