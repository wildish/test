//
//  LaskowskiProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class LaskowskiProjection extends Projection {

	private final static double a10 =  0.975534;
	private final static double a12 = -0.119161;
	private final static double a32 = -0.0143059;
	private final static double a14 = -0.0547009;
	private final static double b01 =  1.00384;
	private final static double b21 =  0.0802894;
	private final static double b03 =  0.0998909;
	private final static double b41 =  0.000199025;
	private final static double b23 = -0.0285500;
	private final static double b05 = -0.0491032;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double l2, p2;

		l2 = lplam * lplam;
		p2 = lpphi * lpphi;
		out.x = lplam * (a10 + p2 * (a12 + l2 * a32 + p2 * a14));
		out.y = lpphi * (b01 + l2 * (b21 + p2 * b23 + l2 * b41) +
			p2 * (b03 + p2 * b05));
		return out;
	}

	public String toString() {
		return "Laskowski";
	}

}
