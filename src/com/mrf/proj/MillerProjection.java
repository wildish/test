//
//  MillerProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class MillerProjection extends CylindricalProjection {

	public MillerProjection() {
	}
	
	public Point2D project(double lplam, double lpphi, Point2D out) {
		out.x = lplam;
		out.y = Math.log(Math.tan(MapMath.QUARTERPI + lpphi * .4)) * 1.25;
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		out.x = xyx;
		out.y = 2.5 * (Math.atan(Math.exp(.8 * xyy)) - MapMath.QUARTERPI);
		return out;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Miller Cylindrical";
	}

}
