//
//  CylindricalEqualAreaProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class CylindricalEqualAreaProjection extends Projection {

	private double qp;
	private double[] apa;
	private double trueScaleLatitude;

	public CylindricalEqualAreaProjection() {
		this(0.0, 0.0, 0.0);
	}
	
	public CylindricalEqualAreaProjection(double projectionLatitude, double projectionLongitude, double trueScaleLatitude) {
		this.projectionLatitude = projectionLatitude;
		this.projectionLongitude = projectionLongitude;
		this.trueScaleLatitude = trueScaleLatitude;
		initialize();
	}
	
	public void initialize() {
		super.initialize();
		double t = trueScaleLatitude;

		scaleFactor = Math.cos(t);
		if (es != 0) {
			t = Math.sin(t);
			scaleFactor /= Math.sqrt(1. - es * t * t);
			apa = MapMath.authset(es);
			qp = MapMath.qsfn(1., e, one_es);
		}
	}
	
	public Point2D project(double lam, double phi, Point2D xy) {
		if (spherical) {
			xy.x = scaleFactor * lam;
			xy.y = Math.sin(phi) / scaleFactor;
		} else {
			xy.x = scaleFactor * lam;
			xy.y = .5 * MapMath.qsfn(Math.sin(phi), e, one_es) / scaleFactor;
		}
		return xy;
	}

	public Point2D projectInverse(double x, double y, Point2D lp) {
		if (spherical) {
			double t;

			if ((t = Math.abs(y *= scaleFactor)) - EPS10 <= 1.) {
				if (t >= 1.)
					lp.y = y < 0. ? -MapMath.HALFPI : MapMath.HALFPI;
				else
					lp.y = Math.asin(y);
				lp.x = x / scaleFactor;
			} else throw new ProjectionException();
		} else {
			lp.y = MapMath.authlat(Math.asin( 2. * y * scaleFactor / qp), apa);
			lp.x = x / scaleFactor;
		}
		return lp;
	}

	public boolean hasInverse() {
		return true;
	}

	public boolean isRectilinear() {
		return true;
	}

}

