//
//  FoucautSinusoidalProjection.java
//  Sprayer
//
//  Created by admin on 1/30/15.
//  Copyright (c) 2015   MRF. All rights reserved.
//
package com.mrf.proj;





public class FoucautSinusoidalProjection extends Projection {
	private double n, n1;

	private final static int MAX_ITER = 10;
	private final static double LOOP_TOL = 1e-7;

	public Point2D project(double lplam, double lpphi, Point2D out) {
		double t;

		t = Math.cos(lpphi);
		out.x = lplam * t / (n + n1 * t);
		out.y = n * lpphi + n1 * Math.sin(lpphi);
		return out;
	}

	public Point2D projectInverse(double xyx, double xyy, Point2D out) {
		double V;
		int i;

		if (n != 0) {
			out.y = xyy;
			for (i = MAX_ITER; i > 0; --i) {
				out.y -= V = (n * out.y + n1 * Math.sin(out.y) - xyy ) /
					(n + n1 * Math.cos(out.y));
				if (Math.abs(V) < LOOP_TOL)
					break;
			}
			if (i == 0)
				out.y = xyy < 0. ? -MapMath.HALFPI : MapMath.HALFPI;
		} else
			out.y = MapMath.asin(xyy);
		V = Math.cos(out.y);
		out.x = xyx * (n + n1 * V) / V;
		return out;
	}

	public void initialize() {
		super.initialize();
//		n = pj_param(params, "dn").f;
		if (n < 0. || n > 1.)
			throw new ProjectionException("-99");
		n1 = 1. - n;
	}

	public boolean hasInverse() {
		return true;
	}

	public String toString() {
		return "Foucaut Sinusoidal";
	}

}
