package com.wildish.msg;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.ruanwei.interfacej.SmsClientKeyword;
import com.ruanwei.interfacej.SmsClientOverage;
import com.ruanwei.interfacej.SmsClientQueryStatusReport;
import com.ruanwei.interfacej.SmsClientSend;
import com.ruanwei.tool.SmsClientAccessTool;

public class MessageTest {
	public static String url = "http://www.duanxin10086.com/sms.aspx";
	public static String statusUrl = "http://www.duanxin10086.com/statusApi.aspx";
	public static String userid = "13509";
	public static String account = "k1668";
	public static String password = "123456";
	public static String checkWord = "这个字符串中是否包含了屏蔽字";
	
	
	@Before
	public void before() {
		
	}
	
	@Ignore
	@Test
	public void keyword() {
		String keyword = SmsClientKeyword.queryKeyWord(url, userid, account,
				password, checkWord);
		System.out.println(keyword);
	}

	@Ignore
	@Test
	public void overage() {
		String overage = SmsClientOverage.queryOverage(url, userid, account, password);
		System.out.println(overage);
	}
	
	/**
	 * 
 <?xml version="1.0" encoding="utf-8" ?>
 <returnsms>
 <returnstatus>Success</returnstatus>
 <message>ok</message>
 <remainpoint>4</remainpoint>
 <taskID>7814842</taskID>
 <successCounts>1</successCounts>
 </returnsms>
	 */
	@Ignore
	@Test
	public void send() {
		String response = SmsClientSend.sendSms(url, userid, account, password, "13353717502", "测试短信内容");
		System.out.println(response);
	}
	
	@Ignore
	@Test
	public void checkStatus() {
		String response = SmsClientQueryStatusReport.queryStatusReport(statusUrl, userid, account, password);
		System.out.println(response);
	}
	
	@After
	public void after() {
		
	}
}