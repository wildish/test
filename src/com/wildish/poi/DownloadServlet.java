package com.wildish.poi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

@WebServlet(name = "DownloadServlet", urlPatterns = "/download")
public class DownloadServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6216895987027501701L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.handleRequest(req, resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		Workbook wb = this.createWorkbook();
		String type = req.getParameter("t");

		if ("0".equals(type)) {
			// xls格式直接下载
			try {
				resp.reset(); // 必要地清除response中的缓存信息
				resp.setHeader("Content-Disposition", "attachment; filename=test.xls");
				// resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				resp.setContentType("application/vnd.ms-excel");

				javax.servlet.ServletOutputStream out = resp.getOutputStream();
				wb.write(out);
				out.flush();
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			// xlsx格式写入到文件中再下载
			File file = new File(System.getProperty(AppConfiguration.getWebRootName()) + "/workbook.xlsx");
			FileOutputStream fos = new FileOutputStream(file);
			wb.write(fos);
			fos.close();
			
//			resp.sendRedirect(req.getContextPath() + "/workbook.xlsx");
			resp.setHeader("Content-Disposition", "attachment; filename=test.xlsx");
			req.getRequestDispatcher("/workbook.xlsx").forward(req, resp);
		}

	}

	/**
	 * 输出一个简单内容的workbook
	 * 
	 * @return
	 */
	private Workbook createWorkbook() {
		// Workbook wb = new XSSFWorkbook();
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		Sheet sheet = wb.createSheet();

		Row r;
		Cell c, c2;
		for (int rownum = 0; rownum < 10; rownum++) {
			r = sheet.createRow(rownum);
			for (int cellnum = 0; cellnum < 10; cellnum += 2) {
				c = r.createCell(cellnum);
				c2 = r.createCell(cellnum + 1);

				c.setCellValue((double) rownum + (cellnum / 10));
				c2.setCellValue(createHelper.createRichTextString("Hello! " + cellnum));
			}
		}

		return wb;
	}
}