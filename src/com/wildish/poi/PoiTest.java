package com.wildish.poi;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PoiTest {
	private static final String location = "d:\\test.xls";
	
	
	@Before
	public void before() {
		
	}
	
	@Test
	public void readFile() {
		
	}
	
	/**
	 * 输出到.xls文件和.xlsx文件
	 */
	@Test
	public void createHSSFFile() throws Exception {
		Workbook[] wbs = new Workbook[] { new HSSFWorkbook(), new XSSFWorkbook() };
		for(int i=0; i<wbs.length; i++) {
		   Workbook wb = wbs[i];
		   CreationHelper createHelper = wb.getCreationHelper();

		   // create a new sheet
		   Sheet s = wb.createSheet();
		   
		   // create 2 cell styles
		   CellStyle cs = wb.createCellStyle();
		   CellStyle cs2 = wb.createCellStyle();
		   DataFormat df = wb.createDataFormat();

		   // create 2 fonts objects
		   Font f = wb.createFont();
		   Font f2 = wb.createFont();

		   // Set font 1 to 12 point type, blue and bold
		   f.setFontName("微软雅黑");
		   f.setFontHeightInPoints((short) 12);
		   f.setColor( IndexedColors.RED.getIndex() );
		   f.setBoldweight(Font.BOLDWEIGHT_BOLD);

		   // Set font 2 to 10 point type, red and bold
		   f2.setFontName("微软雅黑");
		   f2.setFontHeightInPoints((short) 10);
		   f2.setColor( IndexedColors.RED.getIndex() );
		   f2.setBoldweight(Font.BOLDWEIGHT_BOLD);

		   // Set cell style and formatting
		   cs.setFont(f);
		   cs.setDataFormat(df.getFormat("#,##0.0"));

		   // Set the other cell style and formatting
		   cs2.setBorderBottom(CellStyle.BORDER_THIN);
		   cs2.setDataFormat(df.getFormat("text"));
		   cs2.setFont(f2);

		   // Define a few rows
		   Row r;
		   Cell c, c2;
		   for(int rownum = 0; rownum < 30; rownum++) {
			   r = s.createRow(rownum);
			   for(int cellnum = 0; cellnum < 10; cellnum += 2) {
				   c = r.createCell(cellnum);
				   c2 = r.createCell(cellnum+1);
		   
				   c.setCellStyle(cs);
				   c.setCellValue((double)rownum + (cellnum/10));
				   c2.setCellStyle(cs2);
				   c2.setCellValue(createHelper.createRichTextString("Hello! " + cellnum));
			   }
		   }
		   
		   // Save
		   String filename = "workbook.xls";
		   if(wb instanceof XSSFWorkbook) {
		     filename = filename + "x";
		   }
		 
		   FileOutputStream out = new FileOutputStream(filename);
		   wb.write(out);
		   out.close();
		}
	}
	
	@After
	public void after() {
		
	}

	public static void main(String[] args) {
		try {
			// 对读取Excel表格标题测试
			InputStream is = new FileInputStream(location);
			Workbook wb = WorkbookFactory.create(is);
			Sheet sheet = wb.getSheetAt(0);
			for (Iterator<Row> rit = sheet.rowIterator(); rit.hasNext();) {
				// 行
				Row row = (Row) rit.next();
				for (Iterator<Cell> cit = row.cellIterator(); cit.hasNext();) {
					// 列
					Cell cell = (Cell) cit.next();
					System.out.println("位置:" + cell.getRowIndex() + ":" + cell.getColumnIndex());
					
					// 判断数据类型
                    switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        System.out.println(cell.getRichStringCellValue().getString());
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            System.out.println(cell.getDateCellValue());
                        } else {
                            System.out.println(cell.getNumericCellValue());
                        }
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        System.out.println(cell.getBooleanCellValue());
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        System.out.println(cell.getCellFormula());
                        break;
                    case Cell.CELL_TYPE_BLANK:
                    	System.out.println("cell is blank");
                    	break;
                    case Cell.CELL_TYPE_ERROR:
                    default:
                        System.out.println();
                        break;
                    }
				}
			}
		} catch (Exception e) {
			System.out.println("未找到指定路径的文件!");
			e.printStackTrace();
		}
	}
}