package com.wildish.qcloud;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qcloud.CloudClient;
import com.qcloud.PicCloud;
import com.qcloud.UploadResult;
import com.qcloud.fr.QcloudFrSDK;
import com.qcloud.fr.sign.QcloudFrSign;

/**
 * 腾讯云-万象优图-人脸识别测试类
 * 控制台-万象优图：https://console.qcloud.com/image/project
 * 控制台-人脸识别：http://console.qcloud.com/facedetect/guide
 * 万象优图API：https://www.qcloud.com/doc/product/275/3807
 * 人脸识别API：https://www.qcloud.com/doc/product/277/3528
 * @author Keith
 *
 */
public class PicTest {
	
	private static final int APP_ID_V2 = 10070395;
	private static final String APP_ID_V2STR = "10070395";
	private static final String SECRET_ID_V2 = "AKIDwpbEfuHbLLo6IOy5at0sSBNTO6cq31LS";
	private static final String SECRET_KEY_V2 = "60ihS8g598zmTHvIckFbo16zui4JOHEB";
	private static final String BUCKET = "caiwu";
	private static final int EXPIRED_SECONDS = 2592000;  // 30 days
	
	private CloudClient cc;
	private PicCloud pc;
	private QcloudFrSDK qcSDK;
	
	private File face1;
	private File face2;
	private File face3;
	private static final String UPLOAD_PATH1 = "fr/1.jpg";
	private static final String UPLOAD_PATH2 = "fr/2.jpg";
	private static final String UPLOAD_PATH3 = "fr/3.jpg";
	
	private static final String URI_IDCARDCOMPARE = "http://service.image.myqcloud.com/face/idcardcompare";
	
	@Before
	public void before() {
		pc = new PicCloud(APP_ID_V2, SECRET_ID_V2, SECRET_KEY_V2, BUCKET);
		cc = new CloudClient();
		
		qcSDK = iniQcFrSDK();
		
		face1 = new File(getClass().getResource("images/1.jpg").getPath());
		face2 = new File(getClass().getResource("images/2.jpg").getPath());
		face3 = new File(getClass().getResource("images/3.jpg").getPath());
		
	}
	
	private QcloudFrSDK iniQcFrSDK() {
		StringBuffer mySign = new StringBuffer("");
		QcloudFrSign.appSign(APP_ID_V2STR, SECRET_ID_V2, SECRET_KEY_V2,
						System.currentTimeMillis() / 1000 + EXPIRED_SECONDS, "", mySign);
		
		return new QcloudFrSDK(APP_ID_V2STR, mySign.toString());
	}
	
	/**
	 * 基本图片上传
	 * 文件已经存在会返回状态吗400，并且result是null
	 */
	@Ignore
	@Test
	public void test_upload() {
		// 上传一张图片
        //1. 直接指定图片文件名的方式
		UploadResult result1 = pc.upload(face1.getAbsolutePath(), UPLOAD_PATH1);
		UploadResult result2 = pc.upload(face2.getAbsolutePath(), UPLOAD_PATH2);
		UploadResult result3 = pc.upload(face3.getAbsolutePath(), UPLOAD_PATH3);
		if(result1 != null) {
            result1.print(); 
        }
		if(result2 != null) {
			result2.print(); 
		}
		if(result3 != null) {
			result3.print(); 
		}
//		url = http://web.image.myqcloud.com/photos/v2/10070395/caiwu/0/fr/1.jpg
//		downloadUrl = http://caiwu-10070395.image.myqcloud.com/fr/1.jpg
//		fileId = fr/1.jpg
//		width = 578
//		height = 680
//		url = http://web.image.myqcloud.com/photos/v2/10070395/caiwu/0/fr/2.jpg
//		downloadUrl = http://caiwu-10070395.image.myqcloud.com/fr/2.jpg
//		fileId = fr/2.jpg
//		width = 1456
//		height = 2592
//		url = http://web.image.myqcloud.com/photos/v2/10070395/caiwu/0/fr/3.jpg
//		downloadUrl = http://caiwu-10070395.image.myqcloud.com/fr/3.jpg
//		fileId = fr/3.jpg
//		width = 1456
//		height = 2592
	}
	
	/**
	 * 使用已上传的图片进行身份证照片对比
	 * https://www.qcloud.com/doc/product/275/6426
	 */
	@Test
	public void test_fr() {
		// create sign
        long expired = System.currentTimeMillis() / 1000 + 2592000;
        String sign = pc.getSign(expired);
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Authorization", sign);
        header.put("Host", "service.image.myqcloud.com");
        header.put("Content-Type", "application/json");
        
		JSONObject params = new JSONObject();
		params.put("appid", APP_ID_V2STR);
		params.put("bucket", BUCKET);
		params.put("idcard_number", "411303198705082114");
		params.put("idcard_name", "丁栋林");
		params.put("url", "http://caiwu-10070395.image.myqcloud.com/fr/1.jpg");
		
		try{
			String result = cc.post(URI_IDCARDCOMPARE, header, null, params.toString().getBytes());
			System.out.println("匹配结果：" + result);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 创建用户
	 */
	@Ignore
	@Test
	public void newPerson() {
		List<String> groupIds = new ArrayList<>();
		groupIds.add("0371");
		try{
			JSONObject result = qcSDK.NewPerson(face1.getAbsolutePath(), "4113031987050821114", groupIds, "keith");
			System.out.println("新建用户:" + result.toString());
		} catch(Exception e) {
			e.printStackTrace();
		}
/*
	{
	    "suc_face":1,
	    "codeDesc":"Success",
	    "code":0,
	    "group_ids":[
	        "0371"
	    ],
	    "session_id":"",
	    "face_id":"1759627506723389439",
	    "suc_group":1,
	    "message":"",
	    "errorcode":0,
	    "errormsg":"OK",
	    "person_id":"4113031987050821114"
	}
 */
	}
	
	/**
	 * 添加face
	 */
	@Ignore
	@Test
	public void addFace() {
		List<String> list = new ArrayList<>();
		list.add(face1.getAbsolutePath());
		list.add(face2.getAbsolutePath());
		try{
			JSONObject result = qcSDK.AddFace("4113031987050821114", list);
			System.out.println("添加face:" + result.toString());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		/*
		 {
		    "codeDesc":"Success",
		    "code":0,
		    "added":1,
		    "face_ids":[
		        "",
		        "1759632676401250303"
		    ],
		    "session_id":"",
		    "message":"",
		    "ret_codes":[
		        -1312,
		        0
		    ],
		    "errorcode":0,
		    "errormsg":"OK"
		}
		 */
	}
	
	/**
	 * 脸部验证
	 */
	@Ignore
	@Test
	public void comparePerson() {
		try{
			JSONObject result = qcSDK.FaceVerify(face3.getAbsolutePath(), "4113031987050821114");
			System.out.println("验证:" + result.toString());
			/*
			{
			    "ismatch":false,
			    "codeDesc":"Success",
			    "code":0,
			    "confidence":80.536499023438,
			    "session_id":"",
			    "message":"",
			    "errorcode":0,
			    "errormsg":"OK"
			}
			 */
			result = qcSDK.GetFaceIds("4113031987050821114");
			System.out.println("face ids:" + result.toString());
			/*
			{"codeDesc":"Success","code":0,"face_ids":["1759627506723389439","1759632676401250303"],"message":"","errorcode":0,"errormsg":"OK"}
			 */
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}