package com.wildish.xml;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class SaxXmlTest {
	private XMLReader xmlReader;
	private File map = new File("D:/workspaces/myeclipse2014/test_project/src/com/wildish/xml/Vicinity.map");

	@Before
	public void before() throws Exception {
		xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
	}

	@Test
	public void testSaxParser() throws Exception {
		FileInputStream fis = new FileInputStream(map);
		SaxMapHandler handler = new SaxMapHandler();
		xmlReader.setContentHandler(handler);
		xmlReader.parse(new InputSource(fis));
		
		List<LayerInfo> layerList = handler.getLayerList();
		
		System.out.println("读取到的layer数量是:" + layerList.size());
	}

	public class SaxMapHandler extends DefaultHandler {
		private List<LayerInfo> layerList;
		private String tempVal;
		private LayerInfo tempLayer;

		public SaxMapHandler() {
			layerList = new ArrayList<>();
		}

		public List<LayerInfo> getLayerList() {
			return layerList;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			tempVal = "";
			if (qName.equalsIgnoreCase("layer")) {
				tempLayer = new LayerInfo();
				int i = -1;
				if((i = attributes.getIndex("id")) > -1) {
					tempLayer.setId(attributes.getValue(i));
				}
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			tempVal = new String(ch, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if(qName.equalsIgnoreCase("layer")) {
				layerList.add(tempLayer);
			}
		}
	}
}