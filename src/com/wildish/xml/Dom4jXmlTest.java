package com.wildish.xml;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;

public class Dom4jXmlTest {
	private File map = new File("D:/workspaces/myeclipse2014/test_project/src/com/wildish/xml/Vicinity.map");
	
	@Before
	public void before() {
		
	}
	
	@Test
	public void test() throws Exception {
		SAXReader saxReader = new SAXReader();
        Document doc = saxReader.read(map);
        Element root = doc.getRootElement();
        
        List layerList = root.selectNodes("/doc/layers/*");
        System.out.println(layerList.size());
        
        for(Object o: layerList) {
        	Element layer = (Element) o;
        	
        	String id = layer.attributeValue("id", "null");
        	for(Iterator it = layer.nodeIterator(); it.hasNext();) {
        		Node node = (Node)it.next();
        		if(node.getNodeType() == Node.ELEMENT_NODE) {
        			if(node.getName().equals("style")) {
        				System.out.println(node.getStringValue());
        			}
        		}
        	}
        }
	}
}