package com.wildish.imageseg;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Comparator;

import javax.imageio.ImageIO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wildish.utils.ImageUtil;

public class ImageSegmentation {
	
	private String[] imagePaths;
	private String baseFolderPath;
	private int segSize;
	private String suffix;
	
	private File baseFolder;
	private File cropFolder;
	
	@Before
	public void doBefore() {
		baseFolderPath = "F:\\myeclipse workspaces\\test\\src\\com\\wildish\\imageseg";
		imagePaths = new String[]{"shouhuitu4.jpg"};
		
		segSize = 256;
		suffix = "jpg";
		
		baseFolder = new File(baseFolderPath);
		baseFolder.mkdirs();
		cropFolder = new File(baseFolder, "crop");
		cropFolder.mkdirs();
	}
	
	@Ignore
	@Test
	public void doSegmentation() throws Exception {
		// 将图片由小到大排序
		ArrayList<File> fileList = new ArrayList<File>();
		for(String path : imagePaths) {
			File file = new File(baseFolderPath, path);
			fileList.add(file);
		}
		
		fileList.sort(new Comparator<File>() {
			@Override
			public int compare(File f1, File f2) {
				try{
					BufferedImage bi1 = ImageIO.read(f1);
					BufferedImage bi2 = ImageIO.read(f2);
					
					return bi1.getWidth() - bi2.getWidth();
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				return 0;
			}
		});
		
		JSONObject result = new JSONObject();
		
		JSONObject size = new JSONObject();
		BufferedImage tempBi = ImageIO.read(fileList.get(0));
        size.put("x", tempBi.getWidth());
        size.put("y", tempBi.getHeight());
        JSONObject origin = new JSONObject();
        origin.put("x", 0);
        origin.put("y", 0);
        JSONObject multiplier = new JSONObject();
        multiplier.put("x", 1);
        multiplier.put("y", 1);
        JSONObject tile = new JSONObject();
        tile.put("size", segSize);
        tile.put("format", suffix);
        result.put("size", size);
        result.put("origin", origin);
        result.put("multiplier", multiplier);
        result.put("tile", tile);
        result.put("path", baseFolderPath);
        
        JSONArray tileArray = new JSONArray();
        
        int currentLevel = 1;
		// 切图
		for(int l = 0; l != fileList.size(); l++) {
			BufferedImage bi = ImageIO.read(fileList.get(l));
			int srcWidth = bi.getWidth();
	        int srcHeight = bi.getHeight();
	        
	        if(srcWidth <= segSize && srcHeight <= segSize) {
	        	continue;
	        }
	        
	        // 计算level
	        int level = Math.max(
	        		(int)Math.ceil(Math.log(Double.valueOf(srcWidth) / segSize)/Math.log(2)), 
	        		(int)Math.ceil(Math.log(Double.valueOf(srcHeight) / segSize)/Math.log(2)));
	        
	        // 根据level开始切图
	        for(int i = currentLevel; i <= level; i++) {
	        	int unitSize = (int)(Math.pow(2, level-i)) * segSize;
	        	int maxCol = (int)(srcWidth/unitSize) + 1;
	        	int maxRow = (int)(srcHeight/unitSize) + 1;
	        	
	        	JSONObject levelObj = new JSONObject();
	        	levelObj.put("level", i);
	        	
	        	JSONArray levelTileArray = new JSONArray();
	        	int j = 1;
	        	while(j <= maxCol) {
	        		// 切分第j列
	        		int x = (j-1) * unitSize;
	        		if(x < srcWidth) {
	        			int k = 1;
	        			while(k <= maxRow) {
	        				// 切分第j列第k行
	        				int y = (k-1) * unitSize;
	        				if(y < srcHeight) {
//	        					// 获取文件地址
//	        					String destFilePath = String.format("F:\\myeclipse workspaces\\test\\src\\com\\wildish\\imageseg\\crop\\%s\\%s\\%s.png", 
//	        							i, j, k);
//	        					File destFile = new File(destFilePath);
//	        					destFile.getParentFile().mkdirs();
//	        					
//	        					ImageUtil.cropImage(file.getAbsolutePath(), destFilePath, x, y, unitSize, unitSize, "png");
	        					
	        					JSONObject obj = new JSONObject();
	        					obj.put("col", j);
	        					obj.put("row", k);
	        					obj.put("size", unitSize);
	        					obj.put("path", fileList.get(l).getAbsolutePath());
	        					levelTileArray.add(obj);
	        				}
	        				k++;
	        			}
	        		}
	        		j++;
	        	}
	        	levelObj.put("levelTile", levelTileArray);
	        	tileArray.add(levelObj);
	        	
	        	currentLevel = level + 1;
	        }
		}
		result.put("tileArray", tileArray);
		
		System.out.println(result.toString());
	}
	
	@Ignore
	@Test
	public void testImageAdjust() {
		try{
			BufferedImage bi = ImageIO.read(new File(baseFolder, "c.png"));
			System.out.println("width:" + bi.getWidth() + ";height:" + bi.getHeight());
			BufferedImage newBi = ImageUtil.adjustImage(bi, 1000, 4000, true);
			
			ImageUtil.bufferedImageToFile(newBi, "d:/1.png");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void finalTest() {
		try{
			String result = ImageUtil.cropImages(baseFolderPath, imagePaths, segSize, suffix);
			System.out.println(result);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Ignore
	@Test
	public void testBackground() {
		try{
			BufferedImage tag = new BufferedImage(255, 255, BufferedImage.TYPE_INT_RGB);  
			
			// 设置底色
			Graphics2D graphics = tag.createGraphics();
			graphics.setPaint ( new Color ( 255, 255, 255 ) );
			graphics.fillRect ( 0, 0, tag.getWidth(), tag.getHeight() );
			
			byte[] tempBuffer = ImageUtil.newCompressImage(tag, (float) 1);
			
			File file = new File("F:\\myeclipse workspaces\\test\\src\\com\\wildish\\imageseg\\test.jpg");
			file.getParentFile().mkdirs();
			
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(tempBuffer);
			fos.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@After
	public void doAfter() {
		
	}
	
	public static void main(String[] args) {
	}
}