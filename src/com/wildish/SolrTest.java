package com.wildish;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public class SolrTest {
	private String solrServerPath = "http://localhost:8080/solr/collection1";
	 private HttpSolrServer server;
	
	@Before
	public void before() {
		server = new HttpSolrServer(solrServerPath);
		
		server.setSoTimeout(1000);
		server.setDefaultMaxConnectionsPerHost(100);
		server.setMaxTotalConnections(100);
		server.setFollowRedirects(false);
		server.setAllowCompression(true);
	}
	
	@Ignore
	@Test
	public void test(){
		// 创建doc文档
        SolrInputDocument doc = new SolrInputDocument();
        doc.addField("id", 0);
        doc.addField("title", "测试标题");
        doc.addField("timestamp", new java.util.Date());
        doc.addField("description", "这是一条测试内容");
        try {
            // 添加一个doc文档
            UpdateResponse response = server.add(doc);
            // commit后才保存到索引库
            server.commit();
            
            // 输出统计信息
            System.out.println("Query Time：" + response.getQTime());
            System.out.println("Elapsed Time：" + response.getElapsedTime());
            System.out.println("Status：" + response.getStatus());
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("--------------------------");

        query("测试");

        System.out.println("--------------------------");
	}
	
	@Test
	public void selectAll() {
		System.out.println("--------------------------");

        query("天气");

        System.out.println("--------------------------");
	}
	
	@After
	public void destory() {
	    server = null;
	    System.runFinalization();
	    System.gc();
	}
	
	/**
     * 获取查询结果，直接打印输出
     * @param query
     */
    public void query(String query) {
        SolrQuery params = new SolrQuery(query);
        // 表明最多显示5条
        params.set("rows", 5);
        try {
            QueryResponse response = server.query(params);
            SolrDocumentList list = response.getResults();
            System.out.println("总计：" + list.getNumFound() + "条，本批次:" + list.size() + "条");
            for (int i = 0; i < list.size(); i++) {
                SolrDocument doc = list.get(i);
                System.out.println(doc.get("title"));
                System.out.println(doc.get("content"));
                System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:dd").format(doc.get("timestamp")));
            }
        } catch (SolrServerException e) {
            e.printStackTrace();
        }
    }
}