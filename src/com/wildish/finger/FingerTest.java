package com.wildish.finger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wildish.utils.FileUtils;

public class FingerTest {

	private File finger = new File(
			"E:\\WORKSPACES\\myeclipse_workspace\\test_project\\src\\com\\wildish\\finger\\FGTemplet.mb");
	private File stringFiger = new File(
			"E:\\WORKSPACES\\myeclipse_workspace\\test_project\\src\\com\\wildish\\finger\\FGString.mb");
	private File fingerTemp = new File(
			"E:\\WORKSPACES\\myeclipse_workspace\\test_project\\src\\com\\wildish\\finger\\FG.tmp");

	private Connection con;
	private Statement statement;
	private PreparedStatement pstat;
	private String connectString = "jdbc:postgresql://192.168.33.101:5432/fingerdemo";

//	@Before
//	public void before() throws Exception {
//		Class.forName("org.postgresql.Driver");
//		con = DriverManager.getConnection(connectString, "postgres", "postgis");
//		statement = con.createStatement();
//	}

	@Ignore
	@Test
	public void test() {
		byte[] fingerdata = null;
		try {
			FileInputStream fis = new FileInputStream(finger);
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1);
			byte[] b = new byte[1];
			int n;
			while ((n = fis.read(b)) != -1) {
				bos.write(b, 0, n);
			}
			fis.close();
			bos.close();
			fingerdata = bos.toByteArray();
		} catch (Exception e) {

		}

		byte[] fingerdataString = null;
		try {
			FileInputStream fis = new FileInputStream(stringFiger);
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1);
			byte[] b = new byte[1];
			int n;
			while ((n = fis.read(b)) != -1) {
				bos.write(b, 0, n);
			}
			fis.close();
			bos.close();
			fingerdataString = bos.toByteArray();
		} catch (Exception e) {

		}

		System.out.println(fingerdata.equals(fingerdataString));
	}

	@Test
	public void testEncrypt() throws Exception {
		System.out.println(FileUtils.encodeFileToBase64(finger));
	}
	
	@Ignore
	@Test
	public void testFileToString() throws Exception {
		byte[] fingerdata = null;
		FileInputStream fis = new FileInputStream(finger);
		ByteArrayOutputStream bos = new ByteArrayOutputStream(1);
		byte[] b = new byte[1];
		int n;
		while ((n = fis.read(b)) != -1) {
			bos.write(b, 0, n);
		}
		fis.close();
		bos.close();
		fingerdata = bos.toByteArray();

		System.out.println(new String(fingerdata));
		System.out.println(new String(fingerdata, "UTF-8"));
		
		// 写入数据库测试
		pstat = con.prepareStatement("insert into t_finger (card_num, feature) values (?,?)");
		pstat.setString(1, "411303198705082114");
		pstat.setString(2, FileUtils.encodeFileToBase64(finger));
		pstat.executeUpdate();
	}
	
	@Ignore
	@Test
	public void testDecrypt() throws Exception {
		pstat = con.prepareStatement("select feature from t_finger where id = ?");
		pstat.setLong(1, 2232l);
		ResultSet resultset = pstat.executeQuery();
		String encryptString = "";
		while(resultset.next()) {
			encryptString = resultset.getString("feature");
		}
		resultset.close();
		
		byte[] fingerdata = FileUtils.decodeBase64ToFile(encryptString);
		FileOutputStream fos = new FileOutputStream(fingerTemp);
		fos.write(fingerdata);
		fos.close();
	}

	@After
	public void after() throws Exception {
		if(pstat != null) {
			pstat.close();
		}
		if(statement != null) {
			statement.close();
		}
		if (con != null) {
			con.close();
		}
	}
}