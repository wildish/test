package com.wildish.multithread;

import net.sourceforge.groboutils.junit.v1.MultiThreadedTestRunner;
import net.sourceforge.groboutils.junit.v1.TestRunnable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.wildish.utils.TqlDataUtil;

public class BaseTest {
	private TqlDataUtil tql;
	
	// 设置服务器地址
	private static final String url = "http://192.168.18.166:8080/tql_ps/d?d=ps_server&m=add.attention&openid=3&photo_id=15&type=1";
	// 线程数
	private static final int runnerCount = 20;
	
	@Before
	public void before() {
		tql = TqlDataUtil.getInstance(url);
	}

	@Test
	public void test() {
		TestRunnable runner = new TestRunnable() {
			@Override
			public void runTest() throws Throwable {
				long begin = System.currentTimeMillis();
				String result = tql.getTqlData(false);
				System.out.println("======运行时间:" + (System.currentTimeMillis() - begin) + "，运行结果：" + result);
			}
		};

		TestRunnable[] trs = new TestRunnable[runnerCount];
		for (int i = 0; i < runnerCount; i++) {
			trs[i] = runner;
		}

		// 用于执行多线程测试用例的Runner，将前面定义的单个Runner组成的数组传入
		MultiThreadedTestRunner mttr = new MultiThreadedTestRunner(trs);
		try {
			// 执行
			mttr.runTestRunnables();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}

	@After
	public void after() {

	}
}