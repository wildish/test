package com.wildish.imagecompress;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import com.wildish.utils.ImageUtil;

public class ImageCompressTest {
	
	private File image;
	private File destImage;
	
	@Before
	public void before() {
		image = new File("E:/WORKSPACES/myeclipse_workspace/test/src/com/wildish/imagecompress/test.jpg");
		destImage = new File("E:/WORKSPACES/myeclipse_workspace/test/src/com/wildish/imagecompress/after.jpg");
	}
	
	@Test
	public void test() {
		ImageUtil.compressImage(image, destImage, 0.3f);
	}
}