/**
 * Layout对象实现布局页面
 * 
 */
define(['jquery', 'config'], function ($, Config) {
	function Layout(mainPage) {
		this.page = {
			head: $(".main-heading"),
			body: {
				my: $(".main-body"),
				menu: $(".main-menu"),
				content: $(".main-content")
			},
			foot: $(".main-foot")
		};
		this.mainPage = mainPage;
		this.layout();
		this.initEvent();
	};
	
	Layout.prototype.layout = function() {
		var winH = this.mainPage.config.win.height;
		var winW = this.mainPage.config.win.width;
		var headHeight = 100;
		var footHeight = 50;
		this.page.head.height(headHeight);
		this.page.body.my.height(winH - headHeight - footHeight);
		this.page.body.my.css({
			'margin-left': '-15px',
			'margin-right': '-15px',
			'overflow': 'hidden'
		});
		this.page.body.menu.css({
			width: '200px',
			height: '100%',
			overflow: 'auto',
			float: 'left'
		});
		this.page.body.content.css({
			width: (this.page.body.my.width() - 200) + 'px',
			height: '100%',
			overflow: 'hidden',
			float: 'left'
		});
		this.page.foot.height(footHeight);
		
	};
	
	Layout.prototype.layoutContent = function() {
		var height = this.page.body.content.height();
		$("#content-body").css({
			height: height - $()
		});
	};
	
	Layout.prototype.initEvent = function() {
		var _this = this;
		$(window).resize(function() {
			_this.mainPage.config.measureWin();
			_this.layout();
			$("#content-body").height($(".main-content").height() - $("#breadcrumb").height() - $("#pagination").height());
		});
	};
	
	return Layout;
});