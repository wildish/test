﻿package com.wildish.webcompress;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 调用http://tool.css-js.com/提供的相关方法，进行js压缩、css压缩 <br>
 * 使用方法<br>
 * 1. 修改配置项<br>
 * 2. alt+shift+x调出快速运行窗口，按t即可。<br>
 * 
 * @author Keith
 */
public class WebCompressUtil {
	// --------------------------配置项-----------------------
	// 必填，目录地址
	private static final String basePath = "F:\\myeclipse workspaces\\test\\src\\com\\wildish\\webcompress\\test";
	// 必填，任务属性，js: 压缩js, css: 压缩css
	private static final String task = "js";
	// --------------------------配置项end-----------------------

	// --------------------------内部参数---------------------
	// js压缩链接
	private static final String jsCompressUrl = "http://tool.css-js.com/!nodejs3/uglify.do?action=compressor&loops=true&sequences=true&if_return=true&unused=true&evaluate=true&hoist_funs=true&comparisons=true&hoist_vars=true&conditionals=true&dead_code=true&booleans=true&properties=false&unsafe=false&join_vars=true";
	// css合并美化链接
	private static final String cssCompressUrl = "http://tool.css-js.com/!java/?type=css";
	private File baseFolder;
	private CloseableHttpClient httpclient;
	private StringBuffer fileInputBuffer = new StringBuffer();
	private StringBuffer fileOutputBuffer = new StringBuffer();

	@Before
	public void prepareFiles() throws Exception {
		baseFolder = new File(basePath);
		
		if (!baseFolder.isDirectory()) {
			throw new Exception("文件夹不存在，请重新配置baseFolder参数。");
		}

		httpclient = HttpClients.createDefault();
	}
	
	@Test
	public void compressFiles() throws Exception {
		File targetFile = new File(baseFolder.getParentFile(), baseFolder.getName() + ".js");
		File targetMinFile = new File(baseFolder.getParentFile(), baseFolder.getName() + ".min.js");
		
		File[] fileList = baseFolder.listFiles(
			new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith("." + task);
				}
			}
		);
	
		if (fileList.length == 0) {
			throw new Exception("文件夹下没有待压缩的文件。");
		}
		
		
		for (File file : fileList) {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			while ((tempString = reader.readLine()) != null) {
				fileInputBuffer.append(tempString + "\n");
			}
			reader.close();
		}
	
		String url = task.equals("css") ? cssCompressUrl : jsCompressUrl;
	
		HttpPost post = new HttpPost(url);
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("code", fileInputBuffer.toString()));
		post.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
	
		HttpResponse response = httpclient.execute(post);
		if (response.getStatusLine().getStatusCode() == 200) {
			fileOutputBuffer.append(EntityUtils.toString(response.getEntity()));
		}
	
		// 输出未压缩文件
		FileOutputStream fos = new FileOutputStream(targetFile);
		fos.write(fileInputBuffer.toString().getBytes());
		fos.close();
	
		// 输出压缩文件
		FileOutputStream fosMin = new FileOutputStream(targetMinFile);
		fosMin.write(fileOutputBuffer.toString().getBytes());
		fosMin.close();
		
		fileInputBuffer.setLength(0);
		fileOutputBuffer.setLength(0);
	}

	@After
	public void outputFiles() throws Exception {
		httpclient.close();
	}
}