/**
 * Layout对象实现布局页面
 * 
 */
define(['jquery', 'config'], function ($, Config) {
	function Layout(mainPage) {
		this.page = {
			head: $(".main-heading"),
			body: {
				my: $(".main-body"),
				menu: $(".main-menu"),
				content: $(".main-content")
			},
			foot: $(".main-foot")
		};
		this.mainPage = mainPage;
		this.layout();
		this.initEvent();
	};
	
	Layout.prototype.layout = function() {
		var winH = this.mainPage.config.win.height;
		var winW = this.mainPage.config.win.width;
		var headHeight = 100;
		var footHeight = 50;
		this.page.head.height(headHeight);
		this.page.body.my.height(winH - headHeight - footHeight);
		this.page.body.my.css({
			'margin-left': '-15px',
			'margin-right': '-15px',
			'overflow': 'hidden'
		});
		this.page.body.menu.css({
			width: '200px',
			height: '100%',
			overflow: 'auto',
			float: 'left'
		});
		this.page.body.content.css({
			width: (this.page.body.my.width() - 200) + 'px',
			height: '100%',
			overflow: 'hidden',
			float: 'left'
		});
		this.page.foot.height(footHeight);
		
	};
	
	Layout.prototype.layoutContent = function() {
		var height = this.page.body.content.height();
		$("#content-body").css({
			height: height - $()
		});
	};
	
	Layout.prototype.initEvent = function() {
		var _this = this;
		$(window).resize(function() {
			_this.mainPage.config.measureWin();
			_this.layout();
			$("#content-body").height($(".main-content").height() - $("#breadcrumb").height() - $("#pagination").height());
		});
	};
	
	return Layout;
});
/**
 * menu实现目录菜单的动态生成和目录菜单的事件相应工作
 */
define(['jquery'], function() {
	function Menu(mainPage, dataList) {
		this.mainPage = mainPage;
		this.dataList = dataList || [];
		this.selectName = null;
		this.target = $(".main-menu");
		this.initHtml();
		this.initEvent();
	};
	
	Menu.prototype.initHtml = function() {
		var html = "<ul class='list-group group-root'>";
		for(var i = 0; i < this.dataList.length; i++) {
			var menu = this.dataList[i];
			if(menu["child"] != undefined) {
				html += "<li id='menu_node_"+menu.id+"' class='list-group-item group-close' menuUrl='"+menu.url+"'>"+menu.name+"</li>";
				var childs = menu["child"];
				html += "<ul class='list-group group-leaf group-leaf-hide'>";
				for(var j = 0; j < childs.length; j++) {
					var child = childs[j];
					html += "<li id='menu_leaf_"+child.id+"' class='list-group-item' menuUrl='"+child.url+"'>"+child.name+"</li>";
				}
				html += "</ul>";
			} else {
				html += "<li id='menu_node_"+menu.id+"' class='list-group-item' menuUrl='"+menu.url+"'>"+menu.name+"</li>";
			}
			
		}
		html += "</ul>";
		this.target.html(html);
	};
	
	Menu.prototype.initEvent = function() {
		var _this = this;
		$(".group-root li").unbind("click").bind("click", function() {
			if($(this).hasClass("group-close")) { // 打开折叠
				var $openLeaf = $(".group-root>li.group-open").removeClass('group-open').addClass("group-close");
				$openLeaf.next().addClass("group-leaf-hide");
				$(this).removeClass("group-close").addClass("group-open");
				$(this).next().removeClass("group-leaf-hide");
			} else if($(this).hasClass("group-open")) {  // 折叠menu
				$(this).removeClass("group-open").addClass("group-close");
				if(!$(this).next().hasClass("group-leaf-hide")) {
					$(this).next().addClass("group-leaf-hide");
				}
			} else { //直接打开页面
				if($(this).attr("menuUrl") != "") {
					delete window.param;
					_this.selectName = new Array();
					if($(this).parent().prev().get(0) && $(this).parent().prev().get(0).tagName.toLowerCase() == "li") {
						_this.selectName[0] = {id: $(this).parent().prev().attr('id'), name: $(this).parent().prev().text()};
						_this.selectName[1] = {id: $(this).attr('id'), name: $(this).text()};
					} else {
						_this.selectName[0] = {id: $(this).attr('id'), name: $(this).text()};
					}
					_this.mainPage.breadcrumb.initHtml();
					_this.mainPage.loadContent($(this).attr("menuUrl"), {}, $("#content-body"));
				}
			}
			
		});
	};
	
	return Menu;
});
