package com.wildish.alipay;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.wildish.alipay.sign.Base64;
import com.wildish.alipay.sign.RSA;
import com.wildish.utils.HttpHelper;

public class PayCallback {

	private HashMap<String, String> callbackParams = new HashMap<String, String>();
	private String sign;
	private String signType;
	private static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
	private static String notifyVerifyUrl = "https://mapi.alipay.com/gateway.do?service=notify_verify";
	
	
	@Before
	public void before() {
		callbackParams.put("discount", "0.00");
		callbackParams.put("payment_type", "1");
		callbackParams.put("subject", "测试的商品");
		callbackParams.put("trade_no", "2016061521001004390299192570");
		callbackParams.put("buyer_email", "keithknight@qq.com");
		callbackParams.put("gmt_create", "2016-06-15 11:30:12");
		callbackParams.put("notify_type", "trade_status_sync");
		callbackParams.put("quantity", "1");
		callbackParams.put("out_trade_no", "061511295724026");
		callbackParams.put("seller_id", "2088121216596780");
		callbackParams.put("notify_time", "2016-06-15 11:30:13");
		callbackParams.put("body", "该测试商品的详细描述");
		callbackParams.put("trade_status", "TRADE_SUCCESS");
		callbackParams.put("is_total_fee_adjust", "N");
		callbackParams.put("total_fee", "0.01");
		callbackParams.put("gmt_payment", "2016-06-15 11:30:13");
		callbackParams.put("seller_email", "scbd@scbdlbs.com");
		callbackParams.put("price", "0.01");
		callbackParams.put("buyer_id", "2088702982954396");
		callbackParams.put("notify_id", "f4dd489993f7c919d5b4ce04cdb7447j0e");
		callbackParams.put("use_coupon", "N");
		callbackParams.put("sign_type", "RSA");
		callbackParams.put("sign", "NvaovJT4pXEEP9zPL3DOUAeOMEr99qOLkzzeSoULOlqg3IDlOvQDeA9Em2vddt7Zoioglmtm1DSsfDMTAS9YuGjLuPrG9yrQGTVG+a1SOTlUc4E+DRiEjKoqhVG/o3971205gwAJFb+3hFf5wiOb6rpl26ivluXQq+/ULj6KO5s=");
	}

	@Test
	public void Test() {
		// 验证notifyid是否有效
		Boolean notifyVerify = false;
		try{
			String result = HttpHelper.get(notifyVerifyUrl+"&partner="+callbackParams.get("seller_id")+"&notify_id=" + callbackParams.get("notify_id"));
			notifyVerify = Boolean.valueOf(result);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		if(!notifyVerify) {
			System.out.println("请求不是由支付宝发送的，退出处理流程");
			return;
		} else {
			System.out.println("确认请求真实性，继续处理流程");
		}
		
		// 获取验证字符串
		if(callbackParams.containsKey("sign")) {
			sign = callbackParams.get("sign");
			callbackParams.remove("sign");
			signType = callbackParams.get("sign_type");
			callbackParams.remove("sign_type");
		}
		
		String signStr = "";
		Object[] key = callbackParams.keySet().toArray();
		Arrays.sort(key);
		for (int i = 0; i < key.length; i++) {
			signStr += key[i] + "=" +callbackParams.get(key[i]);
			if(i != key.length - 1) {
				signStr += "&";
			}
		}
		
		// 验签
		Boolean verify = RSA.verify(signStr, sign, publicKey, "utf-8");
		if(verify) {
			System.out.println("验签成功，执行后续操作");
		} else {
			System.out.println("验签失败");
		}
	}
	
	public static void main(String[] args) {
		System.out.println(Base64.encode("123333331123123123123213213&123333331123123123123213213&123333331123123123123213213&123333331123123123123213213".getBytes()));
	}
}