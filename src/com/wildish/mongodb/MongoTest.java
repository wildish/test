package com.wildish.mongodb;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.codecs.BsonTimestampCodec;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoTest {

	private MongoClient client;
	
	@Before
	public void before() {
		CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
				CodecRegistries.fromCodecs(new UuidCodec(UuidRepresentation.STANDARD)),
				CodecRegistries.fromCodecs(new BsonTimestampCodec()),
				MongoClient.getDefaultCodecRegistry());
		MongoClientOptions options = MongoClientOptions.builder()
                .codecRegistry(codecRegistry)
                .writeConcern(new WriteConcern(1).withJournal(true))
                .build();
		client = new MongoClient(new ServerAddress("192.168.18.238"), options); 
	}
	
	@After
	public void after() {
		client.close();
	}
	
	@Test
	public void testTimestamp() {
	    MongoDatabase db = client.getDatabase("test");
	    MongoCollection<Document> collection = db.getCollection("test");
	    collection.insertOne(new Document("date", new Date()));
        Date date = (Date) collection.find().first().get("date");
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date));
        collection.drop();
	}
	
}