package com.hzoptical.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.hzoptical.util.AppUtils;
import com.wildish.core.AbstractController;

@Controller
@RequestMapping("/data")
public class DataSyncController extends AbstractController {
	
	@RequestMapping("/sync")
	public ResponseEntity<byte[]> syncData() {
		HttpServletResponse response = webSupport.getResponse().get();
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		PrintWriter pw = new PrintWriter(baos, true); 
		pw.println("This is a string"); 
		int i = -7; 
		pw.println(i); 
		double d = 4.5e-7; 
		pw.println(d); 
		
		pw.flush();
		pw.close();

		byte[] byteArray = baos.toByteArray();
		try {
			baos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return webSupport.getFileResponseEntity(byteArray, "text", "plain", "utf-8");
	}
	
	@RequestMapping("/upload")
	public ResponseEntity<String> uploadFile() {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) (webSupport.getRequest().get());
		MultiValueMap<String, MultipartFile> fileMap = multipartRequest.getMultiFileMap();
		Map<String, String[]> paramMap = multipartRequest.getParameterMap();

		StringBuffer sb = new StringBuffer();
		try {
			JSONArray files = new JSONArray();
			if (fileMap.size() > 0) {
				String basePath = webSupport.getAppBasePath();
				Iterator<String> iterator = fileMap.keySet().iterator();
				if (iterator.hasNext()) {
					String key = iterator.next();
					List<MultipartFile> list = fileMap.get(key);
					for (MultipartFile file : list) {
						InputStream is = file.getInputStream();

						String fullName = AppUtils.IMAGE_TEMP + AppUtils.getFilename(file.getOriginalFilename()).toLowerCase();

						File target = new File(basePath + fullName);
						if (!target.getParentFile().exists()) {
							target.getParentFile().mkdirs();
						}

						FileOutputStream fos = new FileOutputStream(target);

						byte[] arrayOfByte = new byte[1024];
						int k;
						while ((k = is.read(arrayOfByte)) != -1) {
							fos.write(arrayOfByte, 0, k);
						}
						is.close();
						fos.close();

						JSONObject fileObj = new JSONObject();
						fileObj.put("url", fullName);
						files.add(fileObj);
					}
				}
			}
			
			if(paramMap.containsKey("CKEditorFuncNum")){
				// 对CKeditor模块进行特殊处理
				String callback = webSupport.getParameter("CKEditorFuncNum");  
				sb.append("<script type=\"text/javascript\">");
		        sb.append("window.parent.CKEDITOR.tools.callFunction(" + callback + ", ctx + '" + files.getJSONObject(0).getString("url") + "', '');");      
		        sb.append("</script>");
			} else {
				sb.append("{\"list\":").append(files.toString()).append("}");
			}
		} catch (Exception e) {
			e.printStackTrace();
			if(paramMap.containsKey("CKEditorFuncNum")){
				// 对CKeditor模块进行特殊处理
				String callback = webSupport.getParameter("CKEditorFuncNum");  
				sb.append("<script type=\"text/javascript\">");
		        sb.append("window.parent.CKEDITOR.tools.callFunction(" + callback + ", '', '上传失败，请重试。');");      
		        sb.append("</script>");   
			} 
		}
		if(paramMap.containsKey("CKEditorFuncNum")){
			return webSupport.getResponseEntity(sb.toString(), "text", "html", "utf-8", HttpStatus.OK);
		} else {
			return webSupport.getTextResponseEntity(sb.toString());
		}
	}
}