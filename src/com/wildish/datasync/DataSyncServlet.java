package com.wildish.datasync;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;

@WebServlet(name = "DataSyncServlet", urlPatterns = "/scenic_sync")
public class DataSyncServlet extends HttpServlet {
	private static final long serialVersionUID = 3708426132180750998L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.handleRequest(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.handleRequest(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 执行post和get请求
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		
		String basePath = AppConfiguration.getBasePath();
		File tempFile = new File(basePath, "temp.zip");
        
        OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
        response.setContentType("application/octet-stream");
        toClient.write(FileUtils.readFileToByteArray(tempFile));
        toClient.flush();
        toClient.close();
	}
}