package com.wildish;

import java.net.URI;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class QQTest {
	private CloseableHttpClient client;
	
	@Before
	public void before() {
		client = HttpClients.custom()  
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY).build())
				.build(); 
	}
	
	@Ignore
	@Test
	public void test() throws Exception {
		HttpGet get = new HttpGet("http://wpa.qq.com/msgrd?v=3&uin=810998965&site=qq&menu=yes");
		HttpClientContext context = HttpClientContext.create();
		HttpResponse response = client.execute(get, context);
		
		if(response.getStatusLine().getStatusCode() == 200) {
			List<URI> list = context.getRedirectLocations();
			if(list == null) {
				System.out.println("未发现重定向。");
			} else {
				for(int i = 0; i != list.size(); i++) {
					System.out.println(list.get(i).getPath());
				}
			}
		}
	}
	
	@Ignore
	@Test
	public void test1() throws Exception {
		HttpGet get = new HttpGet("http://wpa.qq.com/msgrd?v=3&uin=810998965&site=qq&menu=yes");
		
		HttpResponse response = client.execute(get);
		
		if(response.getStatusLine().getStatusCode() == 200) {
			String text = EntityUtils.toString(response.getEntity());
			System.out.println(text);
			Pattern pattern = Pattern.compile("\"tencent://message/\\?(.*)\"");
			Matcher matcher = pattern.matcher(text);
			String url = null;
			while(matcher.find()){
				url = matcher.group();
			}
			
			String sigT = null, sigU = null;
			if(url == null) {
				System.out.println("未匹配到正确的链接。");
			} else {
				System.out.println(url);
				for(String s : url.replaceAll("\"", "").split("\\\\u0026")){
					String[] pair = s.split("=");
					if(pair[0].equals("sigT")) {
						sigT = pair[1];
					}
					if(pair[0].equals("sigU")) {
						sigU = pair[1];
					}
				}
			}
			
			if(sigT != null && sigU != null) {
				System.out.println("sigT:" + sigT + "\nsigU:" + sigU);
			}
		}
	}
	
	@Test
	public void testRandom() {
		for(int i = 0; i != 5; i++)
			System.out.println(new Random().nextDouble());
	} 
}