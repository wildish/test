package com.wildish.xinge;

import java.util.Map;

import com.tencent.xinge.Message;

public enum PushMessage {
    OVER_SPEED("超速报警", "%s于%s速度达到%s公里/小时，请关注", PushMessageType.TYPE_OVER_SPEED),
    OUT_PROVINCE("出省报警", "%s于%s已驶出四川省，请关注", PushMessageType.TYPE_OUT_PROVINCE), 
    EMERGENCY("紧急报警", "%s于%s在%s附近发出紧急报警，请关注", PushMessageType.TYPE_EMERGENCY), 
    NOTIFY_APPLY("审批通知", "%s申请%s用车，目的地%s，请审批", PushMessageType.TYPE_NOTIFY_APPLY),
    NOTIFY_APPLY_FURTHER("审批通知", "%s%s申请%s用车，目的地%s，请审批", PushMessageType.TYPE_NOTIFY_APPLY_FURTHER),
    NOTIFY_SUCCESS("审批通过", "%s%s的用车已审批通过", PushMessageType.TYPE_NOTIFY_SUCCESS),
    NOTIFY_REJECT("审批驳回", "%s%s的用车未通过审批，请查看", PushMessageType.TYPE_NOTIFY_REJECT),
    NOTIFY_TASK("任务通知", "司机%s，车辆%s有新的出车任务，请在列表中查看", PushMessageType.TYPE_NOTIFY_TASK),
    NOTIFY_BACK("返回通知", "司机%s，车辆%s确认完成任务，已经返回", PushMessageType.TYPE_NOTIFY_BACK),
    NOTIFY_COMMENT("评价通知", "%s%s的用车已归还，请进行评价", PushMessageType.TYPE_NOTIFY_COMMENT);
    
    // 成员变量
    private String title;
    private String description;
    private int type;

    // 构造方法
    private PushMessage(String title, String description, int type) {
        this.title = title;
        this.description = description;
        this.type = type;
    }

    public static String getName(int type) {
        for (PushMessage c : PushMessage.values()) {
            if (c.getType() == type) {
                return c.title;
            }
        }
        return null;
    }
    
    public static String getTitle(int type) {
    	for (PushMessage c : PushMessage.values()) {
    		if (c.getType() == type) {
    			return c.title;
    		}
    	}
    	return null;
    }
    
    public static String getDescription(int type) {
    	for (PushMessage c : PushMessage.values()) {
    		if (c.getType() == type) {
    			return c.description;
    		}
    	}
    	return null;
    }
    
    public static Message createPushMessage(int type, String content, Map<String, Object> params) {
    	if(!PushMessage.isVailidType(type)) {
    		return null;
    	}
    	Message msg = new Message();
		msg.setType(Message.TYPE_MESSAGE);
    	msg.setTitle(PushMessage.getTitle(type));
    	msg.setContent(content);
    	if(params != null) {
    		msg.setCustom(params);
    	}
    	return msg;
    }

    public static boolean isVailidType(int type) {
    	return type <= PushMessageType.TYPE_NOTIFY_COMMENT && type >= PushMessageType.TYPE_OVER_SPEED;
	}

	// getter && setter
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription(String... params) {
		return String.format(this.description, params);
	}
	
	public class PushMessageType {
		public static final int TYPE_OVER_SPEED = 1;
		public static final int TYPE_OUT_PROVINCE = 2;
		public static final int TYPE_EMERGENCY = 3;
		public static final int TYPE_NOTIFY_APPLY = 4;
		public static final int TYPE_NOTIFY_APPLY_FURTHER = 5;
		public static final int TYPE_NOTIFY_SUCCESS = 6;
		public static final int TYPE_NOTIFY_REJECT = 7;
		public static final int TYPE_NOTIFY_TASK = 8;
		public static final int TYPE_NOTIFY_BACK = 9;
		public static final int TYPE_NOTIFY_COMMENT = 10;
	}
}