package com.wildish.xinge;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tencent.xinge.Message;
import com.tencent.xinge.XingeApp;
import com.wildish.xinge.PushMessage.PushMessageType;

/**
 * 信鸽推送演示类
 * @author Keith
 */
public class PushTest {
	// FIXME 这个是临时的测试用的ID和KEY，正式上线需要重新申请
	// 包名com.longtu.gongche，详情参见设计方案
	private static final Long ID = 2100240090l;
	private static final String KEY = "8c983da057f613d3151d93b6be373464";
	
	// FIXME 测试接受推送消息的帐号
	private String account = "13353717502";
	
	private XingeApp app;
	@Before
	public void before() {
		app = new XingeApp(ID, KEY);
	}
	
	/**
	 * 测试向单个帐号推送透传信息
	 */
	@Ignore
	@Test
	public void customPush() {
		Message msg = new Message();
		msg.setType(Message.TYPE_MESSAGE);
		msg.setTitle("公车:测试消息");
		msg.setContent("测试消息内容");
		
		Map<String, Object> params = new HashMap<>();
		params.put("type", 0);
		msg.setCustom(params);
		
		JSONObject result = app.pushSingleAccount(0, account, msg);
		System.out.println(result.toString());
	}
	
	/**
	 * 发送{@link  PushMessage#OVER_SPEED}超速报警类型的消息
	 * 需要传递plateNum（车牌号）参数
	 * 跳转到车辆监控页面
	 */
	@Test
	public void customPush1() {
		String plateNum = "川A0001";
		String content = PushMessage.OVER_SPEED.getDescription(plateNum, "2016-10-25 18:43", "120");
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.OVER_SPEED.getType());
		params.put("plateNum", plateNum);
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_OVER_SPEED, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#OUT_PROVINCE}出省报警类型的消息
	 * 需要传递plateNum（车牌号）参数
	 * 跳转到车辆监控页面
	 */
	@Test
	public void customPush2() {
		String plateNum = "川A0001";
		String content = PushMessage.OUT_PROVINCE.getDescription(plateNum, "2016-10-25 18:43");
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.OUT_PROVINCE.getType());
		params.put("plateNum", plateNum);
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_OUT_PROVINCE, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#EMERGENCY}紧急报警类型的消息
	 * 需要传递plateNum（车牌号）参数
	 * 跳转到车辆监控页面
	 */
	@Test
	public void customPush3() {
		String plateNum = "川A0001";
		String content = PushMessage.EMERGENCY.getDescription(plateNum, "2016-10-25 18:43", "九寨沟");
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.EMERGENCY.getType());
		params.put("plateNum", plateNum);
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_EMERGENCY, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#NOTIFY_APPLY}审批通知类型的消息
	 * 跳转到机构审批员审批页面
	 */
	@Test
	public void customPush4() {
		String content = PushMessage.NOTIFY_APPLY.getDescription("张三", "2016-10-25", "九寨沟");
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.NOTIFY_APPLY.getType());
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_NOTIFY_APPLY, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#NOTIFY_APPLY_FURTHER}审批通知类型的消息
	 * 跳转到机关事务局审批人审批页面
	 */
	@Test
	public void customPush5() {
		String content = PushMessage.NOTIFY_APPLY_FURTHER.getDescription("销售部", "张三", "2016-10-25", "九寨沟");
		
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.NOTIFY_APPLY_FURTHER.getType());
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_NOTIFY_APPLY_FURTHER, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#NOTIFY_SUCCESS}审批通过类型的消息
	 * 跳转到申请列表
	 */
	@Test
	public void customPush6() {
		String content = PushMessage.NOTIFY_SUCCESS.getDescription("张三", "2016-10-25");
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.NOTIFY_SUCCESS.getType());
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_NOTIFY_SUCCESS, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#NOTIFY_REJECT}审批通过类型的消息
	 * 跳转到申请列表
	 */
	@Test
	public void customPush7() {
		String content = PushMessage.NOTIFY_REJECT.getDescription("张三", "2016-10-25");
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.NOTIFY_REJECT.getType());
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_NOTIFY_REJECT, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#NOTIFY_TASK}任务通知类型的消息
	 * 跳转到申请列表
	 */
	@Ignore
	@Test
	@Deprecated
	public void customPush8() {
		String content = PushMessage.NOTIFY_TASK.getDescription("张师傅", "川A0001");
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.NOTIFY_TASK.getType());
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_NOTIFY_TASK, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#NOTIFY_BACK}返回通知类型的消息
	 * 跳转到申请列表
	 */
	@Ignore
	@Test
	@Deprecated
	public void customPush9() {
		String content = PushMessage.NOTIFY_BACK.getDescription("张师傅", "川A0001");
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.NOTIFY_BACK.getType());
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_NOTIFY_BACK, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
	
	/**
	 * 发送{@link  PushMessage#NOTIFY_COMMENT}评价通知类型的消息
	 * 跳转到申请列表
	 */
	@Test
	public void customPush10() {
		String content = PushMessage.NOTIFY_COMMENT.getDescription("张三", "2016-10-25");
		
		Map<String, Object> params = new HashMap<>();
		params.put("type", PushMessage.NOTIFY_COMMENT.getType());
		
		Message msg = PushMessage.createPushMessage(PushMessageType.TYPE_NOTIFY_COMMENT, content, params);
		if(msg != null) {
			JSONObject result = app.pushSingleAccount(0, account, msg);
			System.out.println(result.toString());
		}
	}
}