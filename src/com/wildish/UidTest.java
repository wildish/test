package com.wildish;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.junit.Test;

public class UidTest {	
	@Test
	public void getIdentifyingCode() {
		System.out.println(generateSequenceNo());
	}
	
	/**
	 * 时间格式生成序列
	 * 3位日期（36进制表示年月日）+ 3位36进制随机数
	 * @return String
	 */
	private static String generateSequenceNo() {
		//1. 生成36进制表示年月日
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String date = sdf.format(new Date());
		System.out.println(date.substring(2,4));
		String date36 = Long.toString(Long.valueOf(date.substring(2,4)), 36) + Long.toString(Long.valueOf(date.substring(4,6)), 36) + Long.toString(Long.valueOf(date.substring(6)), 36);
		
		//2. 获取3位36进制随机数
		Random random = new Random(new Date().getTime());
		int max = 36*36*36-1;
		int min = 36*36;
		int s = random.nextInt(max)%(max-min+1) + min;
		String random36 = Long.toString(s, 36);
		
		String result = "";
		for(int i = 0; i != 3; i++){
			result += String.valueOf(date36.charAt(i)) + String.valueOf(random36.charAt(i));
		}
		
		return result;
	}
}