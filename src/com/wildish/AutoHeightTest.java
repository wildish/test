package com.wildish;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class AutoHeightTest {
	// 默认行间距
	private static int DEFAULT_VSPACING = 2;
	// 需要使用的正则
	// 匹配中文，中文标点
	private static Pattern fontFullSize = Pattern
			.compile(
					"^[\u4E00-\u9FA5\u3010\u3011\u3002\uff1b\uff0c\uff1a\u201c\u201d\u2014\uff08\uff09\u3001\u2018\u2019\uFF01\uff1f\u300a\u300b\uFF1F\u2026]{1}$",
					Pattern.CASE_INSENSITIVE);

	/**
	 * 获取文字的总宽度
	 * @param text
	 * @param fontSize
	 * @return
	 */
	public static int getContentWidth(String text, int fontSize) {
		int rwidth = 0;
		for (int i = 0; i != text.length(); i++) {
			char c = text.charAt(i);
			Matcher m = fontFullSize.matcher(String.valueOf(c));
			int addition = fontSize / 2;
			if (m.find()) {
				addition = fontSize;
			}
			rwidth = rwidth + addition;
		}

		return rwidth;
	}

	/**
	 * 限制行宽的情况下获取文字总高度
	 * 
	 * 基本规则
	 * 	1. line-height = vspacing + fontsize
	 *  2. fontsize=中文宽=中文标点宽=英文宽*2=英文标点*2=空格*2
	 *  3. 缩进=fontsize*2
	 *  4. 斜体、粗体不影响高度计算
	 * 
	 * TODO 
	 * 		1. 待显示文本的格式化
	 * 
	 * 		2. 复杂页面表现形式的处理
	 * 			a.缩进
	 * 			b.回车
	 * 
	 * @param text
	 * @param width
	 * @param fontSize
	 * @return
	 */
	public static int getContentHeight(String text, int width, int fontSize) {
		int lineHeight = getVspacing(fontSize) + fontSize;
		int height = lineHeight;
		int rwidth = 0;
		for (int i = 0; i != text.length(); i++) {
			char c = text.charAt(i);
			Matcher m = fontFullSize.matcher(String.valueOf(c));
			int addition = fontSize / 2;
			if (m.find()) {
				addition = fontSize;
			}
			int temp = rwidth + addition;
			if (temp > width) {
				rwidth = addition;
				height = height + lineHeight;
			} else {
				rwidth = temp;
			}
		}

		return height;
	}
	
	/**
	 * 在不设置line-height的情况通过fontsize计算浏览器默认line-height<br>
	 * 
	 * vspacing遵循一定的表现规律，基本为fontsize每增加7px，spacing增加1
	 * 12-17为2
	 * 18-24为3
	 * 25-31为4
	 * 32为6---特例
	 * ...
	 * 
	 * @param fontSize
	 * @return
	 */
	private static Integer getVspacing(int fontSize) {
		Integer vspacing = DEFAULT_VSPACING;
		// 
		if(fontSize > 12) {
			vspacing = 2 + Double.valueOf(Math.floor((fontSize - 12 + 1) / 7)).intValue();
		}
		
		return vspacing;
	}

	
	
	public static void main(String[] args) {
		// Font的leading包含于Font的高度里面，这可能就是宽高不想等的原因
		/**
		 * 方法一 font-size 12 结果 单个字体 width:12 height:16 html中 width:16 height:16
		 * font-size 13 结果 单个字体 width:13 height:16 leading:2 html中 width:16
		 * height:16
		 */
		// Font font = new Font("宋体", Font.PLAIN, 12);
		// Canvas c = new Canvas();
		// FontMetrics fm = c.getFontMetrics(font);
		// System.out.println(fm.stringWidth("中;"));
		// System.out.println(fm.getHeight());
		// System.out.println(fm.getLeading());

		// 方法二
		// Font font = new Font("微软雅黑", Font.PLAIN, 12);
		// BufferedImage image = new BufferedImage(100, 100,
		// BufferedImage.TYPE_INT_ARGB);
		// Graphics2D graphics2D = image.createGraphics();
		// System.out.println(getFontRenderedHeight("中", font, graphics2D));

		System.out.println(getContentHeight("我爱北京天安门。 ", 100, 25));
	}

	private static final Character huiche = 13; // 回车

	/**
	 * 替换text中的预定义字符 &lt;br/&gt;替换回车<br/>
	 */
	private static String dealWithText(String text) {
		return text.replaceAll(Pattern.compile("<br>|<br/>", Pattern.CASE_INSENSITIVE).toString(), huiche.toString());
	}

	
	
	
	
	/*
	 * getFontRenderedHeight
	 * *************************************************************************
	 * Summary: Font metrics do not give an accurate measurement of the rendered
	 * font height for certain strings because the space between the ascender
	 * limit and baseline is not always fully used and descenders may not be
	 * present. for example the strings '0' 'a' 'f' and 'j' are all different
	 * heights from top to bottom but the metrics returned are always the same.
	 * If you want to place text that exactly fills a specific height, you need
	 * to work out what the exact height is for the specific string. This method
	 * achieves that by rendering the text and then scanning the top and bottom
	 * rows until the real height of the string is found.
	 */
	/**
	 * Calculate the actual height of rendered text for a specific string more
	 * accurately than metrics when ascenders and descenders may not be present
	 * <p>
	 * Note: this method is probably not very efficient for repeated measurement
	 * of large strings and large font sizes but it works quite effectively for
	 * short strings. Consider measuring a subset of your string value. Also
	 * beware of measuring symbols such as '-' and '.' the results may be
	 * unexpected!
	 * 
	 * @param string
	 *            The text to measure. You might be able to speed this process
	 *            up by only measuring a single character or subset of your
	 *            string i.e if you know your string ONLY contains numbers and
	 *            all the numbers in the font are the same height, just pass in
	 *            a single digit rather than the whole numeric string.
	 * @param font
	 *            The font being used. Obviously the size of the font affects
	 *            the result
	 * @param targetGraphicsContext
	 *            The graphics context the text will actually be rendered in.
	 *            This is passed in so the rendering options for anti-aliasing
	 *            can be matched.
	 * @return Integer - the exact actual height of the text.
	 * @author Robert Heritage [mrheritage@gmail.com]
	 */
	public static Integer getFontRenderedHeight(String string, Font font, Graphics2D targetGraphicsContext) {
		BufferedImage image;
		Graphics2D g;
		Color textColour = Color.white;

		// In the first instance; use a temporary BufferedImage object to render
		// the text and get the font metrics.
		image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		g = image.createGraphics();
		FontMetrics metrics = g.getFontMetrics(font);
		Rectangle2D rect = metrics.getStringBounds(string, g);

		// now set up the buffered Image with a canvas size slightly larger than
		// the font metrics - this guarantees that there is at least one row of
		// black pixels at the top and the bottom
		image = new BufferedImage((int) rect.getWidth() + 1, (int) metrics.getHeight() + 2, BufferedImage.TYPE_INT_RGB);
		g = image.createGraphics();

		// take the rendering hints from the target graphics context to ensure
		// the results are accurate.
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				targetGraphicsContext.getRenderingHint(RenderingHints.KEY_ANTIALIASING));
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				targetGraphicsContext.getRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING));

		g.setColor(textColour);
		g.setFont(font);
		g.drawString(string, 0, image.getHeight());

		// scan the bottom row - descenders will be cropped initially, so the
		// text will need to be moved up (down in the co-ordinates system) to
		// fit it in the canvas if it contains any. This may need to be done a
		// few times until there is a row of black pixels at the bottom.
		boolean foundBottom, foundTop = false;
		int offset = 0;
		do {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, image.getWidth(), image.getHeight());
			g.setColor(textColour);
			g.drawString(string, 0, image.getHeight() - offset);

			foundBottom = true;
			for (int x = 0; x < image.getWidth(); x++) {
				if (image.getRGB(x, image.getHeight() - 1) != Color.BLACK.getRGB()) {
					foundBottom = false;
				}
			}
			offset++;
		} while (!foundBottom);

		// Scan the top of the image downwards one line at a time until it
		// contains a non-black pixel. This loop uses the break statement to
		// stop the while loop as soon as a non-black pixel is found, this
		// avoids the need to scan the rest of the line
		int y = 0;
		do {
			for (int x = 0; x < image.getWidth(); x++) {
				if (image.getRGB(x, y) != Color.BLACK.getRGB()) {
					foundTop = true;
					break;
				}
			}
			y++;
		} while (!foundTop);

		return image.getHeight() - y;
	}

	/**
	 * 一个可以运行的例子
	 */
	JFrame frame;

	// public static void main(String[] args) {
	// new Test();
	// }

	public AutoHeightTest() {
		frame = new JFrame("test");
		frame.setSize(300, 300);
		addStuffToFrame();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frame.setVisible(true);
			}
		});
	}

	private void addStuffToFrame() {
		JPanel panel = new JPanel(new GridLayout(3, 1));
		final JLabel label = new JLabel();
		final JTextField tf = new JTextField();
		JButton b = new JButton("calc string width");
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FontMetrics fm = label.getFontMetrics(label.getFont());
				String text = tf.getText();
				int textWidth = fm.stringWidth(text);
				int height = fm.getHeight();
				label.setText("\"" + text + "\": width=" + textWidth + ";height=" + height);
			}
		});
		panel.add(label);
		panel.add(tf);
		panel.add(b);
		frame.setContentPane(panel);
	}

}