package com.wildish.mrfmap.servlet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

import MRF.common.Render.SVG.BuildSVGMapRequest;
import MRF.common.geometry.Point;

import com.wildish.mrfmap.servlet.CommonContext;
import com.wildish.mrfmap.servlet.MRFUtil;

public class MapTemplate extends BaseHandler {

	public String getSVGMap() {
		this.contentType = "image/svg+xml; charset=utf-8";
		try {
			String szMapId = this.parameter.optString("MAPID");
			String szBBOX = this.parameter.optString("BBOX");
			int nScreenWith = Integer
					.valueOf(this.parameter.optString("WIDTH")) <= 0 ? 500
					: Integer.valueOf(this.parameter.optString("WIDTH"));
			int nScreenHeight = Integer.valueOf(this.parameter
					.optString("HEIGHT")) <= 0 ? 500 : Integer
					.valueOf(this.parameter.optString("HEIGHT"));
			String strLayerIDs = this.parameter.optString("LAYERS");
			String strLayerVars = this.parameter.optString("LAYERVARS");
			String angle = this.parameter.optString("ANGLE");
			String strAction = this.parameter.has("ACTION") ? this.parameter
					.optString("ACTION") : "";
			String[] strGeoms = szBBOX.split("\\|");
			String[] strCoords = null;
			String strSpGeom = null;
			String strSpOperator = null;
			if (strGeoms.length > 2) {
				strCoords = strGeoms[0].split(",");
				strSpGeom = strGeoms[1];
				strSpOperator = strGeoms[2];
			} else if (strGeoms.length > 0) {
				strCoords = strGeoms[0].split(",");
			}
			double x1 = Double.parseDouble(strCoords[0]);
			double y1 = Double.parseDouble(strCoords[1]);
			double x2 = Double.parseDouble(strCoords[2]);
			double y2 = Double.parseDouble(strCoords[3]);
			double dOriginX = Math.min(x1, x2);
			double dOriginY = Math.max(y1, y2);
			double dScaleX = nScreenWith * 1.0 / Math.abs(x2 - x1);
			double dScaleY = -1.0 * nScreenHeight / Math.abs(y2 - y1);

			// get new origin point
			Point pt1 = new Point(x1, y1);
			Point pt2 = new Point(x1, y2);
			Point pt3 = new Point(x2, y1);
			Point pt4 = new Point(x2, y2);
			double offsetX = Math.abs(x1 - x2) / 2d;
			double offsetY = Math.abs(y1 - y2) / 2d;
			double centerX = (x1 + x2) / 2d;
			double centerY = (y1 + y2) / 2d;
			double angleD = Double.parseDouble(angle) * Math.PI / 180;
			pt1.X = centerX + Math.cos(angleD) * offsetX - Math.sin(angleD)
					* offsetY;
			pt1.Y = centerY + Math.sin(angleD) * offsetX + Math.cos(angleD)
					* offsetY;
			pt2.X = centerX - Math.cos(angleD) * offsetX - Math.sin(angleD)
					* offsetY;
			pt2.Y = centerY - Math.sin(angleD) * offsetX + Math.cos(angleD)
					* offsetY;
			pt3.X = centerX + Math.cos(angleD) * offsetX + Math.sin(angleD)
					* offsetY;
			pt3.Y = centerY + Math.sin(angleD) * offsetX - Math.cos(angleD)
					* offsetY;
			pt4.X = centerX - Math.cos(angleD) * offsetX + Math.sin(angleD)
					* offsetY;
			pt4.Y = centerY - Math.sin(angleD) * offsetX - Math.cos(angleD)
					* offsetY;

			// find the new origin point
			double[] arrayX = { pt1.X, pt2.X, pt3.X, pt4.X };
			double[] arrayY = { pt1.Y, pt2.Y, pt3.Y, pt4.Y };
			Arrays.sort(arrayX);
			Arrays.sort(arrayY);
			Point newOrigin = new Point(arrayX[0], arrayY[3]);
			Point newEnd = new Point(arrayX[3], arrayY[0]);

			BuildSVGMapRequest request = new BuildSVGMapRequest();
			if (!strAction.equalsIgnoreCase("getlayer")) {
				// FIXME
//				String szMapViewName = MRF.common.MapTemplate.Global
//						.getMapTemplateFilePath(mapName);
				String szMapViewName = CommonContext.MAP_FILE.getAbsolutePath();
				if (StringUtils.isBlank(szMapViewName)) {
					return "No map template";
				}
				request.initialize(szMapId, szMapViewName, dOriginX, dOriginY,
						dScaleX, dScaleY, nScreenWith, nScreenHeight,
						strSpGeom, strSpOperator, strLayerIDs, strLayerVars,
						angle, newOrigin, newEnd);
				request.buildSVG();
				return request.getResult();
			} else {
				return "<svg></svg>";
			}
		} catch (Exception ex) {
			return "<svg></svg>";
		}
	}

	@SuppressWarnings("unchecked")
	public String getMapView() {
		// String szMapId = this.parameter.optString("mapName");
		// String szMapViewName = MRF.common.MapTemplate.Global
		// .getMapTemplateFilePath(szMapId);
		//
		// if (StringUtil.IsNullOrEmpty(szMapViewName)) {
		// return "No map template";
		// }

		Document xmlDoc = null;
		try {
			xmlDoc = CommonContext.getDocumentByPath(CommonContext.MAP_FILE
					.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (xmlDoc == null) {
			return "{\"d\":{}}";
		}

		Element rootElement = xmlDoc.getRootElement();
		Element lyer = rootElement.element("layers");
		List<Element> layers = lyer.elements();
		HashMap<String, Object> tempDic = new HashMap<String, Object>();
		List<Attribute> attArray = rootElement.attributes();
		for (int i = 0; i < attArray.size(); i++) {
			String val = "";
			Attribute element = attArray.get(i);
			if (!StringUtils.isBlank(element.getName())) {
				if (!StringUtils.isBlank(element.getValue())) {
					val = element.getValue();
				}
				tempDic.put(element.getName(), val);
			}
		}
		StringBuilder result = new StringBuilder();
		result.append("{\"d\":{");
		result.append(" \"DisplayCrs\":\"\"");
		result.append(String.format(" ,\"Name\":\"%s\"", "scenicnet"));
		result.append(String.format(" ,\"DisplayUom\":\"%s\"",
				tempDic.get("uomName")));
		result.append(String.format(" ,\"HighX\":%s",
				Double.parseDouble(String.valueOf(tempDic.get("highx")))));
		result.append(String.format(" ,\"HighY\":%s",
				Double.parseDouble(String.valueOf(tempDic.get("highy")))));
		result.append(String.format(" ,\"LowX\":%s",
				Double.parseDouble(String.valueOf(tempDic.get("lowx")))));
		result.append(String.format(" ,\"LowY\":%s",
				Double.parseDouble(String.valueOf(tempDic.get("lowy")))));
		result.append(String.format(" ,\"MaxScale\":%s",
				Double.parseDouble(String.valueOf(tempDic.get("maxscale")))));
		result.append(String.format(" ,\"MinScale\":%s",
				Double.parseDouble(String.valueOf(tempDic.get("minscale")))));
		result.append(String.format(" ,\"MaxScale\":%s",
				Double.parseDouble(String.valueOf(tempDic.get("maxscale")))));
		result.append(String.format(" ,\"UomFactor\":%s",
				Double.parseDouble(String.valueOf(tempDic.get("uomFactor")))));
		result.append(" ,\"Layers\":[");
		boolean isFirstLayer = true;

		for (int i = 0; i < layers.size(); i++) {
			Element layer = layers.get(i);
			if (layer.attributes().size() == 0) {
				continue;
			}
			if (layer.getNodeType() != Element.ELEMENT_NODE) {
				continue;
			}
			Element ele = layer;
			boolean accessibility = MRFUtil.getAttrBoolWithDefault(ele,
					"Accessibility", true);
			if (!accessibility) {
				continue;
			}
			if (!isFirstLayer) {
				result.append(",");
			}
			result.append("{");
			result.append(String.format(" \"ID\":\"%s\"",
					ele.attributeValue("id")));
			result.append(String.format(" ,\"Name\":\"%s\"",
					ele.attributeValue("name")));
			result.append(String.format(" ,\"Accessibility\":\"%s\"",
					MRFUtil.getAttrValueWithDefault(ele, "Accessibility", "")));
			result.append(String.format(" ,\"LayerType\":\"%s\"",
					layer.getName()));
			result.append(String.format(" ,\"GroupName\":\"%s\"",
					MRFUtil.getAttrValueWithDefault(ele, "group", "")));
			result.append(String.format(" ,\"SubGroupName\":\"%s\"",
					MRFUtil.getAttrValueWithDefault(ele, "subGroup", "")));
			result.append(String.format(" ,\"Title\":\"%s\"",
					MRFUtil.getAttrValueWithDefault(ele, "title", "")));
			result.append(String.format(" ,\"SourceCRS\":\"%s\"",
					MRFUtil.getAttrValueWithDefault(ele, "crs", "")));
			result.append(String.format(" ,\"UomFactor\":%f",
					MRFUtil.getAttrDoubleWidthDefault(ele, "uomFactor", 0)));

			result.append(String.format(" ,\"Visible\":%s",
					MRFUtil.getTextWithDefault(ele, "visible", "true")));
			result.append(String.format(" ,\"Active\":\"%s\"",
					MRFUtil.getTextWithDefault(ele, "active", "false")));
			result.append(String.format(" ,\"Icon\":\"%s\"",
					MRFUtil.getTextWithDefault(ele, "displayIcon", "point")));
			result.append(String.format(" ,\"ScaleLow\":%s",
					MRFUtil.getTextDoubleWithDefault(ele, "scaleLow", 0)));
			result.append(String.format(" ,\"ScaleHigh\":%s",
					MRFUtil.getTextDoubleWithDefault(ele, "scaleHigh", 0)));
			result.append(String.format(" ,\"Highlight\":\"%s\"",
					MRFUtil.getTextWithDefault(ele, "highlight", "")));

			String tableName = "";
			String columnlist = "";
			Element dataSource = MRFUtil.getChildByName(ele, "dataSource");
			if (dataSource != null) {
				Element subDataSource = MRFUtil.getFirstChild(dataSource);
				if (subDataSource != null) {
					Element tableNode = subDataSource.element("table");
					if (tableNode != null) {
						tableName = tableNode.getText();
					}
					Element columnNode = subDataSource.element("columnlist");
					if (columnNode != null) {
						columnlist = columnNode.getTextTrim();
					}
				}
			}
			result.append(String.format(" ,\"TableName\":\"%s\"", tableName));
			result.append(String.format(" ,\"Fields\":\"%s\"", columnlist));
			result.append("}");
			isFirstLayer = false;
		}

		result.append("]}}");
		return result.toString();
	}
}
