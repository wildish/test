package com.wildish.mrfmap.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

@WebServlet(name = "CommonServlet", urlPatterns = "/d")
public class CommonServlet extends HttpServlet {
	private static final long serialVersionUID = 3751402881851825215L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.handleRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		this.handleRequest(request, response);
	}

	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String result = "";
		String contentType = "text/json";
		try{
			String className = request.getParameter("c");
			String methodName = request.getParameter("m");
			if(StringUtils.isBlank(className)) {
				throw new Exception("class param has not been defined.");
			}
			if(StringUtils.isBlank(className)) {
				throw new Exception("method param has not been defined.");
			}
			
			JSONObject paramsObj = parseUrlParameters(request.getQueryString());
			BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			String data = buffer.toString();
			if(StringUtils.isNotBlank(data)){
				JSONObject dataObj = JSONObject.fromObject(URLDecoder.decode(data, "UTF-8"));
				
				Iterator it = dataObj.keys();
				while (it.hasNext()) {
					String key = (String) it.next();
					Object value = dataObj.get(key);
					paramsObj.put(key, String.valueOf(value));
				}
			}
			System.out.println("======>request params:" + paramsObj.toString());
			
			String fullClassName = String.format("com.wildish.mrfmap.servlet" + ".%s", className);
            Class<?> clazz = Class.forName(fullClassName);
            BaseHandler handler = (BaseHandler) clazz.newInstance();
            if(containMethod(clazz, methodName)) {
            	Method method = clazz.getMethod(methodName, new Class[]{});
            	handler.setParameter(paramsObj);
            	method.setAccessible(true);
            	result = (String) method.invoke(handler, new Object[]{});
            	contentType = handler.getContentType();
            }
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		if (StringUtils.isBlank(result)) {
			result = "{\"success\": false, \"code\": \"-1\", \"msg\": \"请求数据失败。\"}";
		}
		
		response.setHeader("Content-Type", contentType);
		response.setCharacterEncoding("UTF-8");
		response.getWriter().append(result.toString());
	}
	
	private boolean containMethod(Class<?> clazz, String methodName) {
		boolean result = false;
		for(int i = 0; i != clazz.getMethods().length; i++) {
			if(methodName.equals(clazz.getMethods()[i].getName())){
				result = true;
			}
		}
		return result;
	}
	
	private JSONObject parseUrlParameters(String urlPath) {
		JSONObject obj = new JSONObject();
		try {
			if (urlPath == null || urlPath.length() == 0) {
				return obj;
			}

			int paramIndex = urlPath.indexOf("?");
			String paramString = urlPath.substring(paramIndex + 1);
			paramString = java.net.URLDecoder.decode(paramString, "UTF-8");
			String[] paramArray = paramString.split("&");
			for (int i = 0; i < paramArray.length; i++) {
				String p = paramArray[i];
				String[] pArray = p.split("=");
				String pKey = pArray[0].toUpperCase();
				String pValue = "";
				if (pArray.length > 1) {
					pValue = pArray[1];
				}
				obj.put(pKey, pValue);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
	
}