package com.wildish.mrfmap.servlet;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

public class CommonContext {
	
	public static final File DATA_FOLDER = new File("F:\\myeclipse workspaces\\test\\WebRoot\\maptest\\data");
	public static final File MAP_FILE = new File(DATA_FOLDER, "scenicnet.map");
	public static final File IMAGELAYER_FOLDER = new File(DATA_FOLDER, "crop");
	
	public static Document getDocumentByPath(String path) {
        SAXReader reader = new SAXReader();
        Document doc = null;
        try {
            doc = reader.read(new File(path));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return doc;
    }
}