package com.wildish;

import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aliyun.openservices.HttpMethod;
import com.aliyun.openservices.oss.OSSClient;
import com.aliyun.openservices.oss.model.GeneratePresignedUrlRequest;
import com.aliyun.openservices.oss.model.ListObjectsRequest;
import com.aliyun.openservices.oss.model.OSSObjectSummary;
import com.aliyun.openservices.oss.model.ObjectListing;

public class OSSTest {
	private final String ossid = "EnQMQW5hXFCZKuBV";
	private final String osskey = "OAvD8cJPbMcN5UeTJ0qMmlGlVC532n";
	private final String bucketName = "ps-private";

	private OSSClient client = null;
	private OSSFolderInfo folder = null;

	@Before
	public void before() {
		client = new OSSClient(ossid, osskey);
		folder = new OSSFolderInfo(client, bucketName, "1/");
	}

	/**
	 * 目录大小测试
	 */
	@Ignore
	@Test
	public void test() {
		System.out.println("总大小" + folder.getSize());
	}
	
	/**
	 * 文件结构测试
	 */
	@Ignore
	@Test
	public void test1() {
		List<String> subFolders = folder.getSubFolder();
		if(subFolders.size() > 0) {
			for(String subFolder : subFolders) {
				System.out.println(subFolder);
			}
		} else {
			System.out.println("无子目录");
		}
		
		List<OSSObjectSummary> subFiles = folder.getFiles();
		if(subFiles.size() > 0) {
			System.out.println(folder.getKey() + "目录中一共有" + subFiles.size() + "个文件:");
			for(OSSObjectSummary subFile : subFiles) {
				System.out.println(subFile.getKey());
			}
		} else {
			System.out.println("无文件");
		}
	}

	/**
	 * Object过期时间测试
	 */
	@Ignore
	@Test
	public void test2() {
//		GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName, "dmhf/sys/2014/10/10/41e90afb-9624-4f3f-8c67-6c66d60d57bc.jpg", HttpMethod.GET);
		GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName, "12365749841598456.jpg", HttpMethod.GET);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, 30);

		request.setExpiration(calendar.getTime());
		
		URL url = client.generatePresignedUrl(request);
		System.out.println(url.toString());
	}
	
	@Ignore
	@Test
	public void testDelete(){
		OSSFolderInfo folder = new OSSFolderInfo(client, bucketName, "1/108");
		List<OSSObjectSummary> list = folder.getFiles();
		for(OSSObjectSummary summary : list) {
			System.out.println("删除文件：" + summary.getKey());
			client.deleteObject(bucketName, summary.getKey());
		}
	}
	
	@Ignore
	@Test
	public void testCopy() {
		try{
			client.copyObject("ps-private", "1/1.jpg", "ps-public", "share/1.jpg");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testHtml() {
		
	}
	
	public class OSSFolderInfo {
		private OSSClient client;
		private String bucketName;
		private String key;
		private Long size;
		private List<String> subFolder;
		private List<OSSObjectSummary> files;

		public OSSFolderInfo(OSSClient client, String bucketName, String key) {
			this.client = client;
			this.bucketName = bucketName;
			this.key = key;
		}
		
		public OSSClient getClient() {
			return client;
		}

		public void setClient(OSSClient client) {
			this.client = client;
		}

		public String getBucketName() {
			return bucketName;
		}

		public void setBucketName(String bucketName) {
			this.bucketName = bucketName;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public Long getSize() {
			size = calculateFolderSize(bucketName, key, null);
			return size;
		}

		public List<String> getSubFolder() {
			subFolder = this.getSubFolders(this.key);
			return subFolder;
		}

		public List<OSSObjectSummary> getFiles() {
			files = getFiles(this.key, null);
			return files;
		}
		
		/**
		 * 计算文件夹大小
		 * @param bucketName
		 * @param key
		 * @param marker
		 * @return
		 */
		private long calculateFolderSize(String bucketName, String key, String marker) {
			long totalSize = 0;
			ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName);
			// 设置请求参数
			listObjectsRequest.setDelimiter("/");
			if (!key.equals("/")) {
				listObjectsRequest.setPrefix(key);
			}
			if (StringUtils.isNotBlank(marker)) {
				listObjectsRequest.setMarker(marker);
			}
			listObjectsRequest.setMaxKeys(100);
			ObjectListing listing = client.listObjects(listObjectsRequest);
			if (listing.isTruncated()) {
				totalSize += calculateFolderSize(bucketName, key, listing.getNextMarker());
			}

			// 获取本次listing中所有object的文件大小
			for (OSSObjectSummary objectSummary : listing.getObjectSummaries()) {
				totalSize += objectSummary.getSize();
			}

			// 获取子目录的文件大小
			for (String commonPrefix : listing.getCommonPrefixes()) {
				totalSize += calculateFolderSize(bucketName, commonPrefix, null);
			}

			return totalSize;
		}
		
		private List<String> getSubFolders(String key) {
			ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName);
			// 设置请求参数
			listObjectsRequest.setDelimiter("/");
			if (!key.equals("/")) {
				listObjectsRequest.setPrefix(key);
			}
			ObjectListing listing = client.listObjects(listObjectsRequest);
			return listing.getCommonPrefixes();
		}
		
		private List<OSSObjectSummary> getFiles(String key, String marker) {
			ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName);
			// 设置请求参数
			listObjectsRequest.setDelimiter("/");
			if (!key.equals("/")) {
				listObjectsRequest.setPrefix(key);
			}
			if (StringUtils.isNotBlank(marker)) {
				listObjectsRequest.setMarker(marker);
			}
			listObjectsRequest.setMaxKeys(100);
			ObjectListing listing = client.listObjects(listObjectsRequest);
			// 获取文件
			List<OSSObjectSummary> list = listing.getObjectSummaries();
			if (listing.isTruncated()) {
				list.addAll(getFiles(key, listing.getNextMarker()));
			}
			// 获取子目录文件
			List<String> subFolders = this.getSubFolders(key);
			for(String subFolder : subFolders) {
				list.addAll(getFiles(subFolder, null));
			}
			
			return list;
		}
	}
}