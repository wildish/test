package com.wildish.utils;

import java.net.URLEncoder;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

public final class TqlDataUtil {
	private String baseUrl = null;
	private static TqlDataUtil instance;
	
	private TqlDataUtil (String url) {
		baseUrl = url;
	}
	
	public static TqlDataUtil getInstance(String url) {
		if(instance == null) {
			instance = new TqlDataUtil(url);
		}
		return instance;
	}
	
	public String getTqlData(Boolean https) throws Exception {
		return getTqlData(baseUrl, "", https);
	}
	
	/**
	 * 从tql中获取业务数据
	 */
	public String getTqlData(HashMap<String, String> values, Boolean https) throws Exception {
		String paramsStr = "";
		if (values != null) {
			for (String key : values.keySet()) {
				paramsStr += key + "=" + URLEncoder.encode(values.get(key), "UTF-8");
			}
			paramsStr = paramsStr.replace("?", "&");
			paramsStr = paramsStr.substring(1);
		}

		return getTqlData(paramsStr, https);
	}
	
	/**
	 * 从tql中获取数据
	 */
	public String getTqlData(String paramsStr, Boolean https) throws Exception {
		return getTqlData(baseUrl, paramsStr, https);
	}

	/**
	 * 获取数据
	 */
	private String getTqlData(String urlBase, String paramsStr, Boolean https) throws Exception {
		if(StringUtils.isBlank(urlBase)) {
			throw new Exception("未定义服务器url");
		}
		
		return HttpHelper.get(urlBase + paramsStr, https);
	}
}