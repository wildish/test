package com.wildish.utils;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

/**
 * 图像处理工具类。
 * 
 * @version 00 2010-1-15
 * @since 5
 * @author
 */
public class ImageUtil {

	/**
	 * 旋转图像。
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @param degree
	 *            旋转角度。
	 * @return 旋转后的图像。
	 */
	public static BufferedImage rotateImage(final BufferedImage bufferedImage,
			final int degree) {
		
		Graphics2D graphics2D = null;
		try {
			int width = bufferedImage.getWidth();
			int height = bufferedImage.getHeight();
			int type = bufferedImage.getColorModel().getTransparency();

			BufferedImage image = new BufferedImage(width, height, type);
			graphics2D = image.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);

			graphics2D.rotate(Math.toRadians(degree), width / 2, height / 2);
			graphics2D.drawImage(bufferedImage, 0, 0, null);
			return image;
		} finally {
			if (graphics2D != null) {
				graphics2D.dispose();
			}
		}
	}

	/**
	 * 将图像按照指定的比例缩放。 比如需要将图像放大20%，那么调用时scale参数的值就为20；如果是缩小，则scale值为-20。
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @param scale
	 *            缩放比例。
	 * @return 缩放后的图像。
	 */
	public static BufferedImage resizeImageScale(
			final BufferedImage bufferedImage, final double scale) {
		if (scale == 0) {
			return bufferedImage;
		}

		int width = bufferedImage.getWidth();
		int height = bufferedImage.getHeight();

		int newWidth = 0;
		int newHeight = 0;

		double nowScale = Math.abs(scale) / 100;
		if (scale > 0) {
			newWidth = (int) (width * (1 + nowScale));
			newHeight = (int) (height * (1 + nowScale));
		} else if (scale < 0) {
			newWidth = (int) (width * (1 - nowScale));
			newHeight = (int) (height * (1 - nowScale));
		}

		return resizeImage(bufferedImage, newWidth, newHeight);
	}

	/**
	 * 将图像缩放到指定的宽高大小,只能缩小
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @param width
	 *            新的宽度。
	 * @param height
	 *            新的高度。
	 * @return 缩放后的图像。
	 */
	public static BufferedImage resizeImage(final BufferedImage bufferedImage,
			int limitWidth, int limitHeight) {
		// int type = bufferedImage.getColorModel().getTransparency();
		limitWidth = Math.min(limitWidth, 1024);
		limitHeight = Math.min(limitHeight, 1024);
		int srcWidth = bufferedImage.getWidth();
		int srcHeight = bufferedImage.getHeight();
		int newWidth = srcWidth;
		int newHeight = srcHeight;
		if (newWidth <= limitWidth && newHeight <= limitHeight)
			return bufferedImage;
		// check whether source size exceed limit size
		if (limitWidth > 0 && srcWidth > limitWidth) {
			newHeight = (int) (srcHeight * 1.0 * limitWidth / srcWidth);
			newWidth = limitWidth;
		}
		if (limitHeight > 0 && srcHeight > limitHeight) {
			newHeight = limitHeight;
			newWidth = (int) (srcWidth * 1.0 * limitHeight / srcHeight);
		}
		// check whether adjusted size exceed limit size
		if (limitWidth > 0 && newWidth > limitWidth) {
			newHeight = (int) (newHeight * 1.0 * limitWidth / newWidth);
			newWidth = limitWidth;
		}
		if (limitHeight > 0 && newHeight > limitHeight) {
			newHeight = limitHeight;
			newWidth = (int) (newWidth * 1.0 * limitHeight / newHeight);
		}

		
		Graphics2D graphics2D = null;
		try {
			BufferedImage image = new BufferedImage(newWidth, newHeight,
					BufferedImage.TYPE_USHORT_565_RGB);

			graphics2D = image.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);

			graphics2D.drawImage(bufferedImage, 0, 0, newWidth, newHeight, 0, 0,
					bufferedImage.getWidth(), bufferedImage.getHeight(), null);
			return image;
		} finally {
			if (graphics2D != null) {
				graphics2D.dispose();
			}
		}
	}
	
	/**
	 * 将图像缩放到指定的宽高大小,只能缩小
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @param width
	 *            新的宽度。
	 * @param height
	 *            新的高度。
	 * @return 缩放后的图像。
	 */
	public static BufferedImage resizeImageStric(final BufferedImage bufferedImage,
			int limitWidth, int limitHeight) {
		Graphics2D graphics2D = null;
		try {
			BufferedImage image = new BufferedImage(limitWidth, limitWidth,
					BufferedImage.TYPE_USHORT_565_RGB);

			graphics2D = image.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);

			graphics2D.drawImage(bufferedImage, 0, 0, limitWidth, limitWidth, 0, 0,
					bufferedImage.getWidth(), bufferedImage.getHeight(), null);
			return image;
		} finally {
			if (graphics2D != null) {
				graphics2D.dispose();
			}
		}
	}

	/**
	 * 将图像缩放到指定的宽高大小，可以放大和缩小
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @param width
	 *            新的宽度。
	 * @param height
	 *            新的高度。
	 * @return 缩放后的图像。
	 */
	public static BufferedImage adjustImage(
			BufferedImage bufferedImage, int width, int height,
			boolean isKeepTransparent) {
		Graphics2D graphics2D = null;
		try {
			BufferedImage image = null;
			if (isKeepTransparent) {
				bufferedImage.getColorModel();
				image = new BufferedImage(width, height,
						BufferedImage.TYPE_4BYTE_ABGR_PRE);
			} else
				image = new BufferedImage(width, height,
						BufferedImage.TYPE_USHORT_565_RGB);
			graphics2D = image.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);

			graphics2D.drawImage(bufferedImage, 0, 0, width, height, 0,
					0, bufferedImage.getWidth(), bufferedImage.getHeight(),
					null);
			return image;
		} finally {
			if (graphics2D != null) {
				graphics2D.dispose();
			}
		}
	}

	/**
	 * 将图像水平翻转。
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @return 翻转后的图像。
	 */
	public static BufferedImage flipImage(final BufferedImage bufferedImage) {
		
		Graphics2D graphics2D=null;
		try {
			int width = bufferedImage.getWidth();
			int height = bufferedImage.getHeight();
			int type = bufferedImage.getColorModel().getTransparency();

			BufferedImage image = new BufferedImage(width, height, type);
			graphics2D = image.createGraphics();
			graphics2D.drawImage(bufferedImage, 0, 0, width, height, width, 0, 0,
					height, null);
			return image;
		} finally {
			if (graphics2D != null) {
				graphics2D.dispose();
			}
		}
	}

	/**
	 * 获取图片的类型。如果是 gif、jpg、png、bmp 以外的类型则返回null。
	 * 
	 * @param imageBytes
	 *            图片字节数组。
	 * @return 图片类型。
	 * @throws java.io.IOException
	 *             IO异常。
	 */
	public static String getImageType(final byte[] imageBytes)
			throws IOException {
		
		ImageInputStream imageInput = null;
		ByteArrayInputStream input =null;
		try {
			input = new ByteArrayInputStream(imageBytes);
			imageInput = ImageIO.createImageInputStream(input);
			Iterator<ImageReader> iterator = ImageIO.getImageReaders(imageInput);
			String type = null;
			if (iterator.hasNext()) {
				ImageReader reader = iterator.next();
				type = reader.getFormatName().toUpperCase();
			}
			return type;
		} finally {
			if (imageInput != null) {
				imageInput.close();
			}
			if (input!=null)
			{
				input.close();			
			}
		}
	}

	/**
	 * 验证图片大小是否超出指定的尺寸。未超出指定大小返回true，超出指定大小则返回false。
	 * 
	 * @param imageBytes
	 *            图片字节数组。
	 * @param width
	 *            图片宽度。
	 * @param height
	 *            图片高度。
	 * @return 验证结果。未超出指定大小返回true，超出指定大小则返回false。
	 * @throws java.io.IOException
	 *             IO异常。
	 */
	public static boolean checkImageSize(final byte[] imageBytes,
			final int width, final int height) throws IOException {
		BufferedImage image = byteToImage(imageBytes);
		int sourceWidth = image.getWidth();
		int sourceHeight = image.getHeight();
		if (sourceWidth > width || sourceHeight > height) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 将图像字节数组转化为BufferedImage图像实例。
	 * 
	 * @param imageBytes
	 *            图像字节数组。
	 * @return BufferedImage图像实例。
	 * @throws java.io.IOException
	 *             IO异常。
	 */
	public static BufferedImage byteToImage(final byte[] imageBytes)
			throws IOException {
		
		ByteArrayInputStream input = null;
		try {
			input = new ByteArrayInputStream(imageBytes);
			BufferedImage image = ImageIO.read(input);
			return image;
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}

	/**
	 * 将BufferedImage持有的图像转化为指定图像格式的字节数组。
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @param formatName
	 *            图像格式名称。
	 * @return 指定图像格式的字节数组。
	 * @throws java.io.IOException
	 *             IO异常。
	 */
	public static byte[] imageToByte(final BufferedImage bufferedImage,
			final String formatName) throws IOException {
		byte[] tempBuffer = null;
		if (formatName.compareToIgnoreCase("jpg") == 0
				|| formatName.compareToIgnoreCase("jpeg") == 0) {
			tempBuffer = ImageUtil
					.newCompressImage(bufferedImage, (float) 0.95);
		} else {
			//tempBuffer = ImageUtil.imageToByte(bufferedImage, "png");
			ByteOutputStream bos = new ByteOutputStream();
			ImageIO.write(bufferedImage, "PNG", bos);
			tempBuffer = bos.getBytes();
		}
		return tempBuffer;
	}

	/**
	 * 将图像以指定的格式进行输出，调用者需自行关闭输出流。
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @param output
	 *            输出流。
	 * @param formatName
	 *            图片格式名称。
	 * @throws java.io.IOException
	 *             IO异常。
	 */
	public static void writeImage(final BufferedImage bufferedImage,
			final OutputStream output, final String formatName)
			throws IOException {
		byte[] tempBuffer = null;
		if (formatName.compareToIgnoreCase("jpg") == 0
				|| formatName.compareToIgnoreCase("jpeg") == 0) {
			tempBuffer = ImageUtil
					.newCompressImage(bufferedImage, (float) 0.95);
		} else {
			tempBuffer = ImageUtil.imageToByte(bufferedImage, "png");
		}
		output.write(tempBuffer);
	}

	/**
	 * 通过 com.sun.image.codec.jpeg.JPEGCodec提供的编码器来实现图像压缩
	 * 
	 * @param image
	 * @param quality
	 * @return
	 */
	public static byte[] newCompressImage(BufferedImage image, float quality) {
		// 如果图片空，返回空
		if (image == null) {
			return null;
		}
		// 开始开始，写入byte[]
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(); // 取得内存输出流
		// 设置压缩参数
		JPEGEncodeParam param = JPEGCodec.getDefaultJPEGEncodeParam(image);
		param.setQuality(quality, false);
		// 设置编码器
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(
				byteArrayOutputStream, param);
		try {
			encoder.encode(image);
		} catch (Exception ef) {
			ef.printStackTrace();
		}
		return byteArrayOutputStream.toByteArray();

	}
	
	/**
	 * 压缩图片，只支持jpg，jpeg格式
	 * @param sourceFile
	 * @param destFile
	 * @param quality
	 */
	public static boolean compressImage(File sourceFile, File destFile, float quality) {
		boolean result = false;
		try{
			BufferedImage source = ImageIO.read(sourceFile);
			byte[] imageBytes = newCompressImage(source, quality);
			
			OutputStream os = new FileOutputStream(destFile);
			os.write(imageBytes);
			os.flush();
			os.close();
			result = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 压缩图片
	 * @param bytes
	 * @param destFile
	 * @param quality
	 * @return
	 */
	public static boolean compressImage(byte[] bytes, File destFile, float quality) {
		boolean result = false;
		try{
			BufferedImage source = byteToImage(bytes);
			byte[] imageBytes = newCompressImage(source, quality);
			
			OutputStream os = new FileOutputStream(destFile);
			os.write(imageBytes);
			os.flush();
			os.close();
			result = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 将图像以指定的格式进行输出，调用者需自行关闭输出流。
	 * 
	 * @param imageBytes
	 *            图像的byte数组。
	 * @param output
	 *            输出流。
	 * @param formatName
	 *            图片格式名称。
	 * @throws java.io.IOException
	 *             IO异常。
	 */
	public static void writeImage(final byte[] imageBytes,
			final OutputStream output, final String formatName)
			throws IOException {
		BufferedImage image = byteToImage(imageBytes);
		writeImage(image, output, formatName);
	}
	
	
	/**
	 * 图片剪裁
	 * @param srcImageFile	源地址
	 * @param dirImageFile 目标地址
	 * @param x				起点x坐标
	 * @param y				起点y坐标
	 * @param destWidth		剪裁宽度
	 * @param destHeight	剪裁高度
	 */
	public static void cropImage(String srcImageFile, String dirImageFile, 
			int x, int y, int destWidth, int destHeight, String suffix) {  
        try {  
            Image img;  
            ImageFilter cropFilter;  
            // 读取源图像  
            BufferedImage bi = ImageIO.read(new File(srcImageFile));  
            int srcWidth = bi.getWidth(); 		// 源图宽度  
            int srcHeight = bi.getHeight(); 	// 源图高度            
            if (srcWidth >= destWidth && srcHeight >= destHeight) {  
                Image image = bi.getScaledInstance(srcWidth, srcHeight, Image.SCALE_DEFAULT);  
                cropFilter = new CropImageFilter(x, y, destWidth, destHeight); 
                img = Toolkit.getDefaultToolkit().createImage(  
                        new FilteredImageSource(image.getSource(), cropFilter)); 
                int type = "png".equalsIgnoreCase(suffix)?BufferedImage.TYPE_INT_ARGB:BufferedImage.TYPE_INT_RGB;
                BufferedImage tag = new BufferedImage(destWidth, destHeight, type);  
                Graphics g = tag.getGraphics();

                // 绘制缩小后的图
                g.drawImage(img, 0, 0, null);   
                g.dispose();  
                
                // 输出为文件
                suffix = StringUtils.isBlank(suffix)?"JPEG":suffix.toUpperCase();
                
                byte[] tempBuffer = null;
        		if (suffix.compareToIgnoreCase("jpg") == 0
        				|| suffix.compareToIgnoreCase("jpeg") == 0) {
        			tempBuffer = ImageUtil
        					.newCompressImage(tag, (float) 1);
        		} else {
        			tempBuffer = ImageUtil.imageToByte(tag, "png");
        		}
        		
        		FileOutputStream fos = new FileOutputStream(new File(dirImageFile));
        		fos.write(tempBuffer);
        		fos.close();
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
	
	/** 
     * 彩色转为黑白 
     *  
     * @param source 图片源地址
     * @param result 保留图片地址
     */  
    public static void gray(String source, String result) {  
        try {  
            BufferedImage src = ImageIO.read(new File(source));  
            ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);  
            ColorConvertOp op = new ColorConvertOp(cs, null);  
            src = op.filter(src, null); 
            
            byte[] tempBuffer = ImageUtil.newCompressImage(src, (float) 1);
            FileOutputStream fos = new FileOutputStream(new File(result));
            fos.write(tempBuffer);
            fos.close();
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }
    
    /**
     * 复制图片
     * @param source 源地址
     * @param result 目标地址
     */
    public static void copyImage(String source, String result) {
    	try{		
    		File resultFile = new File(result);
    		if(!resultFile.getParentFile().exists()){
    			resultFile.getParentFile().mkdirs();
    		}
    		FileUtils.copyFile(new File(source), resultFile);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    /**
     * 获取本地图片宽高
     * @param source
     * @return
     */
    public static String getLocalImageSize(String source) {
    	String temp = "";
    	try{
    		BufferedImage image = ImageIO.read(new File(source));
    		temp = image.getWidth() + "x" + image.getHeight();
    	} catch(Exception e){
    		e.printStackTrace();
    		temp = null;
    	}
    	
    	return temp;
    }
    
    /**
     * 批量切图
     */
    public static String cropImages(String baseFolderPath, String[] images, int cropSize, String suffix) 
    		throws Exception {
    	File baseFolder = new File(baseFolderPath);
    	File cropFolder = new File(baseFolder, "crop");
    	baseFolder.mkdirs();
    	cropFolder.mkdirs();
    	
    	// 排序
    	ArrayList<File> fileList = new ArrayList<File>();
		for(String path : images) {
			File file = new File(baseFolderPath, path);
			fileList.add(file);
		}
		
		fileList.sort(new Comparator<File>() {
			@Override
			public int compare(File f1, File f2) {
				try{
					BufferedImage bi1 = ImageIO.read(f1);
					BufferedImage bi2 = ImageIO.read(f2);
					
					return bi1.getWidth() - bi2.getWidth();
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				return 0;
			}
		});
		
		// 组织结果输出
		JSONObject result = new JSONObject();
		
		JSONObject size = new JSONObject();
		BufferedImage tempBi = ImageIO.read(fileList.get(0));
        size.put("x", tempBi.getWidth());
        size.put("y", tempBi.getHeight());
        JSONObject origin = new JSONObject();
        origin.put("x", 0);
        origin.put("y", 0);
        JSONObject multiplier = new JSONObject();
        multiplier.put("x", 1);
        multiplier.put("y", 1);
        JSONObject tile = new JSONObject();
        tile.put("size", cropSize);
        tile.put("format", suffix);
        result.put("size", size);
        result.put("origin", origin);
        result.put("multiplier", multiplier);
        result.put("tile", tile);
        result.put("path", baseFolderPath);
        
        JSONArray tileArray = new JSONArray();
        int currentLevel = 1;
		// 切图
		for(int l = 0; l != fileList.size(); l++) {
			BufferedImage bi = ImageIO.read(fileList.get(l));
			int srcWidth = bi.getWidth();
	        int srcHeight = bi.getHeight();
	        
	        if(srcWidth <= cropSize && srcHeight <= cropSize) {
	        	continue;
	        }
	        
	        // 计算level
	        int level = Math.max(
	        		(int)Math.ceil(Math.log(Double.valueOf(srcWidth) / cropSize)/Math.log(2)), 
	        		(int)Math.ceil(Math.log(Double.valueOf(srcHeight) / cropSize)/Math.log(2)));
	        
	        // 根据level组织切图数据
	        for(int i = currentLevel; i <= level; i++) {
	        	int unitSize = (int)(Math.pow(2, level-i)) * cropSize;
	        	int maxCol = (int)(srcWidth/unitSize) + 1;
	        	int maxRow = (int)(srcHeight/unitSize) + 1;
	        	
	        	JSONObject levelObj = new JSONObject();
	        	levelObj.put("level", i);
	        	levelObj.put("path", fileList.get(l).getAbsolutePath());
	        	
	        	JSONArray levelTileArray = new JSONArray();
	        	int j = 1;
	        	while(j <= maxCol) {
	        		// 切分第j列
	        		int x = (j-1) * unitSize;
	        		if(x < srcWidth) {
	        			int k = 1;
	        			while(k <= maxRow) {
	        				// 切分第j列第k行
	        				int y = (k-1) * unitSize;
	        				if(y < srcHeight) {
	        					JSONObject obj = new JSONObject();
	        					obj.put("col", j);
	        					obj.put("row", k);
	        					obj.put("size", unitSize);
	        					obj.put("x", x);
	        					obj.put("y", y);
	        					levelTileArray.add(obj);
	        				}
	        				k++;
	        			}
	        		}
	        		j++;
	        	}
	        	levelObj.put("levelTile", levelTileArray);
	        	tileArray.add(levelObj);
	        	
	        	currentLevel = level + 1;
	        }
		}
		result.put("tileArray", tileArray);
		
		// 根据切图数据切图
		for(int i = 0; i != tileArray.size(); i++) {
			JSONObject levelObj = tileArray.getJSONObject(i);
			int level = levelObj.getInt("level");
			File file = new File(levelObj.getString("path"));
			if(!file.exists()) {
				continue;
			}
			// 读取源图像 
			BufferedImage bi = ImageIO.read(file); 
			int srcWidth = bi.getWidth();
            int srcHeight = bi.getHeight(); 
            Image image = bi.getScaledInstance(srcWidth, srcHeight, Image.SCALE_DEFAULT);
            
			for(int j = 0; j != levelObj.getJSONArray("levelTile").size(); j++) {
				JSONObject tileObj = levelObj.getJSONArray("levelTile").getJSONObject(j);
				ImageFilter cropFilter = new CropImageFilter(
						tileObj.getInt("x"), 
						tileObj.getInt("y"), 
						tileObj.getInt("size"),
						tileObj.getInt("size")
				);
				Image output = Toolkit.getDefaultToolkit().createImage(  
                        new FilteredImageSource(image.getSource(), cropFilter)); 
				String outputPath = String.format(cropFolder.getAbsolutePath() + "\\%s\\%s\\%s." + suffix, 
						level, tileObj.getInt("col"), tileObj.getInt("row"));
				
				imageToFile(output, tileObj.getInt("size"), tileObj.getInt("size"), cropSize,
						suffix, outputPath);
			}
		}
		
		//result.remove("tileArray");
		return result.toString();
    }
    
    private static void imageToFile(Image output, int width, int height, int cropSize,
    		String suffix, String destFilePath) throws Exception {
    	int type = "png".equalsIgnoreCase(suffix)?BufferedImage.TYPE_INT_ARGB:BufferedImage.TYPE_INT_RGB;
        BufferedImage tag = new BufferedImage(width, height, type);  

        // 设置底色
        Graphics2D graphics = tag.createGraphics();
        graphics.setPaint ( new Color ( 255, 255, 255 ) );
        graphics.fillRect ( 0, 0, tag.getWidth(), tag.getHeight() );

        // 绘制缩小后的图
        Graphics g = tag.getGraphics();
        g.drawImage(output, 0, 0, null);   
        g.dispose();  
        
        // 调整文件大小
        BufferedImage bi = ImageUtil.adjustImage(tag, cropSize, cropSize, "png".equalsIgnoreCase(suffix));
        
        // 输出为文件
        suffix = StringUtils.isBlank(suffix)?"JPEG":suffix.toUpperCase();
        
        byte[] tempBuffer = null;
		if (suffix.compareToIgnoreCase("jpg") == 0
				|| suffix.compareToIgnoreCase("jpeg") == 0) {
			tempBuffer = ImageUtil.newCompressImage(bi, (float) 1);
		} else {
			tempBuffer = ImageUtil.imageToByte(bi, "png");
		}
		
		File file = new File(destFilePath);
		file.getParentFile().mkdirs();
		
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(tempBuffer);
		fos.close();
		
    }
    
    /**
     * 输出BufferedImage
     * @param bi
     * @param destFilePath
     * @throws Exception
     */
    public static void bufferedImageToFile(BufferedImage bi, String destFilePath) throws Exception {
    	File file = new File(destFilePath);
    	file.getParentFile().mkdirs();
    	
    	String suffix = destFilePath.substring(destFilePath.lastIndexOf(".") + 1);
    	byte[] tempBuffer = null;
		if (suffix.compareToIgnoreCase("jpg") == 0
				|| suffix.compareToIgnoreCase("jpeg") == 0) {
			tempBuffer = ImageUtil.newCompressImage(bi, (float) 1);
		} else {
			tempBuffer = ImageUtil.imageToByte(bi, "png");
		}
		
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(tempBuffer);
		fos.close();
    }
}
