package com.wildish.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

public class ZipUtil {
	public static void unZip(String path) {
		int count = -1;

		File file = null;
		InputStream is = null;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;

		File saveFolder = new File(path.substring(0, path.lastIndexOf(File.separator)) + File.separator + path.substring(path.lastIndexOf(File.separator) + 1, path.lastIndexOf(".")));
		if(!saveFolder.exists()) {
			saveFolder.mkdirs();
		}
		
		try {
			ZipFile zipFile = new ZipFile(path);

			Enumeration<?> entries = zipFile.getEntries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				if(entry.isDirectory()) {
					File temp = new File(saveFolder, entry.getName());
					temp.mkdirs();
				} else {
					String filename = entry.getName();
					file = new File(saveFolder, filename);
					is = zipFile.getInputStream(entry);
					fos = new FileOutputStream(file);
					
					int buffer = is.available();
					byte buf[] = new byte[buffer];
					bos = new BufferedOutputStream(fos, buffer);
					
					while ((count = is.read(buf)) > -1) {
						bos.write(buf, 0, count);
					}
					
					fos.close();
					is.close();
				}
			}

			zipFile.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		File file = new File("E:\\Servers\\apache-tomcat-7.0.47\\webapps\\zfinger\\upload\\temp\\1503290159250000000.zip");
		unZip(file.getPath());
	}
}