package com.wildish.utils;

/**
 * BussinessException 如果一些错误仅仅是由于业务逻辑控制产生，不需要记录到日志中
 * 
 * @author zzlt104
 * 
 */
public class BussinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isNeedLog = false;
	private String code = "1";

	public BussinessException(String errorCode, String message,
			boolean isNeedLog) {
		super(message);
		this.code = errorCode;
		this.isNeedLog = isNeedLog;
	}

	public BussinessException(String errorCode, String message) {
		super(message);
		this.code = errorCode;
	}

	public String getCode() {
		return this.code;
	}

	/**
	 * 是否需要记录
	 * 
	 * @return
	 */
	public boolean isNeedLog() {
		return isNeedLog;
	}

	/**
	 * 设置是否需要记录
	 * 
	 * @param isNeedLog
	 */
	public void setNeedLog(boolean isNeedLog) {
		this.isNeedLog = isNeedLog;
	}
}
