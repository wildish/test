package com.wildish.oracle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OracleTest {

	private Connection conn;
	
	@Before
	public void doBefore() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();

			String url = "jdbc:oracle:thin:@192.168.18.99:1521:orcl";
			String user = "scenicnet";
			String password = "scenicnet";
			conn = DriverManager.getConnection(url, user, password);
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("初始化链接失败");
		}
	}
	
	/**
	 * simple test
	 */
	@Test
	public void testOne() {
		try{
			String sql = "select * from demotest where username=?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, "keith");
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				System.out.println("你的第一个字段内容为：" + rs.getString(1));
				System.out.println("你的第二个字段内容为：" + rs.getString(2));
			}
			
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testTwo() {
		Matcher matcher = Pattern.compile("(?i).+?\\.(jpg|gif|bmp|png|jpeg)").matcher("123.jpeg");
		if(matcher.matches()) {
			System.out.println(matcher.group(1)); 
		}
	}
	
	
	@After
	public void doAfter() {
		try{
			if(conn != null) {
				conn.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}